<?php
if ($folder_info['coming_soon'] == 1)
{
	$type = "coming_soon";
}
else
{

	switch ($folder_info['type'])
	{
		//new stuff
		case 'main':
			$type = "main";
			$this->load->model("mbanners");
			$language = ($_COOKIE['language']) ? $_COOKIE['language'] : 'ru';
			$data     = $this->mbanners->get(array('lang' => $language), array(), 0, 'order_id DESC');
			$banners  = array();
			foreach ($data as $k => $v)
			{
				$banners[$v["type"]][] = $v;
			}
			if ($banners != array()) $this->mysmarty->assign("banners", $banners);
			$result = fetchData("https://api.instagram.com/v1/users/1121067179/media/recent/?access_token=1121067179.5a3723f.915f750c3f284f038a789945494be42f");

			//$result = fetchData("https://api.instagram.com/v1/users/145753144/media/recent/?access_token=145753144.ab103e5.4432f66785374abe9c1e0f8396b3d384");
			$result    = json_decode($result);
			$instagram = array();
			$i         = 1;
			if (isset($result->data))
			{
				foreach ($result->data as $post)
				{
					if ($i > 5)
						break;
					$instagram[] = array("img" => $post->images->low_resolution->url, "link" => $post->link);
					$i++;

				}
				$this->mysmarty->assign('instagram', $instagram);
			}
			break;
		case 'blog':
			$this->load->model('mblog');
			if ($url2 == '')
			{
				$type = "blog";
				$blog = $this->mblog->get(array(), array(), 0, 'order_id DESC');
				$this->mysmarty->assign('blog', $blog);
			}
			elseif ($blog_post = $this->mblog->get(array('url' => $url2), array(), 1))
			{
				$type = "blog_post";
				$this->mysmarty->assign('blog_post', $blog_post);
				$blog = $this->mblog->bget(array('url !=' => $url2), array(), 0, 'order_id DESC', 0, 4);
				$this->mysmarty->assign('blog', $blog['result']);
			}
			else
			{
				$this->show_404();

				return;
			}
			break;
		case 'info':


			//ansotov отправка письма о своём дизайне
			if ($this->input->post('email'))
			{

				$this->load->model('mself_design');
				$o_id = $this->mself_design->insert(array('email' => $this->input->post('email'), 'comment' => $this->input->post('message')));


				$this->mysmarty->assign('data', array('email' => $this->input->post('email'), 'message' => $this->input->post('message')));
				$this->mysmarty->assign('order_id', $o_id);
				$template = $this->mysmarty->fetch('new_bloom/ru/self-design.tpl');

				$headline = 'Заявка на изготовление индивидуального чехла номер ' . $o_id . '.';

				$this->send_mail($_REQUEST, 'store@printuha.ru', $headline, $template);
				$folder_info['send_success'] = true;

			}

			break;
		case 'girlsinbloom':
			if ($folder_info['url'] == 'portfolio')
			{
				$type = "portfolio";
				$this->load->model('mportfolio');

				//ansotov если детальный просмотр
				if ($url2)
				{
					$item = $this->mportfolio->get(array('id' => $url2), array(), 1);
					$this->mysmarty->assign('item', $item);
				}
				else
				{
					$portfolio = $this->mportfolio->get(array(), array(), 0, 'order_id DESC');

					$data_arr = array();

					foreach ($portfolio as $k => $v)
					{
						$data_arr[$k] = $v;

						$data_arr[$k]['title']       = $this->getLang($v, 'title');
						$data_arr[$k]['description'] = $this->getLang($v, 'description');
					}

					$this->mysmarty->assign('portfolio', $data_arr);
				}

			}
			elseif ($folder_info['url'] == 'smi')
			{
				$type = "smi";
				$this->load->model('msmi');

				//ansotov если детальный просмотр
				if ($url2)
				{
					$item = $this->msmi->get(array('id' => $url2), array(), 1);
					$this->mysmarty->assign('item', $item);
				}
				else
				{
					$smi = $this->msmi->get(array(), array(), 0, 'order_id DESC');

					$data_arr = array();

					foreach ($smi as $k => $v)
					{
						$data_arr[$k] = $v;

						$data_arr[$k]['title']       = $this->getLang($v, 'title');
						$data_arr[$k]['description'] = $this->getLang($v, 'description');
					}

					$this->mysmarty->assign('smi', $data_arr);
				}
			}
			else
			{
				$this->show_404();

				return;
			}
			break;
		case 'gifts':
			if ($url2 == '')
			{
				$type = "gifts";

				$this->mysmarty->assign('gifts');
			}
			elseif ($url2 != '')
			{
				$type = "gifts_post";

				$this->mysmarty->assign('gifts');
			}
			else
			{
				$this->show_404();

				return;
			}
			break;
		case 'catalog':
			if ($url2 == 'gotovye-kejsy' || $url2 == 'oblozhki-na-pasport')
			{
				$this->mysmarty->assign("hide_cases", 1);
			}

			//ansotov данные о нахождении, сделано очень не правильно, нет id раздела, при возможности переделать
			$this->load->model('mfolders');
			$current_url = '/catalog/' . $url2 . '/';

			//ansotov теги раздела
			$this->load->model('mtags');

			$search = $this->input->get("search");

			//ansotov типы разделов для фильтра
			$clothes_types = array(
				'tolstovki' => 'Толстовки',
				'futbolki'  => 'Топы',
				//'shirts'    => 'Рубашки',
				'skirts'    => 'Юбки'
			);


			//ansotov типы разделов для фильтра
			$accessories_types = array(
				'oblozhki_na_pasport' => 'Обложки',
				'cardholders'         => 'Визитницы',
				//'backpacks' => 'Рюкзаки',
				'berets'              => 'Береты'
			);

			$c_type = null;

			if ($url2 == 'clothes')
			{
				$add_where = false;

				$tolstovki = $this->mfolders->get('`url` = "/catalog/tolstovki/"', array('id'), 1);
				$futbolki  = $this->mfolders->get('`url` = "/catalog/futbolki/"', array('id'), 1);
				$shirts    = $this->mfolders->get('`url` = "/catalog/shirts/"', array('id'), 1);
				$skirts    = $this->mfolders->get('`url` = "/catalog/skirts/"', array('id'), 1);

				//ansotov получаем id раздела
				$sections = array(
					$tolstovki['id'],
					$futbolki['id'],
					$shirts['id'],
					$skirts['id']
				);

				//ansotov типы разделов
				$types = ($_REQUEST['clothes_types']) ? $_REQUEST['clothes_types'] : array('tolstovki', 'futbolki', 'shirts', 'skirts');

				//ansotov список тегов раздела
				if (!$search)
					$section_tags = $this->mtags->get('(SELECT count(id) FROM product_tags WHERE section_id IN(' . implode(',', $sections) . ') AND tag_id = tags.id) > 0', array('id', 'title', 'en_title'));

				$add .= " type IN(" . "'" . implode("', '", $types) . "'" . ") AND EXISTS(SELECT t.id FROM types t WHERE t.url IN(" . "'" . implode("', '", $types) . "'" . ") AND t.active = 1 )";

				$tag = $this->input->get('tag');

				if ($tag)
					$add_where = ' AND (SELECT count(id) FROM product_tags WHERE section_id IN(' . implode(',', $sections) . ') AND product_id = products.id AND tag_id IN(' . implode(",", $tag) . ')) > 0 ';

				$clothes = true;

			}
			else if ($url2 == 'accessories')
			{
				$add_where = false;

				$oblozhki_na_pasport = $this->mfolders->get('`url` = "/catalog/diaries/"', array('id'), 1);
				$cardholders         = $this->mfolders->get('`url` = "/catalog/backpacks/"', array('id'), 1);
				$backpacks           = $this->mfolders->get('`url` = "/catalog/bags/"', array('id'), 1);
				$berets              = $this->mfolders->get('`url` = "/catalog/gotovye-kejsy/"', array('id'), 1);

				//ansotov получаем id раздела
				$sections = array();

				if ($_REQUEST['accessories_types'])
				{
					foreach ($_REQUEST['accessories_types'] as $k => $v)
					{
						$item       = $$v;
						$sections[] = $item['id'];
					}
				}
				else
				{
					$sections = array(
						$oblozhki_na_pasport['id'],
						$cardholders['id'],
						$backpacks['id'],
						$berets['id']
					);
				}

				//ansotov типы разделов
				$types = ($_REQUEST['accessories_types']) ? $_REQUEST['accessories_types'] : array('diaries', 'backpacks', 'bags', 'gotovye-kejsy');

				//ansotov адский костыль
				if (in_array('oblozhki_na_pasport', $types))
				{
					$types[] = 'oblozhki-na-pasport';
				}

				//ansotov список тегов раздела
				if (!$search)
					$section_tags = $this->mtags->get('(SELECT count(id) FROM product_tags WHERE section_id IN(' . implode(',', $sections) . ') AND tag_id = tags.id) > 0', array('id', 'title', 'en_title'));

				$add .= " type IN(" . "'" . implode("', '", $types) . "'" . ") AND EXISTS(SELECT t.id FROM types t WHERE t.url IN(" . "'" . implode("', '", $types) . "'" . ") AND t.active = 1 )";

				$tag = $this->input->get('tag');

				if ($tag)
					$add_where = ' AND (SELECT count(id) FROM product_tags WHERE section_id IN(' . implode(',', $sections) . ') AND product_id = products.id AND tag_id IN(' . implode(",", $tag) . ')) > 0 ';

				$accessories = true;

			}
			else if ($url2 == 't-shirts')
			{
				$add_where = false;

				$female_t_shirts = $this->mfolders->get('`url` = "/catalog/female-t-shirts/"', array('id'), 1);
				$t_shirts_with_sequins         = $this->mfolders->get('`url` = "/catalog/t-shirts-with-sequins/"', array('id'), 1);
				$male_t_shirts           = $this->mfolders->get('`url` = "/catalog/male-t-shirts/"', array('id'), 1);

				//ansotov получаем id раздела
				$sections = array();

				if ($_REQUEST['accessories_types'])
				{
					foreach ($_REQUEST['accessories_types'] as $k => $v)
					{
						$item       = $$v;
						$sections[] = $item['id'];
					}
				}
				else
				{
					$sections = array(
						$female_t_shirts['id'],
						$t_shirts_with_sequins['id'],
						$male_t_shirts['id']
					);
				}

				//ansotov типы разделов
				$types = ($_REQUEST['accessories_types']) ? $_REQUEST['accessories_types'] : array('female-t-shirts', 't-shirts-with-sequins', 'male-t-shirts');

				//ansotov список тегов раздела
				if (!$search)
					$section_tags = $this->mtags->get('(SELECT count(id) FROM product_tags WHERE section_id IN(' . implode(',', $sections) . ') AND tag_id = tags.id) > 0', array('id', 'title', 'en_title'));

				$add .= " type IN(" . "'" . implode("', '", $types) . "'" . ") AND EXISTS(SELECT t.id FROM types t WHERE t.url IN(" . "'" . implode("', '", $types) . "'" . ") AND t.active = 1 )";

				$tag = $this->input->get('tag');

				if ($tag)
					$add_where = ' AND (SELECT count(id) FROM product_tags WHERE section_id IN(' . implode(',', $sections) . ') AND product_id = products.id AND tag_id IN(' . implode(",", $tag) . ')) > 0 ';

				$accessories = true;

			}
			else if ($url2 == 'hoodies')
			{
				$add_where = false;

				$female_sweatshirts = $this->mfolders->get('`url` = "/catalog/female-sweatshirts/"', array('id'), 1);
				$male_sweatshirts         = $this->mfolders->get('`url` = "/catalog/male-sweatshirts/"', array('id'), 1);
				$hoodie_hooded           = $this->mfolders->get('`url` = "/catalog/hoodie-hooded/"', array('id'), 1);

				//ansotov получаем id раздела
				$sections = array();

				if ($_REQUEST['accessories_types'])
				{
					foreach ($_REQUEST['accessories_types'] as $k => $v)
					{
						$item       = $$v;
						$sections[] = $item['id'];
					}
				}
				else
				{
					$sections = array(
						$female_sweatshirts['id'],
						$male_sweatshirts['id'],
						$hoodie_hooded['id']
					);
				}

				//ansotov типы разделов
				$types = ($_REQUEST['accessories_types']) ? $_REQUEST['accessories_types'] : array('female-sweatshirts', 'male-sweatshirts', 'hoodie-hooded');

				//ansotov список тегов раздела
				if (!$search)
					$section_tags = $this->mtags->get('(SELECT count(id) FROM product_tags WHERE section_id IN(' . implode(',', $sections) . ') AND tag_id = tags.id) > 0', array('id', 'title', 'en_title'));

				$add .= " type IN(" . "'" . implode("', '", $types) . "'" . ") AND EXISTS(SELECT t.id FROM types t WHERE t.url IN(" . "'" . implode("', '", $types) . "'" . ") AND t.active = 1 )";

				$tag = $this->input->get('tag');

				if ($tag)
					$add_where = ' AND (SELECT count(id) FROM product_tags WHERE section_id IN(' . implode(',', $sections) . ') AND product_id = products.id AND tag_id IN(' . implode(",", $tag) . ')) > 0 ';

				$accessories = true;

			} else if ($url2 == 'new-year') {

                //ansotov получаем id раздела
                $section_id = $this->mfolders->get('`url` = "' . $current_url . '"', array('id'), 1);

                $section = ($section_id['id'] == 95) ? 84 : $section_id['id'];

                //ansotov список тегов раздела
                if (!$search && $section)
                    $section_tags = $this->mtags->get('(SELECT count(id) FROM product_tags WHERE (SELECT id FROM products WHERE products.id = product_id AND products.active = 1) > 0 AND section_id = ' . $section . $c_type . ' AND tag_id = tags.id) > 0 ORDER BY tags.title ASC', array('id', 'title', 'en_title'));

                $add = ' `new_year` = 1 ';

                if ($search != '')
                {
                    $search = mysql_real_escape_string($search);
                    $add    = "(name LIKE '%$search%' OR sku LIKE '%$search%' OR `content` LIKE '%$search%')";
                }

                $tag = $this->input->get('tag');

                if ($tag)
                    $add_where = ' AND (SELECT count(id) FROM product_tags WHERE section_id = ' . $section_id['id'] . ' AND product_id = products.id AND tag_id IN(' . implode(",", $tag) . ')) > 0 ';

                $clothes = false;
            }
			else
			{

				//ansotov тип кейсов (iPhone или Samsung)
				if ($url2 == 'gotovye-kejsy')
				{
					if ((isset($url3) && $url3 == 'samsung'))
						$c_type = ' AND `c_type` = 2';
					elseif ((isset($url3) && $url3 == 'glitters'))
						$c_type = ' AND `c_type` = 3';
					else
						$c_type = ' AND `c_type` = 1';
				}

				//ansotov получаем id раздела
				$section_id = $this->mfolders->get('`url` = "' . $current_url . '"', array('id'), 1);

				if (in_array($url2, array('costumes'))) {

					//ansotov получаем id раздела
					$section_ids = array();

					foreach (array('/catalog/costumes/', '/catalog/male-costumes/', '/catalog/female-costumes/', '/catalog/children-costumes/') as $k => $v) {
						$data = $this->mfolders->get('`url` = "' . $v . '"', array('id'), 1);
						$section_ids[$k] = $data['id'];
					}

					$types = '"costumes", "male-costumes", "female-costumes", "children-costumes"';

					$section = implode(',', $section_ids);
				} elseif (in_array($url2, array('tolstovki'))) {

					//ansotov получаем id раздела
					$section_ids = array();

					foreach (array(/*'/catalog/tolstovki/', '/catalog/male-sweatshirts/', */'/catalog/female-sweatshirts/'/*, '/catalog/children-sweatshirts/'*/) as $k => $v) {
						$data = $this->mfolders->get('`url` = "' . $v . '"', array('id'), 1);
						$section_ids[$k] = $data['id'];
					}

					//$types = '"tolstovki", "male-sweatshirts", "female-sweatshirts", "children-sweatshirts"';
					$types = '"female-sweatshirts"';

					$section = implode(',', $section_ids);
				} elseif (in_array($url2, array('futbolki'))) {

					//ansotov получаем id раздела
					$section_ids = array();

					foreach (array(/*'/catalog/futbolki/', '/catalog/male-t-shirts/', */'/catalog/female-t-shirts/'/*, '/catalog/children-t-shirts/'*/) as $k => $v) {
						$data = $this->mfolders->get('`url` = "' . $v . '"', array('id'), 1);
						$section_ids[$k] = $data['id'];
					}

					//$types = '"futbolki", "male-t-shirts", "female-t-shirts", "children-t-shirts"';
					$types = '"female-t-shirts"';

					$section = implode(',', $section_ids);
				} else {
					$section = ($section_id['id'] == 95) ? 84 : $section_id['id'];
					$types = "'$url2'";
				}

				//ansotov список тегов раздела
				if (!$search && $section)
					$section_tags = $this->mtags->get('(SELECT count(id) FROM product_tags WHERE (SELECT id FROM products WHERE products.id = product_id AND products.active = 1) > 0 AND section_id IN (' . $section . ')' . $c_type . ' AND tag_id = tags.id) > 0 ORDER BY tags.title ASC', array('id', 'title', 'en_title'));

				$add = "type IN($types) AND EXISTS(SELECT t.id FROM types t WHERE t.url = '$url2' AND t.active = 1 )";
				if ($url2 == 'new')
					$add = " new=1";
				if ($url2 == 'sale')
				{
					$add = " old_price > 0 && not_available = 0";
				}

				if ($search != '')
				{
					$search = mysql_real_escape_string($search);
					$add    = "(name LIKE '%$search%' OR sku LIKE '%$search%' OR `content` LIKE '%$search%')";
				}

				$tag = $this->input->get('tag');

				if ($tag)
					$add_where = ' AND (SELECT count(id) FROM product_tags WHERE section_id IN(' . $section . ') AND product_id = products.id AND tag_id IN(' . implode(",", $tag) . ')) > 0 ';

				$clothes = false;
			}

			$q = ($url2 == 'gotovye-kejsy') ? $c_type : false;

			$query = "active = 1 " . $q . " AND $add $add_where";

			if ($url2 == 'sale')
			{

				$orderBy = ' CASE `type`
											  WHEN "tolstovki"
											    THEN 1
											  WHEN "skirts"
											    THEN 2
											  WHEN "oblozhki-na-pasport"
											    THEN 3
											  WHEN "cardholders"
											    THEN 4
											  WHEN "backpacks"
											    THEN 5
											  WHEN "berets"
											    THEN 6
											  WHEN "futbolki"
											    THEN 7
											  END ASC';
			}
			else
			{
				$orderBy = 'order_id DESC';
			}

			//ansotov общее количество товара
			$countItems =  count($this->mproducts->get($query, array(), 0));

			//ansotov текущая страница сайта
			$this->page = (isset($_REQUEST['page'])) ? (int) $_REQUEST['page'] : $this->page;

			$this->offset = ($this->page - 1) * $this->limit;
			$this->offset = ($this->offset > 0) ? $this->offset : 0;

			$p_data = $this->mproducts->get($query . ' ORDER BY ' . $orderBy . ' LIMIT ' . $this->limit . ' OFFSET ' . $this->offset, array(), 0);

			// filter stuff
			$size  = $this->input->get('size');
			$ftype = $this->input->get('ftype');
			$color = $this->input->get('color');

			foreach ($section_tags as $key => $value)
			{
				$tags_arr[$key]          = $value;
				$tags_arr[$key]['title'] = $this->getLang($value, 'title');
				if (in_array($value['id'], $tag))
					$tags_arr[$key]['selected'] = 1;
			}

			//if($order_by=='' || !isset($sort_array[$order_by]))
			$order_by = 'date DESC';

			$this->load->model('mproducts');
			$query           = "";
			$default_sort_by = "`date` DESC";

			$products     = array();
			$found_types  = array();
			$found_colors = array();
			$found_sizes  = array();

			$colors = array();

			if ($p_data) foreach ($p_data as $k => $v)
			{
				//$selected_price =$v['new_price']>$v['price']?$v['new_price']:$v['price'];
				$found_colors[$v['color']]   = (isset($found_colors[$v['color']]) ? $found_colors[$v['color']]++ : 1);
				$colors[$v['color']]['name'] = $v['color'];
				$found_types[$v['type']]     = (isset($found_types[$v['type']]) ? $found_types[$v['type']]++ : 1);
				$query                       .= $query == "" ? "product_id = " . $v['id'] : " OR product_id = " . $v['id'];
				$query                       .= ' AND active = 1 ';
				//if(!isset($product_type['id']) || $product_type['id'] == $v['type'])
				$products[$k]            = $v;
				$products[$k]['name']    = $this->getLang($v, 'name');
				$products[$k]['content'] = $this->getLang($data, 'content');

				if (is_array($color))
				{
					$flip = array_flip($color);
					if (isset($flip[$v['color']]))
					{
						$colors[$v['color']]['checked'] = 1;
					}
				}
			}

			$this->load->model('mproducts_sizes');
			$this->load->model('mtypes');

			$products_sizes = array();
			$sizes          = array();
			$types          = array();

			if ($p_data) $data = $this->mproducts_sizes->get($query, array(), 0, 'order_id DESC');
			if ($p_data && $data) foreach ($data as $k => $v)
			{

				$sizes[$v['code']]                            = $v;
				$sizes[$v['code']]['qty']                     = isset($sizes[$v['code']]['qty']) ? $sizes[$v['code']]['qty']++ : 1;
				$products_sizes[$v['product_id']][$v['code']] = $v;
				if (is_array($size))
				{
					$flip = array_flip($size);
					if (isset($flip[$v['code']]))
					{
						$sizes[$v['code']]['checked'] = 1;
					}
				}
			}


			$data = $this->mtypes->get(array('active' => 1), array(), 0, 'order_id DESC');
			if ($data) foreach ($data as $k => $v)
			{
				$types[$v['id']]['url']  = $v['url'];
				$types[$v['id']]['name'] = $v['name'];
				$types[$v['id']]['qty']  = (isset($found_types[$v['id']]) ? $found_types[$v['id']] : 0);
				if (is_array($ftype))
				{
					$flip = array_flip($ftype);
					if (isset($flip[$v['id']]))
					{
						$types[$v['id']]['checked'] = 1;
					}
				}
			}

			// фильтрация
			foreach ($products as $k => $v)
			{
				$remove_product       = false;
				$product[$k]['price'] = $v['price'];

				if (is_array($size) && $remove_product == false)
				{
					$remove_product = !$this->check_field($products_sizes[$v['id']], $size, $v['id']);
				}
				if (is_array($ftype) && $remove_product == false)
				{
					$remove_product = !$this->check_val($v['type'], $ftype, $v['id']);
				}
				if (is_array($color) && $remove_product == false)
				{
					$remove_product = true;
					foreach ($color as $k2 => $v2)
					{
						if ($v2 == $v['color'])
						{
							$remove_product = false;
						}
					}
				}

				if ($remove_product == true)
				{
					unset($products[$k]);
				}
			}


			// отрезок страниц

			$products = array_slice($products, 0, 2, true) +
				array("banner" => array('type' => 'banner')) +
				array_slice($products, 2, count($products) - 1, true);

			//ansotov сортировка фильтров по ключам
			krsort($sizes);
			ksort($colors);

			$my_c_type = ($url2 == 'gotovye-kejsy') ? (((isset($url3) && $url3 == 'samsung')) ? 2 : 1) : false;
			$my_c_type = ($url2 == 'gotovye-kejsy') ? (((isset($url3) && $url3 == 'glitters')) ? $my_c_type : 1) : false;

			$this->products = $products;
			$this->mysmarty->assign('filter_colors', $colors);
			$this->mysmarty->assign('section', $section);
			$this->mysmarty->assign('filter_types', $types);
			$this->mysmarty->assign("filter_sizes", $sizes);
			$this->mysmarty->assign('products', $products);
			$this->mysmarty->assign('c_type', $my_c_type);
			$this->mysmarty->assign('clothes', $clothes);
			$this->mysmarty->assign('accessories', $accessories);
			$this->mysmarty->assign('clothes_types', $clothes_types);
			$this->mysmarty->assign('accessories_types', $accessories_types);
			$this->mysmarty->assign('tags', $tags_arr);
			//new stuff end

			break;
		case 'special_order':

			$this->form_validation->set_rules('first_name', 'Имя', 'trim|required|max_length[200]|alpha_dot_dash|required');
			$this->form_validation->set_rules('email', 'E-mail', 'trim|max_length[100]|valid_email');
			$this->form_validation->set_rules('phone', 'Телефон', 'trim|min_length[2]|max_length[90]|required');
			$this->form_validation->set_rules('city', 'Город', 'trim|max_length[200]|alpha_dot_dash|callback_city_check|required');
			$this->form_validation->set_rules('comments', 'Комментарии', 'trim|max_length[2000]');
			$postdata = array();

			$send = intval($this->input->post('send'));

			if ($send == 1)
			{
				if ($this->form_validation->run() == false)
				{
					$postdata = $this->form_validation->get_postdata();
					$errors   = $this->form_validation->get_errors();
					$this->mysmarty->assign('errors', $errors);
					$this->mysmarty->assign('postdata', $postdata);
				}
				else
				{

					$data = $this->form_validation->get_postdata();

					$this->mysmarty->assign('data', $data);
					$headline = "Специальный заказ";
					$template = $this->mysmarty->fetch('bloom/ru/special_order_mail.tpl');

					//if($data['payment_type']!='online'){
					$this->cart->destroy();
					if (isset($data['email']))
						$this->send_mail($data['first_name'], $data['email'], $headline, $template);
					$this->send_mail('store@printuha.ru', 'store@printuha.ru', $headline, $template);
					//$this->send_mail('_raimaker_@mail.ru','_raimaker_@mail.ru',$headline,$template);
					//}
					$this->mysmarty->assign('order_send', 1);
				}
			}
			break;
		case 'press':
			$this->load->model('mpress');
			$this->load->model('mphotos');
			$press        = $this->mpress->get(array(), array(), 0, 'order_id ASC');
			$data         = $this->mphotos->get(array('type' => 'press'), array(), 0, 'order_id ASC');
			$press_photos = array();
			foreach ($data as $k => $v)
			{
				$press_photos[$v['parent_id']][] = $v;
			}
			foreach ($press as $k => $v)
			{
				$press[$k]['link_picture']      = $press_photos[$v['id']][0]['picture'];
				$press[$k]['link_picture_type'] = $press_photos[$v['id']][0]['picture_type'];
				$press[$k]['photos']            = $press_photos[$v['id']];
			}
			$this->mysmarty->assign('press', $press);
		case 'albums':
			$type = 'albums';
			$this->load->model('malbums');

			if ($url2 != '' && $album = $this->malbums->get(array('url' => $url2), array(), 1))
			{
				$this->mysmarty->assign('album', $album);
				$this->load->model('mphotos');
				$photos = $this->mphotos->get(array('parent_id' => $album['id'], 'type' => 'album'), array(), 0, 'order_id ASC');
				$this->mysmarty->assign('photos', $photos);
				$type = 'photos';
				if ($album['type'] != '')
				{
					$type = $album['type'];
					if ($type == 'press')
					{
						$this->load->model('mpress');
						$press        = $this->mpress->get(array(), array(), 0, 'order_id ASC');
						$data         = $this->mphotos->get(array('type' => 'press'), array(), 0, 'order_id ASC');
						$press_photos = array();
						foreach ($data as $k => $v)
						{
							$press_photos[$v['parent_id']][] = $v;
						}
						foreach ($press as $k => $v)
						{
							$press[$k]['link_picture']      = $press_photos[$v['id']][0]['picture'];
							$press[$k]['link_picture_type'] = $press_photos[$v['id']][0]['picture_type'];
							$press[$k]['photos']            = $press_photos[$v['id']];
						}
						$this->mysmarty->assign('press', $press);
					}
				}
				$folder_info['type'] = $type;
			}
			elseif ($url2 == '')
			{
				$albums = $this->malbums->get(array(), array(), 0, 'order_id ASC');
				$this->mysmarty->assign('albums', $albums);
			}
			else
			{
				$this->show_404();

				return;
			}

			break;
		case 'products':

			$this->load->model('mtypes');
			$subtypes_data = array();
			if ($subtypes = $this->mtypes->get(array(), array(), 0, 'order_id ASC'))
			{
				foreach ($subtypes as $k => $v)
				{
					$subtypes_data[$v['url']] = $v['name'];
				}

				$this->mysmarty->assign('subtypes', $subtypes);
			}

			if ($url2 == '')
			{
				$first_type = array_shift(array_values($subtypes));
				$this->mysmarty->assign('selected_subtype', $first_type['url']);
				if ($data = $this->mproducts->get(array('active' => 1, 'type' => $first_type['url']), array(), 0, 'order_id DESC'))
				{
					$products = array();
					$i        = 1;
					foreach ($data as $k => $v)
					{
						//////////////////////
						// if discount = 7
						$v = $this->discount_calc($v);
						// if discount = 7
						//////////////////////
						$products[$k] = $v;
					}
				}
			}
			elseif ($url2 == 'sale')
			{

				if ($data = $this->mproducts->get(array('active' => 1, 'old_price >' => 0), array(), 0, 'order_id DESC'))
				{
					$products = array();
					foreach ($data as $k => $v)
					{
						//////////////////////
						// if discount = 7
						$v = $this->discount_calc($v);
						// if discount = 7
						//////////////////////
						//if(isset($v['display_discount']) && $v['display_discount']==1)
						$products[$k] = $v;
					}

				}
				$this->mysmarty->assign('selected_subtype', $url2);
			}
			elseif ($url2 != '' && isset($subtypes_data[$url2]))
			{
				if ($data = $this->mproducts->get(array('type' => $url2, 'active' => 1), array(), 0, 'order_id DESC'))
				{
					$products = array();
					$i        = 1;
					foreach ($data as $k => $v)
					{
						//////////////////////
						// if discount = 7
						$v = $this->discount_calc($v);
						// if discount = 7
						//////////////////////
						$products[$k] = $v;
					}
				}
				$this->mysmarty->assign('selected_subtype', $url2);
			}
			break;
	}

} ?>
