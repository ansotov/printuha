<?

class gloomdress extends CI_Controller
{
    private $excel_output = array();
    private $row = 1;
    private $writerObj = '';
    private $aSheet = '';

    private $admin_user_id = '';
    private $auth_code = '';
    private $val_user = '';
    private $language = 'ru';

    private $selected_catalog_ids = array();
    private $catalog_structure = array();
    private $catalog_path_name = '';
    private $catalog_level = 0;

    private $folders_indexes = array();
    private $folders_stuct = array();

    function __construct()
    {
        parent::__construct();
        $this->val_user = 'valid_' . $this->config->item('site_name') . '_user';

        $this->load->helper('url');
        $this->load->model('madmin_users');
        $this->load->library('session');
        $this->load->library('common_f');
        $this->load->library('image_lib');
        $this->load->library('admin_builder2');
        $this->auth();

        $this->admin_builder2->set_dir('gloomdress');
        $this->admin_builder2->set_template_dir('admin/ru');
        //
        $this->mysmarty->assign('lang', $this->language);

        /*
		1. Админ - все права.
		2. оператор - забивает товар на сайт.доступен только каталог товара и цвета.
			может заносить новый товар, редактировать существующий, те же операции с цветами. остальные все вкладки не доступны. (аналогично уже реализовано тобой на мебио.) Удалять товар НЕ МОЖЕТ!
		3. верстальщик - видит только заказы от клиентов. может их смотреть и редактировать. удалять не может.(аналогично уже реализовано тобой на мебио.)
		4. всем добавить "галку" запись активна (как на мебио) чтобы можно было не удалять человека, а просто заблокировать на время. но возможность удалить тоже нужна.
		5. Менеджер - объединить права оператора и верстальщика.


		*/

        //виды прав - m (master) - все права над разделом: возможность редактировать, удалять , создавать.
        //			  p (parent) - возможность : создавать , редактировать.
        //			  t (teacher) - возможность : редактировать.

        $this->admin_builder2->set_access(array('admin' => array('all' => 'm'),
            'operator' => array('items' => 'p',
                'colors' => 'p'),
            'maker_up' => array('users' => 'm'),
            'manager' => array('mailing' => 'm',
                'orders' => 'm',
                'users' => 'm')
        ));
        $this->admin_builder2->set_user_auth($this->session->userdata($this->val_user), $this->admin_user_id);
        $this->admin_builder2->set_auth_code($this->auth_code);
        $this->mysmarty->assign('user_auth', $this->session->userdata($this->val_user));


        $this->load->model('mcollections');
        $this->load->model('mseasons');

        $collections = $this->mcollections->get(array(), array(), 0, 'order_id ASC');
        $seasons = $this->mseasons->get(array(), array(), 0, 'order_id ASC');
        $this->mysmarty->assign('collections', $collections);
        $this->mysmarty->assign('seasons', $seasons);
        // MENU BUILDING

        $this->load->model('mfolders');
        $folders = $this->mfolders->get(array('active' => 1), array('name', 'id', 'parent_id', 'type'), 0, 'parent_id ASC, order_id ASC');
        $folders_indexs = array();
        if ($folders) foreach ($folders as $k => $v) {
            $folders_indexs[$v['id']] = $v;
        }
        $this->folders_indexes = $folders_indexs;
        $folders_struc = $this->build_menu(0);
        $this->mysmarty->assign('site_name', $this->config->item('site_name'));
        $this->mysmarty->assign('menu', $folders_struc);

    }

    function build_menu($parent_id = 0, $level = 0)
    {
        $temp = array();
        $level++;
        foreach ($this->folders_indexes as $k => $v) {
            if ($v['parent_id'] == $parent_id && (($parent_id != 0 && isset($this->folders_indexes[$parent_id]) /*&& $this->folders_indexes[$parent_id]['type']!='articles_list'*/) || $parent_id == 0 || $parent_id == 107)) {
                $temp[$v['id']] = $v;
                $temp[$v['id']]['level'] = $level;
                if ($data = $this->build_menu($v['id'], $level))
                    $temp[$v['id']]['subfolders'] = $data;
            }
        }
        if (empty($temp)) {
            return false;
        } else {
            return $temp;
        }
    }

    function logout()
    {
        $this->madmin_users->update(array('auth_code' => ''), array('id' => $this->session->userdata('admin_user_id')));
        $this->session->unset_userdata(array($this->val_user => ''));
        redirect("");
    }

    function auth()
    {
        if (!$this->session->userdata($this->val_user)) {
            $login = $this->input->post('login');
            $pass = $this->input->post('pass');

            $auth_code = $this->input->post('auth_code');

            $search_array = array();
            if ($login != '' && $pass != '') {
                $search_array = array("login" => $login, 'password' => $pass, 'active' => 1);
            } elseif ($auth_code != '') {
                $search_array = array("auth_code" => $auth_code, 'active' => 1);
            }


            if ($search_array == array() || !$user_data = $this->madmin_users->get($search_array, array(), 1)) {
                echo $this->mysmarty->fetch('admin/' . $this->language . '/login');
                exit;
            } else {
                if ($auth_code == '') {
                    $random_string = $this->common_f->genRandomString(80);
                    $this->madmin_users->update(array('auth_code' => $random_string), array('id' => $user_data['id']));
                    $this->session->set_userdata(array($this->val_user => $user_data['type'], 'admin_user_id' => $user_data['id'], 'user_name' => $user_data['name']));
                    $this->auth_code = $random_string;
                }
                $this->admin_user_id = $user_data['id'];
                //redirect('gloomdress/');

            }
        } else {

            $admin_user_id = $this->session->userdata('admin_user_id');
            $this->admin_user_id = $admin_user_id;
            $data = $this->madmin_users->get(array('id' => $admin_user_id), array('auth_code'), 1);
            $this->auth_code = $data['auth_code'];
        }

        $this->mysmarty->assign('admin_user', $this->session->userdata('user_name'));
    }

    /////////////////////////////
    // PROPERTIES

    //Настройка главной страницы
    function main_page()
    {

        $this->admin_builder2->set_core_template('admin/' . $this->language . '/strip_index');
        $structure = array(
            /*"phone1"=>array("type"=>"string","name"=>"Телефон","not_required"=>true),
							"description"=>array("type"=>"string","name"=>"Краткое описание","not_required"=>true),
							"content"=>array("type"=>"text","name"=>"Текст на главной странице","not_required"=>true),*/
            "delivery_price" => array("type" => "string", "name" => "Стоимость доставки по Москве", "not_required" => true),
            "delivery_price2" => array("type" => "string", "name" => "Стоимость доставки в Регионы", "not_required" => true),
            "checkout_banner" => array("type" => "any_file", "name" => "Баннер для оформления заказа(352x470)", "directory" => 'img/banners/', "width" => 100, "not_required" => true),

        );
        $this->admin_builder2->set_model('mmain_page');
        $this->admin_builder2->set_header("gloomdress/main_page/1");
        $this->admin_builder2->set_core_name('main_page');
        $this->admin_builder2->set_id(1);
        $this->admin_builder2->init($structure);
    }

    function admin_users($page = 1)
    {
        $this->admin_builder2->set_core_template('admin/' . $this->language . '/strip_index');
        $structure = array(
            "name" => array("type" => "string", "name" => "ФИО (Имя)"),
            "login" => array("type" => "string", "name" => "Логин"),
            //"type"=>array("type"=>"list","name"=>"User Type","values"=>array("admin"=>"Admin","operator"=>"Operator","maker_up"=>"Maker-up","manager"=>'Manager')),
            "active" => array("type" => "checkbox", "name" => "Активен")
        );
        $this->admin_builder2->set_model('madmin_users');
        $this->admin_builder2->set_header("gloomdress/admin_users/");
        $this->admin_builder2->set_core_name('admin_users');
        $this->admin_builder2->set_title_name("Пользователи админ панели");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_admin_users($id = 0)
    {

        $this->admin_builder2->set_core_template('admin/' . $this->language . '/strip_index');
        $structure = array(
            "name" => array("type" => "string", "name" => "Имя"),
            "login" => array("type" => "string", "name" => "Логин"),
            "password" => array("type" => "string", "name" => "Пароль", 'is_password' => true),
            "type" => array("type" => "list", "name" => "Тип пользователя", "values" => array("admin" => "Admin", "operator" => "Operator", "maker_up" => "Maker-up", "manager" => 'Manager')),
            "active" => array("type" => "checkbox", "name" => "Активен"),
        );
        $this->admin_builder2->set_model('madmin_users');
        $this->admin_builder2->set_header("gloomdress/admin_users/");
        $this->admin_builder2->set_core_name('admin_users');
        $this->admin_builder2->set_title_name("Пользователи админ панели");
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    // PROPERTIES
    ////////////////


    function index($language = 'ru')
    {
        $this->language = $language;
        $this->mysmarty->assign('lang', $language);
        $this->admin_builder2->init();
    }





    ///////////////////////////////////////////////////////
    // FOLDERS
    function folders($parent_id = 0, $page = 1)
    {
        $this->load->model('mfolders');

        $one = "Раздел";
        $many = "Разделы";
        if ($parent_id > 0) {
            $this->load->model('mfolders');
            $parent = $this->mfolders->get(array('id' => $parent_id), array(), 1);
            $parent_type = $parent['type'];
        }
        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "url_to" => 'url', "width" => 200),
            "url" => array('name' => "URL", "type" => "string", "not_required" => true),
            "type" => array('type' => 'list', "name" => "Тип",
                "values" => array(
                    "info" => "Обычный раздел",
                    "products" => "Товары",
                    "albums" => "Альбомы",
                    'press' => 'Пресса',
                    "blog" => "Блог",
                    "gifts" => "Подарки",
                    "main" => "Главная страница",
                    "link" => "Внешняя ссылка",
                    "special_order" => "Спецзаказ",
                    "girlsinbloom" => "Girls in Bloom"
                ),
                "width" => 100),
            "date" => array("type" => "datetime", "name" => "Время написания", 'width' => 100),
            "visible" => array("type" => "checkbox", "name" => "отображается", 'width' => 100)
        );
        $name = 'Главные разделы';
        $this->admin_builder2->set_model('mfolders');
        $this->admin_builder2->set_order_criteria(array("parent_id" => $parent_id));
        if ($parent_id != 0) {
            $parent = $this->mfolders->get(array("id" => $parent_id), array("parent_id", "name"), 1);
            $this->admin_builder2->set_links_before(array("/gloomdress/open_folders/" . $parent['parent_id'] . "/$parent_id/" => "К родительскому разделу `" . $parent['name'] . "`"));
            $name = $many . " `" . $parent['name'] . "`";
        }
        $this->mysmarty->assign('selected_folder', $parent_id);
        $this->mysmarty->assign('selected_subfolder', $parent_id);
        $this->admin_builder2->set_header("gloomdress/folders/");
        $this->admin_builder2->set_core_name("folders");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_folders($id = 0, $parent_id = 0)
    {

        if ($id > 0) {
            $this->load->model('mfolders');
            $inf = $this->mfolders->get(array('id' => $id), array(), 1);
        }
        $one = "Раздел";
        $many = "Разделы";
        $parent_type = '';
        if ($parent_id > 0) {
            $this->load->model('mfolders');
            $parent = $this->mfolders->get(array('id' => $parent_id), array(), 1);
            $parent_type = $parent['type'];
        }

        if ($id > 0)
            $name = $one . ' / ' . $inf['name'];
        else
            $name = 'Новый ' . $one;
        if ($parent_id > 0) {

        }
        if ($id > 0) {
            $links_array["/gloomdress/folders/"] = 'Подразделы ' . $inf['name'];
            $this->mysmarty->assign('c_selected', $id);
            $this->mysmarty->assign('selected_folder', $parent_id);
            $this->mysmarty->assign('selected_subfolder', $id);
        } else {
            $name = $name . ' / ' . $this->catalog_path_name;
            $this->mysmarty->assign('selected_folder', $parent_id);
        }

        $structure = array(
            "name" => array('name' => "Название", "type" => "string", "url_to" => "url"),
            "en_name" => array('name' => "Название (английский)", "type" => "string", "url_to" => "url", "not_required" => true),
            "meta_title" => array('name' => "Meta(title)", "type" => "string", "not_required" => true),
            "meta_description" => array('name' => "Meta(description)", "type" => "string", "not_required" => true),
            "h1" => array('name' => "H1", "type" => "string", "not_required" => true),
            "url" => array('name' => "URL", "type" => "string", "not_required" => true),
            "type" => array('type' => 'list', "name" => "Тип",
                "values" => array(
                    "info" => "Обычный раздел",
                    "products" => "Товары",
                    "albums" => "Альбомы",
                    'press' => 'Пресса',
                    "blog" => "Блог",
                    "gifts" => "Подарки",
                    "catalog" => "Каталог Товаров",
                    "main" => "Главная страница",
                    "link" => "Внешняя ссылка",
                    "special_order" => "Спецзаказ",
                    "girlsinbloom" => "Girls in Bloom"
                ),
                "width" => 100),
            "top_banner_text" => array('name' => "Текст в верхнем баннере", "type" => "string", "not_required" => true),
            "en_top_banner_text" => array('name' => "Текст в верхнем баннере (английский)", "type" => "string", "not_required" => true),
            'banner_picture' => array('type' => "jcrop",
                "name" => "Баннер справа",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 352,
                "max_height" => 470,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "not_required" => true,
                "image_type_name" => "banner_picture_type",
                "directory" => "img/banner_picture/",
                "width" => 100
            ),
            "content" => array("type" => "text", "name" => "Основное содержимое", "not_required" => true),
            "en_content" => array("type" => "text", "name" => "Основное содержимое (английский)", "not_required" => true),
            'photos' => array(
                "type" => "ajax_photogallery",
                "name" => "Фотографии для вставки в текст",
                "model" => "mphotos",
                "fixed_data" => array("type" => "folders"),
                "photo_field" => "picture",
                'photo_folder' => "img/folders_gallery/",
                "max_small_width" => 100,
                "max_small_height" => 100,
                "max_width" => 1600,
                "max_height" => 1600,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "hook" => "parent_id",
                "link_to_folder" => "/gloomdress/open_photoprints/"
            ),
            "date" => array('name' => "Дата создания", "type" => "datetime"),
            //"parent_id"=> array("type"=>"model_list","name"=>"Переместить в подраздел","model"=>"mfolders","field"=>"name","not_required"=>true),
            "coming_soon" => array("type" => "checkbox", "name" => "Coming soon"),
            "visible" => array("type" => "checkbox", "name" => "Отображать")
        );

        if ($id > 0) {
            $this->admin_builder2->set_links_after($links_array);
            if ($inf['type'] == 'products') {
                $structure['product_type'] = array("type" => "model_list", 'key' => 'id', "name" => "Тип товара", "model" => "mtypes", "field" => "name", "not_required" => true, "width" => 100);
            }
        }
        $this->admin_builder2->set_model('mfolders');
        $this->admin_builder2->set_order_criteria(array("parent_id" => $parent_id));
        $this->admin_builder2->set_header("gloomdress/folders/$parent_id/1");
        $this->admin_builder2->set_on_change_header("gloomdress/open_folders/" . $parent_id . '/');
        $this->admin_builder2->set_core_name("folders");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // FOLDERS
    ///////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////
    // blog
    function blog($page = 1)
    {
        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 80),
            'preview' => array('type' => "jcrop",
                "name" => "Превью",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 510,
                "max_height" => 446,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "preview_type",
                "directory" => "img/blog/",
                "width" => 100
            ),
        );

        $this->admin_builder2->set_model('mblog');
        $this->admin_builder2->set_core_name("blog");
        +
        $this->admin_builder2->set_title_name("Блог");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_blog($id = 0)
    {

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 80, 'url_to' => 'url'),
            "url" => array("type" => "string", "name" => "url", "not_required" => true, "width" => 300),
            'preview' => array('type' => "jcrop",
                "name" => "Превью",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 510,
                "max_height" => 446,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "preview_type",
                "directory" => "img/blog/",
                "width" => 100
            ),
            'description' => array("type" => "string", "name" => "Описание", "width" => 100, 'not_required' => true),
            'content' => array("type" => "text", "name" => "Текст", "width" => 100, 'not_required' => true),
            "picture" => array("type" => "any_file", "name" => "Изображение слева", "directory" => 'img/blog_images/', "width" => 100, "not_required" => true),
            'photos' => array(
                "type" => "ajax_photogallery",
                "name" => "Фотографии",
                "model" => "mphotos",
                "fixed_data" => array("type" => "blog"),
                "photo_field" => "picture",
                'photo_folder' => "img/blog_gallery/",
                "max_small_width" => 220,
                "max_small_height" => 330,
                "max_width" => 800,
                "max_height" => 1200,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "hook" => "parent_id",
                "link_to_folder" => "/gloomdress/open_photoprints/"
            ),
            "bottom" => array("type" => "any_file", "name" => "Файл внизу", "directory" => 'img/blog_images/', "width" => 100, "not_required" => true),

        );


        $this->admin_builder2->set_model('mblog');
        $this->admin_builder2->set_header("gloomdress/blog/1");
        $this->admin_builder2->set_core_name("blog");
        $this->admin_builder2->set_title_name('Альбомы');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // blog
    ///////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////
    // albums
    function albums($page = 1)
    {
        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 80),
            'picture' => array('type' => "jcrop",
                "name" => "Обложка",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 300,
                "max_height" => 200,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "directory" => "img/albums/",
                "width" => 100
            ),
        );

        $this->admin_builder2->set_model('malbums');
        $this->admin_builder2->set_core_name("albums");
        $this->admin_builder2->set_title_name("Альбомы");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

	/**
	 * Акции
	 * @author ansotov
	 */
	public function stock()
	{
		$structure = array(
			"title"          => array("type" => "string", "name" => "Название", "width" => 80),
			"products_count" => array("type" => "string", "name" => "Количество товаров", "width" => 80),
			"discount" => array("type" => "string", "name" => "Процент скидки", "width" => 80),
			"start_at"       => array("type" => "date", "name" => "Время начала", 'width' => 100),
			"end_at"         => array("type" => "date", "name" => "Время окончания", 'width' => 100),
			"active"         => array("type" => "checkbox", "name" => "Активен", "width" => 80)
		);

        $this->admin_builder2->set_model('mstock');
        $this->admin_builder2->set_core_name("stock");
        $this->admin_builder2->set_title_name("Акции");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC');
        $this->admin_builder2->init();
    }

    function open_stock($id = 0)
    {

        //"parent_id"=> array("type"=>"model_list","name"=>"Переместить в подраздел","model"=>"mfolders","field"=>"name","not_required"=>true),

		$structure = array(
			"title"          => array("type" => "string", "name" => "Название", "width" => 80),
			"products_count" => array("type" => "string", "name" => "Количество товаров", "width" => 80),
			"discount" => array("type" => "string", "name" => "Процент скидки", "width" => 80),
			"type"           => array('type'   => 'model_list', "name" => "Тип","model"=>"mtypes","field"=>"name", 'key' => 'url', "width"  => 100),
			"start_at"       => array("type" => "date", "name" => "Время начала", 'width' => 100),
			"end_at"         => array("type" => "date", "name" => "Время окончания", 'width' => 100),
			"active"         => array("type" => "checkbox", "name" => "Активен", "width" => 80)

        );


        $this->admin_builder2->set_model('mstock');
        $this->admin_builder2->set_header("gloomdress/stock/1");
        $this->admin_builder2->set_core_name("stock");
        $this->admin_builder2->set_title_name('Акции');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    /**
     * Акции
     * @author ansotov
     */
    public function front_discounts()
    {
        $structure = array(
            "title" => array("type" => "string", "name" => "Название", "width" => 80),
            "amount" => array("type" => "string", "name" => "Сумма (от)", "width" => 80),
            "discount" => array("type" => "string", "name" => "Скидка", "width" => 80),
            "start_at" => array("type" => "date", "name" => "Время начала", 'width' => 100),
            "end_at" => array("type" => "date", "name" => "Время окончания", 'width' => 100),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );

        $this->admin_builder2->set_model('mfront_discounts');
        $this->admin_builder2->set_core_name("front_discounts");
        $this->admin_builder2->set_title_name("Скидки");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC');
        $this->admin_builder2->init();
    }

    function open_front_discounts($id = 0)
    {

        $structure = array(
            "title" => array("type" => "string", "name" => "Название", "width" => 80),
            "amount" => array("type" => "string", "name" => "Сумма (от)", "width" => 80),
            "discount" => array("type" => "string", "name" => "Скидка (%)", "width" => 80),
            "start_at" => array("type" => "date", "name" => "Время начала", 'width' => 100),
            "end_at" => array("type" => "date", "name" => "Время окончания", 'width' => 100),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );


        $this->admin_builder2->set_model('mfront_discounts');
        $this->admin_builder2->set_header("gloomdress/front_discounts/1");
        $this->admin_builder2->set_core_name("front_discounts");
        $this->admin_builder2->set_title_name('Скидки');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    /**
     * Промокоды
     * @author ansotov
     */
    public function promocodes()
    {
        $structure = array(
            "value" => array("type" => "string", "name" => "Код", "width" => 80),
            "type" => array('type' => 'list', "name" => "Тип",
                "values" => array(
                    "percent" => "Процент",
                    "sum" => "Сумма"
                )
            ),
            "discount" => array("type" => "string", "name" => "Скидка", "width" => 80),
            "min_sum" => array("type" => "string", "name" => "От какой суммы", "width" => 80),
            "start_at" => array("type" => "date", "name" => "Время начала", 'width' => 100),
            "end_date" => array("type" => "date", "name" => "Время окончания", 'width' => 100),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );

        $this->admin_builder2->set_model('mpromocodes');
        $this->admin_builder2->set_core_name("promocodes");
        $this->admin_builder2->set_title_name("Общие промокоды");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC');
        $this->admin_builder2->init();
    }


    /**
     * Упаковка
     * @author ansotov
     */
    public function packages()
    {
        $structure = array(
            "title" => array("type" => "string", "name" => "Заголовок", "width" => 80),
            "en_title" => array("type" => "string", "name" => "Заголовок (английский)", "width" => 80, 'not_required' => true),
            "description" => array("type" => "text", "name" => "Описание", "width" => 200, 'not_required' => true),
            "en_description" => array("type" => "text", "name" => "Описание (английский)", "width" => 200, 'not_required' => true),
            "price" => array("type" => "string", "name" => "Цена", "width" => 50),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );

        $this->admin_builder2->set_model('mpackages');
        $this->admin_builder2->set_core_name("packages");
        $this->admin_builder2->set_title_name("Упаковка");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC');
        $this->admin_builder2->init();
    }

    /**
     * Просмотр упаковки
     * @author ansotov
     *
     * @param int $id
     */
    function open_packages($id = 0)
    {
        $structure = array(
            "title" => array("type" => "string", "name" => "Заголовок", "width" => 80),
            "en_title" => array("type" => "string", "name" => "Заголовок (английский)", "width" => 80, 'not_required' => true),
            "description" => array("type" => "text", "name" => "Описание", "width" => 200, 'not_required' => true),
            "en_description" => array("type" => "text", "name" => "Описание (английский)", "width" => 200, 'not_required' => true),
            'img' => array('type' => "jcrop",
                "name" => "Превью",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 510,
                "max_height" => 446,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "img_type",
                "directory" => "img/packages/",
                "width" => 100
            ),
            "price" => array("type" => "string", "name" => "Цена", "width" => 50),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );

        $this->admin_builder2->set_model('mpackages');
        $this->admin_builder2->set_header("gloomdress/packages/1");
        $this->admin_builder2->set_core_name("packages");
        $this->admin_builder2->set_title_name('Упаковка');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    /**
     * Просмотр промокода
     * @author ansotov
     *
     * @param int $id
     */
    function open_promocodes($id = 0)
    {

        $structure = array(
            "value" => array("type" => "string", "name" => "Код", "width" => 80),
            "type" => array('type' => 'list', "name" => "Тип",
                "values" => array(
                    "percent" => "Процент",
                    "sum" => "Сумма"
                )
            ),
            "min_sum" => array("type" => "string", "name" => "От какой суммы", "width" => 80),
            "discount" => array("type" => "string", "name" => "Скидка", "width" => 80),
            "start_at" => array("type" => "date", "name" => "Время начала", 'width' => 100),
            "end_date" => array("type" => "date", "name" => "Время окончания", 'width' => 100),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );

        $this->load->model('mproduct_categories');
        $promocode_categories = $this->mproduct_categories->get();

        $this->load->model('mpromocode_categories');
        $isset_tags = $this->mpromocode_categories->getIn(array('promocode_id' => $id), array('cat_id'), 0, false, 'cat_id');

        foreach ($promocode_categories as $key => $value) {
            $cats[$key] = $value;
            if (in_array($value['id'], $isset_tags))
                $cats[$key]['checked'] = 1;

        }

        $this->mysmarty->assign('categories', $cats);

        $this->admin_builder2->set_model('mpromocodes');
        $this->admin_builder2->set_header("gloomdress/promocodes/1");
        $this->admin_builder2->set_core_name("promocodes");
        $this->admin_builder2->set_title_name('Промокоды');
        $this->admin_builder2->set_categories($cats);
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    /**
     * Установка персональных промокодов
     * @author ansotov
     */
    public function pp_types()
    {
        $structure = array(
            "min_sum" => array("type" => "string", "name" => "От какой суммы", "width" => 80),
            "value" => array("type" => "string", "name" => "Сумма скидки", "width" => 80),
            "discount_type" => array('type' => 'list', "name" => "Тип",
                "values" => array(
                    "percent" => "Процент",
                    "sum" => "Сумма"
                )
            ),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );

        $this->admin_builder2->set_model('mpp_types');
        $this->admin_builder2->set_core_name("pp_types");
        $this->admin_builder2->set_title_name("Персональные промокоды");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC');
        $this->admin_builder2->init();
    }

    /**
     * Просмотр промокода
     * @author ansotov
     *
     * @param int $id
     */
    function open_pp_types($id = 0)
    {

        $structure = array(
            "min_sum" => array("type" => "string", "name" => "От какой суммы", "width" => 80),
            "value" => array("type" => "string", "name" => "Сумма скидки", "width" => 80),
            "discount_type" => array('type' => 'list', "name" => "Тип",
                "values" => array(
                    "percent" => "Процент",
                    "sum" => "Сумма"
                )
            ),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );

        $this->admin_builder2->set_model('mpp_types');
        $this->admin_builder2->set_header("gloomdress/pp_types/?main=pp_types");
        $this->admin_builder2->set_core_name("pp_types");
        $this->admin_builder2->set_title_name('Промокоды');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    /**
     * Установка бесплатной доставки
     * @author ansotov
     */
    public function free_delivery()
    {
        $structure = array(
            "start_time" => array("type" => "time", "name" => "Начало платной доставки", "width" => 80),
            "end_time" => array("type" => "time", "name" => "Конец платной доставки", "width" => 80),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );

        $this->admin_builder2->set_model('mfree_delivery');
        $this->admin_builder2->set_core_name("free_delivery");
        $this->admin_builder2->set_title_name("Бесплатная доставка");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC');
        $this->admin_builder2->init();
    }

    /**
     * Просмотр промокода
     * @author ansotov
     *
     * @param int $id
     */
    function open_free_delivery($id = 0)
    {

        $structure = array(
            "start_time" => array("type" => "time", "name" => "Начало платной доставки", "width" => 80),
            "end_time" => array("type" => "time", "name" => "Конец платной доставки", "width" => 80),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 80)
        );

        $this->admin_builder2->set_model('mfree_delivery');
        $this->admin_builder2->set_header("gloomdress/free_delivery/?main=free_delivery");
        $this->admin_builder2->set_core_name("free_delivery");
        $this->admin_builder2->set_title_name('Бесплатная доставка');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    function open_albums($id = 0)
    {

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 80, 'url_to' => 'url'),
            "url" => array("type" => "string", "name" => "url", "not_required" => true, "width" => 300),
            "type" => array('type' => 'list', "name" => "Тип",
                "values" => array(
                    "" => "обычный",
                    "press" => "Пресса",

                ),
                "not_required" => true,
                "width" => 100),
            'picture' => array('type' => "jcrop",
                "name" => "Обложка",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 300,
                "max_height" => 200,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "directory" => "img/albums/",
                "width" => 100
            ), 'photos' => array(
                "type" => "ajax_photogallery",
                "name" => "Фотографии для альбома",
                "model" => "mphotos",
                "fixed_data" => array("type" => "album"),
                "photo_field" => "picture",
                'photo_folder' => "img/albums_gallery/",
                "max_small_width" => 220,
                "max_small_height" => 330,
                "max_width" => 800,
                "max_height" => 1200,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "hook" => "parent_id",
                "link_to_folder" => "/gloomdress/open_photoprints/"
            ),


        );


        $this->admin_builder2->set_model('malbums');
        $this->admin_builder2->set_header("gloomdress/albums/1");
        $this->admin_builder2->set_core_name("albums");
        $this->admin_builder2->set_title_name('Альбомы');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // albums
    ///////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////
    // press
    function press($page = 1)
    {
        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 80),
            'picture' => array('type' => "jcrop",
                "name" => "Обложка",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 220,
                "max_height" => 330,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "directory" => "img/press/",
                "width" => 100
            ),
        );

        $this->admin_builder2->set_model('mpress');
        $this->admin_builder2->set_core_name("press");
        +
        $this->admin_builder2->set_title_name("Пресса");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_press($id = 0)
    {

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 80, 'url_to' => 'url'),
            'picture' => array('type' => "jcrop",
                "name" => "Обложка",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 220,
                "max_height" => 330,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "directory" => "img/press/",
                "width" => 100
            ),
            'photos' => array(
                "type" => "ajax_photogallery",
                "name" => "Фотографии для альбома",
                "model" => "mphotos",
                "fixed_data" => array("type" => "press"),
                "photo_field" => "picture",
                'photo_folder' => "img/press_gallery/",
                "max_small_width" => 100,
                "max_small_height" => 100,
                "max_width" => 1200,
                "max_height" => 1200,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "hook" => "parent_id",
                "link_to_folder" => "/gloomdress/open_photoprints/"
            ),


        );


        $this->admin_builder2->set_model('mpress');
        $this->admin_builder2->set_header("gloomdress/press/1");
        $this->admin_builder2->set_core_name("press");
        $this->admin_builder2->set_title_name('Пресса');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // press
    ///////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////
    // discounts
    function discounts($page = 1)
    {
        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "url_to" => 'url', "width" => 200),
            "discount" => array("type" => "string", "name" => "Процент скидки", "width" => 20),
            "from" => array('name' => "Начало акции", "type" => "datetime", 'not_required' => true),
            "till" => array('name' => "Окончание акции", "type" => "datetime", 'not_required' => true),
            "type" => array("type" => "model_list", 'key' => 'url', "name" => "Тип одежды", "model" => "mtypes", "field" => "name", "not_required" => true, "width" => 100),
            //"season"=> array("type"=>"model_list",'key'=>'url',"name"=>"Сезон","model"=>"mseasons","field"=>"name","not_required"=>true,"width"=>200),
            //"collection"=> array("type"=>"model_list",'key'=>'url',"name"=>"Коллекция(Линия)","model"=>"mcollections","field"=>"name","not_required"=>true,"width"=>150),
            //"admin"=> array("type"=>"checkbox",'name'=>'Только для админа'),
            //"facebook_like"=>array("type"=>"checkbox","name"=>"Нужен like страницы facebook"),
            //'continuum'=>array("type"=>"checkbox",'name'=>"Нужен континиум"),
            'display' => array('type' => 'checkbox', 'name' => 'Показывать'),
            'active' => array('type' => 'checkbox', 'name' => 'Активен')
        );


        $this->admin_builder2->set_model('mdiscounts');
        $this->admin_builder2->set_core_name("discounts");
        $this->admin_builder2->set_title_name("Скидки и Акции");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id DESC', $page);
        $this->admin_builder2->init();
    }

    function open_discounts($id = 0)
    {

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "url_to" => 'url', "width" => 200),
            "discount" => array("type" => "string", "name" => "Процент скидки", "width" => 20),
            "false_discount" => array("type" => "string", "name" => "Ложный Процент скидки (Для спец акций)", "width" => 20, "not_required" => true),
            'per_product' => array("type" => "string", "name" => "Скидка на каждый N товар", "not_required" => true),
            "from" => array('name' => "Начало акции", "type" => "datetime", 'not_required' => true),
            "till" => array('name' => "Окончание акции", "type" => "datetime", 'not_required' => true),
            "skus" => array("type" => "string", "name" => "Товары со скидкой через запятую( можно указывать часть артикула)", "width" => 100, 'not_required' => true),
            "exclude_skus" => array("type" => "string", "name" => "Исключения со скидкой через запятую( можно указывать часть артикула)", "width" => 100, 'not_required' => true),
            "type" => array("type" => "model_list", 'key' => 'url', "name" => "Тип товара", "model" => "mtypes", "field" => "name", "not_required" => true, "width" => 100),
            "min_cart" => array('name' => "Минимальное кол-во товаров", "type" => "string", 'not_required' => true),
            //"season"=> array("type"=>"model_list",'key'=>'url',"name"=>"Сезон","model"=>"mseasons","field"=>"name","not_required"=>true,"width"=>200),
            //"collection"=> array("type"=>"model_list",'key'=>'url',"name"=>"Коллекция(Линия)","model"=>"mcollections","field"=>"name","not_required"=>true,"width"=>150),
            //'orders_needed'=>array('type'=>'checkbox','name'=>'Нужен минимум 1 завершенный заказ'),
            //'registration_needed'=>array('type'=>'checkbox','name'=>'Нужна регистрация'),
            //"registered_after"=>array('name'=>"Зарегистрирован после","type"=>"datetime",'not_required'=>true),
            //"admin"=> array("type"=>"checkbox",'name'=>'Только для админа'),
            //"facebook_like"=>array("type"=>"checkbox","name"=>"Нужен like страницы facebook"),
            'show_info' => array('type' => 'checkbox', 'name' => 'Показывать информацию о скидке у товара'),
            "description" => array("type" => "text", "name" => "Описание", "width" => 20, "not_required" => true),
            //'continuum'=>array("type"=>"checkbox",'name'=>"Нужен континиум"),
            'display' => array('type' => 'checkbox', 'name' => 'Показывать'),
            'active' => array('type' => 'checkbox', 'name' => 'Активен')


        );


        $this->admin_builder2->set_model('mdiscounts');
        $this->admin_builder2->set_header("gloomdress/discounts/1");
        $this->admin_builder2->set_core_name("discounts");
        $this->admin_builder2->set_title_name("Скидки и Акции");
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // discounts
    ///////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////
    // unloading
    function unloading($type, $page = 1)
    {
        $name = 'Выгрузка пользователей';

        $this->admin_builder2->set_core_name("unloading/users");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->initUnloading();
    }


    function generate_sizes()
    {
        //$this->update_specific_sizes();
        //die();

        $this->load->model('mcases_types');
        $this->load->model('mproducts');
        $this->load->model('mproducts_sizes');
        //$this->mproducts_sizes->delete(array('id !='=>-1));
        $iphone_cases  = $this->mcases_types->get('type = 1 ORDER BY id asc', array(), 0);
        $samsung_cases = $this->mcases_types->get(array('type' => 2), array(), 0);
        $glitter_cases = $this->mcases_types->get(array('type' => 3), array(), 0);

        $products = $this->mproducts->get(array('change_sizes' => true), 0);
        //$products = $this->mproducts->get("type ='gotovye-kejsy' AND c_type = 3", array(), 0);
        //$products = $this->mproducts->get("type ='female-t-shirts' AND NOT EXISTS (SELECT ps.id FROM products_sizes ps WHERE ps.product_id = products.id)", array(), 0);
        if ($products) foreach ($products as $k => $v) {

            $sizes = array();

            if ($v['type'] == 'gotovye-kejsy' || $v['type'] == 'kejs-na-zakaz') {

                $cases = ($v['c_type'] == 1) ? $iphone_cases : $samsung_cases;

                switch ($v['c_type']) {
                    case 1:
                        $cases = $iphone_cases;
                        break;
                    case 2:
                        $cases = $samsung_cases;
                        break;
                    case 3:
                        $cases = $glitter_cases;
                        break;

                }

                foreach ($cases as $k2 => $v2) {
                    $insert = array('product_id' => $v['id'],
                        'code' => $v2['url'],
                        'name' => $v2['name'],
                        'product_sku' => $v['sku'],
                        'on_hand' => 10);
                    if ($v2['price'] > 0)
                        $insert['price'] = $v2['price'];

                    if (!$this->mproducts_sizes->get('code = "' . $v2['url'] . '" AND product_id = ' . $v['id']))
                        $this->mproducts_sizes->insert($insert);
                }
            } else {
                if ((strpos($v['type'], 'ale-') == true) || (in_array($v['type'], array('male-t-shirts', 'female-t-shirts', 'swimsuits', 'hoodie-hooded', 't-shirts-with-sequins')))) {

                    if (in_array($v['type'], array('male-t-shirts', 'female-t-shirts', 't-shirts-with-sequins'))) {
                        $sizes = array(
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'S',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'M',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'L',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'XXL',
                                'on_hand' => 10)
                        );
                    } elseif (in_array($v['type'], array('hoodie-hooded', 'male-sweatshirts', 'female-sweatshirts'))) {
                        print_r($v['type'] . ': ' . $v['id']); echo '<br>';

                        $sizes = array(
                            array('product_id' => $v['id'],
                                  'product_sku' => $v['sku'],
                                  'code' => 'S-M',
                                  'name' => 'S-M',
                                  'on_hand' => 10),
                            array('product_id' => $v['id'],
                                  'product_sku' => $v['sku'],
                                  'code' => 'L-XL',
                                  'name' => 'L-XL',
                                  'on_hand' => 10)
                        );
                    }
                    else {
                        $sizes = array(
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'XS',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'S',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'M',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'L',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'XL',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'XXL',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'XXXL',
                                'on_hand' => 10)
                        );
                    }

                } elseif (strpos($v['type'], 'hildren-') == true) {

                    $sizes = array(
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '5662',
                            'name' => '1-3 мес (56-62 см)',
                            'on_hand' => 10),
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '6268',
                            'name' => '3-6 мес (62-68 см)',
                            'on_hand' => 10),
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '6874',
                            'name' => '6-9 мес (68-74 см)',
                            'on_hand' => 10),
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '7480',
                            'name' => '9-12 мес (74-80 см)',
                            'on_hand' => 10),
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '8086',
                            'name' => '12-18 мес (80-86 см)',
                            'on_hand' => 10),
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '8692',
                            'name' => '18-24 мес (86-92 см)',
                            'on_hand' => 10),
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '9298',
                            'name' => '2-3 года (92-98 см)',
                            'on_hand' => 10),
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '98104',
                            'name' => '3-4 года (98-104 см)',
                            'on_hand' => 10),
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '104110',
                            'name' => '4-5 лет (104-110 см)',
                            'on_hand' => 10),
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => '110118',
                            'name' => '5-6 лет (110-118 см)',
                            'on_hand' => 10)
                    );
                } elseif ($v['type'] == 'down-jackets') {
                    $sizes = array(
                        array('product_id' => $v['id'],
                              'product_sku' => $v['sku'],
                              'code' => 'M',
                              'name' => 'M (42-44)',
                              'on_hand' => 10),
                        array('product_id' => $v['id'],
                              'product_sku' => $v['sku'],
                              'code' => 'L',
                              'name' => 'L (44-46)',
                              'on_hand' => 10),
                        array('product_id' => $v['id'],
                              'product_sku' => $v['sku'],
                              'code' => 'XL',
                              'name' => 'XL (46-48)',
                              'on_hand' => 10),
                        array('product_id' => $v['id'],
                              'product_sku' => $v['sku'],
                              'code' => 'XXL',
                              'name' => 'XXL (50-58)',
                              'on_hand' => 10),
                    );
                } elseif (in_array($v['type'], array('trousers', 'shorts'))) {
                    $sizes = array(
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'XS',
                            'name'        => 'XS',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'S',
                            'name'        => 'S',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'M',
                            'name'        => 'M',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'L',
                            'name'        => 'L',
                            'on_hand'     => 10,
                        ),
                    );
                } elseif (in_array($v['type'], array('body'))) {
                    $sizes = array(
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-pink',
                            'name'        => 'XS (42) розовый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-pink',
                            'name'        => 'S (44) розовый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-pink',
                            'name'        => 'M (46) розовый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-pink',
                            'name'        => 'L (48) розовый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-blue',
                            'name'        => 'XS (42) голубой',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-blue',
                            'name'        => 'S (44) голубой',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-blue',
                            'name'        => 'M (46) голубой',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-blue',
                            'name'        => 'L (48) голубой',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-grey',
                            'name'        => 'XS (42) серый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-grey',
                            'name'        => 'S (44) серый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-grey',
                            'name'        => 'M (46) серый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-grey',
                            'name'        => 'L (48) серый',
                            'on_hand'     => 10,
                        ),
                        /*array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-white',
                            'name'        => 'XS (42) белый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-white',
                            'name'        => 'S (44) белый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-white',
                            'name'        => 'M (46) белый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-white',
                            'name'        => 'L (48) белый',
                            'on_hand'     => 10,
                        ),*/
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-black',
                            'name'        => 'XS (42) черный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-black',
                            'name'        => 'S (44) черный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-black',
                            'name'        => 'M (46) черный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-black',
                            'name'        => 'L (48) черный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-red',
                            'name'        => 'XS (42) красный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-red',
                            'name'        => 'S (44) красный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-red',
                            'name'        => 'M (46) красный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-red',
                            'name'        => 'L (48) красный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-khaki',
                            'name'        => 'XS (42) хаки',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-khaki',
                            'name'        => 'S (44) хаки',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-khaki',
                            'name'        => 'M (46) хаки',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-khaki',
                            'name'        => 'L (48) хаки',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-white',
                            'name'        => 'XS (42) белый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-white',
                            'name'        => 'S (44) белый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-white',
                            'name'        => 'M (46) белый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-white',
                            'name'        => 'L (48) белый',
                            'on_hand'     => 10,
                        ),
                    );
                } elseif ($v['type'] == 'raincoats') {
                    $sizes = array(
                        /*array(
                            'product_id'  => $v['id'],
                            'order_id'  => 14,
                            'product_sku' => $v['sku'],
                            'code'        => 's-neon',
                            'name'        => 'S неон',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 15,
                            'product_sku' => $v['sku'],
                            'code'        => 'm-neon',
                            'name'        => 'M неон',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 16,
                            'product_sku' => $v['sku'],
                            'code'        => 'l-neon',
                            'name'        => 'L неон',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 18,
                            'product_sku' => $v['sku'],
                            'code'        => 'xxl-neon',
                            'name'        => 'XXL неон',
                            'on_hand'     => 10,
                            'active' => 1
                        ),*/
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 8,
                            'product_sku' => $v['sku'],
                            'code'        => 's-red',
                            'name'        => 'S красный',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 9,
                            'product_sku' => $v['sku'],
                            'code'        => 'm-red',
                            'name'        => 'M красный',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 10,
                            'product_sku' => $v['sku'],
                            'code'        => 'l-red',
                            'name'        => 'L красный',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 12,
                            'product_sku' => $v['sku'],
                            'code'        => 'xxl-red',
                            'name'        => 'XXL красный',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 14,
                            'product_sku' => $v['sku'],
                            'code'        => 's-black',
                            'name'        => 'S черный',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 15,
                            'product_sku' => $v['sku'],
                            'code'        => 'm-black',
                            'name'        => 'M черный',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 16,
                            'product_sku' => $v['sku'],
                            'code'        => 'l-black',
                            'name'        => 'L черный',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'order_id'  => 18,
                            'product_sku' => $v['sku'],
                            'code'        => 'xxl-black',
                            'name'        => 'XXL черный',
                            'on_hand'     => 10,
                            'active' => 1
                        ),
                    );
                } elseif (in_array($v['type'], array('dresses'))) {
                    $sizes = array(
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-grey',
                            'name'        => 'XS (42) серый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-grey',
                            'name'        => 'S (44) серый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-grey',
                            'name'        => 'M (46) серый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-grey',
                            'name'        => 'L (48) серый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-black',
                            'name'        => 'XS (42) черный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-black',
                            'name'        => 'S (44) черный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-black',
                            'name'        => 'M (46) черный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-black',
                            'name'        => 'L (48) черный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-black-blue',
                            'name'        => 'XS (42) темно-синий',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-black-blue',
                            'name'        => 'S (44) темно-синий',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-black-blue',
                            'name'        => 'M (46) темно-синий',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-black-blue',
                            'name'        => 'L (48) темно-синий',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-red',
                            'name'        => 'XS (42) красный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-red',
                            'name'        => 'S (44) красный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-red',
                            'name'        => 'M (46) красный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-red',
                            'name'        => 'L (48) красный',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'xs-pink',
                            'name'        => 'XS (42) розовый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 's-pink',
                            'name'        => 'S (44) розовый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'm-pink',
                            'name'        => 'M (46) розовый',
                            'on_hand'     => 10,
                        ),
                        array(
                            'product_id'  => $v['id'],
                            'product_sku' => $v['sku'],
                            'code'        => 'l-pink',
                            'name'        => 'L (48) розовый',
                            'on_hand'     => 10,
                        ),
                    );
                } elseif ($v['type'] != 'bags'
                    && $v['type'] != 'oblozhki-na-pasport'
                    && $v['type'] != 'auto-documents'
                    && $v['type'] != 'boxes'
                    && $v['type'] != 'notes'
                    && $v['type'] != 'bags'
                    && $v['type'] != 'masks'
                    && $v['type'] != 'cardholders') {

                    if ($v['group_name'] == 'сдвоенные') {
                        echo 'сдвоенные';
                        $sizes = array(
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'XS-S',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'M-L',
                                'on_hand' => 10)
                        );
                    } else {
                        $sizes = array(
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'XS',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'S',
                                'on_hand' => 10),
                            array('product_id' => $v['id'],
                                'product_sku' => $v['sku'],
                                'code' => 'M',
                                'on_hand' => 10)
                        );
                    }
                } else {
                    $sizes = array(
                        array('product_id' => $v['id'],
                            'product_sku' => $v['sku'],
                            'code' => 'onesize',
                            'on_hand' => 10)
                    );
                }

                foreach ($sizes as $key => $value) {
                    if (!$this->mproducts_sizes->get('`product_id` = ' . $v['id'] . ' AND `code` = "' . $value['code'] . '"')) {
                        $this->mproducts_sizes->insert($value);
                    }
                }
            }
        }

        //ansotov генерация sitemap
        $this->sitemap();
        echo "Готово!";
    }

    public function update_specific_sizes()
    {
        return false;
        $this->load->model('mproducts');
        $this->load->model('mproducts_sizes');

        $products = $this->mproducts->get(array('type' => 'raincoats'), 0);

        if ($products) foreach ($products as $k => $v) {

            $hasSizes = $this->mproducts_sizes->get(array('product_id' => $v['id']), 0);

            if (count($hasSizes) > 1) {
                continue;
            }

            $sizes = array(
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 14,
                    'product_sku' => $v['sku'],
                    'code'        => 's-neon',
                    'name'        => 'S неон',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 15,
                    'product_sku' => $v['sku'],
                    'code'        => 'm-neon',
                    'name'        => 'M неон',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 16,
                    'product_sku' => $v['sku'],
                    'code'        => 'l-neon',
                    'name'        => 'L неон',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 18,
                    'product_sku' => $v['sku'],
                    'code'        => 'xxl-neon',
                    'name'        => 'XXL неон',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 8,
                    'product_sku' => $v['sku'],
                    'code'        => 's-red',
                    'name'        => 'S красный',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 9,
                    'product_sku' => $v['sku'],
                    'code'        => 'm-red',
                    'name'        => 'M красный',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 10,
                    'product_sku' => $v['sku'],
                    'code'        => 'l-red',
                    'name'        => 'L красный',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 12,
                    'product_sku' => $v['sku'],
                    'code'        => 'xxl-red',
                    'name'        => 'XXL красный',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 14,
                    'product_sku' => $v['sku'],
                    'code'        => 's-black',
                    'name'        => 'S черный',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 15,
                    'product_sku' => $v['sku'],
                    'code'        => 'm-black',
                    'name'        => 'M черный',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 16,
                    'product_sku' => $v['sku'],
                    'code'        => 'l-black',
                    'name'        => 'L черный',
                    'on_hand'     => 10,
                    'active' => 1
                ),
                array(
                    'product_id'  => $v['id'],
                    'order_id'  => 18,
                    'product_sku' => $v['sku'],
                    'code'        => 'xxl-black',
                    'name'        => 'XXL черный',
                    'on_hand'     => 10,
                    'active' => 1
                ),
            );

            foreach ($sizes as $key => $value) {
                $this->mproducts_sizes->insert($value);
            }
        }

        echo 'Обновлено ' . count($products) . ' товаров.';
        die();
    }

    public function update_sitemap()
    {
        //ansotov генерация sitemap
        $this->sitemap();
        echo "Sitemap перегенерирован!";
    }


    ///////////////////////////////////////////////////////
    // collections
    function collections($page = 1)
    {
        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 200),
            "url" => array("type" => "string", "name" => "Url", "width" => 150)
        );

        $this->admin_builder2->set_model('mcollections');
        $this->admin_builder2->set_core_name("collections");
        $this->admin_builder2->set_title_name("Коллекции");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_collections($id = 0)
    {

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 200),
            "url" => array("type" => "string", "name" => "Url", "width" => 150)
        );


        $this->admin_builder2->set_model('mcollections');
        $this->admin_builder2->set_header("gloomdress/collections/1");
        $this->admin_builder2->set_core_name("collections");
        $this->admin_builder2->set_title_name("Коллекции");
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // collections
    ///////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////
    // cases_types
    function cases_types($page = 1)
    {
        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 200),
            "url" => array("type" => "string", "name" => "Url", "width" => 150),
            "price" => array("type" => "string", "name" => "Цена", "width" => 200, "not_required" => true)

        );

        $this->admin_builder2->set_model('mcases_types');
        $this->admin_builder2->set_core_name("cases_types");
        $this->admin_builder2->set_title_name("Типы кейсов");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    /**
     * Список тегов
     * @author ansotov
     *
     * @param int $page
     */
    public function tags($page = 1)
    {
        $structure = array(
            "title" => array("type" => "string", "name" => "Название", "width" => 200)

        );

        $this->admin_builder2->set_model('mtags');
        $this->admin_builder2->set_core_name("tags");
        $this->admin_builder2->set_title_name("Темы");
        $this->admin_builder2->get_output_rows(array(), $structure, 'id ASC', $page);
        $this->admin_builder2->init();
    }

    public function open_tags($id = 1)
    {
        $structure = array(
            "title" => array("type" => "string", "name" => "Название", "width" => 200),
            "en_title" => array("type" => "string", "name" => "Название (английский)", "width" => 200, "not_required" => true)
        );

        $this->admin_builder2->set_model('mtags');
        $this->admin_builder2->set_header("/gloomdress/tags");
        $this->admin_builder2->set_core_name("tags");
        $this->admin_builder2->set_title_name("Темы");
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->set_section_id('section_id', 84);
        $this->admin_builder2->init($structure);

    }

    function open_cases_types($id = 0)
    {
        $this->load->model('mtype_cases');

        $c_data = $this->mtype_cases->get();

        $c_arr = array();

        foreach ($c_data as $k => $v)
            $c_arr[$v['id']] = $v['title'];

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 200),
            "url" => array("type" => "string", "name" => "Url", "width" => 150),
            "price" => array("type" => "string", "name" => "Цена", "width" => 200, "not_required" => true),
            "type" => array("type" => "list", "values" => $c_arr, "name" => "Тип", "not_required" => true),
        );


        $this->admin_builder2->set_model('mcases_types');
        $this->admin_builder2->set_header("gloomdress/cases_types/1");
        $this->admin_builder2->set_core_name("cases_types");
        $this->admin_builder2->set_title_name("Типы кейсов");
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // cases_types
    ///////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////
    // materials
    function materials($page = 1)
    {
        $this->load->model('mtype_cases');
        $c_data = $this->mtype_cases->get();

        $c_arr = array();

        foreach ($c_data as $k => $v)
            $c_arr[$v['id']] = $v['title'];

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "url_to" => "url", "width" => 200),
            "url" => array("type" => "string", "name" => "Url", "width" => 150),
            "type_case_id" => array("type" => "list", "values" => $c_arr, "name" => "Тип", "not_required" => true),
        );

        $this->admin_builder2->set_model('mmaterials');
        $this->admin_builder2->set_core_name("materials");
        $this->admin_builder2->set_title_name("Материалы кейсов");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_materials($id = 0)
    {
        $this->load->model('mtype_cases');
        $c_data = $this->mtype_cases->get();

        $c_arr = array();

        foreach ($c_data as $k => $v)
            $c_arr[$v['id']] = $v['title'];

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "url_to" => "url", "width" => 200),
            "en_name" => array("type" => "string", "name" => "Название (английский)", "url_to" => "url", "width" => 200, "not_required" => true),
            "url" => array("type" => "string", "name" => "Url", "width" => 150),
            'image' => array('type' => "jcrop",
                "name" => "Фото",
                "upload_width" => 300,
                "upload_height" => 300,
                "max_width" => 50,
                "max_height" => 50,
                "max_min_width" => 50,
                "max_min_height" => 50,
                "max_fit" => true,
                "any_image" => true,
                'force_format' => 'jpg',
                "image_type_name" => "image_type",
                "directory" => "img/front_pictures/",
                "width" => 70,
                "not_required" => true
            ),
            'active' => array('type' => 'checkbox', 'name' => 'Активен'),
            "type_case_id" => array("type" => "list", "values" => $c_arr, "name" => "Тип", "not_required" => true),
        );

        $this->admin_builder2->set_model('mmaterials');
        $this->admin_builder2->set_header("gloomdress/materials/1");
        $this->admin_builder2->set_core_name("materials");
        $this->admin_builder2->set_title_name("Материалы кейсов");
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // materials
    ///////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////
    // orders
    function orders($page = 1)
    {
        $structure = array(
            "number" => array("type" => "string", "name" => "Номер", "width" => 100),
            "first_name" => array("type" => "string", "name" => "Имя", "width" => 100, 'not_required' => true),
            "sum" => array("type" => "string", "name" => "Сумма", "width" => 50, 'not_required' => true),
            "email" => array("type" => "string", "name" => "Email", "width" => 50, 'not_required' => true),
            "date" => array("type" => "datetime", "name" => "Время завершения", 'width' => 100),
        );
        $filter_data = array(
            'number' => array('type' => '%like%', 'name' => "Номер"),
        );

        $this->admin_builder2->set_model('morders');
        $this->admin_builder2->set_core_name("orders");
        $this->admin_builder2->set_title_name("Заказы");
        $this->admin_builder2->get_output_rows($filter_data, $structure, 'orders.id DESC', $page);
        $this->admin_builder2->init();
    }

    function preorders($page = 1)
    {
        $structure = array(
            "name" => array("type" => "string", "name" => "Имя", "width" => 100),
            "email" => array("type" => "string", "name" => "Email", "width" => 100, 'not_required' => true),
            "phone" => array("type" => "string", "name" => "Телефон", "width" => 150, 'not_required' => true),
            //"product_id" => array("type" => "string", "name" => "Email", "width" => 50, 'not_required' => true),
            "product_id" => array("type" => "model_list", 'key' => 'id', "name" => "Товар", "model" => "mproducts", "field" => "name", "not_required" => true, "width" => 150),
            "size_id" => array("type" => "model_list", 'key' => 'id', "name" => "Размер", "model" => "mproducts_sizes", "field" => "code", "not_required" => true, "width" => 100),
            //"size_id" => array("type" => "datetime", "name" => "Время завершения", 'width' => 100),
            "created_at" => array("type" => "date", "name" => "Создано", 'width' => 100),
        );
        $filter_data = array(
            'created_at' => array('type' => 'date', 'add_type' => '>=', 'name' => "Создано"),
        );

        $this->admin_builder2->set_model('mreminder');
        $this->admin_builder2->set_core_name("preorders");
        $this->admin_builder2->set_title_name("Предзаказы");
        $this->admin_builder2->get_output_rows($filter_data, $structure, 'reminder.created_at DESC', $page);
        $this->admin_builder2->init();
    }

    function open_preorders($id = 1)
    {
        $structure = array(
            "name" => array("type" => "string", "name" => "Имя", "width" => 100),
            "email" => array("type" => "string", "name" => "Email", "width" => 100, 'not_required' => true),
            "phone" => array("type" => "string", "name" => "Телефон", "width" => 150, 'not_required' => true),
        );

        $this->admin_builder2->set_model('mreminder');
        $this->admin_builder2->set_header("gloomdress/preorders/1");
        $this->admin_builder2->set_core_name("preorders");
        $this->admin_builder2->set_title_name('Предзаказ');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    function open_orders($id = 0)
    {

        $structure = array(
            "number" => array("type" => "string", "name" => "Номер", "width" => 100),
            "type" => array('type' => 'list', "name" => "Тип чехла",
                "values" => array(
                    "" => "",
                    "iphone4" => "iphone 4",
                    "iphone5c" => "iphone 5c",
                    "iphone5s" => "iphone 5s",
                    'galaxyc3' => 'samsung galaxy c3',

                ),
                "width" => 100),
            "first_name" => array("type" => "string", "name" => "Имя", "width" => 100, 'not_required' => true),
            'address' => array("type" => "text", "name" => "Адрес", "width" => 100, 'not_required' => true),
            "sum" => array("type" => "string", "name" => "Сумма", "width" => 100, 'not_required' => true),
            "email" => array("type" => "string", "name" => "Email", "width" => 100, 'not_required' => true),
            "phone" => array("type" => "string", "name" => "Телефон", "width" => 100, 'not_required' => true),
            'comments' => array("type" => "text", "name" => "Комментарии к заказу", "width" => 100, 'not_required' => true),
            "date" => array("type" => "datetime", "name" => "Время создания", 'width' => 100),
            'send' => array('type' => 'checkbox', 'name' => 'Заказ отправлен'),


        );
        if ($id > 0) {
            $links_array["/gloomdress/orders_products/"] = 'Товары заказа';
            $this->admin_builder2->set_links_after($links_array);
        }


        $this->admin_builder2->set_model('morders');
        $this->admin_builder2->set_header("gloomdress/orders/1");
        $this->admin_builder2->set_core_name("orders");
        $this->admin_builder2->set_title_name('Заказ');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // orders
    ///////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////
    // orders_products
    function orders_products($order_id, $page = 1)
    {
        $structure = array(
            "sku" => array("type" => "string", "name" => "Артикул", "width" => 100, 'not_required' => true),
            "size" => array("type" => "string", "name" => "Размер", "width" => 100, 'not_required' => true),
            "color" => array("type" => "string", "name" => "Цвет", "width" => 100, 'not_required' => true),
            "price" => array("type" => "string", "name" => "Сумма", "width" => 50, 'not_required' => true),
        );
        $filter_data = array();


        $this->admin_builder2->set_links_before(array("/gloomdress/open_orders/$order_id/" => "К заказу"));
        $this->admin_builder2->set_model('morders_products');
        $this->admin_builder2->set_order_criteria(array('user_order_id' => $order_id));
        $this->admin_builder2->set_core_name("orders_products");
        +
        $this->admin_builder2->set_title_name("Заказы");
        $this->admin_builder2->get_output_rows($filter_data, $structure, 'orders_products.id DESC', $page);
        $this->admin_builder2->init();
    }

    function open_orders_products($order_id, $id = 0)
    {

        $structure = array(
            "number" => array("type" => "string", "name" => "Номер", "width" => 100),
            "type" => array('type' => 'list', "name" => "Тип чехла",
                "values" => array(
                    "" => "",
                    "iphone4" => "iphone 4",
                    "iphone5c" => "iphone 5c",
                    "iphone5s" => "iphone 5s",
                    'galaxyc3' => 'samsung galaxy c3',

                ),
                "width" => 100),
            "first_name" => array("type" => "string", "name" => "Имя", "width" => 100, 'not_required' => true),
            'address' => array("type" => "text", "name" => "Адрес", "width" => 100, 'not_required' => true),
            "sum" => array("type" => "string", "name" => "Сумма", "width" => 100, 'not_required' => true),
            "email" => array("type" => "string", "name" => "Email", "width" => 100, 'not_required' => true),
            "phone" => array("type" => "string", "name" => "Телефон", "width" => 100, 'not_required' => true),
            'comments' => array("type" => "text", "name" => "Комментарии к заказу", "width" => 100, 'not_required' => true),
            "date" => array("type" => "datetime", "name" => "Время создания", 'width' => 100),
            'send' => array('type' => 'checkbox', 'name' => 'Заказ отправлен'),


        );


        $this->admin_builder2->set_model('morders_products');
        $this->admin_builder2->set_order_criteria(array('user_order_id' => $order_id));
        $this->admin_builder2->set_header("gloomdress/orders_products/1");
        $this->admin_builder2->set_core_name("orders_products");
        $this->admin_builder2->set_title_name('Заказ');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    // orders_products
    ///////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////
    // menu
    function menu($type, $page = 1)
    {
        $structure = array(
            "folder_id" => array("type" => "model_list", "name" => "Раздел", "model" => "mfolders", "field" => "name", "not_required" => true, "width" => 200),
            'active' => array(
                'type' => 'checkbox',
                'name' => 'Активен'
            ),
        );

        if ($type == 'top') {
            $name = 'Верхнее меню';
        } elseif ($type == 'bottom') {
            $name = 'Нижнее меню';
        } elseif ($type == 'bottom2') {
            $name = "Нижнее меню2";
        }
        $this->admin_builder2->set_model('mmenu');
        $this->admin_builder2->set_order_criteria(array('type' => $type));
        $this->admin_builder2->set_core_name("menu");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    /**
     * Раздел girls in bloom
     * @author ansotov
     *
     * @param     $type
     * @param int $page
     */
    function settings($type, $page = 1)
    {
        $structure = array();


        $name = 'Настройки';

        $this->admin_builder2->set_model('mmain_page');
        $this->admin_builder2->set_core_name("main_page");

        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id DESC', $page);
        $this->admin_builder2->init();
    }

    public function open_main_page($id = 0)
    {
        $structure = array(
            "title" => array("type" => "string", "name" => "Meta(title)"),
            "description" => array("type" => "string", "name" => "Meta(description)"),
            "h1" => array("type" => "string", "name" => "H1", "not_required" => true),
        );

        $name = 'Настройки';

        $this->admin_builder2->set_model('mmain_page');
        $this->admin_builder2->set_header("gloomdress/open_main_page/1/");
        $this->admin_builder2->set_core_name("girlsinbloom/open_main_page/1/");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    /**
     * Раздел girls in bloom
     * @author ansotov
     *
     * @param     $type
     * @param int $page
     */
    function girlsinbloom($type, $page = 1)
    {
        $structure = array(
            "title" => array(
                "type" => "text",
                "name" => "Название",
                "field" => "title",
                "width" => 400
            ),
            'active' => array(
                'type' => 'checkbox',
                'name' => 'Активен'
            ),
        );

        //ansotov если это портфолио
        if ($type == 'portfolio') {

            $name = 'Портфолио';

            $this->admin_builder2->set_model('mportfolio');
            $this->admin_builder2->set_core_name("portfolio");
        } elseif ($type == 'smi') {

            $name = 'Пресса о нас';

            $this->admin_builder2->set_model('msmi');
            $this->admin_builder2->set_core_name("smi");
        }

        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id DESC', $page);
        $this->admin_builder2->init();
    }

    /**
     * Портфолио
     * @author ansotov
     *
     * @param int $id
     */
    public function open_portfolio($id = 0)
    {
        $structure = array(
            "title" => array("type" => "string", "name" => "Название", 'url_to' => 'url'),
            "en_title" => array("type" => "string", "name" => "Название (английский)", "not_required" => true),
            "description" => array("type" => "text", "name" => "Описание"),
            "en_description" => array("type" => "text", "name" => "Описание (английский)"),
            "url" => array("type" => "string", "name" => "URL"),
            'image' => array(
                'type' => "jcrop",
                "name" => "Изображение",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "max_fit" => true,
                "any_image" => true,
                'force_format' => 'jpg',
                "image_type_name" => "image_type",
                "directory" => "img/portfolio/",
                "width" => 70
            ),
            'active' => array('type' => 'checkbox', 'name' => 'Активен'),

        );

        $name = 'Портфолио';

        $this->admin_builder2->set_model('mportfolio');
        $this->admin_builder2->set_header("gloomdress/girlsinbloom/portfolio/");
        $this->admin_builder2->set_core_name("girlsinbloom/portfolio");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    /**
     * СМИ
     * @author ansotov
     *
     * @param int $id
     */
    public function open_smi($id = 0)
    {
        $structure = array(
            "title" => array("type" => "string", "name" => "Название", 'url_to' => 'url'),
            "en_title" => array("type" => "string", "name" => "Название (английский)", "not_required" => true),
            "description" => array("type" => "text", "name" => "Описание"),
            "en_description" => array("type" => "text", "name" => "Описание (английский)"),
            "url" => array("type" => "string", "name" => "URL"),
            'image' => array(
                'type' => "jcrop",
                "name" => "Изображение",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "max_fit" => true,
                "any_image" => true,
                'force_format' => 'jpg',
                "image_type_name" => "image_type",
                "directory" => "img/smi/",
                "width" => 70
            ),
            'active' => array('type' => 'checkbox', 'name' => 'Активен'),

        );

        $name = 'Пресса о нас';

        $this->admin_builder2->set_model('msmi');
        $this->admin_builder2->set_header("gloomdress/girlsinbloom/smi/");
        $this->admin_builder2->set_core_name("girlsinbloom/smi");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }

    function open_menu($id = 0, $type)
    {
        $structure = array(
            "folder_id" => array("type" => "model_list", "name" => "Раздел", "model" => "mfolders", "field" => "name", "not_required" => true, "width" => 200),
            "color" => array("type" => "string", "name" => "Код цвета", 'colorpicker' => true, "width" => 100, 'not_required' => true),

            "preview" => array("type" => "text", "name" => "Описание превью", "not_required" => true),
            'preview_picture' => array('type' => "jcrop",
                "name" => "Картинка превью",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 500,
                "max_height" => 500,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "max_fit" => true,
                "any_image" => true,
                "not_required" => true,
                'force_format' => 'jpg',
                "image_type_name" => "preview_picture_type",
                "directory" => "img/preview_pictures/",
                "width" => 70
            ),

        );

        if ($type == 'top') {
            $name = 'Верхнее меню';
        } elseif ($type == 'bottom') {
            $name = 'Нижнее меню';
        } elseif ($type == 'bottom2') {
            $name = "Нижнее меню2";
        }
        $this->admin_builder2->set_model('mmenu');
        $this->admin_builder2->set_order_criteria(array('type' => $type));
        $this->admin_builder2->set_header("gloomdress/menu/$type/1");
        $this->admin_builder2->set_core_name("menu");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    //  menu
    ///////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////
    // products
    function products($page = 1)
    {
        $structure = array(
            "sku" => array("type" => "string", "name" => "Артикул", "width" => 100),
            "name" => array("type" => "string", "name" => "Название", "width" => 80),
            'front_picture' => array('type' => "jcrop",
                "name" => "Фото перед",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 176,
                "max_height" => 235,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                'force_format' => 'jpg',
                "image_type_name" => "front_picture_type",
                "directory" => "img/front_pictures/",
                "width" => 70
            ),
            'back_picture' => array('type' => "jcrop",
                "name" => "Фото зад",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 176,
                "max_height" => 235,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                'force_format' => 'jpg',
                "image_type_name" => "back_picture_type",
                "directory" => "img/back_pictures/",
                "width" => 70
            ),
            "type" => array("type" => "model_list", 'key' => 'url', "name" => "Тип одежды", "model" => "mtypes", "field" => "name", "not_required" => true, "width" => 100),
            "price" => array("type" => "string", "name" => "Цена в рублях", "width" => 100),
            'online_payment' => array('type' => 'checkbox', 'name' => 'Оплата только онлайн'),
            'new' => array('type' => 'checkbox', 'name' => 'Новый'),
            'best' => array('type' => 'checkbox', 'name' => 'Best'),
            'new_year'           => array('type' => 'checkbox', 'name' => 'Новый год'),
            'active' => array('type' => 'checkbox', 'name' => 'Активен'),
            'change_sizes' => array('type' => 'checkbox', 'name' => 'Обновлять размеры'),
            "not_available" => array('type' => 'checkbox', 'name' => 'Нет в наличии')
        );
        $filter_data = array(
            'name' => array('type' => '%like%', 'name' => "Название"),
            'sku' => array('type' => '%like%', 'name' => "Артикул"),
            'type' => array('type' => 'model_list', 'model' => "mtypes", 'key' => 'url', 'field' => 'name', 'order_by' => 'order_id DESC', 'name' => "Тип"),
            'active' => array('type' => 'list', "name" => "Активен", "values" => array(0 => "Нет", 1 => "Да")),
        );


        $this->admin_builder2->set_model('mproducts');
        if ($_GET['filter']) {
            $filter_data = false;
            $this->admin_builder2->set_order_criteria(array('type' => $_GET['filter']));
        }
        if ($_GET['c_type'])
            $this->admin_builder2->set_order_criteria(array('c_type' => $_GET['c_type']));

        $this->admin_builder2->set_core_name("products");
        $this->admin_builder2->set_title_name("Товары");
        $this->admin_builder2->get_output_rows($filter_data, $structure, 'order_id DESC', $page);
        $this->admin_builder2->init();
    }

    function open_products($id = 0)
    {

        if ($id > 0) {
            $this->load->model('mproducts');
            $inf = $this->mproducts->get(array('id' => $id), array(), 1);

        }

        $this->load->model('mtypes');
        $this->load->model('mtype_cases');

        $data = $this->mtypes->get(array(), array(), 0, 'order_id ASC');
        $c_data = $this->mtype_cases->get();

        $c_arr = array();

        foreach ($c_data as $k => $v)
            $c_arr[$v['id']] = $v['title'];

        $types = array();
        foreach ($data as $k => $v) {
            $types[$v['url']] = $v['name'];
        }

        //ansotov type of product
        $product_type = $GLOBALS['URI']->segments[4];

        if ($product_type == 'packages' || (isset($inf) && $inf['type'] == 'packages')) {
            $max_width = 300;
            $max_height = 201;
        } else {
            $max_width = 352;
            $max_height = 470;
        }

        $structure = array(
            "sku" => array("type" => "string", "name" => "Артикул"),
            "content" => array("type" => "text", "name" => "Описание", "not_required" => true),
            "en_content" => array("type" => "text", "name" => "Описание (английский)", "not_required" => true),
            "meta_title" => array('name' => "Meta(title)", "type" => "string", "not_required" => true),
            "meta_description" => array('name' => "Meta(description)", "type" => "string", "not_required" => true),
            "h1" => array('name' => "H1", "type" => "string", "not_required" => true),
            "model_params" => array("type" => "text", "name" => "Параметры модели", "not_required" => true),
            "care" => array("type" => "text", "name" => "Уход за изделием", "not_required" => true),
            "name" => array("type" => "string", "name" => "Название", "width" => 440, 'url_to' => 'url'),
            "en_name" => array("type" => "string", "name" => "Название (английский)", "width" => 440, 'url_to' => 'url', "not_required" => true),
            "url"              => array("type" => "string", "name" => "url", "not_required" => true, "width" => 300),
            'front_picture' => array('type' => "jcrop",
                "name" => "Фото перед",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => $max_width,
                "max_height" => $max_height,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "max_fit" => true,
                "any_image" => true,
                'force_format' => 'jpg',
                "image_type_name" => "front_picture_type",
                "directory" => "img/front_pictures/",
                "width" => 70
            ),
            'back_picture' => array('type' => "jcrop",
                "name" => "Фото зад",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 352,
                "max_height" => 470,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                'force_format' => 'jpg',
                "image_type_name" => "back_picture_type",
                "directory" => "img/back_pictures/",
                "width" => 70,
                "not_required" => true,
            ),
            'sizes_img' => array('type' => "jcrop",
                "name" => "Обмеры",
                "upload_width" => 800,
                "upload_height" => 800,
                "max_width" => 0,
                "max_height" => 0,
                "max_min_width" => 450,
                "max_min_height" => 162,
                "max_fit" => true,
                "any_image" => true,
                'force_format' => 'jpg',
                "image_type_name" => "sizes_img_type",
                "directory" => "img/front_pictures/",
                "width" => 70,
                "not_required" => true
            ),
            'photos' => array(
                "type" => "ajax_photogallery",
                "name" => "Все фотографии",
                "model" => "mphotos",
                "fixed_data" => array("type" => "products"),
                "photo_field" => "picture",
                'photo_folder' => "img/products_pictures/",
                "max_small_width" => 100,
                "max_small_height" => 100,
                "max_width" => 1600,
                "max_height" => 1600,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "hook" => "parent_id",
                'sizes' => array(
                    'large' => array('width' => 440, 'height' => 587),
                    'small2' => array('width' => 120, 'height' => 160),
                    'product' => array('width' => 300, 'height' => 400),
                    'largest' => array('width' => 1200, 'height' => 1600),
                    'mini' => array('width' => 84, 'height' => 112)
                ),
                "link_to_folder" => "/gloomdress/open_photoprints/"
            ),
            "color" => array("type" => "string", "name" => "Цвет", "not_required" => true),
            "group_name" => array("type" => "string", "name" => "Групповое имя", "not_required" => true),
            "en_group_name" => array("type" => "string", "name" => "Групповое имя (английский)", "not_required" => true),
            "group_variant" => array("type" => "string", "name" => "Групповое отличительное название", "not_required" => true),
            "en_group_variant" => array("type" => "string", "name" => "Групповое отличительное название (английский)", "not_required" => true),
            'color_picture' => array('type' => "jcrop",
                "name" => "Фото цвета",
                "upload_width" => 500,
                "upload_height" => 500,
                "max_width" => 200,
                "max_height" => 200,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "max_fit" => true,
                "any_image" => true,
                'force_format' => 'jpg',
                "not_required" => true,
                "image_type_name" => "color_picture_type",
                "directory" => "img/color_pictures/",
                "width" => 70
            ),
            "fabric" => array("type" => "string", "name" => "Состав", "not_required" => true),
            "collection" => array("type" => "model_list", "name" => "Коллекция", "model" => "mcollections", "key" => "url", "field" => "name", "not_required" => true),

            "type" => array("type" => "list", "values" => $types, "name" => "Тип", "not_required" => true),

        );
        if ($id > 0) {
            $links_array["/gloomdress/products_sizes/"] = 'Размеры товара';
            $this->admin_builder2->set_links_after($links_array);
        }

        $structure["c_type"] = array("type" => "list", "values" => $c_arr, "name" => "Тип чехла", "not_required" => true);

        $structure["price"] = array("type" => "string", "name" => "Цена", "width" => 100, "not_required" => true);
        $structure["old_price"] = array("type" => "string", "name" => "Цена без скидки", "width" => 100, "not_required" => true);

        $structure["active"] = array('type' => 'checkbox', 'name' => 'Активен');
        //$structure["change_sizes"] = array('type' => 'checkbox', 'name' => 'Обновлять размеры');
        $structure["new"] = array('type' => 'checkbox', 'name' => 'Новый');
        $structure["best"] = array('type' => 'checkbox', 'name' => 'Best');

        $structure["not_available"] = array('type' => 'checkbox', 'name' => 'Нет в наличии');
        $structure["new_year"] = array('type' => 'checkbox', 'name' => 'Новый год');

        $this->load->model('mtags');
        $tags_arr = $this->mtags->get();

        //ansotov сообщаем о поступлении товара
        if ($this->input->request('not_available') == false && $this->input->request('save_changes') == true && $id > 0) {

            $this->load->model('mreminder');

            $query = "`product_id` = {$id} AND `size_id` = 0";
            $arr = $this->mreminder->get($query, array());

            if ($arr) {
                foreach ($arr as $k => $v) {
                    $result = $this->db->query("SELECT p.*, r.`name` AS r_name, r.`email` AS r_email FROM `reminder` r, `products` p WHERE p.`id` = r.`product_id` AND p.`id` = " . $id . " AND r.email = '" . $v['email'] . "' AND r.`name` = '" . $v['name'] . "'");
                    $reminder = $result->result_array();

                    $this->mysmarty->assign('reminder', $reminder[0]);
                    $template = $this->mysmarty->fetch('new_bloom/ru/reminder_mail.tpl');

                    $headline = "Поступление товара " . $reminder[0]['name'];

                    //ansotov отправляем письмо
                    $this->send_mail($v['name'], $v['email'], $headline, $template);

                    //ansotov удаляем данные о напоминании
                    $this->mreminder->delete(array('name' => $v['name'], 'email' => $v['email'], 'product_id' => $id));
                }
            }
        }

        $this->load->model('mproduct_tags');
        $isset_tags = $this->mproduct_tags->getIn(array('product_id' => $id), array('tag_id'));

        foreach ($tags_arr as $key => $value) {
            $tags[$key] = $value;
            if (in_array($value['id'], $isset_tags))
                $tags[$key]['checked'] = 1;

        }

        $this->mysmarty->assign('tags', $tags);


        $this->admin_builder2->set_on_change_header("gloomdress/open_products/");
        $this->admin_builder2->set_model('mproducts');
        $this->admin_builder2->set_header("gloomdress/products/1");
        $this->admin_builder2->set_core_name("products");
        $this->admin_builder2->set_tags($tags);
        $this->admin_builder2->set_title_name('Товары');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    //  products
    ///////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////
    // products_sizes
    function products_sizes($parent_id, $page = 1)
    {
        $structure = array(
            "code" => array("type" => "string", "name" => "Название", "width" => 300),
            "product_id" => array("type" => "string", "name" => "ID"),
            "on_hand" => array("type" => "string", "name" => "Количество", "width" => 300, "not_required" => true),
            "last_size" => array("type" => "checkbox", "name" => "Последний размер", "width" => 300, "not_required" => true),
            "discount" => array("type" => "string", "name" => "Скидка", "width" => 100, "not_required" => true),
            "discount_type" => array("type" => "model_list", "name" => "Тип скидки", "model" => "mdiscount_types", "key" => "id", "field" => "title", "not_required" => true, "width" => 100),
            "discount_active" => array("type" => "checkbox", "name" => "Активность скидки", "width" => 100, "not_required" => true),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 70, "not_required" => true),
        );

        $this->admin_builder2->set_links_before(array("/gloomdress/open_products/$parent_id/" => "К  товару"));
        $this->admin_builder2->set_model('mproducts_sizes');
        $this->admin_builder2->set_order_criteria(array('product_id' => $parent_id));
        $this->admin_builder2->set_core_name("products_sizes");
        $this->admin_builder2->set_title_name("Размеры товара");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_products_sizes($id = 0, $parent_id)
    {
        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "width" => 300),
            "code" => array("type" => "string", "name" => "Code", "width" => 300),
            "on_hand" => array("type" => "string", "name" => "Количество", "width" => 300, "not_required" => true),
            "last_size" => array("type" => "checkbox", "name" => "Последний размер", "width" => 300, "not_required" => true),
            "discount" => array("type" => "string", "name" => "Скидка", "width" => 100, "not_required" => true),
            "discount_type" => array("type" => "model_list", "name" => "Тип скидки", "model" => "mdiscount_types", "key" => "id", "field" => "title", "not_required" => true, "width" => 100),
            "discount_active" => array("type" => "checkbox", "name" => "Активность скидки", "width" => 100, "not_required" => true),
            "active" => array("type" => "checkbox", "name" => "Активен", "width" => 300, "not_required" => true),
        );

        //ansotov сообщаем о поступлении товара
        if ($this->input->request('active') == true && $this->input->request('save_changes') == true && $id > 0) {

            $this->load->model('mreminder');

            $query = "`product_id` = {$parent_id} AND `size_id` = {$id} AND `active` = 1";
            $arr = $this->mreminder->get($query, array());

            foreach ($arr as $k => $v) {
                $result = $this->db->query("SELECT p.*, r.`name` AS r_name, r.`email` AS r_email FROM `reminder` r, `products` p WHERE p.`id` = r.`product_id` AND p.`id` = " . $parent_id . " AND r.`size_id` = " . $id . " AND r.email = '" . $v['email'] . "' AND r.`name` = '" . $v['name'] . "'");
                $reminder = $result->result_array();

                $this->mysmarty->assign('reminder', $reminder[0]);
                $template = $this->mysmarty->fetch('new_bloom/ru/reminder_mail.tpl');

                $headline = "Поступление товара " . $reminder[0]['name'];

                //ansotov отправляем письмо
                $this->send_mail($v['name'], $v['email'], $headline, $template);

                //ansotov удаляем данные о напоминании
                $this->mreminder->update(
                    array(
                        'active' => 0
                    ),
                    array(
                        'name' => $v['name'],
                        'email' => $v['email'],
                        'product_id' => $parent_id,
                        'size_id' => $id)
                );
            }
        }

        $this->admin_builder2->set_model('mproducts_sizes');
        $this->admin_builder2->set_order_criteria(array('product_id' => $parent_id));
        $this->admin_builder2->set_header("gloomdress/products_sizes/$parent_id/1");
        $this->admin_builder2->set_core_name("products_sizes");
        $this->admin_builder2->set_title_name("Размеры товара");
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    //  products_sizes
    ///////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////
    // types
    function types($page = 1)
    {

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "url_to" => 'url', "width" => 300),
            "url" => array("type" => "string", "name" => "Ссылка", "not_required" => true, "width" => 300),
            "active" => array("type" => "checkbox", "name" => "Активен", 'width' => 100),
        );


        $this->admin_builder2->set_model('mtypes');
        $this->admin_builder2->set_core_name("types");
        $this->admin_builder2->set_title_name("Типы");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_types($id = 0)
    {

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "url_to" => 'url', "width" => 300),
            "url" => array("type" => "string", "name" => "Ссылка", "not_required" => true, "width" => 300),
            "active" => array("type" => "checkbox", "name" => "Активен", 'width' => 100),
        );


        if ($id > 0) {
            $links_array["/gloomdress/subtypes/"] = 'Подтипы';
            $this->admin_builder2->set_links_after($links_array);
        }

        $this->admin_builder2->set_model('mtypes');
        $this->admin_builder2->set_header("gloomdress/types/1");
        $this->admin_builder2->set_core_name("types");
        $this->admin_builder2->set_title_name('Типы');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    //  types
    ///////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////
    // banners
    function banners($type, $page = 1)
    {

        $structure = array(

            "name" => array("type" => "string", "name" => "Название", "width" => 440),


        );
        if ($type == 'large') {
            $structure['picture'] = array('type' => "jcrop",
                "name" => "Картинка",
                "upload_width" => 1100,
                "upload_height" => 1000,
                "max_width" => 1016,
                "max_height" => 562,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "directory" => "img/banners/",
                "width" => 100
            );
        }
        if ($type == 'mobile-large') {
            $structure['picture'] = array('type' => "jcrop",
                "name" => "Картинка",
                "upload_width" => 1100,
                "upload_height" => 1000,
                "max_width" => 1016,
                "max_height" => 562,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "directory" => "img/banners/",
                "width" => 100
            );
        }
        if ($type == 'middle') {
            $structure['picture'] = array('type' => "jcrop",
                "name" => "Картинка",
                "upload_width" => 1100,
                "upload_height" => 1000,
                "max_width" => 503,
                "max_height" => 343,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "directory" => "img/banners/",
                "width" => 100
            );
        }
        if ($type == 'small') {
            $structure['picture'] = array('type' => "jcrop",
                "name" => "Картинка",
                "upload_width" => 503,
                "upload_height" => 343,
                "max_width" => 316,
                "max_height" => 316,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "image_type_name" => "picture_type",
                "directory" => "img/banners/",
                "width" => 100
            );
        }


        $this->admin_builder2->set_model('mbanners');
        $this->admin_builder2->set_order_criteria(array('type' => $type));
        $this->admin_builder2->set_core_name("banners");
        $this->admin_builder2->set_title_name("Банеры");
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_banners($id = 0, $type)
    {

        $structure = array(
            "name" => array("type" => "string", "name" => "Название", "url_to" => 'url', "width" => 440, "not_required" => true),
            "link" => array("type" => "string", "name" => "Ссылка", "not_required" => true),
            "lang" => array("type" => "list", "name" => "Язык", "values" => array('ru' => 'русский', 'en' => 'английский'))
        );
        if ($type == 'large') {
            $structure["gif"] = array("type" => "any_file", "name" => "GIF 1016x562", "directory" => 'img/banners/', "width" => 100, "not_required" => true);
            $structure['picture'] = array('type' => "jcrop",
                "name" => "Картинка",
                "upload_width" => 1100,
                "upload_height" => 1000,
                "max_width" => 1016,
                "max_height" => 562,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "max_fit" => 1,
                "image_type_name" => "picture_type",
                "directory" => "img/banners/",
                "width" => 100
            );
        }
        if ($type == 'mobile-large') {
            $structure["gif"] = array("type" => "any_file", "name" => "GIF 1016x562", "directory" => 'img/banners/', "width" => 100, "not_required" => true);
            $structure['picture'] = array('type' => "jcrop",
                "name" => "Картинка",
                "upload_width" => 1100,
                "upload_height" => 1000,
                "max_width" => 1016,
                "max_height" => 1016,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "max_fit" => 1,
                "image_type_name" => "picture_type",
                "directory" => "img/banners/",
                "width" => 100
            );
        }
        if ($type == 'middle') {
            $structure["gif"] = array("type" => "any_file", "name" => "GIF 503x343", "directory" => 'img/banners/', "width" => 100, "not_required" => true);
            $structure['picture'] = array('type' => "jcrop",
                "name" => "Картинка",
                "upload_width" => 1100,
                "upload_height" => 1000,
                "max_width" => 503,
                "max_height" => 343,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "not_required" => true,
                "max_fit" => 1,
                "image_type_name" => "picture_type",
                "directory" => "img/banners/",
                "width" => 100
            );
        }
        if ($type == 'wide') {
            $structure["gif"] = array("type" => "any_file", "name" => "GIF 970x100", "directory" => 'img/banners/', "width" => 100, "not_required" => true);
            $structure['picture'] = array('type' => "jcrop",
                "name" => "Картинка",
                "upload_width" => 1100,
                "upload_height" => 1000,
                "max_width" => 970,
                "max_height" => 100,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "max_fit" => 1,
                "image_type_name" => "picture_type",
                "directory" => "img/banners/",
                "width" => 100
            );
        }
        if ($type == 'small') {
            $structure["gif"] = array("type" => "any_file", "name" => "GIF 316x316", "directory" => 'img/banners/', "width" => 100, "not_required" => true);
            $structure['picture'] = array('type' => "jcrop",
                "name" => "Картинка",
                "upload_width" => 1100,
                "upload_height" => 1000,
                "max_width" => 316,
                "max_height" => 316,
                "max_min_width" => 100,
                "max_min_height" => 100,
                "any_image" => true,
                "not_required" => true,
                "max_fit" => 1,
                "image_type_name" => "picture_type",
                "directory" => "img/banners/",
                "width" => 100
            );
        }

        $this->admin_builder2->set_model('mbanners');
        $this->admin_builder2->set_order_criteria(array('type' => $type));
        $this->admin_builder2->set_header("gloomdress/banners/$type/1");
        $this->admin_builder2->set_core_name("banners");
        $this->admin_builder2->set_title_name('Баннеры');
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    //  banners
    ///////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////
    // PHOTOS
    function photos($type, $parent_id, $page = 1)
    {
        if ($type == 'gallery') {
            $structure = array(
                "name" => array("type" => "string", "name" => "Название", "url_to" => 'url', "width" => 440),
                'picture' => array('type' => "jcrop",
                    "name" => "Картинка",
                    "upload_width" => 900,
                    "upload_height" => 900,
                    "max_width" => 800,
                    "max_height" => 0,
                    "max_min_width" => 100,
                    "max_min_height" => 100,
                    "any_image" => true,
                    "image_type_name" => "picture_type",
                    "directory" => "img/gallery_pictures/",
                    "width" => 100
                ),
                'preview' => array('type' => "jcrop",
                    "name" => "Картинка",
                    "upload_width" => 500,
                    "upload_height" => 500,
                    "max_width" => 110,
                    "max_height" => 110,
                    "max_min_width" => 100,
                    "max_min_height" => 100,
                    "any_image" => true,
                    "image_type_name" => "preview_type",
                    "directory" => "img/gallery_previews/",
                    "width" => 100
                )
            );
        }


        $this->admin_builder2->set_model('mphotos');
        $this->admin_builder2->set_order_criteria(array('type' => $type, "parent_id" => $parent_id));
        if ($parent_id > 0 && $type == 'gallery') {
            $this->load->model('malbums');
            $parent = $this->malbums->get(array("id" => $parent_id), array("name"), 1);
            $this->admin_builder2->set_links_before(array("/gloomdress/open_albums/$parent_id/" => "К альбому `" . $parent['name'] . "`"));
            $name = "Фотографии из альбома " . $parent['name'];
        }
        $this->admin_builder2->set_core_name("photos");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->get_output_rows(array(), $structure, 'order_id ASC', $page);
        $this->admin_builder2->init();
    }

    function open_photos($type, $parent_id, $id = 0)
    {
        if ($type == 'gallery') {
            $structure = array(
                "name" => array("type" => "string", "name" => "Название", "url_to" => 'url', "width" => 440, 'not_required' => true),
                'picture' => array('type' => "jcrop",
                    "name" => "Фотография",
                    "upload_width" => 900,
                    "upload_height" => 900,
                    "max_width" => 800,
                    "max_height" => 0,
                    "max_min_width" => 100,
                    "max_min_height" => 100,
                    "any_image" => true,
                    "image_type_name" => "picture_type",
                    "directory" => "img/gallery_pictures/",
                    "width" => 100, 'not_required' => true
                ),
                'preview' => array('type' => "jcrop",
                    "name" => "Обложка",
                    "upload_width" => 500,
                    "upload_height" => 500,
                    "max_width" => 110,
                    "max_height" => 110,
                    "max_min_width" => 100,
                    "max_min_height" => 100,
                    "any_image" => true,
                    "image_type_name" => "preview_type",
                    "directory" => "img/gallery_previews/",
                    "width" => 100, 'not_required' => true
                )
            );
        }

        $this->admin_builder2->set_model('mphotos');
        $this->admin_builder2->set_order_criteria(array('type' => $type, "parent_id" => $parent_id));
        if ($parent_id > 0 && $type == 'gallery') {
            $this->load->model('malbums');
            $parent = $this->malbums->get(array("id" => $parent_id), array("name"), 1);
            $name = "Фотография из альбома " . $parent['name'];
        }
        $this->admin_builder2->set_header("gloomdress/photos/$type/$parent_id/1");
        $this->admin_builder2->set_core_name("photos");
        $this->admin_builder2->set_title_name($name);
        $this->admin_builder2->set_id($id);
        $this->admin_builder2->init($structure);
    }
    //  PHOTOS
    ///////////////////////////////////////////////////////


}

?>
