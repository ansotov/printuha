<?php
include_once APPPATH.'models/mmodel.php';
class mproducts_ordered extends mmodel
{
	public function __construct()	
	{
		parent::__construct();
		$this->table="products";
		$this->id="id";	
		$this->default_order = "primary_order_id";
	}
}