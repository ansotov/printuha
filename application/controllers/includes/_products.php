<?php
$this->load->model('mproducts');
if ($url1 == 'new-products')
{
	$this->load->model("mlast_seen_products");
	$product = $this->mproducts->get(array('url' => $url2, 'active' => 1), array(), 1);
	$query   = "active = 1 AND type='" . $url2 . "' AND EXISTS(SELECT t.id FROM types t WHERE t.url = '" . $url2 . "' AND t.active = 1 )";

	$type = ($product['type'] == 'gotovye-kejsy') ? 'case_page' : 'product_page';

	//////////////////////
	//////////////////////
	// if discount = 7
	$product = $this->discount_calc($product);
	// if discount = 7
	//////////////////////
	$this->products[1] = $product;

	$type_name = "";

	$this->load->model("mtypes");
	$type_name = $this->mtypes->get(array('url' => $product['type']));

	$type_name = $type_name[0]['name'];

	if ($product['id'])
	{
		$other_products = $this->mlast_seen_products->bget("ip = '" . $_SERVER['REMOTE_ADDR'] . "' AND ((SELECT count(id) FROM products WHERE id = last_seen_products.product_id AND active = 1) = 1) AND product_id !=" . $product['id'],
			"(SELECT p.front_picture FROM products p WHERE p.id = product_id) front_picture,
																										  (SELECT p.url FROM products p WHERE p.id = product_id) url,
 																										  (SELECT p.name FROM products p WHERE p.id = product_id) name,
 																										  (SELECT p.en_name FROM products p WHERE p.id = product_id) en_name,
 																										  (SELECT p.price FROM products p WHERE p.id = product_id) price,", 0, 'seen_at DESC', 3, 0);


		$o_products = array();

		foreach ($other_products['result'] as $k => $v)
		{
			$o_products[$k]         = $v;
			$o_products[$k]['name'] = $this->getLang($v, 'name');
		}
	}
	else
	{
		$o_products = false;
	}

	$this->mysmarty->assign("other_products", $o_products);
	$this->load->model('mphotos');
	$thumbnails = $this->mphotos->get(array('type' => 'products', 'parent_id' => $product['id']), array(), 0, 'order_id ASC');
	$this->load->model('mproducts_sizes');

	$sizes_data = $this->mproducts_sizes->get(array('product_id' => $product['id']), array(), 0,/*"CAST(code as DECIMAL) ASC"*/
		"order_id ASC");

	$sizes = array();

	foreach ($sizes_data as $k => $v)
	{
		$sizes[$k] = $v;

		if ($v['discount_active'] == 1)
		{
			$v['price']                 = ($v['price']) ? $v['price'] : $product['price'];
			$sizes[$k]['price']         = $this->getDiscountSum($v);
			$sizes[$k]['item_discount'] = $v['discount'];
		}
	}

	$selected_image = array_shift(array_values($thumbnails));
	$this->load->model('mcollections');
	$data        = $this->mcollections->get(array(), array(), 0, 'order_id DESC');
	$collections = array();
	foreach ($data as $k => $v)
	{
		$collections[$v['url']] = $v;
	}

	$this->load->model('mmaterials');

	if ($product['c_type'])
	{
		if (in_array($product['c_type'], array(1, 2)))
			$case_types = 'type_case_id IN(1, 2)';
		else
			$case_types = 'type_case_id = ' . $product['c_type'];

		$data = $this->mmaterials->get($case_types, array(), 0, 'order_id DESC');
	} else {
		$data = false;
	}
	$materials = array();
	foreach ($data as $k => $v)
	{
		$materials[$v['url']]         = $v;
		$materials[$v['url']]['name'] = $this->getLang($v, 'name');
	}

	$product['group_name']    = $this->getLang($product, 'group_name');
	$product['group_variant'] = $this->getLang($product, 'group_variant');

	$product['colors'] = (trim($product['color'])) ? array_map("trim", explode(',', trim($product['color']))) : false;

	//colors
	if ($product['group_name'] != '' && $product['color'] != '')
	{
		if ($group_products = $this->mproducts->get(array('group_name' => $product['group_name']), array(), 0, 'order_id DESC'))
		{
			$this->mysmarty->assign("group_products", $group_products);
		}
	}
	if ($product['group_name'] != '' && $product['group_variant'] != '')
	{
		if ($group_products = $this->mproducts->get(array('group_name' => $product['group_name']), array(), 0, 'order_id DESC'))
		{
			$this->mysmarty->assign("group_products", $group_products);
		}
	}

	$folder_info                    = $this->mfolders->get(array('url' => 'catalog'), array(), 1);
	$folder_info['top_banner_text'] = $this->getLang($folder_info, 'top_banner_text');

	//ansotov последний размер
	$last_size = 0;

	foreach ($sizes as $key => $value)
	{
		if ($value['last_size'] == 1)
			$last_size = 1;
	}

	//ansotov перевод
	$product['name']    = $this->getLang($product, 'name');
	$product['content'] = $this->getLang($product, 'content');

	$this->mysmarty->assign('materials', $materials);
	$this->mysmarty->assign('pcollections', $collections);
	$this->mysmarty->assign('sizes', $sizes);
	$this->mysmarty->assign('last_size', $last_size);
	$this->mysmarty->assign('type_name', $type_name);
	$this->mysmarty->assign('selected_image', $selected_image);
	$this->mysmarty->assign('thumbnails', $thumbnails);
	$this->mysmarty->assign('folder_info', $folder_info);
	$this->mysmarty->assign('product', $product);

	if ($product['id'])
	{
		$is_exists = $this->mlast_seen_products->get("DATE(seen_at)  = CURDATE() AND product_id = " . $product['id'], array(), 1);
		if (!$is_exists)
			$this->mlast_seen_products->insert(array("product_id" => $product['id'], 'ip' => $_SERVER['REMOTE_ADDR'], "seen_at" => date("Y-m-d H:i:s")));
		$this->finish($type);
	}

	return;
}

if ($url1 == 'product' && ($product = $this->mproducts->get(array('url' => $url2, 'active' => 1), array(), 1)))
{
	$type = 'product';
	//////////////////////
	//////////////////////
	// if discount = 7
	$product = $this->discount_calc($product);
	// if discount = 7
	//////////////////////

	$this->products[1] = $product;
	$this->load->model('mphotos');
	$thumbnails = $this->mphotos->get(array('type' => 'products', 'parent_id' => $product['id']), array(), 0, 'order_id ASC');
	$this->load->model('mproducts_sizes');
	$sizes          = $this->mproducts_sizes->get(array('product_id' => $product['id']), array(), 0, "name ASC");
	$selected_image = array_shift(array_values($thumbnails));

	if ($product['type'] == 'gotovye-kejsy' || $product['type'] == 'kejs-na-zakaz')
	{
		$this->load->model('mmaterials');
		$data      = $this->mmaterials->get(array(), array(), 0, 'order_id DESC');
		$materials = array();
		foreach ($data as $k => $v)
		{
			$materials[$v['url']] = $v;
		}
		$this->mysmarty->assign('materials', $materials);
	}

	//ansotov перевод
	$product['name']    = $this->getLang($product, 'name');
	$product['content'] = $this->getLang($product, 'content');

	$this->mysmarty->assign('sizes', $sizes);

	$this->mysmarty->assign('selected_image', $selected_image);

	$this->mysmarty->assign('thumbnails', $thumbnails);
	$this->mysmarty->assign('product', $product);

	//$folder_info = array( 'title'=>$product['name'],'description'=>$product['description']);
	//$data = $this->mproducts->bget("season = '".$product['season']."' AND collection = '".$product['collection']."' AND type = '".$product['type']."' AND id != ".$product['id']." AND active = 1",array(),0,0,4);
	//$this->mysmarty->assign('same_category_products',$data['result']);

}
?>
