<section class="main clearfix">

	{include file="new_bloom/ru/info_block.tpl"}

  {if $banners["large"]}
  <div class="big_banner">
    <div class="slider_pos">
      <ul>
        {foreach $banners["large"] as $k=>$v}
          <li><a href="{$v.link}"><img alt="{$v.name}" src="/img/banners/{$v.picture}.{$v.picture_type}"></a></li>
        {/foreach}
      </ul>
    </div>
  </div>
  {/if}{if $banners["middle"]}<ul class="banners">{foreach $banners["middle"] as $k=>$v}<li class="two">
      <a href="{$v.link}"><img alt="{$v.name}" src="/img/banners/{if $v.gif}{$v.gif}{else}{$v.picture}.{$v.picture_type}{/if}"></a>
    </li>{/foreach}
  </ul>{/if}
  {if $banners["wide"]}
  <div class="thankyou_banner">{foreach $banners["wide"] as $k=>$v}{if $v.link}<a href="{$v.link}">{/if}
    <img alt="{$v.name}" src="/img/banners/{$v.picture}.{$v.picture_type}">{if $v.link}</a>{/if}
    {/foreach}
  </div>
  {/if}
  {if $banners["small"]}<ul class="banners bottom-main-banners">{foreach $banners["small"] as $k=>$v}<li class="three"><a href="{$v.link}"><img alt="{$v.name}" src="/img/banners/{if $v.gif}{$v.gif}{else}{$v.picture}.{$v.picture_type}{/if}"></a></li>{/foreach}</ul>{/if}

    <!-- ansotov баннеры для мобильного -->
    {if $banners['mobile-large']}
        <div class="large-mobile-banners">
            <ul>
                {foreach $banners['mobile-large'] as $key => $value}
                    <li>
                        <a href="{$value.link}"><img alt="{$value.name}" src="/img/banners/{$value.picture}.{$value.picture_type}"></a>
                    </li>
                {/foreach}
            </ul>
        </div>
    {/if}
    <!-- ansotov конец баннеров для мобильного -->

  <!--
  <div class="social">
    <div class="absolute">
      <div class="title">мы в социальных сетях</div>
      <a >
        <span class="bit-icon bit-icon-fb-white"></span>
      </a><a >
        <span class="bit-icon bit-icon-vk-white"></span>
      </a><a >
        <span class="bit-icon bit-icon-instagram-white"></span>
      </a><a >
        <span class="bit-icon bit-icon-pinterest-white"></span>
      </a>
    </div>
  </div>-->
  <div class="new_social">
    <div class="line"></div>
    <ul>
        <li><a href="https://www.instagram.com/printuha_ru/"  target="_blank"><i class="fa fa-instagram"></i></a></li>
    </ul>
  </div>
  {if $instagram}
  <ul class="instagram">{foreach $instagram as $k=>$v}<li><a href="{$v.link}"><img alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}" src="{$v.img}" /></a></li>{/foreach}</ul>
  {/if}

</section>

<script>
$(function(){
  var sudoSlider_true = $(".slider_pos").sudoSlider({
        prevNext: true,
        prevHtml:'<a class="prevBtn" href="#"></a>',
        nextHtml:'<a class="nextBtn" href="#"></a>',
        numeric: false,
        auto:true,
        pause: '4000',
        touch:true,
        autoHeight:false,
        autoWidth:false,
        responsive:true,
        continuous:false,
        effect: "slide"
    });

})
</script>
