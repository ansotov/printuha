<div class="holder">

  <a class="button" href="../">{translateConst const = BACK}</a>
  <center style="max-width:900px;margin:0 auto;display:block;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
          <tr>
              <td align="center" valign="top" id="bodyCell">
                  <!-- BEGIN TEMPLATE // -->
      <!--[if gte mso 9]>
      <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
      <tr>
      <td align="center" valign="top" width="600" style="width:600px;">
      <![endif]-->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
  <tbody class="mcnDividerBlockOuter">
  <tr>
      <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
          <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #666666;">
              <tbody><tr>
                  <td>
                      <span></span>
                  </td>
              </tr>
          </tbody></table>
  <!--
          <td class="mcnDividerBlockInner" style="padding: 18px;">
          <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
  -->
      </td>
  </tr>
  </tbody>
  </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
  <tbody class="mcnTextBlockOuter">
  <tr>
      <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
          <!--[if mso]>
  <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
  <tr>
  <![endif]-->

  <!--[if mso]>
  <td valign="top" width="600" style="width:600px;">
  <![endif]-->
          <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
              <tbody><tr>

                  <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 200%;">

                      <strong>{translateConst const = ORDER_NUMBER}:</strong> {$order.number}<br>
  <strong>{translateConst const = ORDER_DATE}:</strong> {$order.date}<br>
  <strong>{translateConst const = CUSTOMER_NAME}:</strong> {$order.first_name} {$order.last_name}<br>
  <strong>{translateConst const = PHONE}:</strong> {$order.phone}<br>
  {if $order.email}<strong>E-MAIL:</strong> {$order.email}<br>{/if}
  <strong>{translateConst const = DELIVERY_TYPE}:</strong> {$order.delivery_type}{if $order.city} {$order.city}{/if}<br>
  {if $order.zipcode}<strong>{translateConst const = INDEX}:</strong>{$order.zipcode}<br>{/if}
  <strong>{translateConst const = DELIVERY_ADDRESSES}:</strong>{$order.address}<br>
  {if $order.comments}<strong>{translateConst const = COMMENTS}:</strong><br>{$order.comments|nl2br}<br>{/if}
  <strong>{translateConst const = PRODUCT_DESCRIPTION}:</strong>&nbsp;&nbsp; &nbsp;
                  </td>
              </tr>
          </tbody></table>
  <!--[if mso]>
  </td>
  <![endif]-->

  <!--[if mso]>
  </tr>
  </table>
  <![endif]-->
      </td>
  </tr>
  </tbody>
  </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
  <tbody class="mcnDividerBlockOuter">
  <tr>
      <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
          <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #666666;">
              <tbody><tr>
                  <td>
                      <span></span>
                  </td>
              </tr>
          </tbody></table>
  <!--
          <td class="mcnDividerBlockInner" style="padding: 18px;">
          <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
  -->
      </td>
  </tr>
  </tbody>
</table>{foreach $products as $k=>$v}<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
  <tbody class="mcnCaptionBlockOuter">
  <tr>
      <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">

  <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
  <tbody><tr>
  <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
      <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
          <tbody><tr>
              <td class="mcnCaptionRightImageContent" valign="top">



                  <img alt="" src="http://www.girlsinbloom.ru/img/front_pictures/{$v.info.front_picture}.{$v.info.front_picture_type}" width="132" style="max-width:352px;" class="mcnImage">



              </td>
          </tr>
      </tbody></table>
      <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="396">
          <tbody><tr>
              <td valign="top" class="mcnTextContent">
                  <div style="float:left">
  <strong class="null">{$v.info.name}</strong><br>
  {translateConst const = SIZE}: {if $v.size_info.name!=''}{$v.size_info.name}{else}{$v.size}{/if}<br>
  {if $v.material}{translateConst const = MATERIAL}: {$v.material.name}<br>{/if}
  {if $v.color}{translateConst const = COLOR}: {$v.info.color}<br>{/if}
  {translateConst const = QTY}: {$v.qty}</div>

  <div style="float:right">{($v.price*$v.qty)|number_format:0:".":" "} {translateConst const = RUB}</div>

              </td>
          </tr>
      </tbody></table>
  </td>
  </tr>
  </tbody></table>




      </td>
  </tr>
  </tbody>
  </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
  <tbody class="mcnDividerBlockOuter">
  <tr>
      <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
          <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #666666;">
              <tbody><tr>
                  <td>
                      <span></span>
                  </td>
              </tr>
          </tbody></table>
  <!--
          <td class="mcnDividerBlockInner" style="padding: 18px;">
          <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
  -->
      </td>
  </tr>
  </tbody>
  </table>{/foreach}<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;text-align:right;">
  <tbody class="mcnTextBlockOuter">
  <tr>
      <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
          <!--[if mso]>
  <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
  <tr>
  <![endif]-->

  <!--[if mso]>
  <td valign="top" width="390" style="width:390px;">
  <![endif]-->
          <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:390px;" width="100%" class="mcnTextContentContainer">
              <tbody><tr>

                  <td valign="top" class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                      <div style="text-align: right;">{translateConst const = IN_ALL}{if $order.discount_sum>0}
                        <br>{translateConst const = DISCOUNT}{/if}{if $order.delivery_sum>0}<br>
							  {translateConst const = DELIVERY}{/if}</div>

  <h4 class="null" style="text-align: right;">{translateConst const = TOTAL}</h4>

                  </td>
              </tr>
          </tbody></table>
  <!--[if mso]>
  </td>
  <![endif]-->

  <!--[if mso]>
  <td valign="top" width="210" style="width:210px;">
  <![endif]-->
          <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:210px;" width="100%" class="mcnTextContentContainer">
              <tbody><tr>

                  <td valign="top" class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                      <div style="text-align: right;">{($order.sum-$order.delivery_sum+$order.discount_sum)|number_format:0:".":" "} {translateConst const = RUB}{if $order.discount_sum>0}<br>-{$order.discount_sum|number_format:0:".":" "} {translateConst const = RUB}{/if}{if $order.delivery_sum>0}<br>{$order.delivery_sum|number_format:0:".":" "} {translateConst const = RUB}{/if}</div>

  <h4 class="null" style="text-align: right;">{$order.sum|number_format:0:".":" "} {translateConst const = RUB}</h4>

                  </td>
              </tr>
          </tbody></table>
  <!--[if mso]>
  </td>
  <![endif]-->

  <!--[if mso]>
  </tr>
  </table>
  <![endif]-->
      </td>
  </tr>
  </tbody>
  </table></td>
                      </tr>
                  </table>
      <!--[if gte mso 9]>
      </td>
      </tr>
      </table>
      <![endif]-->
                  <!-- // END TEMPLATE -->
              </td>
          </tr>
      </table>
  </center>
  </body>









</div>
