<p>{translateConst const = THANK_YOU_FOR_REGISTRATION}!</p>

<p>{translateConst const = YOUR_LOGIN}: {$data.email}</p>
<p>{translateConst const = YOUR_PASSWORD}: {$data.password}</p>

<p>{translateConst const = HAVE_A_GOOD_DAY}!</p>