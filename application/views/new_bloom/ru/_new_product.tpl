{if $added_product}
<div class="new_product_inner">
  <h2>{translateConst const = ADDED_PRODUCT}</h2>
  <div class="product">
    <img alt="{$added_product.info.name|@escape}" src="/img/front_pictures/{$added_product.info.front_picture}.{$added_product.info.front_picture_type}">
  </div>
  <div class="product_info">
    <h3>{$added_product.info.name}</h3>
    <div class="price">{$added_product.price|number_format:0:".":" "} <i class="fa fa-ruble"></i></div>
  </div>
  <div class="total_sum clearfix">
    <strong>{translateConst const = ORDER_AMOUNT}</strong>
    <span>{$total_sum|number_format:0:".":" "} <i class="fa fa-ruble"></i></span>
  </div>
  <a class="button continue" href="./">{translateConst const = YET_BUYS}</a>
  <div class="or">{translateConst const = OR}</div>
  <a class="button" href="/checkout/">{translateConst const = GO_TO_CARD}</a>
</div>
{/if}