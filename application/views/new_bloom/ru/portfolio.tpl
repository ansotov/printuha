<!-- ansotov общий листинг -->
{if $portfolio}
    <div class="wrapper clearfix">
        <div class="top-divider"></div>
            <section class="girlsinbloom-wrapper">
                <ul id="info-wall">
                    {foreach $portfolio as $k => $v}
                        <li>
                            <div class="wrapper">
                                <a class="portfolio" href="/portfolio/{$v.id}/">
                                    <img alt="{$v.title|@escape}" src="/img/portfolio/{$v.image}.{$v.image_type}">
                                </a>
                                <div class="description">{$v.description|truncate:100:'...'}</div>
                            </div>
                        </li>
                    {/foreach}
                </ul>
            </section>
        <div class="top-divider"></div>
    </div>
{elseif $item}
<!-- ansotov детальный просмотр -->
    <div class="wrapper">
        <img alt="{$item.title|@escape}" src="/img/portfolio/{$item.image}.{$item.image_type}">
        <div class="description">{$item.description}</div>
    </div>
{/if}