<div id="promocode">

    {if $promocode_isset == false}
        <div class="divider"></div>
        <div class="promocodes">
            <form method="post" action="/promocode">
                <input type="text" name="promocode" placeholder="{translateConst const = ENTER_PROMOCODE}">
                <input type="submit" class="button" value="{translateConst const = APPLY}">
            </form>
        </div>
        <div class="divider"></div>
    {/if}

    {if $promocode && $promocode_isset}<div style="margin: 10px 0;text-align: center">({translateConst const = PROMOCODE_APPLIED})</div>{/if}
</div>