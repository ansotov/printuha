<article class="blog_post clearfix">
  <h1>{$blog_post.name}</h1>
  <div class="left"><img src="/img/blog_images/{$blog_post.picture}"></div>
  <div class="right">{$blog_post.content}</div>
</article>
<div class="blog_line clearfix"></div>
<div class="blog_bottom clearfix">
  <img src="/img/blog_images/{$blog_post.bottom}">
</div>
<div class="blog_line clearfix"></div>
<section class="blog">
  <h2>{translateConst const = ANOTHER_MATERIALS}</h2>
  <ul>
  {foreach $blog as $k=>$v}<li {if $v@iteration is div by 3}class="third"{/if}><a href="./{$v.url}/"><img src="/img/blog/{$v.preview}.{$v.preview_type}" title="{$v.name}"><h2>{$v.name}</h2><p>{$v.description}</p><div class="read_more">{translateConst const = READ_MORE}</div></a>
  </li>{/foreach}
  </ul>
</section>
