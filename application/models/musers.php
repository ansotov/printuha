<?php
include_once APPPATH.'models/mmodel.php';
class musers extends mmodel
{
	public function __construct()	
	{
		parent::__construct();
		$this->table="users";
		$this->id="id";	
	}
	
	protected function send_mail($user_name,$user_mail,$subject,$body,$from_mail='',$from_name=''){
		
		$this->load->library('mailer');
		
		$this->mailer->Subject =$this->mailer->mime_header_encode($subject);
		
		@$body = iconv('cp1251', 'KOI8-R', $body);
		$this->mailer->Body = $body;
		$this->mailer->AddAddress($user_mail, $this->mailer->mime_header_encode($user_name));
		
		$this->mailer->IsHTML(true);
		if($from_mail!='')
			$this->mailer->From = $from_mail;
		if($from_name!='')
			$this->mailer->FromName = $this->mailer->mime_header_encode($from_name);
		$this->mailer->Send();
		$this->mailer->ClearAddresses();
		$this->mailer->ClearAttachments();
		
	}

	function update($data,$where_data){
		if(isset($data['active']) && $data['active']==1){
			$this->send_mail($data['name'],$data['email'],'��� ������� ����������� �� ����� http://www.blyzki.ru',"<p>��� ������� ����������� </p>");
		}
		parent::update($data,$where_data);
	}
}