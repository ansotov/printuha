<?php

	class SendForm {

		var $from;
		var $rcpt;
		var $sender;
		var $topic;
		var $message;

		function SendForm () { // From - Recipient - Topic - HTMLTopic

			$args = func_num_args();
			if ($args) $this->SetFrom(func_get_arg(0));
			if ($args > 1) $this->SetRcpt(func_get_arg(1));
			if ($args > 2) $this->SetTopic(func_get_arg(2));
			if ($args > 3) $this->MakeHead(func_get_arg(3));
			$this->message = '';
			$this->colspan = 0;			

		}

		function SetSender ($id) {

			if (!$this->sender) $this->sender = $id;

		}

		function WriteHeader ($id) {

			$this->MakeHead();
			if ($this->colspan) $this->CloseTable();
			$this->message .= "<p align=center class=body>" . $id . "</p>" . "\n";

		}

		function MakeHead ($id = '') {			
			
			if (!$id) $id = $this->topic;
			if ($this->message) return;
			$this->message = '<HTML><HEAD><TITLE> ' . $id . ' </TITLE>' . "\n";
			$this->message .= '<STYLE>.body {font: 8pt tahoma; color: #3E4133;}</STYLE></HEAD><BODY>' . "\n";

		}

		function SetFrom ($id) {

			$this->from = $id;

		}

		function SetRcpt ($id) {

			$this->rcpt = $id;

		}

		function SetTopic ($id) {

			$this->topic = $id;

		}

		function HeadEncode ($id) {

			return '=?KOI8-R?b?'.base64_encode(convert_cyr_string($id,"w","k")).'?=';

		}

		function AddRcpt ($id) {

			$this->rcpt ? $this->rcpt .= ';' . $id : $this->rcpt = $id;

		}

		function WriteText ($text) {

			$this->MakeHead();
			if ($this->colspan) $this->CloseTable();
			$this->message .= '<font class=body>' . $text . '<br/></font>' . "\n";

		}

		function ParseTpl ($tpl,$data) {
			
			
			$fh = $tpl;
			if (!file_exists($fh)) return false;
			
			$body = file_get_contents($fh);
			foreach ($data as $k => $v) {
				$body = str_replace('<##' . strtoupper($k) . '##>',$v,$body);
			} 
			$this->message .= $text;
			$this->message .= $body;

		}
		
		function WriteRow () {

			$this->MakeHead();
			$args = func_num_args();
			if (!$this->colspan) $this->OpenTable ($args);

			$counter = 0;
			$col_hash = array ();
			for ($i = 0; $i < $this->colspan; $i++) {
				if ($i == $args - 1) {
					$col_hash[$counter] = $this->colspan - $counter;
					break;
				} else {
					if (!func_get_arg($i)) {
						$col_hash[$counter - 1] += 1;
					} else {						
						$col_hash[$counter] = 1;
						$counter++;
					}
				}
			} $counter = 0;
			$this->message .= '<tr>' . "\n";
			count ($col_hash) > 1 ? $align = 'left' : $align = 'center';
			foreach (func_get_args() as $item) {
				if ($item) {
					$this->message .= '<td valign=top align=' . $align . ' colspan=' . $col_hash[$counter] . ' class=body>' . $item . '</td>' . "\n";
					$counter++;
				}
			} $this->message .= '</tr>' . "\n";

		}

		function OpenTable ($colcount) {

			if ($this->colspan) $this->CloseTable();
			$this->message .= '<p><table width=70% border=1 align=center cellspacing=0 cellpadding=3 bordercolor=#333333>' . "\n";
			$this->colspan = $colcount;

		}

		function CloseTable () {

			$this->message .= '</table></p>' ."\n" . '<br/>';
			$this->colspan = 0;

		}

		function RFCHeaders () {

			return "From: " . $this->from . "\n" . "Content-Type: text/html; charset=Windows-1251" . "\n" . "Content-Transfer-Encoding: 8bit" . "\n";

		}

		function CloseMessage() {

			if ($this->colspan) $this->CloseTable();
			$this->message .= '</body></html>' . "\n";

		}

		function Send ($debug = false) {

			$this->CloseMessage();
#			$this->SetSender($this->from);
			$status = mail ($this->rcpt,$this->HeadEncode($this->topic),$this->message,$this->RFCHeaders());

			if ($debug) {
				if (!$status) die ("Unable to send message...");
			}			

		}

	}

?>
