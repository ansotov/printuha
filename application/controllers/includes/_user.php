<?php
if($url1=='signin'){
    //ansotov авторизация через соцсети

    if ($this->input->request('social_id')) {
        $data = $this->musers->get(array('social_id' => $this->input->request('social_id')),array(),1);


        $data['email'] = $this->input->request('social_id');

        $this->set_session($data);
        redirect('/cabinet/');
    } else {

        $fail = false;
        $ajax = $this->input->post('ajax');
        $type = 'signup';
        $send = $this->input->post('send');
        $email = $this->input->post('email');
        $post_form = $this->input->post("post_form");
        $password = $this->input->post('password');
        if ($post_form == 'signin') {
            $this->mysmarty->assign("post_form", "signin");
            if ($email != '' && $password !== '') {
                if (($data = $this->musers->get(array('email' => $email), array(), 1)) && $this->user_info == array()) {
                    $salt = $data['salt'];
                    $encoded_pass = $this->encodepass($password, $salt);

                    if ($encoded_pass == $data['password']) {
                        $this->set_session($data);
                    } else {
                        $fail = true;
                    }

                } else {
                    $fail = true;
                }

                if ($ajax == 1) {
                    $errors = array();
                    if ($this->user_info != array()) {
                        $errors['password'] = "Вы уже авторизованны";
                    }
                    if ($fail) {
                        $errors["email"] = "Данное сочетание email и пароля не найдено.";
                    } else {
                        $this->mysmarty->assign("success", 1);
                    }
                    $this->mysmarty->assign("postdata", array('email' => $email));
                    $this->mysmarty->assign('errors', $errors);
                    $template = $this->mysmarty->fetch("new_bloom/ru/_signup_form.tpl");
                    echo json_encode(array("template" => $template));
                    exit;
                } else {
                    if ($this->user_info != array()) {
                        $this->session->set_flashdata('error', 'Вы уже авторизованы');
                        redirect('/');
                    }
                    if ($fail) {
                        $this->session->set_flashdata('email', $email);
                        $this->session->set_flashdata('error', 'Данное сочетание email и пароля не найдено.');
                        redirect('/login/');
                    } else {
                        $this->session->set_flashdata('notice', 'Успешно произведен вход');
                        $this->musers->update(array('logged_in' => 1), array('id' => $data['id']));
                        $redirect_to = $this->input->post('redirect_to');
                        if ($redirect_to != '')
                            redirect($redirect_to);
                        else
                            redirect('/');
                    }
                }
            }
        }
    }
}

if($url1=='signup'){
    if ($this->input->request('social-auth-data')) {
        $social_data = json_decode($this->input->request('social-auth-data'));

        $salt = $this->common_f->genRandomString(20);

        $data['salt'] = $salt;
        $data['created_at'] = date("Y-m-d H:i:s");

        //ansotov если это facebook
        if ($this->input->request('type') == 'facebook') {
            $data['social_id'] = $social_data->id;
            $data['email'] = ($social_data->email) ? $social_data->email : '';
            $data['gender'] = ($social_data->gender) ? $social_data->gender : '';
            $data['birthdate'] = ($social_data->birthday) ? date('Y-m-d', strtotime($social_data->birthday)) : '';
            $social_data->uid = $social_data->id;
        } elseif ($this->input->request('type') == 'vk') {
            $data['social_id'] = $social_data->uid;

            if ($social_data->sex == 2 )
                $data['gender'] = 'male';
            elseif ($social_data->sex == 1)
                $data['gender'] = 'female';
            else
                $data['gender'] = '';

            $data['birthdate'] = ($social_data->bdate) ? date('Y-m-d', strtotime($social_data->bdate)) : '';
        }

        $data['first_name'] = ($social_data->first_name) ? $social_data->first_name : '';
        $data['last_name'] = ($social_data->last_name) ? $social_data->last_name : '';

        $user_id = $this->musers->insert($data);
        $data['id'] = $user_id;
        $data['email'] = ($social_data->email) ? $social_data->email : $social_data->uid;
        $this->set_session($data);

        redirect('/cabinet/');

    } else {
        $type = "signup";
        $this->lang->load('form_validation', 'russian');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|max_length[100]|required|valid_email|callback_email_check');
        $this->form_validation->set_rules('password', 'Пароль', 'trim|required|min_length[6]|max_length[30]|latin_numeric');
        $post_form = $this->input->post("post_form");
        $this->mysmarty->assign("post_form", "signup");
        $ajax = $this->input->post('ajax');
        if ($post_form == 'signup') {
            if ($this->form_validation->run() == FALSE) {
                $data = $this->form_validation->get_postdata();
                $this->mysmarty->assign('errors', $this->form_validation->get_errors());
            } else {
                $data = $this->form_validation->get_postdata();
                //unset($data['passconf']);
                $password = $data['password'];
                $salt = $this->common_f->genRandomString(20);

                $data['password'] = $this->encodepass($data['password'], $salt);
                $data['salt'] = $salt;
                $data['created_at'] = date("Y-m-d H:i:s");

                $user_id = $this->musers->insert($data);
                $data['id'] = $user_id;
                $this->set_session($data);
                $this->user_info = $data;

                $headline = "Регистрация на сайте " . $this->config->item('site_name');

                $data['password'] = $password;
                $this->mysmarty->assign('data', $data);
                $template = $this->mysmarty->fetch('new_bloom/ru/registration_email.tpl');
                $this->send_mail($data['email'], $data['email'], $headline, $template);

                $this->mysmarty->assign("success", 1);
                $this->session->set_flashdata('notice', 'Регистрация произведена успешно');
                if ($ajax != 1) {
                    redirect('/');
                }
            }
            $this->mysmarty->assign('postdata', $this->form_validation->get_postdata());
            if ($ajax == 1) {
                $template = $this->mysmarty->fetch("new_bloom/ru/_signup_form.tpl");
                echo json_encode(array("template" => $template));
                exit;
            }
        }
    }
}

if($url1=='resetpassword'){
	$this->load->model('musers');
  $post_form = $this->input->post("post_form");
  $ajax = $this->input->post('ajax');
	$this->mysmarty->assign("post_form","signin");
	if($url2==''){
		$type='resetpassword';
		$email = $this->input->post('email');

		if($email !='' && $info = $this->musers->get(array("email"=>$email),array(),1)){
			$string=$this->common_f->genRandomString(20);

			$this->musers->update(array("reset_password_token"=>$string),array("email"=>$email));
			$headline= $this->config->item('site_name')." Инструкция по восстановлению пароля";
			$this->mysmarty->assign('user_info',$info);
			$this->mysmarty->assign('reset_token',$string);
			$template = $this->mysmarty->fetch('new_bloom/ru/reset_password_email.tpl');
			//$this->send_mail( $info['first_name'].' '.$info['last_name'],'orders@norsoyan.com',$headline,$template);
			$this->send_mail( $info['email'],$email,$headline,$template);
			$this->session->set_flashdata('notice', "На указанный адрес выслано письмо с инструкциями.");
			if($ajax==1){
				$template = $this->mysmarty->fetch("new_bloom/ru/_signup_form.tpl");
				echo json_encode(array("template"=>$template));
				exit;
			}else{
				redirect('/login');
			}
		}elseif($email!='')
			$this->mysmarty->assign('error','Извините,такой email не найден');


	}elseif($url2!='' && ($info = $this->musers->get(array("reset_password_token"=>$url2),array(),1))){

		$type='resetpassword_renew';
		$this->lang->load('form_validation', 'russian');
		$this->load->library('form_validation');
		$send = intval($this->input->post('send'));
		$this->form_validation->set_rules('password', 'Новый Пароль', 'trim|required|min_length[6]|max_length[30]|latin_numeric|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Подтверждение Пароля', 'trim|required');
		if($send == 1){
			if($this->form_validation->run() == TRUE){

				$salt = $this->common_f->genRandomString(20);

				$data=$this->form_validation->get_postdata();
				unset($data['passconf']);
				$source_pass = $data['password'];
				$data['password'] = $this->encodepass($source_pass,$salt);
				$data['salt'] = $salt;
				$this->musers->update(array("password"=>$data['password'],'salt'=>$data['salt'],"reset_password_token"=>""),array("reset_password_token"=>$url2));
				$this->set_session($info);
				$this->session->set_flashdata('notice', "Пароль был изменен");
				redirect("/");
			}else{
				$this->mysmarty->assign('errors',$this->form_validation->get_errors());
			}
		}
	}

}
?>
