<div class="whatsapp-data">
	<a href="https://api.whatsapp.com/send?phone=79100137413" target="_blank">
		<img width="100%" style="margin: 0 0 -6px 0" src="/img/whatsapp.jpg">
	</a>
</div>
<div class="footer big-footer absolute">
	<div class="container">
		<div class="main-wrapper">
			<ul class="menu menu-large">
				<li>
					<div class="separator"></div>
					<div class="title" style="padding-top: 40px;"><img src="/img/bottom_menu_title.png"></div>
					<ul>
						{foreach $bottom2 as $k=>$v}
							<li><a href="{$v.url}">{$v.name}</a></li>
						{/foreach}
					</ul>
				</li>
				<li>
					<div class="separator"></div>

					<div class="pay">
						<span class="wrap">
						   <a class="bit-icon bit-icon-visa" href="/payment"></a>
						</span>
						<span class="wrap">
						   <a class="bit-icon bit-icon-mastercard" href="/payment"></a>
						</span>
						<span class="wrap">
						   <a class="bit-icon bit-icon-paypal" href="/payment"></a>
						</span>
						<!--<span class="wrap">
							<a class="bit-icon bit-icon-yad" href="/payment"></a>
						</span>
						<span class="wrap">
						  <a class="bit-icon bit-icon-webmoney" href="/payment"></a>
						</span>-->
					</div>
				</li>

			</ul>
			<div class="copy">
				{translateConst const = SITE_TOWN}
			</div>
		</div>
	</div>
</div>