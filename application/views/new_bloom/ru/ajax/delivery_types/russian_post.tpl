<li id="russian-post_block">
	<label onclick="check_delivery(4);">
		<!--<div class="picture">
				<img alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}" src="/img/girlsinbloom/russian-post.jpg">
			</div>-->
		<div class="delivery_block field">
			<input type="radio" autocomplete="off" name="delivery_type" value="4"
				   id="russian-post"{if $delivery_type == 4 or $foreign_lands == true} checked{/if}>
			<span class="info-label">
                    <strong>{translateConst const = POST_OF_RUSSIA}<span> - {if $russian_post.price > 0}
							<span>{$russian_post.price}</span> {translateConst const = RUB}</span>{else}{translateConst const = FREE}{/if} <span style="color:red;text-transform: initial;font-size:15px">({translateConst const = SELF_PAYMENT_ONLY_ONLINE_PAYMENT})</span></strong>
                    <i><span>{translateConst const = DELIVERY_TIME}
							- до <span>{$russian_post.days} {if $russian_post.days == 1}{translateConst const = DAY}{else}{translateConst const = DAYS}{/if}</span></i>
                </span>
		</div>
	</label>
</li>
