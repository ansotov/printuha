<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$folder_name}{if $title_name}{$title_name} - {/if}{$site_name} - Admin panel</title>
<link href="/css/rainadmin.css?333" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/css/jquery.growl.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="/css/fileup.css" type="text/css" media="screen"/>
<script src="/js/jquery-1.7.1.min.js"></script>
<script src="/js/jquery-ui-1.8.9.custom.min.js"></script>
<script src="/js/jquery-ui-timepicker-addon.js"></script>
<script src="/js/evol.colorpicker.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/jquery.synctranslit.min.js"></script>
<!-- FANCYBOX -->
<script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<script src="/js/ajaxupload.js"></script>
<link href="/css/custom-theme/jquery-ui-1.8.9.custom.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
<link href="/css/evol.colorpicker.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="/js/jquery.growl.js"></script>
<script type="text/javascript" src="/js/fileup.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tinymce/jscripts/tiny_mce/tiny_mce.js',
			// General options
			theme : "advanced",
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,blockquote ,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			relative_urls : false,

			content_css : "/css/admin.style.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			},

        	plugins : "paste,media",
			paste_use_dialog : true,
			paste_convert_middot_lists : false,
			paste_unindented_list_class : "unindentedList",
			paste_convert_headers_to_strong : true,
			//paste_strip_class_attributes:"all",
			paste_remove_spans:true,
			paste_remove_styles:true,
			//invalid_elements: "span",
			extended_valid_elements : "div[*],h1,h2,h3,h4,h5,p[*],span[*],</br>,table[cellpadding|cellspacing|width|border],td[*],tr[*],thead,tbody,iframe[src|width|height|name|align],object[classid|codebase|width|height|align|type|data],param[id|name|type|value|valuetype<DATA?OBJECT?REF]"
		});

		$("a.fancybox").fancybox({
			 'width' : '90%',
			 'height' : '90%',
			 'autoScale' : false,
			 'transitionIn' : 'none',
			 'transitionOut' : 'none',
			 'type' : 'iframe'
     	});

        $.fileup({
            url: document.location.href,
            inputID: 'upload-images',
            fieldName: 'ajax_photo_photos',
            extraFields: { 'auth_code': '{$auth_code}','post': 1, 'type': 'ajax_photogallery_insert'},
            autostart: true,
            onSelect: function(file) {
                $('#types .control-button').show();
            },
            onRemove: function(file, total) {
                if (file === '*' || total === 1) {
                    $('#types .control-button').hide();
                }
            },
            onSuccess: function(response, file_number, file) {
                var count = $("#photos_image_holder .gallery_photo").length;
                $('#photos_image_holder').width((count * 110) + 120);
                $('#photos_image_holder').append(response);
                $.growl.notice({ title: "Добавлено!", message: file.name });
            },
            onError: function(event, file, file_number) {
                console.log(event);
                $.growl.error({ message: "Ошибка загрузки!" });
            }
        });
	});
function toggle_prop($sw){
	if( $sw ==1 ){
		$('.large_slider').show();
		$('.slider').hide();
	}else{
		$('.large_slider').hide();
		$('.slider').show();
	}
}
</script>
<script type="text/javascript" src="/js/swfupload.js"></script>
<script type="text/javascript" src="/js/swfupload.cookies.js"></script>
<script type="text/javascript" src="/js/jquery.swfupload.js"></script>
<link rel="stylesheet" href="/css/jquery.Jcrop.css" type="text/css" media="screen" />
<script type="text/javascript"src="/js/jquery.Jcrop.min.js"></script>
<!-- /TinyMCE -->
</head>
<body>
<div class="large_slider">
	<a onclick="toggle_prop(0);" id="close">Закрыть</a>
    <div class="topMenu">
    <a class="fancybox" href="/{$dir}/admin_users/1/">Пользователи</a>
    <a class="fancybox" href="/{$dir}/main_page/">Настройки главной страницы</a>
    </div>
</div>
<div class="slider">
	<div class="user_name">Пользователь : {$admin_user} / <a href="/{$dir}/logout/">Выйти</a></div>
    <a onclick="toggle_prop(1);">Настройки</a>
</div>
<table class="table" width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td class="left_panel" width="240" valign="top"><div class="menu">{include file="admin/ru/menu.tpl"}</div></td>
    <td valign="top"><div class="main_part">{if $load_page!=''}{include file="$load_page.tpl"}{/if}</div></td>
</tr>
</table>
</body>
</html>
