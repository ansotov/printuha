<?php
include_once APPPATH . 'models/mmodel.php';

class mdiscount_types extends mmodel
{
	public function __construct()
	{
		parent::__construct();
		$this->table = "discount_types";
		$this->id    = "id";
	}

}