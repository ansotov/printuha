<article class="product_page clearfix">
	{include file="new_bloom/ru/info_block.tpl"}
	<!--{if $folder_info.top_banner_text!=''}<div class="top_banner" style="margin: -5px 0 20px 0">{$folder_info.top_banner_text}</div>{/if}-->
	<div class="top_banner" style="margin-top: -5px">{include file="new_bloom/ru/top_banner.tpl"}</div>
	<div class="left">
		<div class="image">
			<div class="mobile">
				<div id="slides" class="slides">
					{foreach $thumbnails as $k=>$v}
						<img alt="{$product.name|@escape}" src="/img/products_pictures/{$v.picture}_large.{$v.picture_type}">{/foreach}
				</div>
			</div>
			<!--<ul class="mobile">{foreach $thumbnails as $k=>$v}<li data-id="{$k}"  id="picture_mobile_{$k}"
          class="zoom{if $k==0} selected{/if}"><img src="/img/products_pictures/{$v.picture}_large.{$v.picture_type}"></li>{/foreach}</ul>-->
			<ul class="desktop">
				{foreach $thumbnails as $k=>$v}
					<li data-id="{$k}" id="picture_desktop_{$k}" class="{if $k==0} selected{/if}">
						<img alt="{$product.name|@escape}" class="zoom-image" src="/img/products_pictures/{$v.picture}_large.{$v.picture_type}">
					</li>
				{/foreach}
			</ul>
		</div>
		<div class="previews_part">
			<div class="previews">
				<ul>
					{foreach $thumbnails as $k=>$v}
					<li data-id="{$k}" {if $k==0}class="selected"{/if}>
						<a target="_blank" href="/img/products_pictures/{$v.picture}_large.{$v.picture_type}"><img alt="{$product.name|@escape}"
									src="/img/products_pictures/{$v.picture}_large.{$v.picture_type}"></a>
						</li>{/foreach}
				</ul>
			</div>
		</div>
	</div>
	<div class="slides right">
		<h1>{$product.name}</h1>
		<div class="properties">
			{$product.properties}
		</div>
		<div class="price">
			{if $product.old_price}
				<span class="old-price"><del>{$product.old_price|number_format:0:".":" "} <i
								class="fa fa-ruble"></i></del></span>
			{/if}
			<span id="price_val">{$product.price|number_format:0:".":" "}</span> <i class="fa fa-ruble"></i>
		</div>

		{if $product.not_available==1}
			<div style="font-size: 20px;margin: 0 0 20px 0">{translateConst const = EXPECTED_RECEIPT}</div>
		{/if}

		{if $group_products}
			<h2>{translateConst const = IMAGE}</h2>
			<ul class="select pictures">
				{foreach $group_products as $k=>$v}
					<li {if $v.id==$product.id}class="selected"{/if}><a
								href="/new-products/{$v.url}">{$v.group_variant}</a></li>
				{/foreach}
			</ul>
		{/if}

		{if $last_size}
			<div class="last_size">
				<img src="/img/last_size_clock.png" height="auto" width="18">
				<span data-bind="text:lowInStockText">{translateConst const = GOODS_QTY_LIMITED}</span>
			</div>
		{/if}

		<h2>{translateConst const = MODEL}</h2>
		<ul class="sizes select">
			{foreach $sizes as $k=>$v}
				<li data-price="{if $v.price >0}{$v.price|number_format:0:".":" "}{else}{$product.price|number_format:0:".":" "}{/if}"
					data-size="{$v.code}">
					<div class="radio"><input data-size="{$v.code}" type="radio" class="size" name="size"
											  value="{$v.code}"></div>{if $v.name!=''}{$v.name}{else}{$v.code}{/if}</li>
			{/foreach}
			<li class="select_size">
				<span>{translateConst const = CHOICE}</span>&nbsp;<span>{translateConst const = MODEL}</span></li>
		</ul>

		<h2>{translateConst const = MATERIAL}</h2>
		<ul class="custom-navigation materials select">
			{foreach $materials as $k=>$v}
				<li title="{$v.name}" class="custom-item{if $product.c_type == 3} glitters{/if}" data-id="{$v.id}"
					data-order="{$v@index}"
					data-item="{$v@index}" data-material="{$v.name}">
					{if $product.c_type == 3}
						<img alt="{$product.name|@escape}" src="/img/front_pictures/{$v.image}.{$v.image_type}">
						<div class="radio">
							<input data-material="{$v.id}" type="radio" name="material" class="material"
								   value="{$v.id}">
						</div>
					{else}
						<div class="radio"><input data-material="{$v.id}" type="radio" name="material" class="material"
												  value="{$v.id}"></div>
						{$v.name}
					{/if}
				</li>
			{/foreach}
			<li id="sel-mat" class="select_size select_material" style="width: 70px;">
				<span>{translateConst const = CHOICE}</span>&nbsp;<span>{translateConst const = MATERIAL}</span>
			</li>
		</ul>
		<input type="hidden" name="material" id="material" value="">

		<h2>{translateConst const = QTY}</h2>
		<select class="qty" name="qty">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
		</select>

		{if $product.not_available==0}
			<button class="cart_button cases-in-cart" style="min-width: 150px">{translateConst const = IN_CARD}</button>
			<button class="one-click-button mobile" style="min-width: 150px">{translateConst const = ONE_CLICK}</button>
		{else}
			<button class="cart_button_reminder cases-in-cart">{translateConst const = SAY_ABOUT_NEW}</button>
		{/if}

		<div class="info_block" style="margin: 25px 0 20px 0">
			<div class="name">{translateConst const = VENDOR}:</div>
			<div class="value">{$product.sku}</div>
			<div class="name">{translateConst const = TERM_OF_MANUFACTURE}:</div>
			<div class="value">2-3 {translateConst const = WORK_DAYS}</div>
		</div>

	</div>
	<!--<div class="description-details clearfix">
    <h2>Описание</h2>
    {$product.content}
  </div>-->

	{if $other_products}
		<section class="you-also-viewed products">
		<div class="name">{translateConst const = RECENTLY_VIEWED}</div>
		<ul>
			{foreach $other_products as $k=>$v}
				<li class="">
				<a href="/new-products/{$v.url}/">
					<img alt="{$v.name|@escape}" src="/img/front_pictures/{$v.front_picture}.jpg" class="front">
				</a>
				<div class="info">
					<h3>{$v.name}</h3>
					<div class="lower">
						<div class="price">{$v.price|number_format:0:".":" "}
							<div class="fa fa-rub"></div>
						</div>
					</div>
				</div>
				</li>{/foreach}
		</ul>
		</section>{/if}
	<div class="veil_white">
		{foreach $thumbnails as $k=>$v}<img alt="{$product.name|@escape}" class="fullsize_image"
											src="/img/products_pictures/{$v.picture}_large.{$v.picture_type}"
											id="fullsize_{$k}">{/foreach}
	</div>
</article>

<script>
    $("#slides").slidesjs({
        width: 50,
        height: 70,
        callback: {
            loaded: function () {
                $(".custom-item").click(function (e) {
                    e.preventDefault();
                    // use data-item value when triggering default pagination link
                    $('a[data-slidesjs-item="' + $(this).attr("data-item") + '"]').trigger('click');
                });
            }
        }
    });

    $("h2.header").click(function (e) {
        id = $(this).data('id')
        $("h2.header").removeClass('selected')
        $(this).addClass('selected')
        $(".right .desc").removeClass('selected')
        $(".desc[data-id='" + id + "']").addClass('selected')
    })
    $(".image > ul > li").click(function (e) {
        id = $(this).data('id')
        //console.log('picture_id = '+id)
        //$(".veil_white").addClass('selected')
        //$("#fullsize_"+id).addClass('selected')
    })
    $(".veil_white").click(function (e) {
        $('.fullsize_image').removeClass('selected')
        $(this).removeClass('selected')
    })
    $("select").selectBoxIt();

    //ansotov добавление в корзину
    $(".cart_button").click(function () {
        add_to_cart(1)
    });


    $(".sizes li").click(function (e) {
        $(".sizes li").removeClass('selected');
        $(this).addClass('selected');
        $(this).find('.size').attr('checked', true);
        val = $(this).data('price');

        $("#price_val").html(val);
    })

    $(".materials li").click(function (e) {
        $(".materials li").removeClass('selected');
        $(this).addClass('selected');
        $(this).find('.material').attr('checked', true);
        val = $(this).data('price');

        $("#price_val").html(val);
    })
	{if $sizes|@count==1}
    $(".sizes li").click()
	{/if}
    //$(".product_page .image .desktop .selected").elevateZoom();
    $(".materials li").click(function () {
        $(".materials li").removeClass('selected')
        $(this).addClass('selected')
        id = $(this).data('id')
        $("#material").val(id)
        order = $(this).data('order')
        $(".previews li[data-id='" + order + "']").find('a').click()
    })
    $(".previews li a").click(function () {
        $(".previews li").removeClass('selected')
        $(this).parent().addClass('selected')
        $(".image li").removeClass("selected")
        li = $(this).parent()
        id = $(li).data('id')
        $("#picture_desktop_" + id).addClass('selected')
        $("#picture_mobile_" + id).addClass('selected')
        //$(".product_page .image  .desktop .selected").elevateZoom();
        return false;
    })
</script>
