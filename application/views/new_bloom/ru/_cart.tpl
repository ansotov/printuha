<p>
	<span class="bPicture"></span>
	<span class="bName"></span>
	<span class="bSku"></span>
	<span class="bSize"></span>
	<span class="bColor"></span>
	<span class="bPrice"></span>
</p>
{foreach $cart as $k=>$v}
	<p class="cart-item">
  <span class="bPicture">
	{if $v.info.type != 'packages'}<a href="/new-products/{$v.info.url}/">{/if}
		  <img src="/img/front_pictures/{$v.info.front_picture}.{$v.info.front_picture_type}" alt="{$v.info.name}">
		  {if $v.info.type != 'packages'}</a>{/if}
  </span>
		<span class="bName">
    <strong>{$v.info.name}</strong>
			{if $v.size_info.name!=''}
		<br>{translateConst const = SIZE}: {$v.size_info.name}{else}{translateConst const = SIZE}: {$v.size}{/if}<br>
			{if $v.color}{translateConst const = COLOR}: {$v.color}
				<br>
			{/if}
			{if $v.material}{translateConst const = MATERIAL}: {$v.material.name}
				<br>
			{/if}
			{if $v.qty}{translateConst const = QTY}:
				<input class="minus" type="button"{if $v.qty > 1} onclick="productQty('{$k}', 0)"{/if} value="-">
				<span style="vertical-align: middle">{$v.qty}</span>
				<input class="plus" type="button" onclick="productQty('{$k}', 1)" value="+">
				<br>
			{/if}
			{if $v.in_stock}
				<br>
				<span style="text-transform: uppercase;color: red">{if $v.in_stock_discount < 100}{translateConst const = DISCOUNT} {$v.in_stock_discount}%{else}{translateConst const = ACTION_GIFT}{/if}</span>
			{/if}
  </span>
		<span class="bSku"></span>
		<span class="bSize"></span>
		<span class="bColor"></span>
		<span class="bPrice">{( ($v.in_stock) ? ($v.price * $v.qty) - ((($v.price / 100) * $v.in_stock_discount) * $v.in_stock_qty) : $v.price * $v.qty)|number_format:0:".":" "}
			.-</span>
		<a class="remove" href="#" onClick="remove_p('{$v.rowid}')">x</a>
	</p>
{/foreach}

<!-- ansotov информер бесплатной доставки -->
<div id="free-delivery-info">
	<div class="inner">
		{if $total_sum < $main_page.free_delivery}
			<script type="text/javascript">
                $(function () {
                    if ($('#courier-radio').prop('checked')) {
                        $('#courier_price').show();
                    }
                    if ($('#russian-post').prop('checked')) {
                        $('#russian-post_price').show();
                    }
                    if ($('#pickpoint-radio').prop('checked')) {
                        $('#pickpoint_price').show();
                    }
                });
			</script>
			<span style="vertical-align: sub">{translateConst const = YET_FOR_FREE_DELIVERY}</span>
			<span class="free-delivery-price">{($main_page.free_delivery - $total_sum)} {translateConst const = RUB}</span>
		{else}
			<script type="text/javascript">
                $(function () {
                    $('#courier_price').hide();
                    $('#pickpoint_price').hide();
                    $('#russian-post_price').hide();
                });
			</script>
			{translateConst const = FREE_DELIVERY}!
			<input id="free_delivery" type='hidden' name="free_delivery" value="1">
		{/if}
	</div>
</div>

<div class="divider"></div>
{if $amount}
	{if $amount->current_discount > 0}
		<div style="font-size: 16px;margin: 30px 0;text-align: center;border: 1px solid #000;">
			{translateConst const = YOUR_DISCOUNT} {$amount->current_discount}%.
			{if $amount->next_discount > 0}
				{translateConst const = FOR_DICOUNT_IN} {$amount->next_discount}%, {translateConst const = NEED_PRODUCTS_FOR} {$amount->yet_need} {translateConst const = RUB}
			{/if}
		</div>
	{else}
		<div style="font-size: 16px;margin: 30px 0;text-align: center;border: 1px solid #000;">
			{if $amount->next_discount > 0}
				{translateConst const = FOR_DICOUNT_IN} {$amount->next_discount}%, {translateConst const = NEED_PRODUCTS_FOR} {$amount->yet_need} {translateConst const = RUB}
			{/if}
		</div>
	{/if}
{/if}

<div style="height: 20px" class="packages-devider"></div>
<!-- ansotov упаковка -->

{*<div class="cart-item cardStickers">
	<span class="bPicture">
		<img src="/img/card/card_stickers.jpg" alt="Футболка со своим принтом">
	</span>
	<div class="bName" style="background: #93c9c4;padding: 10px;font-size: 16px;width: auto;margin: 7% 0 0 20px">
		НАКЛЕЙКИ В ПОДАРОК!
		<br>
		при заказе от 3000 руб.
	</div>
</div>*}

<div style="height: 40px"></div>

<!--{if $packages}
	<div style="height: 80px" class="packages-devider"></div>
	<!-- ansotov упаковка -->
	<!--{foreach $packages as $k => $v}
		<div class="package">
			<!--<div class="right">
			  <img width="140px" src="/img/girlsinbloom/boxes_bb.jpg" alt="НЕ ЗАБУДЬТЕ СТИКЕРЫ 🙂">
			</div>-->
			<!--<div class="left">
				<img width="120" src="/img/front_pictures/{$v.front_picture}.{$v.front_picture_type}">
			</div>
			<div class="center gifts-in-gift">{$v.name}</div>
			<!--<div class="qty">
			{translateConst const = QTY}:
			<input class="minus" type="button"{if $v.qty > 1} onclick="productQty('{$k}', 0)"{/if} value="-">
			<span style="vertical-align: middle">1</span>
			<input class="plus" type="button" onclick="productQty('{$k}', 1)" value="+">
		</div>-->
			<!--<div class="addToCard">
				<div class="price">{$v.price} <i class="fa fa-ruble"></i></div>
				<input type="hidden" class="qty" value="1">
				<input type="hidden" class="material" value="1">
				<ul class="sizes select" style="display: none">
					<li class="selected" data-size="onesize">
						<input data-size="onesize" type="radio" name="size" class="size" value="onesize"
							   checked="checked">
					</li>
				</ul>
				<button class="button cases-in-cart"
						onclick="addToCard('/new-products/{$v.url}/')">{translateConst const = ADD_TO_CARD}</button>
			</div>
		</div>
	{/foreach}
	<div style="height: 80px"></div>
{/if}-->
<script type="text/javascript">
    $(function () {
        var foreign_lands = $('input[name="foreign_lands"]').val();

        if (foreign_lands == 1)
            $('#free-delivery-info').hide();
        else
            $('#free-delivery-info').show();
    });
    //ansotov добавление в корзину
    $(".cart_button").click(function () {
        add_to_cart(1);
    });
</script>