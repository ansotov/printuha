<?php
include_once APPPATH.'models/mmodel.php';
class morders extends mmodel
{
	public $number = "";
	public function __construct()	
	{
		parent::__construct();
		$this->table="orders";
		$this->id="id";	
	}
	public function complete($order_id){
		$query = " UPDATE orders SET `status` = 'complete' , `completed_at` = NOW() WHERE id =".$order_id;
		$this->db->query($query);
	}
	public function generate_order_number(){
		
		$exist = true;
		while($exist == true ){
			$rand = "R".mt_rand(100000000,999999999);
			if($this->get(array('number'=>$rand),('id'),1))
				$exist = true;
			else{
				$exist = false;
			}
		}
		return $rand;
	}

	public function insert($data){
		if(!isset($data['number'])){
			$data['number'] = $this->generate_order_number();
			$this->number = $data['number'];
		}
		$id = parent::insert($data);
		return $id;
	}


}