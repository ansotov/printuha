<div class="reminder_wrapper one_click">
	<section class="clearfix reminder">
		<a href="#" class="close delete-button"><i class="fa fa-times"></i></a>
		<article>
			<form id="one_click" method="post">
				<h2>{translateConst const = GIVE_YOUR_DATA}</h2>
				<input required type="text" name="name" value="{$postdata.name}"
					   placeholder="{translateConst const = NAME}">
				<input required type="tel" class="phone" name="phone" value="{$postdata.phone}"
					   placeholder="{translateConst const = PHONE_NUMBER}">
				<input type="hidden" name="product_id" value="{$product.id}">
				<input type="hidden" name="size_id" value="0">
				<input type="hidden" name="material" value="">
				<input type="hidden" name="comment" value="">
				<button type="submit">{translateConst const = SEND}</button>
			</form>
		</article>
	</section>
</div>
<script type="text/javascript">
    $(function () {

        $('form#one_click').on('submit', function (event) {
            event.preventDefault();
            var val = $(this).serializeArray();

            var res = false;

            $.ajax({
                type: "POST",
                url: "/one-click-order/",
                data: val,
                success: function (data) {
                    fbq('track', 'AddToCart');
                    $('.reminder_wrapper.one_click article').html('<span style="text-align: center;font-size: 20px">{translateConst const = WILL_SEND_YOU_INFO_ONE_CLICK}!</span>');
                    $('#one_click').delay(2000).fadeOut(1000, function (e) {
                        window.location.reload();
                    });
                }
            });

        });


    });
</script>