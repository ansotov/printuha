<section class="signup_form clearfix">
	<article>
		<form id="signup_form" action="/signup/" method="post">
			<h2>{translateConst const = REGISTRATION}</h2>
			{if $success==1 && $post_form=='signup'}
				{translateConst const = SUCCESFUL_REGISTRATION}
				<script>
                    window.location.replace("/");
				</script>
			{else}
				<label for="email">E-mail</label>
				<input type="email" name="email" value="{$postdata.email}" autocomplete="off">
			{if $errors.email && $post_form=='signup'}
				<div class="error">{$errors.email}</div>{/if}
				<label for="password">{translateConst const = PASSWORD}</label>
				<input type="password" name="password" autocomplete="off">
			{if $errors.password && $post_form=='signup'}
				<div class="error">{$errors.password}</div>{/if}
				<button>{translateConst const = REGISTRATION}</button>
			{/if}
		</form>
	</article>
	<article>
		<a href="#" class="close"><i class="fa fa-times"></i></a>
		<form id="signin_form" action="/signin/" method="post">
			<input type="hidden" name="post_form" value="signin">
			<h2>{translateConst const = ENTRANCE}</h2>
			{if $success==1 && $post_form=='signin'}
				{translateConst const = SUCCESFUL_ENTRANCE}
				<script>
                    window.location.replace("/");
				</script>
			{else}
				<label for="email">E-mail</label>
				<input type="text" name="email" value="{$postdata.email}" autocomplete="off">
			{if $errors.email && $post_form=='signin'}
				<div class="error">{$errors.email}</div>{/if}
				<label for="password">{translateConst const = PASSWORD}</label>
				<input type="password" name="password" autocomplete="off">
				<!--<a href="{$auth_vk_link}" class="social-buttons vk"></a>
				<a href="{$auth_facebook_link}" class="social-buttons facebook"></a>-->
			{if $errors.password && $post_form=='signin'}
				<div class="error">{$errors.password}</div>{/if}
				<button>Войти</button>
			{/if}
		</form>
	</article>
</section>
