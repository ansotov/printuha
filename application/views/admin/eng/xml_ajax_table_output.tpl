<xml_table>
{if $output_rows}
{foreach from=$output_rows item=v}
<row_data>
	<order_id><![CDATA[{$v.order_id}]]></order_id>
	{if $hide_id}<id>{$v.id}</id>{/if}
    <options><![CDATA[<a href="/{$dir}/{$links.link.link}/{$end_link}{if $v.id}{$v.id}/{/if}" class="option"><img src="/img/admin/edit_icon.png" title="Edit" width="30" height="30" /></a><a  onClick="delete_o({$v.id})" class="option"><img src="/img/admin/delete_icon.png" title="Delete" width="30" height="30" /></a>]]></options>
{foreach from=$select_names key=sk item=sv}
    {assign var='field_value' value=$v[$sk]}
    <{$sk} width="{$sv.width}"><![CDATA[{strip}
    {if $sv.type=='string'}{$field_value}{/if}
    {if $sv.type=='integer'}{$field_value}{/if}
    {if $sv.type=='date'}{$field_value}{/if}
    {if $sv.type=='text'}{$field_value}{/if}
    {if $sv.type=='ajax_data'}{$field_value}{/if}
    {if $sv.type=='jpg_file' || $sv.type=='jcrop'}<a href="/{$sv.directory}{$field_value}.jpg"><img src="/{$sv.directory}{$field_value}{if !$v.max_size_thumb}_small{/if}.jpg" /></a>{/if}
    {if $sv.type=='flv_file'}<a href="/{$sv.directory}{$field_value}.flv">file</a>{/if}
    {if $sv.type=='list'}{$sv.values[$field_value]}{/if}
    {if $sv.type=='model_list'}{$sv.values[$field_value]}{/if}
    {if $sv.type=='checkbox'}<input onclick="aj_checkbox(this,'{$sk}','{$v.id}')" type="checkbox"{if $field_value==1} checked="checked"{/if} />{/if}
    {strip}]]></{$sk}>
{/foreach}
</row_data>
{/foreach}
{/if}

<pages><![CDATA[<div class="leftTxt">Pages :</div> 
{foreach from=$pages_info.pages_array item=i}
    {if $i == '...'}
        <div class="normal">...</div>
    {else}
        {if $i <> $page}                    
         <div class="normal"><a onclick="to_page({$i});">{$i}</a></div>
        {else}
           <div class="selected">{$page}</div>
        {/if}                    
    {/if}
{/foreach}
{if $pages_info.back_page}
    <div class="normal"><a  onclick="to_page({$pages_info.back_page});" ><</a></div>
{/if}
{if $pages_info.foward_page}
    <div class="normal"><a   onclick="to_page({$pages_info.foward_page});"  >></a></div>
{/if}]]></pages>
</xml_table>