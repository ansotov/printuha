<div class="reminder_wrapper">
  <section class="clearfix reminder">
    <article>
        <form id="reminder" method="post">
          <h2>{translateConst const = SAY_ABOUT_NEW}</h2>
          <br>
            <label for="name">{translateConst const = NAME} <span class="required-label">*</span>
              <input required type="text" name="name" value="{$postdata.name}">
            </label>
            <label for="email">E-mail <span class="required-label">*</span>
              <input required type="email" name="email" value="{$postdata.email}">
            </label>
            <label for="phone">{translateConst const = PHONE}
              <input type="tel" class="phone" name="phone" value="{$postdata.phone}">
            </label>
              <input type="hidden" name="product_id" value="{$product.id}">
              <input type="hidden" name="size_id" value="0">
            <button style="margin: 0" type="submit">{translateConst const = SEND}</button>
        </form>
      </article>
  </section>
</div>
<script type="text/javascript">
    $(function() {
        $('form#reminder').on('submit', function (event) {
            event.preventDefault();
            var val = $( this ).serializeArray();

            var res = false;

            $.ajax({
                type: "POST",
                url: "/reminder/",
                data: val,
                success: function (data) {
                    $('.reminder_wrapper').html('<span style="text-align: center">{translateConst const = WILL_SEND_YOU_INFO}!</span>');
                    $('#reminder').delay(2000).fadeOut(1000);
                }
            });

        });

        //ansotov маска для номера телефона
        $.mask.definitions['X'] = '[01234569]';
        $('.phone').mask('+7 (X99) 999-99-99');
    });
</script>