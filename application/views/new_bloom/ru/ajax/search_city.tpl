<div style="text-align: center">
	<input required type="text" name="search_city_text" id="search_city_text"
		   placeholder="{translateConst const = ADD_YOUR_CITY}">
    <input name="search_city_text_hidden" id="search_city_text_hidden" type="hidden">
</div>


<script type="text/javascript">

    (function ($) {

        $('#search_city_text').on('keyup', function () {
            $('#search_city_text').css('border', '2px solid #e0e0e0')
        });

        jQuery.fn.lightTabs = function (options) {

            var createTabs = function () {
                tabs = this;
                i = 0;

                showPage = function (i) {
                    $(tabs).children("div").children("div").hide();
                    $(tabs).children("div").children("div").eq(i).show();
                    $(tabs).children("ul").children("li").removeClass("active");
                    $(tabs).children("ul").children("li").eq(i).addClass("active");
                }

                showPage(0);

                $(tabs).children("ul").children("li").each(function (index, element) {
                    $(element).attr("data-page", i);
                    i++;
                });

                $(tabs).children("ul").children("li").click(function () {
                    showPage(parseInt($(this).attr("data-page")));
                });
            };
            return this.each(createTabs);
        };
    })(jQuery);
    $(document).ready(function () {
        $(".tabs").lightTabs();
    });

    $(function () {

        setDefaultDelivery();

        $('#search_city_text').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/city-search',
                    type: 'POST',
                    data: {
                        maxRows: 15, // показать первые 10 результатов
                        query: request.term // поисковая фраза
                    },
                    success: function (data) {

                        response($.map($.parseJSON(data), function (item) {
                            return {
                                price: item.price, //ansotov цена
                                delivery_price: item.delivery_price, //ansotov цена доставки
                                zone: item.zone, //ansotov количество дней
                                days: item.days, //ansotov количество дней
                                label: item.label, //ansotov название города и области
                                name: item.name, //ansotov название города
                                region: item.region //ansotov название региона
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {

                //ansotov подстановка города
                $('#search_city_text, #search_city_text_hidden').val(ui.item.label);

                var city = ui.item;

                $.ajax({
                    url: '/all-types',
                    type: 'POST',
                    data: {
                        name: ui.item.name, // поисковая фраза
                        region: ui.item.region // поисковая фраза
                    },
                    success: function (data) {

                        var data_arr = $.parseJSON(data);

                        delivery_types(city, data_arr.couriers, data_arr.pickpoint);
                    }
                });

                return false;
            },
            minLength: 2 // начинать поиск с трех символов
        });

        $('#search_city_text').on('change', function (e) {
            //ansotov подстановка города
            var name = $('#search_city_text').val();
            var coordinatesArr = name.split(" - ")

			if (coordinatesArr) {
                $.ajax({
                    url: '/all-types',
                    type: 'POST',
                    data: {
                        name: coordinatesArr[0],
                        region: coordinatesArr[1]
                    },
                    success: function (data) {

                        var city = $.parseJSON(data);

                        //ansotov подстановка города
                        $('#search_city_text').val(city.russia_post[0].label);

                        $.ajax({
                            url: '/all-types',
                            type: 'POST',
                            data: {
                                name: city.russia_post[0].name, // поисковая фраза
                                region: city.russia_post[0].region // поисковая фраза
                            },
                            success: function (data) {

                                var data_arr = $.parseJSON(data);

                                delivery_types(city.russia_post[0], data_arr.couriers, data_arr.pickpoint);
                            }
                        });
                    }
                });
            }
        });
    });
</script>
