<?
class mtemp_data extends Model{
	public function __construct(){
		parent::Model();
	}
	
	public function upload($field_name,$value){
		$this->db->insert("temp_data",array("field_name"=>$field_name,"value"=>$value));
	}
	public function delete($field_name){
		echo $field_name;
		$this->db->delete("temp_data",array("field_name"=>$field_name));
	}
	public function show($field_name){
		$this->db->select("value");
		$r=$this->db->get_where("temp_data",array("field_name"=>$field_name));
		if($r->num_rows()>0){
				$rr=$r->result_array();
				return $rr[0]['value'];
		}else
			return false;
	}
}
?>