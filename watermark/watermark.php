<?php waterMark($_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI'], "watermark.png", "middle=5,center=5");


function waterMark($original, $watermark, $placement = 'bottom=5,right=5', $destination = null)
{

	$original = urldecode($original);
	$info_o   = @getImageSize($original);
	if (!$info_o)
		return false;
	$info_w = @getImageSize($watermark);
	if (!$info_w)
		return false;

	list ($vertical, $horizontal) = explode(',', $placement, 2);
	list($vertical, $sy) = explode('=', trim($vertical), 2);
	list($horizontal, $sx) = explode('=', trim($horizontal), 2);

	switch (trim($vertical))
	{
		case 'bottom':
			$y = $info_o[1] - $info_w[1] - (int) $sy;
			break;
		case 'middle':
			$y = ceil($info_o[1] / 2) - ceil($info_w[1] / 2) + (int) $sy;
			break;
		default:
			$y = (int) $sy;
			break;
	}

	switch (trim($horizontal))
	{
		case 'right':
			$x = $info_o[0] - $info_w[0] - (int) $sx;
			break;
		case 'center':
			$x = ceil($info_o[0] / 2) - ceil($info_w[0] / 2) + (int) $sx;
			break;
		default:
			$x = (int) $sx;
			break;
	}

	header("Content-Type: " . $info_o['mime']);
	$original  = @imageCreateFromString(file_get_contents($original));
	$watermark = @imageCreateFromString(file_get_contents($watermark));
	$out       = imageCreateTrueColor($info_o[0], $info_o[1]);

	if ($info_o[2] == 3)
	{
		$transparent = imagecolorallocatealpha($out, 0, 0, 0, 127);
		imagefill($out, 0, 0, $transparent);
		imagesavealpha($out, true);
	}
	imageCopy($out, $original, 0, 0, 0, 0, $info_o[0], $info_o[1]);

	if (($info_o[0] > 200) && ($info_o[1] > 150))
	{


		if (strpos($_SERVER['REQUEST_URI'], '_large.jpg'))
		{

			//ansotov это очень страшный костыль, но подругому никак
			$str = str_replace('_large.jpg', '', str_replace('/img/products_pictures/', '', $_SERVER['REQUEST_URI']));

			$db = mysql_connect("u407410.mysql.masterhost.ru", "u407410", "fle_TicAt.o55C"); /*Подключение к серверу */
			mysql_select_db("u407410", $db); /*Подключение к базе данных на сервере*/

			$sql_query = "SELECT p.type FROM products p, photos ph WHERE ph.parent_id = p.id AND `picture` = '" . $str . "'";
			$result = mysql_fetch_assoc(mysql_query($sql_query, $db)) or die(mysql_error());

			if (isset($result) && $result['type'] == 'gotovye-kejsy')
				imageCopy($out, $watermark, $x, $y, 0, 0, $info_w[0], $info_w[1]);
		}
	}

	switch ($info_o[2])
	{
		case 1:
			imageGIF($out);
			break;
		case 2:
			imageJPEG($out, null, 95);
			break;
		case 3:
			imagePNG($out);
			break;
	}

	imageDestroy($out);
	imageDestroy($original);
	imageDestroy($watermark);

	return true;
}

?>
