<div class="left-column" style="border-top: 1px solid #ccc;width:100%;padding: 10px 0 0;margin: 25px 0 0 0">
    <p class="payment_type field">
        <strong><label for="order_bill_address_attributes_address1">{translateConst const = PAYMENT_TYPE}</label></strong>
        <span class="area selector" id="courier_payment"><input type="radio" name="payment_type" value="courier" /> Курьеру boxberry</span>
        <span class="area selector" id="online_payment"><input type="radio" name="payment_type" value="online" /> Оплата онлайн.</span>
    {if $errors.payment_type}<p class="selector"><label class="error">{$errors.payment_type}</label></p>{/if}
    </p>
</div>
