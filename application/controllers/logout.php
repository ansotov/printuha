<?php
include_once './application/controllers/main_common.php';
class logout extends main_common {

	public function index(){
		$this->session->unset_userdata(array('user_mail'=>"",'user_id'=>"",'site_user'=>""));
		unset($_SESSION['user_id']);
		redirect('/');
	}
}
?>