<div class="total">
	<div class="sum">
		Cумма заказа:<strong>{($total_sum + $discount_sum)|number_format:0:".":" "}.-</strong>
		{if $discount_sum > 0}
			{translateConst const = DISCOUNT}:
			<strong>-{($discount_sum)|number_format:0:".":" "}.-</strong>
		{/if}
	</div>
	<div class="delivery ">
		{translateConst const = DELIVERY}:<strong
				id="delivery_sum">{if $total_sum < $main_page.free_delivery or ($delivery and $delivery.price > 0)}{if $delivery and $delivery.price > 0}{($delivery.price)|number_format:0:".":" "}.-{else}-{/if}{else}{translateConst const = FREE}{/if}</strong>
	</div>
	<div class="plus">
		<span>{translateConst const = TOTAL}:</span><strong
				id="full_price">{if $delivery}{($total_sum + $delivery.price)|number_format:0:".":" "}{else}{($total_sum)|number_format:0:".":" "}{/if}
			.-</strong>
	</div>
</div>