<div id="header">
	<div class="top-block absolute">
		<div class="container">
			<div class="main-wrapper">
				<div class="phones_wrap">
					<a href="#"><i class="fa fa-phone"></i></a>
					<div class="phones_block">
						<div class="call-center-info">
							<span>{translateConst const = WORK_TIME} ({translateConst const = WORK_TIME_CITY})</span>
						</div>
						<div class="phones">
							<div class="phone phone-1">
								<a href="https://api.whatsapp.com/send?phone=79100137413" target="_blank">
									<img width="20" src="/img/whatsapp.png"
																style="margin: 3px 0 -5px 0"> +7 910 013-74-13</a>
							</div>
						</div>
					</div>
				</div>
				<div id="basket-info">
					<div id="cart-informer">
						{include file="new_bloom/ru/ajax/cart-informer.tpl"}
					</div>
					<div id="cart-wrapper">
						{include file="new_bloom/ru/window-cart.tpl"}
					</div>
				</div>
				<div ctrl-basket="" data-offset-top="118" class="basket affix-top">
				</div>
			</div>
		</div>
	</div>
	<div class="container container-logo">
		<div class="row">
			<a href="/" class="logo">
				<img title="{translateConst const = SHOP_GIRLS_IN_BLOOM}"
					 alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}" src="/img/logo.jpg">
			</a>
		</div>
	</div>
	<div data-offset-top="118" class="menu-block affix-top">
		<div class="container">
			<div class="main-wrapper">
				<div class="row">
					<!--noindex-->
					<form action="/catalog/" class="search">
						<input type="search" required name="search" placeholder="{translateConst const = SEARCH}"
							   class="form-control input-sm" value="{$smarty.get.search}">
						<input type="submit" value="" class="submit bit-icon bit-icon-search">
					</form>
					<!--/noindex-->
					<div class="top-menu-wrap">
						<a href="#menu"><i class="fa fa-bars"></i></a>
						<div class="mobile-menu">
							<a href="#" class="close"><i class="fa fa-times"></i></a>
							<ul class="top-menu">
								<li class="parent{if $current_url == '/catalog/new/'} selected{/if}">
									<a href="/catalog/new/"><span>NEW</span></a>
								</li>
								<!--<li class="parent{if $current_url == '/catalog/new-year/'} selected{/if}">
									<a href="/catalog/new-year/" style="color: #ff0000"><span>Новый год</Новый></span></a>
								</li>-->
								<li class="parent{if $current_url == '/catalog/female-t-shirts/'} selected{/if}">
									<a href="/catalog/female-t-shirts/"><span>Футболки женские</span></a>
								</li>
								<li class="parent{if $current_url == '/catalog/t-shirts-with-sequins/'} selected{/if}">
									<a href="/catalog/t-shirts-with-sequins/"><span>Футболки с блестками</span></a>
								</li>
								<!--<li class="parent{if $current_url == '/catalog/female-t-shirts/'} selected{/if}">
									<a href="/catalog/female-t-shirts/"><span>Футболки женские</span></a>
								</li>-->
								<li class="parent{if $current_url == '/catalog/male-t-shirts/'} selected{/if}">
									<a href="/catalog/male-t-shirts/"><span>Футболки мужские</span></a>
								</li>
								<li class="parent{if $current_url == '/catalog/swimsuits/'} selected{/if}">
									<a href="/catalog/swimsuits/"><span>Купальники</span></a>
								</li>
								<!--<li class="parent{if $current_url == '/catalog/female-sweatshirts/'} selected{/if}">
									<a href="/catalog/female-sweatshirts/"><span>Толстовки женские</span></a>
								</li>-->
								<li class="parent{if $current_url == '/catalog/masks/'} selected{/if}">
									<a href="/catalog/masks/"><span>Маски</span><!-- <span style="color: magenta;">Скоро новинки</span>--></a>
								</li>
								<li class="parent{if $current_url == '/catalog/body-with-long-sleeves/'} selected{/if}">
									<a href="/catalog/body-with-long-sleeves/"><span>Боди с длинным рукавом</span><!-- <span style="color: magenta;">Скоро новинки</span>--></a>
								</li>
								{*<li class="parent{if $current_url == '/catalog/body-without-sleeves/'} selected{/if}">
									<a href="/catalog/body-without-sleeves/"><span>Боди без рукавов</span><!-- <span style="color: magenta;">Скоро новинки</span>--></a>
								</li>*}
								<li class="parent{if $current_url == '/catalog/trousers'} selected{/if}">
									<a href="/catalog/trousers/"><span>Штаны</span></a>
								</li>
                                <li class="parent{if $current_url == '/catalog/shorts'} selected{/if}">
									<a href="/catalog/shorts/"><span>Майки</span></a>
								</li>
								{*<li class="parent{if $current_url == '/catalog/dresses/'} selected{/if}">
									<a href="/catalog/dresses/"><span>Платья</span></a>
								</li>*}
								<li class="parent{if $current_url == '/catalog/female-sweatshirts/'} selected{/if}">
									<a href="/catalog/female-sweatshirts/"><span>Толстовки женские</span></a>
								</li>
								<li class="parent{if $current_url == '/catalog/male-sweatshirts/'} selected{/if}">
									<a href="/catalog/male-sweatshirts/"><span>Толстовки мужские</span></a>
								</li>
								<li class="parent{if $current_url == '/catalog/hoodie-hooded/'} selected{/if}">
									<a href="/catalog/hoodie-hooded/"><span>Худи с капюшоном</span></a>
								</li>
								{*<li class="parent{if $current_url == '/catalog/down-jackets/'} selected{/if}">
									<a href="/catalog/down-jackets/"><span>Пуховики</span></a>
								</li>*}
								{*<li class="parent">
									<a href="/catalog/raincoats/"><span>Дождевики</span></a>
								</li>*}
								<!--<li class="parent{if $current_url == '/catalog/notes/'} selected{/if}">
									<a href="/catalog/notes/"><span>Ежедневники</span></a>
								</li>
								<li class="parent">
									<a href="javascript:void(0)"><span>Рюкзаки</span> <span style="color: magenta;">Скоро!</span></a>
								</li>
								<li class="parent{if $current_url == '/catalog/bags/'} selected{/if}">
									<a href="/catalog/bags/"><span>Сумки</span></a>
								</li>-->
								<!--<li class="parent{if $current_url == '/catalog/male-costumes/'} selected{/if}">
									<a href="/catalog/male-costumes/"><span>Костюмы мужские</span></a>
								</li>-->
								<li class="parent{if $current_url == '/catalog/female-costumes/'} selected{/if}">
									<a href="/catalog/female-costumes/"><span>Костюмы</span></a>
								</li>
								<!--<li class="parent{if $current_url == '/catalog/children-costumes/'} selected{/if}">
									<a href="/catalog/children-costumes/"><span>Костюмы детские</span></a>
								</li>-->
								<!--<li class="parent{if $current_url == '/catalog/children-sweatshirts/'} selected{/if}">
									<a href="/catalog/children-sweatshirts/"><span>Толстовки детские</span></a>
								</li>-->

								<!--<li class="parent{if $current_url == '/catalog/children-t-shirts/'} selected{/if}">
									<a href="/catalog/children-t-shirts/"><span>Футболки детские</span></a>
								</li>-->
								<!--<li class="parent{if $current_url == '/catalog/gotovye-kejsy/'} selected{/if}">
									<a href="/catalog/gotovye-kejsy/"><span>Чехлы на iPhone</span></a>
								</li>
								<li class="parent{if $current_url == '/catalog/gotovye-kejsy/samsung/'} selected{/if}">
									<a href="/catalog/gotovye-kejsy/samsung/"><span>Чехлы на Samsung</span></a>
								</li>-->
								<li class="parent{if $current_url == '/catalog/sale/'} selected{/if}">
									<a href="/catalog/sale/" style="color: red"><span>Sale</span></a>
								</li>
								<!--<li class="parent{if $current_url == '/payment/'} selected{/if}">
									<a href="/payment"><span>Оплата</span></a>
								</li>
								<li class="parent{if $current_url == '/delivery/'} selected{/if}">
									<a href="/delivery/"><span>Доставка</span></a>
								</li>
								<li class="parent{if $current_url == '/return-polisy/'} selected{/if}">
									<a href="/return-polisy/"><span>Возврат товара</span></a>
								</li>-->
							</ul>
						</div>
						<div id="main-menu">
							<ul class="top-menu">
								{foreach $top as  $k=>$v}
									{if $v.url == '/catalog/gift-certificates/'}{continue}{/if}
									<li class="parent{if strpos($v.url, $product.type) or $current_url|strstr:$v.url or (in_array($current_url, array('/about-us/', '/partners/', '/portfolio/', '/smi/')) and $v.url == '/girls-in-bloom/')} selected{/if}">
										<a href="{if $v.url == '/girls-in-bloom/'}javascript::void(0);{else}{$v.url}{/if}"><span
													{if $v.color}style="color:{$v.color}"{/if}>{$v.name}</span></a>
										{if $v.url == '/girls-in-bloom/'}
											<ul class="girls-in-bloom menu">
												<li{if $current_url == '/about/'} class="selected"{/if}>
													<a href="/about-us/">
														<div>{translateConst const = ABOUT_COMPANY}</div>
														<img alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}"
															 src="/img/menu/about.jpg">
													</a>
												</li>
												<li{if $current_url == '/partners/'} class="selected"{/if}>
													<a href="/partners/">
														<div>{translateConst const = PARTNERS}</div>
														<img alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}"
															 src="/img/menu/partners.jpg">
													</a>
												</li>
												<li{if $current_url == '/portfolio/'} class="selected"{/if}>
													<a href="/portfolio/">
														<div>{translateConst const = PORTFOLIO}</div>
														<img alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}"
															 src="/img/menu/portfolio.jpg">
													</a>
												</li>
												<li{if $current_url == '/smi/'} class="selected"{/if}>
													<a href="/smi/">
														<div>{translateConst const = PRESS_ABOUT_US}</div>
														<img alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}"
															 src="/img/menu/smi.jpg">
													</a>
												</li>
											</ul>
										{/if}
										{if $v.url == '/catalog/clothes/'}
											<ul class="under_menu">
												<li>
													<a href="/catalog/tolstovki/">{translateConst const = HOODIES}</a>
												</li>
												<li>
													<a href="/catalog/futbolki/">{translateConst const = T_SHIRTS}</a>
												</li>
												<!--<li>
													<a href="/catalog/shirts/">{translateConst const = SHIRTS}</a>
												</li>-->
												<li>
													<a href="/catalog/skirts/">{translateConst const = SKIRTS}</a>
												</li>
												<!--<li>
													<a href="/catalog/dresses/">Платья</a>
												</li>-->
											</ul>
										{/if}
										{if $v.url == '/catalog/body-with-long-sleeves/'}
											{*<ul class="under_menu">
												*}{*<li>
													<a href="/catalog/body-with-long-sleeves/">{translateConst const = BODY_WITH_LONG_SLEEVES}</a>
												</li>*}{*
												*}{*<li>
													<a href="/catalog/body-without-sleeves/">{translateConst const = BODY_WITHOUT_SLEEVES}</a>
												</li>*}{*
											</ul>*}
										{/if}
										{if $v.url == '/catalog/female-t-shirts/'}
											<ul class="under_menu">
												<li>
													<a href="/catalog/female-t-shirts/">{translateConst const = FEMAIL_T_SHIRTS}</a>
												</li>
												<li>
													<a href="/catalog/t-shirts-with-sequins/">{translateConst const = T_SHIRTS_WITH_SEQUINS}</a>
												</li>
												<li>
													<a href="/catalog/male-t-shirts/">{translateConst const = MALE_T_SHIRTS}</a>
												</li>
											</ul>
										{/if}
										{if $v.url == '/catalog/female-sweatshirts/'}
											<ul class="under_menu">
												<li>
													<a href="/catalog/female-sweatshirts">{translateConst const = FEMAIL_SWEATSHIRTS}</a>
												</li>
												<li>
													<a href="/catalog/male-sweatshirts/">{translateConst const = MALE_SWEATSHIRTS}</a>
												</li>
												<li>
													<a href="/catalog/hoodie-hooded/">{translateConst const = HOODIE_HOODED}</a>
												</li>
											</ul>
										{/if}
										{*{if $v.url == '/catalog/costumes/'}
											<ul class="under_menu">
												<!--<li>
													<a href="/catalog/male-costumes/">{translateConst const = MALE_COSTUMES}</a>
												</li>-->
												<li>
													<a href="/catalog/female-costumes/">{translateConst const = FEMAIL_COSTUMES}</a>
												</li>
												<li>
													<a href="/catalog/children-costumes/">{translateConst const = CHILDREN_COSTUMES}</a>
												</li>
											</ul>
										{/if}*}
										{*{if $v.url == '/catalog/female-sweatshirts/'}
										<ul class="under_menu">
												<li>
													<a href="/catalog/male-sweatshirts/">{translateConst const = MALE_SWEATSHIRTS}</a>
												</li>
												<li class="{if $current_url == '/catalog/hoodie-hooded/'} selected{/if}">
													<a href="/catalog/hoodie-hooded/">{translateConst const = HOODIE_HOODED}</a>
												</li>
												<!--<li>
													<a href="/catalog/female-sweatshirts/">{translateConst const = FEMAIL_SWEATSHIRTS}</a>
												</li>
												<li>
													<a href="/catalog/children-sweatshirts/">{translateConst const = CHILDREN_SWEATSHIRTS}</a>
												</li>-->
											</ul>
										{/if}*}
										{*{if $v.url == '/catalog/female-t-shirts/'}
										<ul class="under_menu">
												<li>
													<a href="/catalog/t-shirts-with-sequins/">{translateConst const = T_SHIRTS_WITH_SEQUINS}</a>
												</li>
												<li>
													<a href="/catalog/male-t-shirts/">{translateConst const = MALE_T_SHIRTS}</a>
												</li>
												<!--<li>
													<a href="/catalog/children-t-shirts/">{translateConst const = CHILDREN_T_SHIRTS}</a>
												</li>-->
											</ul>
										{/if}*}
										<!--{if $v.url == '/catalog/gotovye-kejsy/'}
											<ul class="under_menu">
												<li>
													<a href="{$v.url}">iPhone</a>
												</li>
												<li>
													<a href="{$v.url}samsung/">Samsung</a>
												</li>
											</ul>
										{/if}-->
										{if $v.url == '/catalog/accessories/'}
											<ul class="under_menu">
												<li>
													<a href="/catalog/notes/">{translateConst const = DIARIES}</a>
												</li>
												<!--<li>
													<a href="/catalog/backpacks/">{translateConst const = BACKPACKS}</a>
												</li>-->
												<li>
													<a href="/catalog/bags/">{translateConst const = BAGS}</a>
												</li>
												<!--<li>
													<a href="/catalog/gotovye-kejsy/">{translateConst const = CASES_FOR} iPhone</a>
												</li>-->
											</ul>
										{/if}
									</li>
								{/foreach}
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
