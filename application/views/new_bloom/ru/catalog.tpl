<div class="wrapper clearfix">
	{if $page_type=='catalog'}
		<!--{if $folder_info.top_banner_text!=''}<div class="top_banner">{$folder_info.top_banner_text}</div>{/if}-->
		<div class="top_banner">{include file="new_bloom/ru/top_banner.tpl"}</div>
	{if strpos($smarty.server.REQUEST_URI, 'sale') == true}
		<aside class="filter">
			{if $tags}
				<a href="#" class="filter-button">{translateConst const = TAGS}</a>
			{/if}

			<form id="filter_form" action="" method="get">
				<nav>
					<div id="block_options" class="options selected">

						{if $clothes}
							<div class="filter-block">
								<div class="spacer" style="margin: 0"></div>
								<div class="block">
									<h3></h3>
									<ul class="tags">
										{foreach $clothes_types as $k => $v}
											<li>
												<div class="checkbox">
													<input
															type="checkbox"{if in_array($k, $smarty.REQUEST.clothes_types)} checked{/if}
															class="checkbox" id="clothes_types_{$k}"
															name="clothes_types[]" value="{$k}">
													<label for="clothes_types_{$k}">{$v}</label>
												</div>
											</li>
										{/foreach}
									</ul>
								</div>
							</div>
						{/if}

						{if $accessories}
							<div class="filter-block">
								<div class="spacer" style="margin: 0"></div>
								<div class="block">
									<h3></h3>
									<ul class="tags">
										{foreach $accessories_types as $k => $v}
											<li>
												<div class="checkbox">
													<input
															type="checkbox"{if in_array($k, $smarty.REQUEST.accessories_types)} checked{/if}
															class="checkbox" id="accessories_types_{$k}"
															name="accessories_types[]" value="{$k}">
													<label for="accessories_types_{$k}">{$v}</label>
												</div>
											</li>
										{/foreach}
									</ul>
								</div>
							</div>
						{/if}

						{if $filter_sizes|count > 1 && strpos($smarty.server.REQUEST_URI, 'gotovye-kejsy') == false}
							<div class="filter-block">
								<div class="spacer" style="margin: 0"></div>
								<h3>{translateConst const = SIZE}</h3>
								<div id="block_sizes" class="block">
									<ul class="sizes">
										{foreach $filter_sizes as $k=>$v}
											{if $v.qty > 0 && strpos($v.code, 'hone') == false && strpos($v.name, 'sung') == false }
												<li>
													<div class="checkbox">
														<input
																type="checkbox" {if $v.checked}checked="checked"{/if}
																class="checkbox" id="check_{$k}" name="size[]"
																value="{$k}">
														<label for="check_{$k}">
															{if $v.name}
																{$v.name}
															{else}
																{if $v.name}
																	{$v.name}
																{else}
																	{if $v.code == 'iphone4'}
																		IPhone 4/4s
																	{elseif $v.code == 'iphone5s'}
																		IPhone 5/5s/5SE
																	{elseif $v.code == 'iphone5c'}
																		IPhone 5c
																	{elseif $v.code == 'iphone6/6s' or $v.code == 'iphone6' or $v.code == 'iPhone 6/6s'}
																		IPhone 6/6s
																	{elseif $v.code == 'iphone6plus'}
																		IPhone 6+
																	{else}
																		{$v.code}
																	{/if}
																{/if}
															{/if}
														</label>
													</div>
												</li>
											{/if}
										{/foreach}
									</ul>
								</div>
							</div>
						{/if}

						<!-- ansotov теги -->
						{if $tags}
							<div class="filter-block isset-mobile">
								<div class="spacer" style="margin: 0"></div>
								<div class="block">
									<h3>{translateConst const = TAGS}</h3>
									<ul class="tags">
										{foreach $tags as $k => $v}
											<li>
												<div class="checkbox">
													<input
															type="checkbox" {if $v.checked}checked="checked"{/if}
															class="checkbox" id="tag_{$k}" name="tag[]"
															value="{$v.id}"{if $v.selected} checked{/if}>
													<label for="tag_{$k}">{$v.title}</label>
												</div>
											</li>
										{/foreach}
									</ul>
								</div>
							</div>
						{/if}


						{if $filter_colors|@count > 1}
							<div class="filter-block">
								<div class="spacer"></div>
								<h3>{translateConst const = COLOR}</h3>
								<div id="block_colors" class="block">
									<ul class="colors">
										{foreach $filter_colors as $k=>$v}
											{if $v.name}
												<li>
													<div class="checkbox">
														<input
																type="checkbox" {if $v.checked}checked="checked"{/if}
																class="checkbox" id="color_{$k}" name="color[]"
																value="{$k}">
														<label for="color_{$k}">{$v.name|to_charset:"utf8"}</label>
													</div>
												</li>
											{/if}
										{/foreach}
									</ul>
								</div>
							</div>
						{/if}

					</div>
				</nav>
			</form>
		</aside>
		<script>
            $(".switcher").click(function () {
                block = $(this).data('block')
                if ($(this).hasClass('off')) {
                    $(this).removeClass('off')
                    $("#block_" + block).addClass('selected')
                } else {
                    $(this).addClass('off')
                    $("#block_" + block).removeClass('selected')
                }
            });
			{if !$smarty.request.search}
            $(".filter .checkbox").click(function () {
                $("#filter_form").submit()
            });
			{/if}
		</script>
	{/if}
	{/if}

	{if $smarty.get.search && {$products|@count} > 1}
		<div class="search-informer isset-items">
			{translateConst const = SEARCH_QUERY}
			<b>{$smarty.get.search}</b> {translateConst const = SEARCH_QUERY_ISSET} {($products|@count) - 1} {translateConst const = SEARCH_QUERY_ITEMS}
		</div>
	{elseif $smarty.get.search}
		<div class="search-informer">
			{translateConst const = SEARCH_QUERY}
			<b>{$smarty.get.search}</b> {translateConst const = SEARCH_QUERY_NOT_ISSET}
		</div>
	{/if}

	{if $products|@count > 1}
		{if $tags}
			<div style="height: 20px"></div>
		{/if}
		{include file="new_bloom/ru/info_block.tpl"}
		<section class="products{if strpos($smarty.server.REQUEST_URI, 'sale') == true} all{/if}">
			<ul>{foreach $products as $k=>$v}
				<li class="{if $v@iteration is div by 3}third{/if} {if $v.type=='banner'}banner{/if}">
					{if $v.type=='banner'}
						<a href="/catalog/sale/">
							<img src="/img/banners/sale.jpg">
						</a>
					{else}
						<a href="/new-products/{$v.url}/">
							{if $v.best}
								<img
										alt="{$v.name|@escape}" src="http://printuha.ru/img/girlsinbloom/best.png"
										class="best-img">
							{/if}
							<img
									alt="{$v.name|@escape}" class="front"
									src="/img/front_pictures/{$v.front_picture}.{$v.front_picture_type}">
							{if $v.back_picture!=''}
								<img alt="{$v.name|@escape}" class="back"
									 src="/img/back_pictures/{$v.back_picture}.{$v.back_picture_type}">{/if}
							{if $v.last_size}
								<div
										class="last_size"
										style="opacity: 0.7;background: #fff;position: absolute;margin: -55px 0 0 0;width: 100%;padding: 0 0 11px 0;text-align: center">
									<div class="grid-row low-in-stock" style="margin: 14px 0 0 0">
										<img
												alt="{$v.name|@escape}" src="/img/last_size_clock.png" height="auto"
												width="18">
										<span style="font-size: 11px !important;">{translateConst const = GOODS_QTY_LIMITED}</span>
									</div>
								</div>
							{/if}
						</a>
						<div class="info">
							<h3>{$v.name}</h3>
							<div class="lower">
								{if $v.not_available==0}
									<div class="price listing">
										{if $v.old_price>0}
											<span class="old-price"><del>{$v.old_price|number_format:0:".":" "} <div
															class="fa fa-rub"></div></del></span>
											<span class="new_price">{$v.price|number_format:0:".":" "} <div
														class="fa fa-rub"></div></span>
										{else}
											{$v.price|number_format:0:".":" "}
											<div class="fa fa-rub"></div>
										{/if}
									</div>
									<div style="color: red;font-size: 12px">
										{foreach $v.discount_sizes as $key => $value}
											<div>{$key} - {$value}
												<div class="fa fa-rub"></div> &nbsp;
											</div>
										{/foreach}
									</div>
								{else}
									<div class="not_available">{translateConst const = EXPECTED_RECEIPT}</div>
								{/if}
								<!--<div class="add_to_cart">В Корзину</div>-->
							</div>
						</div>
					{/if}
					</li>{/foreach}</ul>
			{include file="new_bloom/ru/includes/pagination.tpl"}
		</section>
	{/if}
</div>
{literal}
	<script type="text/javascript">
        var height = $('.wrapper').width();
        if (height < 767)
            $('.products li.banner').remove();

	</script>
{/literal}
