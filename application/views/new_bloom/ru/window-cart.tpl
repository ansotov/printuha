<div class="inner-wrapper">
  <ul class="total_sum clearfix">
    {foreach $cart as $item}
      <li style="clear: both;height: 40px;">
          <a href="/new-products/{$item.info.url}/" target="_blank" style="margin: 0 0 0 5px">
            <img alt="{$item.info.name|@escape}" src="/img/front_pictures/{$item.info.front_picture}.{$item.info.front_picture_type}" style="float: left;height: 30px">{$item.info.name} {if $item.material}({$item.material.name}){/if}
          </a>
          <span>x {$item.qty}</span>
      </li>
    {/foreach}
  </ul>
  <div class="total_sum clearfix order-sum">
    <strong>{translateConst const = ORDER_AMOUNT}</strong>
    <span>{$total_sum|number_format:0:".":" "} <i class="fa fa-ruble"></i></span>
  </div>
  <a class="button" href="/checkout/">{translateConst const = GO_TO_CARD}</a>
</div>