<li id="cdek_block">
	<label onclick="check_delivery(5);">
		<div class="delivery_block field">
			<input type="radio" autocomplete="off" name="delivery_type" value="5"
				   id="russian-post"{if $delivery_type == 5} checked{/if}>
			<span class="info-label">
                    <strong>{translateConst const = SDEK}<span> - {if $sdek.price > 0}
							<span>{$sdek.price}</span> {translateConst const = RUB}</span>{else}{translateConst const = FREE}{/if} <span style="color:red;text-transform: initial;font-size:15px">({translateConst const = SELF_PAYMENT_ONLY_ONLINE_PAYMENT})</span></strong>
                    <i><span>{translateConst const = DELIVERY_TIME}
							- до <span>{$sdek.max_days} {if $sdek.max_days == 1}{translateConst const = DAY}{else}{translateConst const = DAYS}{/if}</span></i>
                </span>
		</div>
	</label>
</li>
