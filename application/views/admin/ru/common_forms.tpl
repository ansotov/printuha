<form id="ff" action="" method="post">
<input id="f" type="hidden" name="delete_file" value="" />
</form>
<form id="fp" action="" method="post">
<input id="p" type="hidden" name="delete_photo" value="" />
</form>
<form id="delete_form" action="" method="post">
<input id="delete" type="hidden" name="delete" value="" />
</form>
<form id="fav_form" action="" method="post">
<input id="fav" type="hidden" name="fav" value="" />
</form>
<form id="mp" action="" method="post">
<input id="move_up" name="move_up" type="hidden"  />
<input id="move_down" name="move_down" type="hidden"  />
</form>
{foreach from=$structure item=v key=k}
{if $v.type=='jpg_file' || $v.type=='jcrop' || $v.type=='any_file' || $v.type=='flv_file'}
    <form id="d{$k}" action="" method="post">
    <input type="hidden" name="post" value="1" />
    <input type="hidden" name="delete_{$k}" value="{$id}" />
    </form>
{/if}
{if $v.url_to && $id == 0}
	<script>
	{if $v.url_to && $v.url_lang =='russia'}
    $().ready(function(){
    	$('#{$k}').syncTranslit({
			destination: '{$v.url_to}',
			lang:	'rus'
		});
    });
    {else}
    $().ready(function(){
    	$('#{$k}').syncTranslit({
			destination: '{$v.url_to}'
		});
    });
    {/if}
    </script>
{/if}
{if $v.type == 'string' && $v.colorpicker == 1}
	<script>
    $().ready(function(){
       $(document).ready(function() {
           $('#{$k}').colorpicker();
       });
    })
	</script>
{/if}
{if $v.type =='datetime'}
	<script>
    $().ready(function(){
      $('#{$k}').datetimepicker({
                lang:"ru",
				timeOnlyTitle: 'Выбрать время',
				timeText: 'Время',
				hourText: 'Часы',
				minuteText: 'Минуты',
				secondText: 'Секунды',
				currentText: 'Сейчас',
				closeText: 'Закрыть',
                firstDay:1,
                dateFormat: "dd.mm.yy",
				timeFormat: "HH:mm"
            });
    });
    </script>
{/if}
{if $v.type =='time'}
	<script>
    $().ready(function(){
      $('#{$k}').timepicker({
                lang:"ru",
				timeOnlyTitle: 'Выбрать время',
				timeText: 'Время',
				hourText: 'Часы',
				minuteText: 'Минуты',
				secondText: 'Секунды',
				currentText: 'Сейчас',
				closeText: 'Закрыть',
                firstDay:1,
                dateFormat: "dd.mm.yy",
				timeFormat: "HH:mm"
            });
    });
    </script>
{/if}
{if $v.type =='date'}
	<script>
    $().ready(function(){
      $('#{$k}').datepicker({
                lang:"ru",
                firstDay:1,
                dateFormat: "dd.mm.yy"
            });
    });
    </script>
{/if}

{if $v.type=='map'}
	<script>
    $().ready(function(){
		map_control('{$k}','{$v.x_id}','{$v.y_id}');
    });
    </script>
    <style>
    #map_{$k}{
        position:relative;
        background-image:url("{$v.source}");
        width:{$v.width}px;
        height:{$v.height}px;
    }
    </style>
{/if}

{if $v.type=='ajax_photogallery_insert'}
<!-- PHOTOGALLERY INSERT -->
<script type="text/javascript">
jQuery.fn.bindAll = function(options) {
	var $this = this;
	jQuery.each(options, function(key, val){
		$this.bind(key, val);
	});

	return this;
}
$(function(){
	var photo_folder = '{$v.photo_folder}';
	var listeners = {
		swfuploadLoaded: function(event){
			$('.log', this).append('<li>Loaded</li>');
		},
		fileQueued: function(event, file){
			$('.log', this).append('<li>File queued - '+file.name+'</li>');
			// start the upload once it is queued
			// but only if this queue is not disabled
			if (!$('input[name=disabled]:checked', this).length) {
				$(this).swfupload('startUpload');
			}
		},
		fileQueueError: function(event, file, errorCode, message){
			$('.log', this).append('<li>File queue error - '+message+'</li>');
		},
		fileDialogStart: function(event){
			$('.log', this).append('<li>File dialog start</li>');
		},
		fileDialogComplete: function(event, numFilesSelected, numFilesQueued){
			$('.log', this).append('<li>File dialog complete</li>');
		},
		uploadStart: function(event, file){
			$('.log', this).append('<li>Upload start - '+file.name+'</li>');
			// don't start the upload if this queue is disabled
			if ($('input[name=disabled]:checked', this).length) {
				event.preventDefault();
			}
		},
		uploadProgress: function(event, file, bytesLoaded){
			pixels = Math.round(  (bytesLoaded*300) / file.size  );
			$('#{$k}_progress').width(pixels);
			$('.log', this).append('<li>Upload progress - '+bytesLoaded+'</li>');
		},
		uploadSuccess: function(event, file, serverData){
			$('.progress').width(0);
			$('.log', this).append('<li>Upload success - '+file.name+'</li>');
			$('#{$k}_image_holder').append(serverData);
			$('#{$k}_image_holder').width(110+$('#{$k}_image_holder').width());
		},
		uploadComplete: function(event, file){
			$('.log', this).append('<li>Upload complete - '+file.name+'</li>');
			// upload has completed, lets try the next one in the queue
			// but only if this queue is not disabled
			if (!$('input[name=disabled]:checked', this).length) {
				$(this).swfupload('startUpload');
			}
		},
		uploadError: function(event, file, errorCode, message){
			$('.log', this).append('<li>Upload error - '+message+'</li>');
		}
	};
	$('#ajax_photogallery_{$k}').bindAll(listeners);
	// start the queue if the queue is already disabled
	$('#ajax_photogallery_{$k} input[name=disabled]').click(function(){
		if (!this.checked) {
			$(this).parents('#ajax_photogallery_{$k}').swfupload('startUpload');
		}
	});
	photo_gallery_get_insert('{$k}','{$v.photo_folder}','{$v.link_to_folder}');
	$(".copy_image").click(function(){
		alert('hello')
	})
});

$(function(){
	$('#ajax_photogallery_{$k}').swfupload({
			upload_url :document.location.href,
			file_size_limit : "10240",
			file_types : "*.jpg;*.png",
			file_post_name : 'ajax_photo_{$k}',
			file_types_description : "JPG or PNG image files",
			file_upload_limit : "0",
			flash_url : "/js/swfupload.swf",
			button_image_url : '/img/admin/add_gallery.png',
			button_width : 45,
			post_params  : { 'auth_code':'{$auth_code}','post':1 },
			button_height : 50,
			button_placeholder : $('#gbutton_{$k}', this)[0],
			debug: false
	});

	$( "#{$k}_image_holder" ).sortable({
		revert: false,
		axis: 'x',
		update: function(event, ui) {
			data_up = { 'post':1 };
			data_up['gallery_reorder_{$k}']=$(this).sortable('toArray');
			$.ajax({
				  url: 				document.location.href,
				  dataType: 		'json',
				  type:				'POST',
				  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
				  data:    			data_up,
				  success: 			function(data) {
									}
			});
		}

	});
	$("#{$k}_image_holder").disableSelection();
});
</script>
<!--/// PHOTOGALLERY INSERT -->
{/if}

{if $v.type=='ajax_photogallery'}
<!-- PHOTOGALLERY -->
<script type="text/javascript">

jQuery.fn.bindAll = function(options) {
	var $this = this;
	jQuery.each(options, function(key, val){
		$this.bind(key, val);
	});

	return this;
}
$(function(){
	var photo_folder = '{$v.photo_folder}';
	 listeners = {
		swfuploadLoaded: function(event){
			$('.log', this).append('<li>Loaded</li>');
		},
		fileQueued: function(event, file){
			$('.log', this).append('<li>File queued - '+file.name+'</li>');
			// start the upload once it is queued
			// but only if this queue is not disabled
			if (!$('input[name=disabled]:checked', this).length) {
				$(this).swfupload('startUpload');
			}
		},
		fileQueueError: function(event, file, errorCode, message){
			$('.log', this).append('<li>File queue error - '+message+'</li>');
		},
		fileDialogStart: function(event){
			$('.log', this).append('<li>File dialog start</li>');
		},
		fileDialogComplete: function(event, numFilesSelected, numFilesQueued){
			$('.log', this).append('<li>File dialog complete</li>');
		},
		uploadStart: function(event, file){
			if($("#watermark_{$k}").is(':checked'))
				$watermark = '1';
			else
				$watermark = '0';

			// Check to see if SWFUpload is available
			var swfu = $.swfupload.getInstance('#ajax_photogallery_{$k}');
			swfu.setPostParams({ 'auth_code':'{$auth_code}','post':1, 'watermark_{$k}':$watermark });
			swfu.setDebugEnabled();

			$('.log', this).append('<li>Upload start - '+file.name+'</li>');
			// don't start the upload if this queue is disabled
			if ($('input[name=disabled]:checked', this).length) {
				event.preventDefault();
			}
		},
		uploadProgress: function(event, file, bytesLoaded){
			pixels = Math.round(  (bytesLoaded*300) / file.size  );
			$('#{$k}_progress').width(pixels);
			$('.log', this).append('<li>Upload progress - '+bytesLoaded+'</li>');
		},
		uploadSuccess: function(event, file, serverData){
			$('.progress').width(0);
			$('.log', this).append('<li>Upload success - '+file.name+'</li>');
			$('#{$k}_image_holder').append(serverData);
			$('#{$k}_image_holder').width(110+$('#{$k}_image_holder').width());
		},
		uploadComplete: function(event, file){
			console.log('Complete')
			console.log(file)
			$('.log', this).append('<li>Upload complete - '+file.name+'</li>');
			// upload has completed, lets try the next one in the queue
			// but only if this queue is not disabled
			if (!$('input[name=disabled]:checked', this).length) {
				$(this).swfupload('startUpload');
			}
		},
		uploadError: function(event, file, errorCode, message){

			$('.log', this).append('<li>Upload error - '+message+'</li>');
		}
	};
	$('#ajax_photogallery_{$k}').bindAll(listeners);
	// start the queue if the queue is already disabled
	$('#ajax_photogallery_{$k} input[name=disabled]').click(function(){
		if (!this.checked) {
			$(this).parents('#ajax_photogallery_{$k}').swfupload('startUpload');
		}
	});
	photo_gallery_get('{$k}','{$v.photo_folder}','{$v.link_to_folder}');
});



$(function(){

	/*$('#ajax_photogallery_{$k}').swfupload({
			upload_url :document.location.href,
			file_size_limit : "10240",
			file_types : "*.jpg; *.png",
			file_post_name : 'ajax_photo_{$k}',
			file_types_description : "JPG or PNG image files",
			file_upload_limit : "0",
			flash_url : "/js/swfupload.swf",
			button_image_url : '/img/admin/add_gallery.png',
			button_width : 45,
			post_params  : { 'auth_code':'{$auth_code}','post':1},
			button_height : 50,
			button_placeholder : $('#gbutton_{$k}', this)[0],
			debug: false
	});*/

	$( "#{$k}_image_holder" ).sortable({
		revert: false,
		axis: 'x',
		update: function(event, ui) {
			data_up = { 'post':1 };
			data_up['gallery_reorder_{$k}']=$(this).sortable('toArray');
			$.ajax({
				  url: 				document.location.href,
				  dataType: 		'json',
				  type:				'POST',
				  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
				  data:    			data_up,
				  success: 			function(data) {
									}
			});
		}

	});
	$("#{$k}_image_holder").disableSelection();
});
</script><!--///PHOTOGALLERY-->
{/if}

{if $v.type=='jcrop'}
<script>
	$(function(){
		jcrop_image_init('{$k}',{$v.max_width|default:0},{$v.max_height|default:0},{$v.max_min_width|default:$v.max_width},{$v.max_min_height|default:$v.max_height},'{$v.directory}',{$user_id},'{$rand}',{if $v.from}1{else}0{/if} ,{$v.aspect_ratio|default:1},{$v.max_fit|default:0});
	});
</script>
{/if}

{if $v.type=='ajax_data'}
	<script>
    $().ready(function(){
        {foreach from=$v.folders item=v2 key=k2}
			{if $v2.type == 'image'}
				show_temp_photo('{$k}','{$k2}');
				a_photos('{$k}','{$k2}');
			{/if}
		{/foreach}
    })
    </script>
	<script language="javascript">
	function ajax_{$k}(xml_put,del_id,move_up,move_down){
		if(del_id){
			if(!confirm("delete ?"))
				return;
		}
		data_up = {
						"post":1,
						"ajax_field":"{$k}",
						"del_id":del_id,
						"move_up":move_up,
						"move_down":move_down,
						{if $v.hook}"hook":"{$v.hook}"{/if}
					};

		if(xml_put != 0){
			data_up['xml_put'] = xml_put ;
			{foreach from=$v.folders key=folder_key item=folder}
				data_up["ajax_{$k}_{$folder_key}"] : $("#ajax_{$k}_{$folder_key}").val(),
			{/foreach}
		}

		$.ajax({
			  url: 				document.location.href,
			  dataType: 		'xml',
			  type:				'POST',
			  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
			  data:    			data_up,
			  beforeSend:		function(){
									$("#area_{$k}").html("<div>Загрузка...</div>");
								},
			  success: 			function(xml) {
									x=0;
									if(x==0)$("#area_{$k}").empty();
									$("data",xml).each(function(id){
										data = $("data",xml).get(id);
										string = "<div class=\"block\">"+
												{foreach from=$v.get key=folder_key item=folder}
													$("{$folder}",data).text()+" "+
												{/foreach}
												"<img onclick=\"ajax_{$k}(0,"+$("id",data).text()+")\" src=\"/img/btn_del.png\" class=\"del_bb\"> "+
												"<img onclick=\"ajax_{$k}(0,0,"+$("order_id",data).text()+",0)\" class=\"del_bb\" src=\"/img/ar_up.jpg\"> "+
												"<img onclick=\"ajax_{$k}(0,0,0,"+$("order_id",data).text()+")\" class=\"del_bb\" src=\"/img/ar_down.jpg\"> </div>";

										$("#area_{$k}").append(string);x++;
									});
								},
			  error:			function(){
									$("#area_{$k}").html("");
								}
		});

	}
$(document).ready(function(){
	ajax_{$k}(0);
})
</script>
{/if}
{/foreach}
