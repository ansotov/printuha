<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class common_f {
	private $cyr=array(
    "�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�",
	"�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","X","�","�","�","�",".");
    private $lat=array(
    "sch","sh","ch","ts","yu","ya","zh","a","b","v","g","d","e","e","z","i","j","k","l","m","n","o","p","r","s","t","u","f","h","","y","","e",
	"sch","sh","ch","ts","yu","ya","zh","a","b","v","g","d","e","e","z","i","j","k","l","m","n","o","p","r","s","t","u","f","h","","y","","e","-");
    private $lat_additional=array(
    "W","X","Q","Yo","Ja","Ju","'","`","y");
    private $cyr_additional=array(
    "�","��","�","�","�","�","�","�","�");
	
	
    function __construct(){
		$this->CI =& get_instance();	
		log_message('debug', "common_f Class Initialized");
    }
    function cyr2lat($input){
		$this->CI->load->helper('url');
    	$input=str_replace($this->cyr,$this->lat,$input);
    	$input=url_title($input,'dash');
		return(strtolower($input));
    }
	//////////////////////////////////////////////////////
	//// �������� ����� , ������� ����� �������� �� ���� ������
	///send_mime_mail('������� �������������� ���������','info@rfc-online.ru', '',$row['email'],$headline,$content,'CP1251','KOI8-R');
	function send_mime_mail(
							$name_from, // ��� �����������
							$email_from, // email �����������
							$name_to, // ��� ����������
							$email_to, // email ����������
							$subject, // ���� ������
							$body, // ����� ������
							$data_charset="CP1251", // ��������� ���������� ������
							$send_charset="KOI8-R" // ��������� ������
							) {
	  $to = $this->mime_header_encode($name_to, $data_charset, $send_charset)
					 . ' <' . $email_to . '>';
	  $subject = $this->mime_header_encode($subject, $data_charset, $send_charset);
	  $from =  $this->mime_header_encode($name_from, $data_charset, $send_charset)
						 .' <' . $email_from . '>';
	  if($data_charset != $send_charset) {
		$body = iconv($data_charset, $send_charset, $body);
	  }
	  $headers = "From: $from\r\n";
	  $headers .= "Content-type: text/html; charset=$send_charset\r\n";
	  $headers .= "Mime-Version: 1.0\r\n";
	
	  return mail($to, $subject, $body, $headers);
	}
	private function mime_header_encode($str, $data_charset, $send_charset) {
		  if($data_charset != $send_charset) {
			$str = iconv($data_charset, $send_charset, $str);
		  }
		  return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
	}
	
	///
	//////////////////////////////////////////
	
	
	function prepare_tag_string($str){
		$str = preg_replace('/\s\s+/', ' ', $str);
		$str = preg_replace('/,\s+/', ',', $str);
		$str = preg_replace('/\s,+/', ',', $str);
		$str = preg_replace('/^ /','',$str);
		$str = preg_replace('/ $/','',$str);
		return $str;
	}
    function lat2cyr($input){
     for($i=0;$i<count($this->lat_additional);$i++){
       $current_cyr=$this->cyr_additional[$i];
       $current_lat=$this->lat_additional[$i];
       $input=str_replace($current_lat,$current_cyr,$input);
       $input=str_replace(strtolower($current_lat),strtolower($current_cyr),$input);
     }
     for($i=0;$i<count($this->lat);$i++){
       $current_cyr=$this->cyr[$i];
       $current_lat=$this->lat[$i];
       $input=str_replace($current_lat,$current_cyr,$input);
       $input=str_replace(strtolower($current_lat),strtolower($current_cyr),$input);
     }
    return($input);
    }
	function correct_sql_date($date){
		if(preg_match("/^([0-9]{2}).([0-9]{2}).([0-9]{4})$/",$date,$matches) && checkdate($matches[2],$matches[1],$matches[3])){
			return $matches[3]."-".$matches[2]."-".$matches[1];
		}else
			return false;
	}
	function browser_info($agent=null) {
	  // Declare known browsers to look for
	  $known = array('msie', 'firefox', 'safari', 'webkit', 'opera', 'netscape',
		'konqueror', 'gecko');
	
	  // Clean up agent and build regex that matches phrases for known browsers
	  // (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	  // version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	  $agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	  $pattern = '#(?<browser>' . join('|', $known) .
		')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	  // Find all phrases (or return empty array if none found)
	  if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	  // Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	  // Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	  // in the UA).  That's usually the most correct.
	  $i = count($matches['browser'])-1;
	  return array($matches['browser'][$i] => $matches['version'][$i]);
	}
	
	function genRandomString($length) {
		$characters = "0123456789abcdefghijklmnopqrstuvwxyz";
		$len=strlen($characters)-1;
		$string = '';    
		for ($p = 0; $p < $length; $p++) {
			$letter=mt_rand(0,$len);
			$string .= $characters[$letter];
		}
		return $string;
	}
	
	
	 function pages($count,$n=1,$per_page){
		$all=$count;
		//���c��� ���������� �������
		$n_pages = round($all/$per_page,0);
		if($all>($n_pages*$per_page)) $n_pages++;
		if ($n_pages == 0) $n_pages=1;
		$x=1;
		//�������� ������� ��� �������� �������.
		$pages_array = array();
		if($n_pages<10){
			for($i=0;$i<$n_pages;$i++)
			{
				$pages_array[$i]=$x;
				$x++;
			}
		}
		else{
			$x=0;
			if($n<=4){
				$n_pages<6?$max_v=$n_pages:$max_v=8;
				for($i=1;$i<=$n+2;$i++)
				{
					$pages_array[$x]=$i;
					$x++;
				}
				$pages_array[$x+1]='...';
				$pages_array[$x+2]=$n_pages;
			}
			if($n>4 && $n<$n_pages-4){
				$pages_array[$x]='1';
				$pages_array[$x+1]='...';
				$x=$x+2;
				for($i=$n-3;$i<=$n+2;$i++)
				{
					$pages_array[$x]=$i;
					$x++;
				}
				$pages_array[$x+1]='...';
				$pages_array[$x+2]=$n_pages;
			}
			if($n>=$n_pages-4){
				$pages_array[$x]='1';
				$pages_array[$x+1]='...';
				$x=$x+2;
				for($i=$n-2;$i<=$n_pages;$i++)
				{
					$pages_array[$x]=$i;
					$x++;
				}
			}
				
		}

		//����� �������� ���������� � ��������� �������
		if($n-1>0 && $n<=$n_pages)
			$return_array['back_page']=($n-1);
		if($n+1<=$n_pages && $n+1>0)
			$return_array['foward_page']=($n+1);
		
		
		$return_array['pages_array']=$pages_array;
		
		return $return_array;
	}
	
	
	function strip_attributes($msg, $tag, $attr=array(), $suffix = "")
	{
		 $lengthfirst = 0;
		 while (strstr(substr($msg, $lengthfirst), "<$tag ") != "") {
			  $tag_start = $lengthfirst + strpos(substr($msg, $lengthfirst), "<$tag ");
	
			  $partafterwith = substr($msg, $tag_start);
	
			  $img = substr($partafterwith, 0, strpos($partafterwith, ">") + 1);
			  $img = str_replace(" =", "=", $img);
	
			  $out = "<$tag";
			  for($i = 0; $i < count($attr); $i++) {
				   if (empty($attr[$i])) {
						continue;
				   }
				   $long_val =
				   (strpos($img, " ", strpos($img, $attr[$i] . "=")) === false) ?
				   strpos($img, ">", strpos($img, $attr[$i] . "=")) - (strpos($img, $attr[$i] . "=") + strlen($attr[$i]) + 1) :
				   strpos($img, " ", strpos($img, $attr[$i] . "=")) - (strpos($img, $attr[$i] . "=") + strlen($attr[$i]) + 1);
				   $val = substr($img, strpos($img, $attr[$i] . "=") + strlen($attr[$i]) + 1, $long_val);
				   if (!empty($val)) {
						$out .= " " . $attr[$i] . "=" . $val;
				   }
			  }
			  if (!empty($suffix)) {
				   $out .= " " . $suffix;
			  }
	
			  $out .= ">";
			  $partafter = substr($partafterwith, strpos($partafterwith, ">") + 1);
			  $msg = substr($msg, 0, $tag_start) . $out . $partafter;
			  $lengthfirst = $tag_start + 3;
		 }
		 return $msg;
	}

}

?>