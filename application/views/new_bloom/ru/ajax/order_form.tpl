
<!--<div class="long_line">
    <div class="name">{translateConst const = YOUR_DATA_FOR_ORDER}</div>
</div>-->

<!--ansotov полная информация по pickpoint {$pickpoint_info|print_r}-->
<div class="left-column">
    <fieldset >
        <p id="btype1" class="field">
            {if $errors.type}<label class="error">{$errors.type}</label>{/if}
        </p>
        <div data-hook="billing_inner" class="inner order-form">

            <input name="user_id" value="{$user.id}" type="hidden">

            <p id="bfirstname" class="field">
                <strong>
                    <label for="first_name">{translateConst const = NAME}, {translateConst const = SURNAME}</label>
                </strong>
                <input class="save_data" value="{$user.first_name}" type="text" size="30" name="first_name" id="first_name">
                {if $errors.first_name}<label class="error">{$errors.first_name}</label>{/if}
            </p>

            <!--<p id="blastname" class="field">
                <strong>
                    <label for="last_name">{translateConst const = SURNAME}</label> <span class="required-label">*</span>
                </strong>
                <input class="save_data" type="text" value="{$user.last_name}" size="30" name="last_name" id="last_name" required>
                {if $errors.last_name}<label class="error">{$errors.last_name}</label>{/if}
            </p>-->


            <p id="bphone" class="field">
                <strong>
                    <label for="{if $foreign_lands != 1}{else}f_{/if}phone">{translateConst const = PHONE}</label> <span class="required-label">*</span>
                </strong>
                <input class="save_data" type="text" value="{$user.phone}" size="30" name="phone" id="{if $foreign_lands != 1}{else}f_{/if}phone" required>
                {if $errors.phone}<label class="error">{$errors.phone}</label>{/if}
            </p>


            <p id="bphone" class="field">
                <strong>
                    <label for="email">Email</label> <span class="required-label">*</span>
                </strong>
                <input class="save_data" type="email" value="{$user.email}" size="30" name="email" id="email" required>
                {if $errors.email}<label class="error">{$errors.email}</label>{/if}
            </p>

            {if $delivery_type != 1}
                {if $pickpoint_info}
                    <p id="bbcity" class="field">
                        <!--<strong>
                            <label>{translateConst const = ADDRESS}</label> <span class="required-label">*</span>
                        </strong>-->
                        PICKPOINT: {$pickpoint_info.region} г.{$pickpoint_info.cityname}, {$pickpoint_info.shortaddress}

                        <input id="pickpoint_id" type="hidden" name="pickpoint_address" value="{$pickpoint_info.region} г.{$pickpoint_info.cityname}, {$pickpoint_info.shortaddress}">
                        <input id="pickpoint_city" type="hidden" name="pickpoint_city" value="{$pickpoint_info.cityname}">
                        <input id="pickpoint_region" type="hidden" name="pickpoint_region" value="{$pickpoint_info.region}">
                    </p>
                {else}
                    <p id="baddress1" class="field">
                        <strong>
                            <label for="attr_address">{translateConst const = ADDRESS}</label><!-- <span class="required-label">*</span>-->
                            {if $foreign_lands == 1}
                                <br><span style="font-size: 10px;text-transform: none">( {translateConst const = COUNTRY_CITY_ADDRESS} )</span>
                            {/if}
                        </strong>
                        {if $foreign_lands == 1}
                            <input type="hidden" name="foreign_lands" value="1">
                            <textarea class="save_data" rows="10" name="address" id="attr_address" cols="20" style="height: 50px;"></textarea>
                        {else}
                            <input type="text" class="save_data" name="address" id="attr_address">
                            <!--<input type="text" value="{$user.address.flat}" size="30" placeholder="{translateConst const = FLAT_OFFICE}" name="flat"  class="flat required save_data">
                            <input type="text" value="{$user.address.house}" size="30" placeholder="{translateConst const = HOUSE}" name="house" class="house required save_data" required>
                            <input type="text" value="{$user.address.street}" placeholder="{translateConst const = STREET}" size="30" name="street" id="attr_address" class="addr save_data" required>
                            {if $errors.flat || $errors.house || $errors.street}
                                <label class="error">
                                    {if $errors.street}{$errors.street}{/if}
                                    {if $errors.house}<br>{$errors.house}{/if}
                                    {if $errors.flat}<br>{$errors.flat}{/if}
                                </label>
                            {/if}-->
                        {/if}
                    </p>
                {/if}
            {/if}

            <!--{if $delivery_type == 4 and $foreign_lands != 1}
                <p id="baddress2" class="field">
                    <strong>
                        <label for="zipcode">{translateConst const = INDEX}</label> <span class="required-label">*</span>
                    </strong>
                    <input class="save_data" type="text" value="{$user.address.zipcode}" size="30" name="zipcode" id="zipcode" required>
                    {if $errors.zipcode}<label class="error">{$errors.zipcode}</label>{/if}
                </p>
            {/if}-->

            <p class="payment_type field">
                <strong>
                    <label>{translateConst const = PAYMENT_TYPE}</label> <span class="required-label">*</span>
                </strong>

                {if ($delivery_type == 2 || !$delivery_type) and $onlyOnlinePayment != 1}
                    <label onclick="onlinePayment(0);">
                    <span class="area selector" id="courier_payment">
                        <input type="radio" name="payment_type" value="courier" required/> {translateConst const = UPON_RECEIPT}
                    </span>
                    </label>
                {/if}

                <label onclick="onlinePayment(1);">
                    <span class="area selector" id="online_payment">
                        <input type="radio" name="payment_type" value="online" required/> {translateConst const = ONLINE} <span style="color: red;text-transform: lowercase"">- {translateConst const = DISCOUNT} 3%</span>
                    </span>
                </label>
                <!--<label onclick="onlinePayment(1);">
                    <span class="area selector" id="paypal_payment">
                        <input type="radio" name="payment_type" value="paypal" required/> PayPal <span style="color: red;text-transform: lowercase"">- {translateConst const = DISCOUNT} 5%</span>
                    </span>
                </label>-->
            {if $errors.payment_type}<p class="selector"><label class="error">{$errors.payment_type}</label></p>{/if}
            </p>

            <p id="order-comment" class="field">
                <strong>
                    <label for="comment">{translateConst const = COMMENT}</label>
                </strong>
                <textarea class="save_data" rows="20" name="comments" id="comment" cols="40">{$postdata.comments}</textarea>
                {if $errors.comments}<label class="error">{$errors.comments}</label>{/if}
            </p>

            <p id="baddress2" class="field">
                <div class="required-fields">{translateConst const = ALL_FIELDS_NEED_FOR_DATA}</div>
            </p>
        </div>
    </fieldset>
</div>
<script type="text/javascript">

    $(".save_data").change(function () {
        var value = $(this).val();
        value = (value) ? value : $(this).html();
        var name = $(this).attr('name');

        $('input[name="data_' + name + '"]').val(value);

    }).keyup();


    {literal}
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }
    {/literal}
    $(document).ready(function(e) {

        $('form').on('submit', function (e) {
            let email = $('input[type=email]').val();

            var valid = isValidEmailAddress(email);

            {literal}
            if (valid == false) {
                e.preventDefault();
                $('input[name=email]').css({"border": "1px solid red"}).focus();
                $('html,body').stop().animate({ scrollTop: $('input[name=email]').offset().top - 90 }, 500);
            }
            {/literal}
        });

        if (!$('input[name=user_id]'). val()) {
            $('input[name="first_name"]').val($('input[name="data_first_name"]').val());
            $('input[name="last_name"]').val($('input[name="data_last_name"]').val());
            $('input[name="phone"]').val($('input[name="data_phone"]').val());
            $('input[name="email"]').val($('input[name="data_email"]').val());
            $('input[name="street"]').val($('input[name="data_street"]').val());
            $('input[name="house"]').val($('input[name="data_house"]').val());
            $('input[name="flat"]').val($('input[name="data_flat"]').val());
            $('input[name="zipcode"]').val($('input[name="data_zipcode"]').val());
        }

        $('textarea[name="comments"]').html($('input[name="data_comments"]').val());

        //ansotov маска для номера телефона
        $.mask.definitions['X'] = '[01234569]';
        $('#phone').mask('+7 (X99) 999-99-99');
    });
</script>