{if $courier_city}
	<li id="courier_block">
		<label onclick="check_delivery(2);">
			<!--<div class="picture">
					<img alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}" src="/img/girlsinbloom/boxes_car.jpg">
				</div>-->
			<div class="delivery_block field">
				<input type="radio" autocomplete="off" name="delivery_type" id="courier-radio"
					   value="2"{if $delivery_type == 2} checked{/if}>
				<span class="info-label">
                        <strong>{translateConst const = COURIER}<span> - {if $courier_city[0].price > 0}
								<span>{$courier_city[0].price}</span> {translateConst const = RUB}</span>{elseif !$courier_city[0]}уточняйте у оператора{else}{translateConst const = FREE}{/if}</strong>
						{if $courier_city[0].name == 'Москва' || $courier_city[0].name == 'Санкт-Петербург'}
							<i><span>{translateConst const = DELIVERY_TIME}
									- <span>2-3</span> {translateConst const = DAYS}</span></i>
						{else}
							<i><span>{translateConst const = DELIVERY_TIME} - {if $courier_city[0]}
									<span>до {$courier_city[0].max_days} {if $courier_city[0].max_days == 1}{translateConst const = DAY}{else}{translateConst const = DAYS}{/if}</span>{else}
									уточняйте у оператора{/if}</i>
						{/if}
                    </span>
			</div>
		</label>
	</li>
{/if}
