<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$folder_name}{if $title_name}{$title_name} - {/if}Nome gold - Admin panel</title>
<link href="/css/rainadmin.css" rel="stylesheet" type="text/css" />
<script src="/js/jquery-1.7.1.min.js"></script>
<script src="/js/jquery-ui-1.8.9.custom.min.js"></script>
<script src="/js/jquery.synctranslit.min.js"></script>
<!-- FANCYBOX -->
<script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<script src="/js/ajaxupload.js"></script>
<link href="/css/custom-theme/jquery-ui-1.8.9.custom.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tinymce/jscripts/tiny_mce/tiny_mce.js',
			// General options
			theme : "advanced",
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			
			relative_urls : false,
			
			content_css : "/css/nome.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			},
			
        	plugins : "paste,media",
			paste_use_dialog : true,
			paste_convert_middot_lists : false,
			paste_unindented_list_class : "unindentedList",
			paste_convert_headers_to_strong : true,
			//paste_strip_class_attributes:"all",
			paste_remove_spans:true,
			paste_remove_styles:true,
			//invalid_elements: "span",
			extended_valid_elements : "div[*],h1,h2,h3,h4,h5,p,</br>,table[cellpadding|cellspacing|width|border],td,tr,thead,tbody,iframe[src|width|height|name|align],object[classid|codebase|width|height|align|type|data],param[id|name|type|value|valuetype<DATA?OBJECT?REF]"
		});
		
		$("a.fancybox").fancybox({
			 'width' : '90%',
			 'height' : '90%',
			 'autoScale' : false,
			 'transitionIn' : 'none',
			 'transitionOut' : 'none',
			 'type' : 'iframe'
     	});
	});
function toggle_prop($sw){
	if( $sw ==1 ){
		$('.large_slider').show();
		$('.slider').hide();
	}else{
		$('.large_slider').hide();
		$('.slider').show();
	}
}
</script>
<script type="text/javascript" src="/js/swfupload.js"></script>
<script type="text/javascript" src="/js/jquery.swfupload.js"></script>
<link rel="stylesheet" href="/css/jquery.Jcrop.css" type="text/css" media="screen" />
<script type="text/javascript"src="/js/jquery.Jcrop.min.js"></script>
<!-- /TinyMCE -->
</head>
<body>
<div class="large_slider">
	<a onclick="toggle_prop(0);" id="close">Close</a>
    <div class="topMenu">
    <a class="fancybox" href="/{$dir}/admin_users/1/">Users</a>
    <a class="fancybox" href="/{$dir}/main_page/">Home page options</a></div>
</div>
<div class="slider">
	<div class="user_name">User : {$admin_user} / <a href="/{$dir}/logout/">Log out</a></div>
    <a onclick="toggle_prop(1);">Settings</a>
</div>
<table class="table" width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td class="left_panel" width="240" valign="top"><div class="menu">{include file="admin/menu.tpl"}</div></td>
    <td valign="top"><div class="main_part">{if $load_page!=''}{include file="$load_page.tpl"}{/if}</div></td>
</tr>
</table>
</body>
</html>