<xml_table>
{if $output_rows}
{foreach from=$output_rows item=v}
<row_data>
	<order_id><![CDATA[{$v.order_id}]]></order_id>
	{if $hide_id}<id>{$v.id}</id>{/if}
    <options><![CDATA[<a href="/{$dir}/{$links.link.link}/{if $v.id}{$v.id}/{/if}{$end_link}/" class="option"><i title="Редактировать" class="fa fa-pencil"></i></a><a  onClick="delete_o({$v.id})" class="option"><i class="fa fa-times-circle"></i> </a>]]></options>
{foreach from=$select_names key=sk item=sv}
    {assign var='field_value' value=$v[$sk]}
    <{$sk} width="{$sv.width}"><![CDATA[{strip}
    {if $sv.type=='string'}{$field_value}{/if}
    {if $sv.type=='integer'}{$field_value}{/if}
    {if $sv.type=='date'}{$field_value}{/if}
    {if $sv.type=='datetime'}{$field_value}{/if}
    {if $sv.type=='time'}{$field_value}{/if}
    {if $sv.type=='text'}{$field_value}{/if}
    {if $sv.type=='ajax_data'}{$field_value}{/if}
    {if $sv.type=='jpg_file'}<a href="/{$sv.directory}{$field_value}.jpg"><img src="/{$sv.directory}{$field_value}{if !$v.max_size_thumb}_small{/if}.jpg" /></a>{/if}
    {if $sv.type=='jcrop'}<a href="/{$sv.directory}{$field_value.value}.{$field_value.image_type}"><img src="/{$sv.directory}{$field_value.value}{if !$v.max_size_thumb}_small{/if}.{$field_value.image_type}" /></a>{/if}
    {if $sv.type=='any_file'}<a target="_blank" href="/{$sv.directory}{$field_value}">Ссылка на файл</a>{/if}
    {if $sv.type=='flv_file'}<a href="/{$sv.directory}{$field_value}.flv">file</a>{/if}
    {if $sv.type=='list'}{$sv.values[$field_value]}{/if}
    {if $sv.type=='model_list'}{$sv.values[$field_value]}{/if}
    {if $sv.type=='checkbox'}<input onclick="aj_checkbox(this,'{$sk}','{$v.id}', '{$v.product_id}')" type="checkbox"{if $field_value==1} checked="checked"{/if} />{/if}
    {strip}]]></{$sk}>
{/foreach}
</row_data>
{/foreach}
{/if}
<total>
	{$total_rows}
</total>
<pages><![CDATA[<div class="leftTxt">Страницы :</div>
{foreach from=$pages_info.pages_array item=i}
    {if $i == '...'}
        <div class="normal">...</div>
    {else}
        {if $i <> $page}
         <div class="normal"><a onclick="to_page({$i});">{$i}</a></div>
        {else}
           <div class="selected">{$page}</div>
        {/if}
    {/if}
{/foreach}
{if $pages_info.back_page}
    <div class="normal"><a  onclick="to_page({$pages_info.back_page});" ><</a></div>
{/if}
{if $pages_info.foward_page}
    <div class="normal"><a   onclick="to_page({$pages_info.foward_page});"  >></a></div>
{/if}]]></pages>
</xml_table>
