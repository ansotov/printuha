<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
	  xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!-- NAME: 1 COLUMN -->
	<!--[if gte mso 15]>
	<xml>
		<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Заказ на сайте Girls in bloom.</title>

	<style type="text/css">
		p {
			margin: 10px 0;
			padding: 0;
		}

		table {
			border-collapse: collapse;
		}

		h1, h2, h3, h4, h5, h6 {
			display: block;
			margin: 0;
			padding: 0;
		}

		img, a img {
			border: 0;
			height: auto;
			outline: none;
			text-decoration: none;
		}

		body, #bodyTable, #bodyCell {
			height: 100%;
			margin: 0;
			padding: 0;
			width: 100%;
		}

		#outlook a {
			padding: 0;
		}

		img {
			-ms-interpolation-mode: bicubic;
		}

		table {
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		.ReadMsgBody {
			width: 100%;
		}

		.ExternalClass {
			width: 100%;
		}

		p, a, li, td, blockquote {
			mso-line-height-rule: exactly;
		}

		a[href^=tel], a[href^=sms] {
			color: inherit;
			cursor: default;
			text-decoration: none;
		}

		p, a, li, td, body, table, blockquote {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}

		.ExternalClass, .ExternalClass p, .ExternalClass td, .ExternalClass div, .ExternalClass span, .ExternalClass font {
			line-height: 100%;
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}

		#bodyCell {
			padding: 10px;
		}

		.templateContainer {
			max-width: 600px !important;
		}

		a.mcnButton {
			display: block;
		}

		.mcnImage {
			vertical-align: bottom;
		}

		.mcnTextContent {
			word-break: break-word;
		}

		.mcnTextContent img {
			height: auto !important;
		}

		.mcnDividerBlock {
			table-layout: fixed !important;
		}

		/*
		@tab Page
		@section Background Style
		@tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
		*/
		body, #bodyTable {
			/*@editable*/
			background-color: #FAFAFA;
		}

		/*
		@tab Page
		@section Background Style
		@tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
		*/
		#bodyCell {
			/*@editable*/
			border-top: 0;
		}

		/*
		@tab Page
		@section Email Border
		@tip Set the border for your email.
		*/
		.templateContainer {
			/*@editable*/
			border: 0;
		}

		/*
		@tab Page
		@section Heading 1
		@tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
		@style heading 1
		*/
		h1 {
			/*@editable*/
			color: #202020;
			/*@editable*/
			font-family: Helvetica;
			/*@editable*/
			font-size: 26px;
			/*@editable*/
			font-style: normal;
			/*@editable*/
			font-weight: bold;
			/*@editable*/
			line-height: 125%;
			/*@editable*/
			letter-spacing: normal;
			/*@editable*/
			text-align: left;
		}

		/*
		@tab Page
		@section Heading 2
		@tip Set the styling for all second-level headings in your emails.
		@style heading 2
		*/
		h2 {
			/*@editable*/
			color: #202020;
			/*@editable*/
			font-family: Helvetica;
			/*@editable*/
			font-size: 22px;
			/*@editable*/
			font-style: normal;
			/*@editable*/
			font-weight: bold;
			/*@editable*/
			line-height: 125%;
			/*@editable*/
			letter-spacing: normal;
			/*@editable*/
			text-align: left;
		}

		/*
		@tab Page
		@section Heading 3
		@tip Set the styling for all third-level headings in your emails.
		@style heading 3
		*/
		h3 {
			/*@editable*/
			color: #202020;
			/*@editable*/
			font-family: Helvetica;
			/*@editable*/
			font-size: 20px;
			/*@editable*/
			font-style: normal;
			/*@editable*/
			font-weight: bold;
			/*@editable*/
			line-height: 125%;
			/*@editable*/
			letter-spacing: normal;
			/*@editable*/
			text-align: left;
		}

		/*
		@tab Page
		@section Heading 4
		@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
		@style heading 4
		*/
		h4 {
			/*@editable*/
			color: #202020;
			/*@editable*/
			font-family: Helvetica;
			/*@editable*/
			font-size: 18px;
			/*@editable*/
			font-style: normal;
			/*@editable*/
			font-weight: bold;
			/*@editable*/
			line-height: 125%;
			/*@editable*/
			letter-spacing: normal;
			/*@editable*/
			text-align: left;
		}

		/*
		@tab Preheader
		@section Preheader Style
		@tip Set the background color and borders for your email's preheader area.
		*/
		#templatePreheader {
			/*@editable*/
			background-color: #FAFAFA;
			/*@editable*/
			border-top: 0;
			/*@editable*/
			border-bottom: 0;
			/*@editable*/
			padding-top: 9px;
			/*@editable*/
			padding-bottom: 9px;
		}

		/*
		@tab Preheader
		@section Preheader Text
		@tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
		*/
		#templatePreheader .mcnTextContent, #templatePreheader .mcnTextContent p {
			/*@editable*/
			color: #656565;
			/*@editable*/
			font-family: Helvetica;
			/*@editable*/
			font-size: 12px;
			/*@editable*/
			line-height: 150%;
			/*@editable*/
			text-align: left;
		}

		/*
		@tab Preheader
		@section Preheader Link
		@tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
		*/
		#templatePreheader .mcnTextContent a, #templatePreheader .mcnTextContent p a {
			/*@editable*/
			color: #656565;
			/*@editable*/
			font-weight: normal;
			/*@editable*/
			text-decoration: underline;
		}

		/*
		@tab Header
		@section Header Style
		@tip Set the background color and borders for your email's header area.
		*/
		#templateHeader {
			/*@editable*/
			background-color: #FFFFFF;
			/*@editable*/
			border-top: 0;
			/*@editable*/
			border-bottom: 0;
			/*@editable*/
			padding-top: 9px;
			/*@editable*/
			padding-bottom: 0;
		}

		/*
		@tab Header
		@section Header Text
		@tip Set the styling for your email's header text. Choose a size and color that is easy to read.
		*/
		#templateHeader .mcnTextContent, #templateHeader .mcnTextContent p {
			/*@editable*/
			color: #202020;
			/*@editable*/
			font-family: Helvetica;
			/*@editable*/
			font-size: 16px;
			/*@editable*/
			line-height: 150%;
			/*@editable*/
			text-align: left;
		}

		/*
		@tab Header
		@section Header Link
		@tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
		*/
		#templateHeader .mcnTextContent a, #templateHeader .mcnTextContent p a {
			/*@editable*/
			color: #2BAADF;
			/*@editable*/
			font-weight: normal;
			/*@editable*/
			text-decoration: underline;
		}

		/*
		@tab Body
		@section Body Style
		@tip Set the background color and borders for your email's body area.
		*/
		#templateBody {
			/*@editable*/
			background-color: #FFFFFF;
			/*@editable*/
			border-top: 0;
			/*@editable*/
			border-bottom: 2px solid #EAEAEA;
			/*@editable*/
			padding-top: 0;
			/*@editable*/
			padding-bottom: 9px;
		}

		/*
		@tab Body
		@section Body Text
		@tip Set the styling for your email's body text. Choose a size and color that is easy to read.
		*/
		#templateBody .mcnTextContent, #templateBody .mcnTextContent p {
			/*@editable*/
			color: #202020;
			/*@editable*/
			font-family: Helvetica;
			/*@editable*/
			font-size: 16px;
			/*@editable*/
			line-height: 150%;
			/*@editable*/
			text-align: left;
		}

		/*
		@tab Body
		@section Body Link
		@tip Set the styling for your email's body links. Choose a color that helps them stand out from your text.
		*/
		#templateBody .mcnTextContent a, #templateBody .mcnTextContent p a {
			/*@editable*/
			color: #2BAADF;
			/*@editable*/
			font-weight: normal;
			/*@editable*/
			text-decoration: underline;
		}

		/*
		@tab Footer
		@section Footer Style
		@tip Set the background color and borders for your email's footer area.
		*/
		#templateFooter {
			/*@editable*/
			background-color: #FAFAFA;
			/*@editable*/
			border-top: 0;
			/*@editable*/
			border-bottom: 0;
			/*@editable*/
			padding-top: 9px;
			/*@editable*/
			padding-bottom: 9px;
		}

		/*
		@tab Footer
		@section Footer Text
		@tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
		*/
		#templateFooter .mcnTextContent, #templateFooter .mcnTextContent p {
			/*@editable*/
			color: #656565;
			/*@editable*/
			font-family: Helvetica;
			/*@editable*/
			font-size: 12px;
			/*@editable*/
			line-height: 150%;
			/*@editable*/
			text-align: center;
		}

		/*
		@tab Footer
		@section Footer Link
		@tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
		*/
		#templateFooter .mcnTextContent a, #templateFooter .mcnTextContent p a {
			/*@editable*/
			color: #656565;
			/*@editable*/
			font-weight: normal;
			/*@editable*/
			text-decoration: underline;
		}

		@media only screen and (min-width: 768px) {
			.templateContainer {
				width: 600px !important;
			}

		}

		@media only screen and (max-width: 480px) {
			body, table, td, p, a, li, blockquote {
				-webkit-text-size-adjust: none !important;
			}

		}

		@media only screen and (max-width: 480px) {
			body {
				width: 100% !important;
				min-width: 100% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			#bodyCell {
				padding-top: 10px !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnImage {
				width: 100% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnCaptionTopContent, .mcnCaptionBottomContent, .mcnTextContentContainer, .mcnBoxedTextContentContainer, .mcnImageGroupContentContainer, .mcnCaptionLeftTextContentContainer, .mcnCaptionRightTextContentContainer, .mcnCaptionLeftImageContentContainer, .mcnCaptionRightImageContentContainer, .mcnImageCardLeftTextContentContainer, .mcnImageCardRightTextContentContainer {
				max-width: 100% !important;
				width: 100% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnBoxedTextContentContainer {
				min-width: 100% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnImageGroupContent {
				padding: 9px !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnCaptionLeftContentOuter .mcnTextContent, .mcnCaptionRightContentOuter .mcnTextContent {
				padding-top: 9px !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnImageCardTopImageContent, .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
				padding-top: 18px !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnImageCardBottomImageContent {
				padding-bottom: 9px !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnImageGroupBlockInner {
				padding-top: 0 !important;
				padding-bottom: 0 !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnImageGroupBlockOuter {
				padding-top: 9px !important;
				padding-bottom: 9px !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnTextContent, .mcnBoxedTextContentColumn {
				padding-right: 18px !important;
				padding-left: 18px !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcnImageCardLeftImageContent, .mcnImageCardRightImageContent {
				padding-right: 18px !important;
				padding-bottom: 0 !important;
				padding-left: 18px !important;
			}

		}

		@media only screen and (max-width: 480px) {
			.mcpreview-image-uploader {
				display: none !important;
				width: 100% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Heading 1
			@tip Make the first-level headings larger in size for better readability on small screens.
			*/
			h1 {
				/*@editable*/
				font-size: 22px !important;
				/*@editable*/
				line-height: 125% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Heading 2
			@tip Make the second-level headings larger in size for better readability on small screens.
			*/
			h2 {
				/*@editable*/
				font-size: 20px !important;
				/*@editable*/
				line-height: 125% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Heading 3
			@tip Make the third-level headings larger in size for better readability on small screens.
			*/
			h3 {
				/*@editable*/
				font-size: 18px !important;
				/*@editable*/
				line-height: 125% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Heading 4
			@tip Make the fourth-level headings larger in size for better readability on small screens.
			*/
			h4 {
				/*@editable*/
				font-size: 16px !important;
				/*@editable*/
				line-height: 150% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Boxed Text
			@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
			*/
			.mcnBoxedTextContentContainer .mcnTextContent, .mcnBoxedTextContentContainer .mcnTextContent p {
				/*@editable*/
				font-size: 14px !important;
				/*@editable*/
				line-height: 150% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Preheader Visibility
			@tip Set the visibility of the email's preheader on small screens. You can hide it to save space.
			*/
			#templatePreheader {
				/*@editable*/
				display: block !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Preheader Text
			@tip Make the preheader text larger in size for better readability on small screens.
			*/
			#templatePreheader .mcnTextContent, #templatePreheader .mcnTextContent p {
				/*@editable*/
				font-size: 14px !important;
				/*@editable*/
				line-height: 150% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Header Text
			@tip Make the header text larger in size for better readability on small screens.
			*/
			#templateHeader .mcnTextContent, #templateHeader .mcnTextContent p {
				/*@editable*/
				font-size: 16px !important;
				/*@editable*/
				line-height: 150% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Body Text
			@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
			*/
			#templateBody .mcnTextContent, #templateBody .mcnTextContent p {
				/*@editable*/
				font-size: 16px !important;
				/*@editable*/
				line-height: 150% !important;
			}

		}

		@media only screen and (max-width: 480px) {
			/*
			@tab Mobile Styles
			@section Footer Text
			@tip Make the footer content text larger in size for better readability on small screens.
			*/
			#templateFooter .mcnTextContent, #templateFooter .mcnTextContent p {
				/*@editable*/
				font-size: 14px !important;
				/*@editable*/
				line-height: 150% !important;
			}

		}</style>
</head>
<body>
<center style="max-width:900px;margin:0 auto;display:block;">
	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
		<tr>
			<td align="center" valign="top" id="bodyCell">
				<!-- BEGIN TEMPLATE // -->
				<!--[if gte mso 9]>
				<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
					<tr>
						<td align="center" valign="top" width="600" style="width:600px;">
				<![endif]-->
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock"
					   style="min-width:100%;">
					<tbody class="mcnImageBlockOuter">
					<tr>
						<td valign="top" style="padding:9px" class="mcnImageBlockInner">
							<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0"
								   class="mcnImageContentContainer" style="min-width:100%;">
								<tbody>
								<tr>
									<td class="mcnImageContent" valign="top"
										style="padding-right: 9px; padding-left: 9px; padding-top: 15px; padding-bottom: 0; text-align:center;">

										<a href="http://printuha.ru" title="" class="" target="_blank">
											<img align="center" alt=""
												 src="http://printuha.ru/img/logo.jpg"
												 width="231"
												 style="max-width:700px; padding-bottom: 0; display: inline !important; vertical-align: bottom;"
												 class="mcnImage">
										</a>

									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
					<tr>
						<td valign="top" id="templatePreheader"></td>
					</tr>
					<tr>
						<td valign="top" id="templateBody">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
								   style="min-width:100%;">
								<tbody class="mcnTextBlockOuter">
								<tr>
									<td valign="top" class="mcnTextBlockInner"
										style="padding-top:25px;text-align: center">
										<!--[if mso]>
										<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
											   style="width:100%;">
											<tr>
										<![endif]-->

										<!--[if mso]>
										<td valign="top" width="600" style="width:600px;">
										<![endif]-->
										<table align="left" border="0" cellpadding="0" cellspacing="0"
											   style="max-width:100%; min-width:100%;" width="100%"
											   class="mcnTextContentContainer">
											<tbody>
											<tr>

												<td valign="top" class="mcnTextContent"
													style="padding: 0px 18px 9px; line-height: 200%;">

													<div style="font-family: arial, 'helvetica neue', helvetica, sans-serif;">
                                                        <span style="font-family:arial,helvetica neue,helvetica,sans-serif">{translateConst const = DEAR}{if $order.first_name} {$order.first_name}{/if}
															, {translateConst const = THANK_YOU_FOR_ORDER} printuha.ru.
                                                       {* <br>
															{translateConst const = IF_YOU_HAVE_PROBLEMS}*}
                                                        </span>
													</div>

													<ul style="padding: 0; margin: 9px 0 -5px 0">
														<li style="list-style-type: none;display: inline-block">
															<span style="font-family:arial,helvetica neue,helvetica,sans-serif">&#9993; info@printuha.ru</span>
														</li>
														<li style="display:inline-block;margin: 0 0 0 60px;">
															<span style="font-family:arial,helvetica neue,helvetica,sans-serif">&#9743; +7 910 013-74-13</span>
														</li>
													</ul>

													<ul>
														<li style="list-style-type: none;display: inline-block;width:100%"><span
																	style="margin: 0px 0 0 -35px;font-family:arial,helvetica neue,helvetica,sans-serif"><img
																		src="http://printuha.ru/img/clock.png"
																		style="width: 12px; height: 12px"> {translateConst const = SHOP_WORK_TIME_SHORT}
																({translateConst const = WORK_TIME_CITY})</span></li>
														<li style="list-style-type: none;display: inline-block;line-height: 18px;margin:20px 0 0 0">
															<span style="margin: 0 0 0 -35px;font-family:arial,helvetica neue,helvetica,sans-serif;font-weight: bold">
																{translateConst const = HOLIDAYS_ORDER_INFO}
															</span>
														</li>
													</ul>

												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<![endif]-->

										<!--[if mso]>
										</tr>
										</table>
										<![endif]-->
									</td>
								</tr>
								</tbody>
							</table>
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock"
								   style="min-width:100%;">
								<tbody class="mcnDividerBlockOuter">
								<tr>
									<td class="mcnDividerBlockInner" style="min-width:100%; padding:15px;">
										<table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0"
											   width="100%"
											   style="min-width: 100%;border-top-width: 1px;border-top-style: solid;border-top-color: #666666;">
											<tbody>
											<tr>
												<td>
													<span></span>
												</td>
											</tr>
											</tbody>
										</table>
										<!--
														<td class="mcnDividerBlockInner" style="padding: 18px;">
														<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
										-->
									</td>
								</tr>
								</tbody>
							</table>
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
								   style="min-width:100%;">
								<tbody class="mcnTextBlockOuter">
								<tr>
									<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
										<!--[if mso]>
										<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
											   style="width:100%;">
											<tr>
										<![endif]-->

										<!--[if mso]>
										<td valign="top" width="600" style="width:600px;">
										<![endif]-->
										<table align="left" border="0" cellpadding="0" cellspacing="0"
											   style="max-width:100%; min-width:100%;" width="100%"
											   class="mcnTextContentContainer">
											<tbody>
											<tr>

												<td valign="top" class="mcnTextContent"
													style="padding: 0 18px 9px 38px; line-height: 200%;">

													<strong>{translateConst const = ORDER_NUMBER}
														: </strong> {$order_number}<br>
													<strong>{translateConst const = ORDER_SEND_DATE}
														: </strong> {$order.date|date_format:"%d.%m.%Y. в %H:%m."}<br>
													<!--<strong>ИМЯ ЗАКАЗЧИКА:</strong> {$order.first_name} {$order.last_name}<br>-->
													<strong>{translateConst const = PHONE}:</strong> {$order.phone}<br>
													{if $order.email}<strong>E-MAIL: </strong>{$order.email}<br>{/if}
													<strong>{translateConst const = DELIVERY_TYPES}: </strong>
													<span>
													{if $order.delivery_type == 1}
														{translateConst const = PICKUP} {translateConst const = PICKUP_ADDRESS}
													{elseif $order.delivery_type == 2}
														{translateConst const = COURIER}
													{elseif $order.delivery_type == 3}
														pickpoint
													{elseif $order.delivery_type == 4}
														{translateConst const = POST_OF_RUSSIA}
													{elseif $order.delivery_type == 5}
														{translateConst const = SDEK}
													{/if}
													</span>
													<span>
													{if $order.delivery_type != 1}, {translateConst const = DELIVERY_TIME}
														{if ($order.delivery_type == 2 || $order.delivery_type == 3) && ($order.city == 'Москва' || $order.city == 'Санкт-Петербург')}
															2-3 {translateConst const = DAYS}
														{else}
															до {$order.delivery_days} {if $order.delivery_days == 1}{translateConst const = DAY}{else}{translateConst const = DAYS}{/if}
														{/if}
													{/if}
													</span>
													<br>
													<strong>{translateConst const = DELIVERY_TYPE}: </strong>
													{if $order.payment_type == 'courier'}
														{translateConst const = UPON_RECEIPT}
													{elseif $order.payment_type == 'online'}
														{translateConst const = ONLINE}
													{elseif $order.payment_type == 'paypal'}
														PayPal
													{/if}
													<br>
													{if $order.zipcode}<strong>{translateConst const = INDEX}
														: </strong>{$order.zipcode}
														<br>
													{/if}
													<!-- ansotov если это самовывоз, то не показывать адрес -->
													{if $order.delivery_type != 1}
														<strong>{translateConst const = DELIVERY_ADDRESSES}
														: </strong>{if $order.delivery_type != 3 and $foreign_lands != 1}г. {$order.city}, {/if}{$order.address|unescape}
														<br>
													{/if}
													{if $order.comments}<strong>{translateConst const = COMMENT}
														: </strong>{$order.comments|unescape}
														<br>
													{/if}
												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<![endif]-->

										<!--[if mso]>
										</tr>
										</table>
										<![endif]-->
									</td>
								</tr>
								</tbody>
							</table>
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock"
								   style="min-width:100%;">
								<tbody class="mcnDividerBlockOuter">
								<tr>
									<td class="mcnDividerBlockInner" style="min-width:100%; padding:14px;">
										<table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0"
											   width="100%"
											   style="min-width: 100%;border-top-width: 1px;border-top-style: solid;border-top-color: #666666;">
											<tbody>
											<tr>
												<td>
													<span></span>
												</td>
											</tr>
											</tbody>
										</table>
										<!--
														<td class="mcnDividerBlockInner" style="padding: 18px;">
														<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
										-->
									</td>
								</tr>
								</tbody>
							</table>
							{foreach $cart as $k=>$v}
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
								<tbody class="mcnCaptionBlockOuter">
								<tr>
									<td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">

										<table border="0" cellpadding="0" cellspacing="0"
											   class="mcnCaptionRightContentOuter" width="100%">
											<tbody>
											<tr>
												<td valign="top" class="mcnCaptionRightContentInner"
													style="padding:0 30px ;">
													<table align="left" border="0" cellpadding="0" cellspacing="0"
														   class="mcnCaptionRightImageContentContainer">
														<tbody>
														<tr>
															<td class="mcnCaptionRightImageContent" valign="top">


																<img alt=""
																	 src="http://printuha.ru/img/front_pictures/{$v.info.front_picture}.{$v.info.front_picture_type}"
																	 width="132" style="max-width:352px;"
																	 class="mcnImage">


															</td>
														</tr>
														</tbody>
													</table>
													<table class="mcnCaptionRightTextContentContainer" align="right"
														   border="0" cellpadding="0" cellspacing="0" width="70%">
														<tbody>
														<tr>
															<td valign="top" class="mcnTextContent">
																<div style="float:left">
																	<strong class="null">{$v.info.name}</strong><br>
																	{translateConst const = SIZE}
																	: {if $v.size_info.name!=''}{$v.size_info.name}{else}{$v.size}{/if}
																	<br>
																	{if $v.material}{translateConst const = MATERIAL}: {$v.material.name}
																		<br>
																	{/if}
																	{if $v.color}{translateConst const = COLOR}: {$v.color}
																		<br>
																	{/if}
																	{translateConst const = QTY}: {$v.qty}

																	{if $v.in_stock}
																		<br>
																		<span style="text-transform: uppercase;color: red">{if $v.in_stock_discount < 100}{translateConst const = DISCOUNT} {$v.in_stock_discount}%{else}{translateConst const = ACTION_GIFT}{/if}</span>
																	{/if}
																</div>

																<div style="float:right">{( ($v.in_stock) ? ($v.price * $v.qty) - ((($v.price / 100) * $v.in_stock_discount) * $v.in_stock_qty) : $v.price * $v.qty)|number_format:0:".":" "}.- {translateConst const = RUB}</div>

															</td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>


									</td>
								</tr>
								</tbody>
							</table>
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock"
								   style="min-width:100%;">
								<tbody class="mcnDividerBlockOuter">
								<tr>
									<td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
										<table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0"
											   width="100%"
											   style="min-width: 100%;border-top-width: 1px;border-top-style: solid;border-top-color: #666666;">
											<tbody>
											<tr>
												<td>
													<span></span>
												</td>
											</tr>
											</tbody>
										</table>
										<!--
														<td class="mcnDividerBlockInner" style="padding: 18px;">
														<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
										-->
									</td>
								</tr>
								</tbody>
							</table>{/foreach}
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
								   style="min-width:100%;text-align:right;">
								<tbody class="mcnTextBlockOuter">
								<tr>
									<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
										<!--[if mso]>
										<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
											   style="width:100%;">
											<tr>
										<![endif]-->

										<!--[if mso]>
										<td valign="top" style="width:280px;">
										<![endif]-->
										<table align="left" border="0" cellpadding="0" cellspacing="0"
											   style="max-width:74%;" width="100%" class="mcnTextContentContainer">
											<tbody>
											<tr>

												<td valign="top" class="mcnTextContent"
													style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

													{if $order.delivery_sum > 0}
														<div style="text-align: right;">{translateConst const = IN_ALL}
															:{if $order.discount_sum>0}
														<br>{translateConst const = DISCOUNT}:{/if}<br>
															{translateConst const = DELIVERY}:
														</div>
													{else}
														<div style="text-align: right;">{translateConst const = DELIVERY}
															:
														</div>
													{/if}
													<h4 class="null"
														style="text-align: right;margin: 12px 0 0 0">{translateConst const = TOTAL}
														:</h4>

												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<![endif]-->

										<!--[if mso]>
										<td valign="top" width="155">
										<![endif]-->
										<table align="right" border="0" cellpadding="0" cellspacing="0"
											   style="max-width:155px;margin: 0 24px 0 0" width="100%"
											   class="mcnTextContentContainer">
											<tbody>
											<tr>

												<td valign="top" class="mcnTextContent"
													style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">
													{if $order.delivery_sum > 0}
														<div style="text-align: right;">
															{($order.sum-$order.discount_sum)|number_format:0:".":" "}.- {translateConst const = RUB}
															{if $order.discount_sum>0}
															<br>
															-{$order.discount_sum|number_format:0:".":" "}.- {translateConst const = RUB}{/if}{if $order.delivery_sum>0}
															<br>
															{$order.delivery_sum|number_format:0:".":" "}.- {translateConst const = RUB}{/if}
														</div>
													{else}
														{translateConst const = FREE}
													{/if}

													<h4 class="null"
														style="text-align: right;margin: 12px 0 0 0">{($order.sum + $order.delivery_sum)|number_format:0:".":" "}
														.- {translateConst const = RUB}</h4>

												</td>
											</tr>
											</tbody>
										</table>
										<!--[if mso]>
										</td>
										<![endif]-->

										<!--[if mso]>
										</tr>
										</table>
										<![endif]-->
									</td>
								</tr>
								</tbody>
							</table>
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock"
								   style="min-width:100%;">
								<tbody class="mcnDividerBlockOuter">
								<tr>
									<td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
										<table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0"
											   width="100%"
											   style="min-width: 100%;border-top-width: 1px;border-top-style: solid;border-top-color: #666666;">
											<tbody>
											<tr>
												<td>
													<span></span>
												</td>
											</tr>
											</tbody>
										</table>
										<!--
														<td class="mcnDividerBlockInner" style="padding: 18px;">
														<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
										-->
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td valign="top" id="templateFooter">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock"
								   style="min-width:100%;">
								<tbody class="mcnFollowBlockOuter">
								<tr>
									<td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
										<table border="0" cellpadding="0" cellspacing="0" width="100%"
											   class="mcnFollowContentContainer" style="min-width:100%;">
											<tbody>
											<tr>
												<td align="right" style="padding-left:9px;padding-right:9px;">
													<table border="0" cellpadding="0" cellspacing="0" width="100%"
														   style="min-width:100%;" class="mcnFollowContent">
														<tbody>
														<tr>
															<td align="center" valign="top"
																style="padding-top:9px; padding-right:9px; padding-left:9px;">
																<table align="left" border="0" cellpadding="0"
																	   cellspacing="0">
																	<tbody>
																	<tr>
																		<td align="center" valign="top">
																			<div style="height: 10px"></div>
																			<a href="http://printuha.ru/catalog/new/"
																			   style="padding: 5px 15px;font-size:17px;background: #000;color: #fff;text-transform: uppercase;text-decoration: none">{translateConst const = NEWS}</a>
																		</td>
																	</tr>
																	</tbody>
																</table>
																<table align="right" border="0" cellpadding="0"
																	   cellspacing="0">
																	<tbody>
																	<tr>
																		<td align="center" valign="top">
																			<!--[if mso]>
																			<table align="right" border="0"
																				   cellspacing="0" cellpadding="0">
																				<tr>
																			<![endif]-->

																			<!--[if mso]>
																			<td align="center" valign="top">
																			<![endif]-->

																			<table align="left" border="0"
																				   cellpadding="0" cellspacing="0"
																				   class="mcnFollowStacked"
																				   style="display:inline;">

																				<tbody>

																				</tbody>
																			</table>


																			<!--[if mso]>
																			</td>
																			<![endif]-->

																			<!--[if mso]>
																			<td align="center" valign="top">
																			<![endif]-->

																			<table align="left" border="0"
																				   cellpadding="0" cellspacing="0"
																				   class="mcnFollowStacked"
																				   style="display:inline;">

																				</tbody></table>


																			<!--[if mso]>
																			</td>
																			<![endif]-->

																			<!--[if mso]>
																			<td align="center" valign="top">
																			<![endif]-->

																			<table align="left" border="0"
																				   cellpadding="0" cellspacing="0"
																				   class="mcnFollowStacked"
																				   style="display:inline;">

																				<tbody>
																				</tbody>
																			</table>


																			<!--[if mso]>
																			</td>
																			<![endif]-->

																			<!--[if mso]>
																			</tr>
																			</table>
																			<![endif]-->
																		</td>
																	</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>

									</td>
								</tr>
								</tbody>
							</table>
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
								   style="min-width:100%;">
								<tbody class="mcnTextBlockOuter">
								<tr>
									<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
										<!--[if mso]>
										<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
											   style="width:100%;">
											<tr>
										<![endif]-->

										<!--[if mso]>
										<td valign="top" width="600" style="width:600px;">
										<![endif]-->
										<!--[if mso]>
										</td>
										<![endif]-->

										<!--[if mso]>
										</tr>
										</table>
										<![endif]-->
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</table>
				<!--[if gte mso 9]>
				</td>
				</tr>
				</table>
				<![endif]-->
				<!-- // END TEMPLATE -->
			</td>
		</tr>
	</table>
</center>
</body>
</html>
