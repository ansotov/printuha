<div class="holder">
    {if $success}<label class="success">{translateConst const = SUCCESS_SAVE}</label>{/if}
  <table class="list">
    {if $addresses}
    <thead>
      <tr>
        <td>{translateConst const = ADDRESS}</td>
        <td class="last">{translateConst const = ACTUAL}</td>
        <td class="last"></td>
      </tr>
    </thead>
    <tbody>
      {foreach $addresses as $k=>$v}
      <tr>
        <td class="left">{if $user_info.first_name || $user_info.last_name}{$user_info.first_name} {$user_info.last_name}<br>{/if}
        {$v.city} {$v.street} {$v.house} {$v.flat}</td>
        <td class="last"><input class="address" type="radio" name="address" {if $v.active==1}checked="checked"{/if} value="{$v.id}"></td>
        <td class="last"><button class="address_remove" value="{$v.id}">{translateConst const = DELETE}</button></td>
      </tr>
      {/foreach}
    </tbody>
    {else}
    <thead>
      <tr>
        <td>{translateConst const = ADDRESS}</td>
      </tr>
    </thead>
    <tbody>
      <tr><td>{translateConst const = HAVENT_DELIVERY_ADDRESSES}</td></tr>
    </tbody>
    {/if}
  </table>
  <a class="button" href="/cabinet/addresses/add/">{translateConst const = ADD_NEW_DELIVERY_ADDRESS}</a>
</div>
<script>
$(".address").click(function(e){
  id = $(this).val()
  $.ajax({
			  url: 				document.location.href,
			  dataType: 		'json',
			  type:				'POST',
			  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
			  data:    		{ 'select_address':id},
			  success:		function(data){

								}
	   })
});

<!-- ansotov удаление адреса -->
$(".address_remove").click(function(e){
    var id = $(this).val();

    if (!confirm("{translateConst const = ARE_YOU_SURE_DELETE_ADDRESS}") )
        return false;

    $.ajax({
			  url: 				document.location.href,
			  dataType: 		'json',
			  type:				'POST',
			  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
			  data:    		{ 'delete_address':id},
			  success:		function(data){
			        if (data.success)
                        location.reload();
              }
	   })
})
</script>
