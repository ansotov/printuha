<?php
include_once APPPATH.'models/mmodel.php';
class mwishlist extends mmodel
{
	public function __construct()	
	{
		parent::__construct();
		$this->table="wishlist";
		$this->id="id";	
	}
	public function get_with_products($user_id){
		$query ="SELECT p.* , DATE_FORMAT(w.date,'%d.%m.%Y %H:%i') `wishlist_date`,w.size , w.id wishlist_id FROM wishlist w INNER JOIN products p ON p.id = w.product_id  WHERE w.user_id = $user_id ORDER BY w.date DESC ";
		$r = $this->db->query($query);
		if($r->num_rows()>0)
			return $r->result_array();
		else
			return false;
	}

}