{if $pickpoint}
	<li id="pickpoint_block">
		<label onclick="check_delivery(3);">
			<!--<div class="picture">
					<img alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}" src="/img/girlsinbloom/pickpoint.jpg">
				</div>-->
			<div class="delivery_block field">
				<input
						type="radio" autocomplete="off" class="delivery_type" name="delivery_type"
						id="pickpoint-radio" value="3"{if $delivery_type == 3} checked{/if}>
				<span class="info-label">
                        <strong>{translateConst const = POSTAMATS_AND_POINTS_OF_PICKPOINT}{if $pickpoint.price > 0}
								<span> -
								<span id="pickpoint-price">{$pickpoint.price}</span>
								{translateConst const = RUB}</span>{else}
								<span>
								- {translateConst const = FREE}</span>{/if}
								{if $pickpoint.only_online == true}
								<span style="color:red;text-transform: initial;font-size:15px">({translateConst const = SELF_PAYMENT_ONLY_ONLINE_PAYMENT})</span>
								{/if}
								</strong>
						{if $pickpoint.city_id == 3536 || $pickpoint.city_id == 3223}
							<i><span>{translateConst const = DELIVERY_TIME}
									- <span>2-3</span> {translateConst const = DAYS}</span></i>
							{else}
							<i><span id="pickpoint_days">{translateConst const = DELIVERY_TIME}
								- до <span>{$pickpoint.days}</span> {if $pickpoint.days == 1}{translateConst const = DAY}{else}{translateConst const = DAYS}{/if}</span></i>
						{/if}
                    </span>
			</div>
		</label>
	</li>
{/if}
