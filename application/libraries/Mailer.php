<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH."libraries/phpmailer/class.phpmailer.php");

class Mailer extends PHPmailer{
	
    public $priority = 3;
    public $to_name;
    public $to_email;
    public $From = null;
    public $FromName = null;
    public $Sender = null;
    public $CharSet;
    public $Source_CharSet;
    public $Encoding; 
    
    function __construct($config = array())
    {
		if(!isset($config['Source_CharSet']) || !isset($config['CharSet']))
			show_error('Source Charset or CharSet is not set.');
        
		// Setting characters encoding
		$this->CharSet = $config['CharSet'];
		$this->Source_CharSet = $config['Source_CharSet'];
		
		
		
        if($config['smtp_mode'] == 'enabled')
        {
            $this->Host = $config['smtp_host'];
            $this->Port = $config['smtp_port'];
            if($config['smtp_username'] != '')
            {
            	
          		$this->IsSMTP();
                $this->SMTPAuth = true;
          		$this->SMTPSecure = 'ssl';
		        $this->Port = 465;
	
                $this->Username = $config['smtp_username'];
                $this->Password = $config['smtp_password'];
            }
            $this->Mailer = "smtp";
        }
        if($config['encoding']!=''){
			$this->Encoding = $config['encoding'];
		}
		if(!$this->From)
        {
            $this->From = $config['from_email'];
        }
        if(!$this->FromName)
        {
			$this->FromName = $this->mime_header_encode($config['from_name']);
            //$this-> FromName = mime_header_encode($config['from_name']);
        }
        if(!$this->Sender)
        {
			$this->Sender = $config['from_email'];
            //$this->Sender = mime_header_encode($config['from_email'];
        }
		
	
        $this->Priority = $this->priority;
		
		
    }
	
	public function mime_header_encode($str) {
	  if($this->Source_CharSet != $this->CharSet) {
		$str = iconv($this->Source_CharSet, $this->CharSet, $str);
	  }
	  return '=?' . $this->CharSet . '?B?' . base64_encode($str) . '?=';
	}
}
