<a href="/checkout/" id="header-basket"{if $total_number == 0} class="disabled"{/if}>
	<div class="basket-name">{translateConst const = CARD}</div>
	<div class="header-bag" id="header-shopping-bag"
		 style="display: block; visibility: visible; opacity: 1;margin: 0 0 0 13px">
		<span></span>
		<span id="basket-items-count">{if $total_number > 0}{$total_number}{/if}</span>
	</div>
</a>