<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//admin builder by rainmaker
//v2.5 24.11.2013
/*
Example usage
function news($page=1){
	$structure=array(
						"name"=>array("type"=>"string","name"=>"Заголовок"),
						"preview"=>array("type"=>"string","name"=>"Превью"),
						"icon"=>array("type"=>"jpg_file","name"=>"Картинка","directory"=>"img/news_icons/","max_width"=>"200","max_height"=>200),
						"text"=>array("type"=>"text","name"=>"Описание")
			   );
	$this->admin_builder->set_model('mnews');
	$this->admin_builder->set_header('admin/news');
	$this->admin_builder->set_core_name('news');
	$this->admin_builder->set_title_name('Новости');
	$this->admin_builder->get_output_rows(array(),$structure,'order_id ASC',$page);
	$this->admin_builder->init();
}
function open_news($id=0){
	$structure=array(ajax_photogallery
						"name"=>array("type"=>"string","name"=>"Заголовок"),
						"preview"=>array("type"=>"string","name"=>"Превью"),
						"icon"=>array("type"=>"jpg_file","name"=>"Картинка","directory"=>"img/news_icons/","max_width"=>"200","max_height"=>200),
						"text"=>array("type"=>"text","name"=>"Описание")
	);
	$this->admin_builder->set_model('mnews');
	$this->admin_builder->set_header('admin/news');
	$this->admin_builder->set_id($id);
	$this->admin_builder->set_core_name('news');
	$this->admin_builder->set_title_name('Новости');
	$this->admin_builder->init($structure);
}
*/

class Admin_Builder2
{
	private $CI;
	public $force_validation_load = 0;
	private $id = 0;
	private $id_name = "id";
	private $header = "";
	private $on_change_header = "";
	private $model_name = "";
	private $core_model = "";
	private $main_template = "";
	private $core_template = "admin/index";
	private $templates_dir = "admin";
	private $dir_name = "admin";

	private $image_dir = 'img/';
	private $create_image = false;

	private $core_name = "";
	private $title_name = "";
	private $tags = array();
	private $categories = array();
	private $select_where = array();
	private $links_after = array();
	private $links_before = array();
	private $insert_data_array = array();

	private $auth_code = '';
	private $user_auth = '';
	private $user_id = 0;
	private $section_id = 0;
	private $user_access = array();
	private $user_status = '';

	private $end_link = "";

	private $search_array_to_string = false;

	//example data
	private $data_array = array(
		"name"    => array("type" => "string", "name" => "Заголовок"),
		"icon"    => array("type" => "jpg_file", "name" => "Иконка", "directory" => "img/", "max_width" => "200", "max_height" => 200),
		"text"    => array("type" => "text", "name" => "Описание"),
		"numbers" => array("type" => "int", "name" => "Варианты", "options" => array(0 => "hello", 1 => "bye"))
	);
	private $show_per_page_array = array("10" => "10", "25" => "25", "50" => "50", "100" => "100", "250" => "250", "all" => "Все"
	);

	private $filter_array = array();
	private $show_per_page = 50;
	private $create_links = array();
	private $show_create_links = true;
	private $order = 'ASC';

	private $restric_access = 0;

	function __construct($da = array())
	{
		$this->CI =& get_instance();
		$this->CI->load->library('common_f');
		$this->CI->load->library('image_lib');
		$this->CI->load->library('mysmarty');
		$this->CI->load->library('session');
		$this->CI->lang->load('form_validation', 'russian');
		$this->CI->load->library('form_validation');
		$this->CI->load->helper('url');
		log_message('debug', "Admin_Builder Class Initialized");
	}

	public function set_model($main_model_name)
	{
		$this->model_name = $main_model_name;
		$this->CI->load->model($main_model_name, 'main_model');
		$this->id_name = $this->CI->main_model->get_id();
	}

	public function set_core_model()
	{
		$this->core_model = $this->model_name;

	}

	public function set_order_criteria($data)
	{
		$this->CI->main_model->set_orders($data);
		if ($this->end_link == '') foreach ($data as $k => $v)
		{
			$this->end_link .= "$v/";
		}
		$this->select_where = $data;
	}

	public function set_structure($data)
	{
		$this->data_array = $data;
	}
	////////////////////
	//SET FILTER ARRAY
	// admin 2.0
	public function set_filter_array($data)
	{
		$this->data_array = $data;
	}

	public function set_id($id)
	{
		$this->id = $id;
	}

	public function set_section_id($id)
	{
		$this->section_id = $id;
	}

	// user must  authorise via database authethentication
	public function set_user_auth($data, $user_id)
	{
		$this->user_auth = $data;
		$this->user_id   = $user_id;
	}

	public function set_auth_code($data)
	{
		$this->auth_code = $data;
	}

	//??
	public function set_access($data)
	{
		$this->user_access = $data;
	}

	public function hide_create_links()
	{
		$this->show_create_links = false;
	}

	// set directory of admin panel
	public function set_dir($data)
	{
		$this->dir_name = $data;
	}

	public function set_template_dir($dir)
	{
		$this->templates_dir = $dir;
		$this->core_template = $this->templates_dir . '/index';
	}

	// name that will be used in various operations including permission to access different folders
	public function set_core_name($data)
	{
		$this->core_name = $data;
		preg_match('/([A-Za-z]+)/', $data, $matches);
		if ($matches != array())
			$data = $matches[0];

		if ($this->user_access != array() && $this->user_auth != '')
		{
			if (!isset($this->user_access[$this->user_auth][$data]) && !isset($this->user_access[$this->user_auth]['all']))
			{
				$this->restric_access();
			}
			elseif (isset($this->user_access[$this->user_auth][$data]))
			{
				$this->user_status = $this->user_access[$this->user_auth][$data];
			}
			elseif (isset($this->user_access[$this->user_auth]['all']))
			{
				$this->user_status = $this->user_access[$this->user_auth]['all'];
			}
		}
	}

	//show error that user can't see content
	private function restric_access()
	{
		$this->CI->mysmarty->assign('load_page', $this->templates_dir . '/access_restricted');
		$this->restric_access = 1;
		echo $this->CI->output->final_output = $this->CI->mysmarty->fetch($this->core_template);
		exit;
	}

	public function set_links_before($data)
	{
		$this->links_before = $data;
	}

	public function set_links_after($data)
	{
		$this->links_after = $data;
	}

	public function set_end_link($value)
	{
		$this->end_link = $value;
	}

	public function set_title_name($data)
	{
		$this->title_name = $data;
	}

	public function set_tags($data)
	{
		$this->tags = $data;
	}

	public function set_categories($data)
	{
		$this->categories = $data;
	}

	public function edit_links($data)
	{
		$this->create_links = $data;
	}

	private function file_upload($name, $directory, $file_name, $type)
	{
		if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != '')
		{
			$file_error = $_FILES[$name]['error'];
			if ($file_error > 0)
			{
				switch ($file_error)
				{
					case 1:
						$error = 'problem: file is larger than upload_max_filesize';
						break;
					case 2:
						$error = 'problem: file is larger than max_file_size';
						break;
					case 3:
						$error = 'problem: only part of file is loaded';
						break;
					case 4:
						$error = 'problem: file is not loaded';
						break;
					case 6:
						$error = 'problem: Missing a temporary folder';
						break;
					case 7:
						$error = 'problem: Failed to write file to disk';
						break;
					case 8:
						$error = 'problem: File upload stopped by extension';
						break;
				}
				show_error($error);
			}
			if (!is_dir($this->CI->config->item('core_web') . $directory))
			{
				mkdir($this->CI->config->item('core_web') . $directory, 0777);
			}
			$where = $this->CI->config->item('core_web') . $directory . $file_name . "." . $type;

			if (!move_uploaded_file($_FILES[$name]['tmp_name'], $where))
			{
				$error = 'problem: cannot tranfer file to destination:' . $where;
				show_error($error);
			}
			chmod($where, 0777);

			return true;
		}
	}

	//main photo upload function
	private function photo_upload($name, $directory, $file_name, $max_width = 1000, $max_height = 1000, $max_small_width = 100, $max_small_height = 100, $watermark = '', $any_file = false, $force_format = '', $add_sizes = array())
	{
		if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != '')
		{


			$file_error = $_FILES[$name]['error'];
			if ($file_error > 0)
			{
				switch ($file_error)
				{
					case 1:
						$error = 'problem: file is larger than upload_max_filesize';
						break;
					case 2:
						$error = 'problem: file is larger than max_file_size';
						break;
					case 3:
						$error = 'problem: only part of file is loaded';
						break;
					case 4:
						$error = 'problem: file is not loaded';
						break;
					case 6:
						$error = 'problem: Missing a temporary folder';
						break;
					case 7:
						$error = 'problem: Failed to write file to disk';
						break;
					case 8:
						$error = 'problem: File upload stopped by extension';
						break;
				}
				show_error($error);
			}
			$image_type = exif_imagetype($_FILES[$name]['tmp_name']);
			if ($image_type != 2 && $image_type != 3)
			{
				show_error("can't use this image type " . $image_type);
			}
			if ($any_file == true)
			{
				if ($force_format != '')
				{
					$image_name = $force_format;
				}
				else
				{
					switch ($image_type)
					{
						case 2:
							$image_name = "jpg";
							break;
						case 3:
							$image_name = "png";
							break;
					}
				}
			}
			else
				$image_name = "jpg";

			if (!is_dir($this->CI->config->item('core_web') . $directory))
			{
				mkdir($this->CI->config->item('core_web') . $directory, 0777);
			}


			$where  = $this->CI->config->item('core_web') . $directory . $file_name . "." . $image_name;
			$where2 = $this->CI->config->item('core_web') . $directory . $file_name . "_small." . $image_name;


			if (!move_uploaded_file($_FILES[$name]['tmp_name'], $where))
			{
				$error = 'problem: cannot tranfer file to destination:' . $where;
				show_error($error);
			}

			$config = array();
			list($width, $height, $type, $attr) = getimagesize($where);
			if ($width > $max_width)
				$width = $max_width;
			if ($height > $max_height)
				$height = $max_height;
			$config['source_image']   = $where;
			$config['maintain_ratio'] = true;
			$config['width']          = $width;
			$config['height']         = $height;
			$this->CI->image_lib->initialize($config);
			if (!$this->CI->image_lib->resize())
			{
				//  echo 'photo_upload error:: '.$this->CI->image_lib->display_errors();
			}


			$config                   = array();
			$config['master_dim']     = 'auto';
			$config['source_image']   = $where;
			$config['maintain_ratio'] = true;
			$config['width']          = $max_small_width;
			$config['height']         = $max_small_height;
			$config['new_image']      = $where2;
			$this->CI->image_lib->initialize($config);
			if (!$this->CI->image_lib->resize())
			{
				//echo 'photo_upload error:: '.$this->CI->image_lib->display_errors();
			}


			if ($add_sizes != array())
			{
				foreach ($add_sizes as $size_name => $v)
				{
					$where3                   = $this->CI->config->item('core_web') . $directory . $file_name . "_$size_name." . $image_name;
					$config                   = array();
					$config['master_dim']     = 'auto';
					$config['source_image']   = $where;
					$config['maintain_ratio'] = true;
					$config['width']          = $v['width'];
					$config['height']         = $v['height'];
					$config['new_image']      = $where3;
					$this->CI->image_lib->initialize($config);
					if (!$this->CI->image_lib->resize())
					{
						//echo 'photo_upload error:: '.$this->CI->image_lib->display_errors();
					}
					chmod($where, 0777);
					$this->CI->image_lib->clear();
				}
			}
			chmod($where, 0777);
			$this->CI->image_lib->clear();
			if ($watermark != '' && $watermark != 0)
			{
				$config                     = array();
				$config['source_image']     = $where;
				$config['wm_type']          = 'overlay';
				$config['wm_overlay_path']  = $this->CI->config->item('core_web') . $watermark;
				$config['wm_opacity']       = 100;
				$config['wm_vrt_alignment'] = 'middle';
				$config['wm_hor_alignment'] = 'center';

				$this->CI->image_lib->initialize($config);
				$this->CI->image_lib->watermark();
				$this->CI->image_lib->clear();
			}
			if ($any_file == true)
				return $image_name;
			else
				return "jpg";
		}
	}

	//?????
	private function put_posts_to_insert($data = array())
	{
		foreach ($data as $v)
		{
			$this->insert_data_array[$v] = $this->CI->input->post($v);
		}
	}

	// set main template //current folder
	public function set_main_template($template)
	{
		$this->main_template = $template;
	}

	public function set_core_template($temp)
	{
		$this->core_template = $temp;
	}

	public function set_on_change_header($header)
	{
		$this->on_change_header = $header;
	}

	// head somewhere incase of succesfull operation
	public function set_header($header)
	{
		$this->header = $header;
	}

	public function set_links($data)
	{
		$this->create_links = $data;
	}

	//??????
	public function set_data_value($data, $value)
	{
		$this->insert_data_array[$data] = $value;
	}

	// получить список переменных для структрурного отображения данных
	private function get_data_names()
	{
		$data = array();

		//игнорировать AJAX параметры поскольку они не являются частью извлекаемой таблицы данных
		foreach ($this->data_array as $k => $v) if ($v['type'] != "ajax_data" && $v['type'] != "ajax_photogallery_insert" && $v['type'] != 'map' && $v['type'] != "ajax_photogallery" && $v['type'] != 'checklist')
		{

			if ($this->data_array[$k]['type'] == "date")
				$k = "DATE_FORMAT(`$k`, '%d.%m.%Y') as `$k`";
			elseif ($this->data_array[$k]['type'] == "datetime")
				$k = "DATE_FORMAT(`$k`, '%d.%m.%Y %H:%i') as `$k`";
			elseif ($this->data_array[$k]['type'] == "time")
				$k = "DATE_FORMAT(`$k`, '%H:%i') as `$k`";

			$data[] = $k;
		}
		//getting picture for jcrop
		foreach ($this->data_array as $k => $v)
		{
			if ($v['type'] == 'jcrop' && isset($v['image_type_name']))
			{
				$data[] = $v['image_type_name'];
			}
			if ($v['type'] == 'jcrop' && isset($v['from']['field_name']))
			{
				$data[] = $v['from']['field_name'];
			}
		}


		return $data;
	}

	//////////////////////////////
	// grab ajax data on key up
	//////////////////////////////
	private function active_string_search()
	{
		$model = $this->CI->input->post('ajax_data_post');
		if ($model != '')
		{
			//$this->set_model($model);
			$this->CI->load->model($model);


			$get_field = $this->CI->input->post('get_field');
			$like      = $this->CI->input->post('like');
			$data      = $this->CI->$model->bget(array($get_field . " like" => $like . "%"), array($get_field), 0, $get_field . " ASC ", 5, 0);
			$this->CI->mysmarty->assign('data', $data['result']);
			$this->CI->mysmarty->assign('get_field', $get_field);
			$output = $this->CI->mysmarty->fetch('xml/active_string');
			//$output=iconv('Windows-1251','utf-8',$output);
			header("Content-type: text/xml;charset=utf-8;");
			header("Cache-Control: no-cache");
			echo $output;
			exit;
		}
	}

	////////////////////////////////////////
	//AJAX PHOTO SHOW
	/////////////////////////////////
	private function ajax_photo_show($directory, $k, $folder_name)
	{
		$this->CI->load->model("mtemp_data");
		$photo_name = $this->CI->mtemp_data->show($this->model_name . "_" . $this->id . "_" . $k . "_" . $folder_name);
		$this->CI->mysmarty->assign('photo_name', $photo_name);
		$this->CI->mysmarty->assign('directory', $directory);
		$output = $this->CI->mysmarty->fetch('xml/temp_photo');
		//$output=iconv('Windows-1251','utf-8',$output);
		header("Content-type: text/xml;charset=utf-8;");
		header("Cache-Control: no-cache");
		echo $output;
		exit;
	}

	/////////////////////////////////////////////
	////////////////////////////////////////////
	// AJAX GALLERY PHOTO UPLOAD    ///////////
	//////////////////////////////////////////
	public function ajax_gallery_photo_upload($key, $checkbox = false)
	{

		/*
		// DATA ARRAY EXAMPLE //
			"some_photos"=>array(	"type"=>"ajax_photogallery",
								 	"name"=>"Тест фотогаллереи",
									"model"=>"mphotos",
									"photo_field"=>"name",
									'photo_folder'=>"img/lookbook_photos/",
									"fixed_data"=>array("type"=>"lookbook"),
									"hook"=>"collection_id",
									"link_to_folder"=>"/admin/open_photos/"
								)
		*/
		//key  = $some_photos

		if ( ! isset($data['fixed_data'])) {
			$data['fixed_data'] = array();
		}


		$get_photos = $this->CI->input->post('get_gallery_photos_' . $key);
		$reorder    = $this->CI->input->post('gallery_reorder_' . $key);
		$delete     = $this->CI->input->post('gallery_photo_delete_' . $key);

		$watermark = $this->CI->input->post('watermark_' . $key);

		$post = $this->CI->input->post('post');
		if ($get_photos != '' || $reorder != '' || $delete != '' || isset($_FILES['ajax_photo_' . $key])) {

			$data_array = $this->data_array[$key];
			$model      = $data_array['model'];

			//$this->set_model($model);
			$this->CI->load->model($model);
			$hook = $data_array['hook'];
			if ($this->id > 0) {
				$hook_id = $this->id;
			} else {
				$hook_id = ($this->user_id - (2 * $this->user_id));
			}
			if (isset($data_array['fixed_data'])) {
				$order_array = $data_array['fixed_data'];
			}
			$order_array[$hook] = (string)$hook_id;
			$this->CI->$model->set_orders($order_array);

		}
		//GET PHOTOS LIST
		if ($get_photos != '') {
			if (isset($data_array['fixed_data'])) {
				$where_array = $data_array['fixed_data'];
			}
			$where_array[$hook] = (string)$hook_id;
			if (isset($data_array['any_image']) == true) {
				$photos = $this->CI->$model->get($where_array, array('id', $data_array['photo_field'], $data_array['image_type_name']), 0, "order_id ASC");
			} else {
				$photos = $this->CI->$model->get($where_array, array('id', $data_array['photo_field']), 0, "order_id ASC");
			}
			if ($photos) {
				foreach ($photos as $k => $v) {
					$photo_field               = $v[$data_array['photo_field']];
					$photos[$k]['photo_field'] = $photo_field;
					$photos[$k]['image_type']  = $v[$data_array['image_type_name']];
				}
			}
			echo json_encode($photos);
			exit;
		}
		//REORDER LIST
		if ($reorder != '') {
			$temp_array = array();
			foreach ($reorder as $k => $v) {

				$photo_id          = str_replace('gallery_photo_' . $key . '_', '', $v);
				$where_array       = $data_array['fixed_data'];
				$where_array['id'] = $photo_id;
				$order_id          = $k + 1;
				$this->CI->$model->update(array('order_id' => $order_id), $where_array);
				echo 'UPDATE ' . $data_array['model'] . " SET order_id = $order_id WHERE id = $photo_id AND type = common  ";
			}
			exit;
		}
		//DELETE PHOTO
		if ($delete != '' && $photo_data = $this->CI->$model->get(array('id' => $delete), array(), 1)) {
			@unlink($this->CI->config->item('core_web') . 'photo_folder' . $photo_data['name'] . '.jpg');
			@unlink($this->CI->config->item('core_web') . 'photo_folder' . $photo_data['name'] . '_small.jpg');
			@unlink($this->CI->config->item('core_web') . 'photo_folder' . $photo_data['name'] . '.png');
			@unlink($this->CI->config->item('core_web') . 'photo_folder' . $photo_data['name'] . '_small.png');
			$this->CI->$model->delete(array('id' => $delete));
			exit;
		}

		// INSERT PHOTO
		// if ajax photo uploading in progress
		if (isset($_FILES['ajax_photo_' . $key])) {

			! isset($data_array['max_width']) ? $max_width = 1000 : $max_width = $data_array['max_width'];
			! isset($data_array['max_height']) ? $max_height = 1000 : $max_height = $data_array['max_height'];
			! isset($data_array['max_small_width']) ? $max_small_width = 100 : $max_small_width = $data_array['max_small_width'];
			! isset($data_array['max_small_height']) ? $max_small_height = 100 : $max_small_height = $data_array['max_small_height'];
			! isset($data_array['force_format']) ? $force_format = '' : $force_format = $data_array['force_format'];
			! isset($data_array['sizes']) ? $add_sizes = array() : $add_sizes = $data_array['sizes'];


			$random_string = $this->CI->common_f->genRandomString(20);


			if ($watermark == '1') {
				$watermark = $data_array['watermark'];
			}


			if ($image_type = $this->photo_upload('ajax_photo_' . $key, $data_array['photo_folder'], $random_string, $max_width, $max_height, $max_small_width, $max_small_height, $watermark, true, $force_format, $add_sizes)) {

				if (isset($data_array['fixed_data'])) {
					$insert_data = $data_array['fixed_data'];
				}
				if (isset($data_array['any_image']) && $data_array['any_image'] == true && isset($data_array['image_type_name'])) {
					$insert_data[$data_array['image_type_name']] = $image_type;
				}
				$insert_data[$hook]                          = $hook_id;
				$insert_data[$data_array['photo_field']]     = $random_string;
				$insert_data[$data_array['image_type_name']] = $image_type;

				$photo_id = $this->CI->$model->insert($insert_data);
				if ($checkbox == false) {
					echo '<div class="gallery_photo" id="gallery_photo_' . $key . '_' . $photo_id . '"><div onclick="delete_photo_gallery_photo(\'' . $key . '\',' . $photo_id . ',this)" class="photo_dell"></div><a target="_new" href="' . $data_array['link_to_folder'] . '' . $photo_id . '/"><img src="/' . $data_array['photo_folder'] . '' . $random_string . '_small.' . $image_type . '"></a><a target="blank" href=\'/' . $data_array['photo_folder'] . $random_string . '.' . $image_type . '\'>Link</a></div>';
				} else {
					echo '<div class="gallery_photo" id="gallery_photo_' . $key . '_' . $photo_id . '"><div onclick="delete_photo_gallery_photo(\'' . $key . '\',' . $photo_id . ',this)" class="photo_dell"></div><a target="_new" href="' . $data_array['link_to_folder'] . '' . $photo_id . '/"><img src="/' . $data_array['photo_folder'] . '' . $random_string . '_small.' . $image_type . '"></a><div class="photo_sel"><input type="checkbox" name="/' . $data_array['photo_folder'] . '' . $random_string . '.' . $image_type . '" class="select_' . $key . '" id="select_' . $key . '_' . $photo_id . '"></div><a target="blank" href=\'/' . $data_array['photo_folder'] . $random_string . '.' . $image_type . '\'>Link</a></div>';
				}
				exit;
			}
		}

	}

	/////////////////////////////\\\\\\\\\\\\
	/////////////////////////////\\\\\\\\\\\\\
	// AJAX GALLERY PHOTO UPLOAD  \\\\\\\\\\\\\
	//////////////////////////////\\\\\\\\\\\\\\
	//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	///////////////////////////////////////
	/// JCROP
	private function ajax_jcrop($key)
	{

		/*
				crop image_pars
					 'picture'=>array('type'=>"jcrop",
										  "name"=>"Картинка",
										  "upload_width"=>1300,
										  "upload_height"=>1300,
										  "max_width"=>447,
										  "max_height"=>237,
										  "max_min_width"=>100,
										  "max_min_height"=>100,
										  "directory"=>"img/some_more_pictures/"
						  ),
		*/

		$default_data = array('type'           => "jcrop",
		                      "name"           => "Картинка",
		                      "upload_width"   => 1300,
		                      "upload_height"  => 1300,
		                      "max_min_width"  => 100,
		                      "max_min_height" => 100,
		                      "directory"      => "img/somewhere/"
		);


		$data = $this->data_array[$key];


		// if directory is empty?, terminate
		if (!isset($data['directory']) || $data['directory'] == '')
		{
			json_encode('fail');
			exit;
		}

		// if directory do not exist, create it
		if (!is_dir($this->CI->config->item('core_web') . $data['directory']))
		{
			mkdir($this->CI->config->item('core_web') . $data['directory'], 0777);
		}

		$upload_cropable = $this->CI->input->post('upload_cropable');

		$file_name = $key . '_' . $this->user_id;
		$directory = $data['directory'];


		!isset($data['force_format']) ? $force_format = '' : $force_format = $data['force_format'];


		$source = $this->CI->config->item('core_web') . $directory . $file_name;

		$new = $this->CI->config->item('core_web') . $directory . $file_name . '_cropped';

		if ($upload_cropable == $key && isset($_FILES[$key . '_ajax_file']['name']) && $_FILES[$key . '_ajax_file']['name'] != '')
		{
			if (is_file($source . '.jpg'))
				unlink($source . '.jpg');
			if (is_file($source . '_small.jpg'))
				unlink($source . '_small.jpg');
			if (is_file($source . '.png'))
				unlink($source . '.png');
			if (is_file($source . '_small.png'))
				unlink($source . '_small.png');
			$image_type                           = $this->photo_upload($key . '_ajax_file', $data['directory'], $file_name, $data['upload_width'], $data['upload_height'], $max_small_width = 100, $max_small_height = 100, '', true, $force_format);
			$this->data_array[$key]['image_type'] = $image_type;
			chmod($source . '.' . $image_type, 0777);
			chmod($source . '_small.' . $image_type, 0777);
		}

		$crop = $this->CI->input->post($key . '_crop_image');
		$x1   = $this->CI->input->post('x1');
		$x2   = $this->CI->input->post('x2');
		$y1   = $this->CI->input->post('y1');
		$y2   = $this->CI->input->post('y2');
		$w    = $this->CI->input->post('w');
		$h    = $this->CI->input->post('h');
		//grab image type
		if ($i_type = $this->CI->input->post($key . '_image_type'))
			$this->data_array[$key]['image_type'] = $i_type;
		if ($crop == 1)
		{
			$image_type = $this->CI->input->post('image_type');

			if (is_file($new . '.' . $image_type))
				unlink($new . '.' . $image_type);
			if (is_file($new . '_small.' . $image_type))
				unlink($new . '_small.' . $image_type);

			if (!isset($data['from']))
			{
				copy($source . '.' . $image_type, $new . '.' . $image_type);
				// if we are coping image from already loaded source
			}
			else
			{
				$info   = $this->CI->main_model->get(array('id' => $this->id), array($data['from']['field_name']), 1);
				$f_name = $info[$data['from']['field_name']];
				$way    = $this->CI->config->item('core_web') . $data['from']['directory'] . $f_name . "." . $image_type;
				copy($way, $new . '.' . $image_type);
			}
			$config                 = array();
			$config['source_image'] = $new . '.' . $image_type;
			$config['x_axis']       = $x1;
			$config['y_axis']       = $y1;


			$config['width']          = $w;
			$config['height']         = $h;
			$config['maintain_ratio'] = false;

			$this->CI->image_lib->initialize($config);

			if (!$this->CI->image_lib->crop())
			{
				echo 'crop error:: ' . $this->CI->image_lib->display_errors();
			}


			if (!isset($data['max_height']))
				$data['max_height'] = 0;
			if (!isset($data['max_width']))
				$data['max_width'] = 0;


			if (!isset($data['max_min_width']))
				$data['max_min_width'] = 100;
			if (!isset($data['max_min_height']))
				$data['max_min_height'] = 100;

			//if selected area is larger than written
			if (($w > $data['max_width'] && $data['max_width'] != 0) || ($h > $data['max_height'] && $data['max_height'] != 0))
			{
				if ($data['max_width'] == 0)
				{
					$w = round($w * $data['max_height'] / $h);
					$h = $data['max_height'];
				}
				elseif ($data['max_height'] == 0)
				{
					$h = round($h * $data['max_width'] / $w);
					$w = $data['max_width'];
				}
				else
				{
					$w = $data['max_width'];
					$h = $data['max_height'];
				}
				$config['master_dim']   = 'auto';
				$config['source_image'] = $new . '.' . $image_type;
				$config['new_image']    = $new . '.' . $image_type;
				$config['width']        = $w;
				$config['height']       = $h;
				$this->CI->image_lib->initialize($config);
				if (!$this->CI->image_lib->resize())
				{
					echo ' resize error :: ' . $this->CI->image_lib->display_errors();
				}

			}

			$config                   = array();
			$config['master_dim']     = 'auto';
			$config['source_image']   = $new . '.' . $image_type;
			$config['maintain_ratio'] = true;
			$config['width']          = $data['max_min_width'];
			$config['height']         = $data['max_min_height'];
			$config['new_image']      = $new . '_small.' . $image_type;

			$this->CI->image_lib->initialize($config);
			if (!$this->CI->image_lib->resize())
			{
				echo 'photo_upload error:: ' . $this->CI->image_lib->display_errors();
			}
			if (is_file($new . '.' . $image_type))
				chmod($new . '.' . $image_type, 0777);
			if (is_file($new . '_small.' . $image_type))
				chmod($new . '_small.' . $image_type, 0777);
			$this->CI->image_lib->clear();

			if (isset($data['watermark']))
			{
				$config                     = array();
				$config['source_image']     = $new . '.' . $image_type;
				$config['wm_type']          = 'overlay';
				$config['wm_overlay_path']  = $this->CI->config->item('core_web') . $data['watermark'];
				$config['wm_opacity']       = 100;
				$config['wm_vrt_alignment'] = 'middle';
				$config['wm_hor_alignment'] = 'center';

				$this->CI->image_lib->initialize($config);
				$this->CI->image_lib->watermark();
				$this->CI->image_lib->clear();
			}
		}
		if (isset($image_type))
		{
			echo json_encode($image_type);
			exit;
		}
		else
			return true;

	}
	/// JCROP
	///////////////////////////////////////


	////////////////////////////////////////
	//AJAX PHOTO UPLOAD
	/////////////////////////////////
	private function ajax_photo_upload($k, $folder_name)
	{
		$this->CI->load->model('mtemp_data');
		$data              = $this->data_array[$k];
		$insert_data       = array();
		$insert_data['id'] = 0;
		$string            = $this->CI->common_f->genRandomString(20);
		$string            = $string;
		$folder_info       = $data['folders'][$folder_name];
		$adress            = $this->model_name . "_" . $this->id . "_" . $k . "_" . $folder_name;
		if ($old_data = $this->CI->mtemp_data->show($adress))
		{
			//	@unlink($this->CI->config->item('core_web').$folder_info['directory'].$old_data.".jpg");
			//	@unlink($this->CI->config->item('core_web').$folder_info['directory'].$old_data."_small.jpg");
			$this->CI->mtemp_data->delete($adress);
		}
		!isset($folder_info['max_width']) ? $max_width = 1000 : $max_width = $folder_info['max_width'];
		!isset($folder_info['max_height']) ? $max_height = 1000 : $max_height = $folder_info['max_height'];
		!isset($folder_info['max_small_width']) ? $max_small_width = 100 : $max_small_width = $folder_info['max_small_width'];
		!isset($folder_info['max_small_height']) ? $max_small_height = 100 : $max_small_height = $folder_info['max_small_height'];
		// добавить информацию о фотографии во временное хранилище
		$this->photo_upload($k . "_" . $folder_name, $folder_info['directory'], $string, $max_width, $max_height, $max_small_width, $max_small_height, true);
		$this->CI->form_validation->reset_all_data();

		$this->CI->mtemp_data->upload($adress, $string);
		echo 'success';
		exit;
	}

	/////////////////////////////////
	//Grab ajax data on data post or not
	////////////////////////////////
	private function xml_data($k)
	{
		$this->CI->load->model('mtemp_data');
		$data    = $this->data_array[$k];
		$xml_put = $this->CI->input->post('xml_put');
		// to be inserted data
		$insert_data = array();
		// insert
		$this->CI->form_validation->reset_all_data();
		$main_model = $this->model_name;
		$hook       = $this->CI->input->post('hook');
		//$this->set_model($data['model']);
		$this->CI->load->model($data['model']);
		if ($hook != '')
		{
			$insert_data[$hook] = $this->id;
			$order_ar           = array();
			$order_ar[$hook]    = $this->id;
			$this->CI->$data['model']->set_orders($order_ar);
		}
		$this->CI->$data['model']->order();
		foreach ($data['folders'] as $folder_key => $folder)
		{
			if ($folder['type'] == 'image')
			{
				$way        = $main_model . "_" . $this->id . "_" . $k . "_" . $folder_key;
				$photo_name = $this->CI->mtemp_data->show($way);
				$val        = $photo_name;
			}
			else
				$val = $this->CI->input->post("ajax_" . $k . "_" . $folder_key);
			$insert_data[$folder_key] = $val;
		}
		if ($xml_put == 1)
			$this->CI->$data['model']->insert($insert_data);

		$del_id = $this->CI->input->post('del_id');
		if ($del_id > 0)
		{
			foreach ($data['folders'] as $folder_key => $folder)
			{
				if ($folder['type'] == 'image')
				{
					$way        = $main_model . "_" . $del_id . "_" . $k . "_" . $folder_key;
					$photo_name = $this->CI->mtemp_data->show($way);
					$data2      = $this->CI->$data['model']->get(array("id" => $del_id), $folder_key, 1);
					@unlink($this->CI->config->item('core_web') . $folder['directory'] . $data2[$folder_key] . ".jpg");
					@unlink($this->CI->config->item('core_web') . $folder['directory'] . $data2[$folder_key] . "_small.jpg");
				}
			}

			$this->CI->$data['model']->delete(array("id" => $del_id, $hook => $this->id));
		}

		$move_up = $this->CI->input->post("move_up");
		if ($move_up > 0) $this->CI->$data['model']->move($move_up, "up");

		$move_down = $this->CI->input->post("move_down");
		if ($move_down > 0) $this->CI->$data['model']->move($move_down, "down");

		$data['get'][] = "order_id";
		$data['get'][] = "id";
		$get_data      = $data['get'];

		$data2 = $this->CI->$data['model']->get(array($hook => $this->id), $data['get'], 0, "order_id ASC ");
		if ($data2) foreach ($data['folders'] as $k2 => $v2)
		{
			if ($v2['type'] == 'image')
			{
				foreach ($data2 as $k3 => $v3) if (isset($v3[$k2]))
				{
					$data2[$k3][$k2] = "<img src=\"/" . $v2['directory'] . $data2[$k3][$k2] . "_small.jpg\" >";
				}
			}
			if (isset($v2['editable']) && $v2['editable'] == true)
			{
				foreach ($data2 as $k3 => $v3) if (isset($v3[$k2]))
				{

					$code       = $this->CI->input->post('code');
					$code_value = $this->CI->input->post('code_value');
					if ($code == $k2 . "_" . $k . "_" . $v3['id'] && $code_value != '')
					{
						$this->CI->$data['model']->update(array($k2 => $code_value), array("id" => $v3['id']));
						exit;
					}
					//$this->CI->main_model->update(array($k2=>3),array("id"=>$v3['id']));

					$data2[$k3][$k2] = "<input type=\"string\" value=\"" . $data2[$k3][$k2] . "\" id=\"" . $k2 . "_" . $k . "_" . $v3['id'] . "\" > <input onclick=\"modify_data('" . $k2 . "_" . $k . "_" . $v3['id'] . "','" . $k . "','" . $hook . "')\" type=\"button\" value=\"сохранить\">";
				}
			}
			foreach ($data2 as $k3 => $v3) if (isset($v3[$k2]))
			{
				$data2[$k3][$k2] = "<div class=\"ajax_block\">" . $data2[$k3][$k2] . "</div>";
			}
		}


		$this->CI->mysmarty->assign('data', $data2);
		$this->CI->mysmarty->assign('ajax_fields', $get_data);
		$output = $this->CI->mysmarty->fetch('xml/ajax');
		//$output=iconv('Windows-1251','utf-8',$output);
		header("Content-type: text/xml; charset=utf-8;");
		header("Cache-Control: no-cache");
		echo $output;
		exit;
	}


	/////////////////////////////////
	//
	//used in CHECKBOX validation
	private function list_includes($str, $k)
	{
		$data = $this->data_array[$k];
		if (!isset($data['values'][$str]))
		{
			$this->CI->form_validation->set_message('list_includes', "Неправильное значение поля '%s' ");

			return false;
		}
		else
		{
			return true;
		}
	}
	//CHECKBOX validation
	////////////////////////////////////

	/////////////////////////////////////
	//DATE validation
	//
	private function datetime_validation($str)
	{
		if (preg_match("/^([0-9]{2}).([0-9]{2}).([0-9]{4}) ([0-9]{2}):([0-9]{2})$/", $str, $parts))
		{
			if (checkdate($parts[3], $parts[1], $parts[2])) return true;
			else return false;
		}
		else return false;
	}

	private function date_validation($str)
	{
		if (preg_match("/^([0-9]{2}).([0-9]{2}).([0-9]{4})$/", $str, $parts))
		{
			if (checkdate($parts[3], $parts[1], $parts[2])) return true;
			else return false;
		}
		else return false;
	}

	private function time_validation($str)
	{
		if (preg_match("/^([0-9]{2}):([0-9]{2})$/", $str, $parts))
		{
			if (checkdate($parts[3], $parts[1], $parts[2])) return true;
			else return false;
		}
		else return false;
	}
	//
	//DATE validation
	//////////////////////////////////

	////////////////////////////////////
	// Get data for output table
	public function get_output_rows($filter_array = array(), $select_data = array(), $order_by = 'order_id ASC', $page = 1, $show_per_page = 50)
	{
		$this->main_template = $this->templates_dir . '/generate_table';
		//////////////////////////////////////////////////
		/////
		/////  AJAX OUTPUT TRIGGER
		/////
		$ajax_output = $this->CI->input->post('ajax_output');

		//ajax  change perpage parameter
		$change_per_page = $this->CI->input->post('show_per_page');

		if ($change_per_page != '' && (((int) $change_per_page > 0) || $change_per_page == 'all'))
		{

			$show_per_page = $change_per_page;
			if ($change_per_page == 'all')
				$show_per_page = 0;
		}

		//ajax change page
		$change_page = $this->CI->input->post('page');
		if ($change_page != '' && (int) $change_page > 0)
		{
			$page = $change_page;
		}
		///////////////////////

		//ajax check or uncheck varible
		$set_par = $this->CI->input->post("set_aj_par");
		$set_val = $this->CI->input->post("set_aj_val");
		$set_id  = $this->CI->input->post("set_aj_id");

		if (isset($select_data[$set_par]) && $set_id > 0)
		{
			$this->CI->main_model->update(array($set_par => $set_val), array($this->id_name => $set_id));
			echo json_encode('1');
			exit;
		}
		//////////////


		$delete = $this->CI->input->post('delete');
		if ($delete > 0 && ($this->user_status == '' || ($this->user_status != '' && $this->user_status == 'm')))
		{
			$delete_files = array();
			$delete_names = array();
			foreach ($select_data as $k => $v)
			{
				if ($v['type'] == 'jpg_file' || $v['type'] == 'jcrop')
					$delete_names[] = $k;
			}
			if (count($delete_names) > 0)
			{
				$values = $this->CI->main_model->get(array($this->id_name => $delete), $delete_names, 1);
				foreach ($delete_names as $v)
				{
					@unlink($this->CI->config->item('core_web') . $select_data[$v]['directory'] . $values[$v] . ".jpg");
					@unlink($this->CI->config->item('core_web') . $select_data[$v]['directory'] . $values[$v] . "_small.jpg");
					@unlink($this->CI->config->item('core_web') . $select_data[$v]['directory'] . $values[$v] . ".png");
					@unlink($this->CI->config->item('core_web') . $select_data[$v]['directory'] . $values[$v] . "_small.png");
				}
			}
			$this->CI->main_model->delete(array("id" => $delete));
		}


		//?????????????
		$change_order_by   = $this->CI->input->post('order_by');
		$change_order_type = $this->CI->input->post('order_type');
		if ($change_order_by != '' && isset($select_data[$change_order_by]))
		{
			$change_order_type == 'DESC' ? 'DESC' : 'ASC';
			$order_by = $change_order_by . ' ' . $change_order_type;
		}

		// финальный массив поиска
		$select_array = array();

		// массив фильтрационных данных

		/*
			$filter_data = array(
				'name' 			=>  	array('type'=>'%like%'),	- строковые данные содержащие в себе введеное слово
				'id'   			=>		array('type'=>'='),			- числовые данные с равенством
				'order_id 		=>		array('type'=>'>'),			-
				'birthdate' 	=>		array('type'=>'date','add_type'='>=') 		- дата

			);
		*/


		foreach ($filter_array as $k => $v)
		{

			switch ($v['type'])
			{
				case 'model_list':
					$model_name = $v['model'];
					$this->CI->load->model($model_name);
					isset($v['key']) ? $key = $v['key'] : $key = 'id';
					if (!is_array($v['field'])) $v['field'] = array($v['field']);
					$fields   = array_merge(array($key), $v['field']);
					$data     = $this->CI->$model_name->get(isset($v['select_by']) ? $v['select_by'] : array(), $fields, 0, isset($v['order_by']) ? $v['order_by'] : '');
					$data_new = array();
					if ($data) foreach ($data as $k2 => $v2)
					{
						$dd = $v2;
						unset($dd[$key]);
						$show_fs             = implode(" ", $dd);
						$data_new[$v2[$key]] = $show_fs;
					}
					$filter_array[$k]['values'] = $data_new;

					break;
			}
			if ($val = $this->CI->input->post('ajax_filter_' . $k))
			{
				if (isset($v['var']))
					$k = $v['var'];
				switch ($v['type'])
				{
					case 'array':
						$this->search_array_to_string = true;
						$key                          = $k . ' !=';
						$data                         = preg_replace('!\s+!', ' ', $val);

						$emails       = explode(' ', $data);
						$email_string = '';
						foreach ($emails as $email) if ($email != '')
						{
							$email_string .= (($email_string == '') ? "$k = '$email'" : " OR $k = '$email'");
						}
						$email_string = "'' AND ($email_string)";
						$value        = $email_string;
						break;
					case 'model_list':
					case 'list':
						$key   = $k;
						$value = $val;
						break;
					case '%like%':
						$key   = $k . ' LIKE';
						$value = "%" . $val . "%";
						break;
					case '=':
						$key   = $k;
						$value = $val;
						break;
					case '!=':
						$key   = $k . ' !=';
						$value = $val;
						break;
					case '>':
						$key   = $k . ' >';
						$value = $val;
						break;
					case '<':
						$key   = $k . ' <';
						$value = $val;
						break;
					case '>=':
						$key   = $k . ' >=';
						$value = $val;
						break;
					case '<=':
						$key   = $k . ' <=';
						$value = $val;
						break;
					case 'time':
						if (isset($v['add_type']) && ($v['add_type'] == '>' || $v['add_type'] == '<' || $v['add_type'] == '>=' || $v['add_type'] == '<='))
							$key = $k . ' ' . $v['add_type'];
						else
							$key = $k;
						if (preg_match("/^([0-9]{2}):([0-9]{2})$/", $val, $parts))
						{
							if (checkdate($parts[2], $parts[1], $parts[3]))
							{
								$value = $parts[3] . '-' . $parts[2] . '-' . $parts[1] . ' ' . $parts[4] . ':' . $parts[5];
							}
							else return;
						}
						else return;

						break;
					case 'datetime':
						if (isset($v['add_type']) && ($v['add_type'] == '>' || $v['add_type'] == '<' || $v['add_type'] == '>=' || $v['add_type'] == '<='))
							$key = $k . ' ' . $v['add_type'];
						else
							$key = $k;
						if (preg_match("/^([0-9]{2}).([0-9]{2}).([0-9]{4}) ([0-9]{2}):([0-9]{2})$/", $val, $parts))
						{
							if (checkdate($parts[2], $parts[1], $parts[3]))
							{
								$value = $parts[3] . '-' . $parts[2] . '-' . $parts[1] . ' ' . $parts[4] . ':' . $parts[5];
							}
							else return;
						}
						else return;

						break;
					case 'date':
						if (isset($v['add_type']) && ($v['add_type'] == '>' || $v['add_type'] == '<' || $v['add_type'] == '>=' || $v['add_type'] == '<='))
							$key = $k . ' ' . $v['add_type'];
						else
							$key = $k;
						if (preg_match("/^([0-9]{2}).([0-9]{2}).([0-9]{4})$/", $val, $parts))
						{

							if (checkdate($parts[2], $parts[1], $parts[3]))
							{
								$value = $parts[3] . '-' . $parts[2] . '-' . $parts[1];
							}
							else return;
						}
						else return;

						break;


				}
				$select_array[$key] = $value;
			}
		}

		$limit  = ($show_per_page);
		$offset = $show_per_page * ($page - 1);

		$move_up = $this->CI->input->post("move_up");
		if ($move_up > 0)
		{
			$this->CI->main_model->move($move_up, "up");
			redirect($this->header . $page);
		}


		// ajax PUSH operation
		$push_from = $this->CI->input->post("push_from");
		$push_to   = $this->CI->input->post("push_to");
		$push_type = $this->CI->input->post('push_type');


		strpos($order_by, 'ASC') ? $order = 'ASC' : $order = 'DESC';

		$this->order = $order;

		if ($push_from > 0 && $push_to > 0 && ($push_type == 'forward' || $push_type == 'backward') && (strpos($order_by, 'ASC') || strpos($order_by, 'DESC')))
		{


			if ($this->CI->main_model->push($push_from, $push_to, $push_type, $order))
			{
				$this->final_touch(true);
				echo json_encode(array('done' => 1));
				exit;
			}
		}

		$move_down = $this->CI->input->post("move_down");
		if ($move_down > 0)
		{
			$this->CI->main_model->move($move_down, "down");
			redirect($this->header . $page);
		}


		$fav = $this->CI->input->post('fav');
		if ($fav > 0)
		{
			$data = $this->CI->main_model->get(array($this->id_name => $fav), array("favourite"), 1);
			if ($data['favourite'] == 1)
				$obj->update(array("favourite" => 0), array($this->id_name => $fav));
			else
				$obj->update(array("favourite" => 1), array($this->id_name => $fav));
		}

		$data         = explode(' ', $order_by);
		$set_order_id = $data[0];
		$sel_d        = array($this->id_name, $set_order_id . " AS order_id");

		foreach ($select_data as $k => $v)
		{
			if ($v['type'] == 'date') $k = "DATE_FORMAT(`$k`, '%d.%m.%Y') as `$k`";
			if ($v['type'] == 'datetime') $k = "DATE_FORMAT(`$k`, '%d.%m.%Y %H:%i') as `$k`";
			if ($v['type'] == 'time') $k = "DATE_FORMAT(`$k`, '%H:%i') as `$k`";
			if ($v['type'] != 'ajax_data')
				if ($v['type'] == 'jcrop') $k = " `$k` , `" . $v['image_type_name'] . "` ";
			$sel_d[] = $k;
		}
		$total_cols = count($sel_d) + 1;

		foreach ($select_data as $k => $v)
		{
			switch ($v['type'])
			{
				case "model_list":
					$model_name = $v['model'];
					$this->CI->load->model($model_name);

					isset($v['key']) ? $key = $v['key'] : $key = 'id';


					if (!is_array($v['field'])) $v['field'] = array($v['field']);
					$fields = array_merge(array($key), $v['field']);


					$data     = $this->CI->$model_name->get(isset($v['select_by']) ? $v['select_by'] : array(), $fields, 0);
					$data_new = array();
					if ($data) foreach ($data as $k2 => $v2)
					{
						$dd = $v2;
						unset($dd[$key]);
						$show_fs             = implode(" ", $dd);
						$data_new[$v2[$key]] = $show_fs;
					}
					$select_data[$k]['key']    = $key;
					$select_data[$k]['values'] = $data_new;
					break;
				case "ajax_data":
					break;
			}
		}
		$select_string = "";

		$select_array = array_merge($select_array, $this->select_where);
		if ($this->search_array_to_string == true)
		{
			foreach ($select_array as $k => $v)
			{
				if (!preg_match('!\s+!', $v, $m))
					$v = "'$v'";
				$select_string .= ($select_string == '') ? "$k $v" : " AND $k $v";
			}
		}
		$dt = $this->CI->main_model->bget(($select_string == '') ? $select_array : $select_string, $sel_d, 0, $order_by, $limit, $offset);


		if ($dt['result']) foreach ($dt['result'] as $row_n => $row)
		{
			foreach ($select_data as $col_n => $col)
			{
				//выписываем данные
				switch ($col['type'])
				{
					case "jcrop":
						$dt['result'][$row_n][$col_n] = array('value' => $row[$col_n], 'image_type' => $dt['result'][$row_n][$col['image_type_name']]);
						break;
					case "ajax_data":
						$model_name = $col['model'];
						$this->CI->load->model($model_name);

						$data     = $this->CI->$model_name->get(array($col['hook'] => $dt['result'][$row_n]['id']), $col['get'], 0);
						$data_new = array();
						if ($data) foreach ($data as $k2 => $v2)
						{
							$dd = $v2;
							if (isset($dd['id']))
								unset($dd['id']);
							$show_fs    = implode(" ", $dd);
							$data_new[] = $show_fs;
						}
						$dt['result'][$row_n][$col_n] = "<div class=\"row_row\">" . implode("</div><div class=\"row_row\">", $data_new) . "</div>";

						break;
				}
			}
		}
		$hide_id = 0;
		foreach ($select_array as $k => $v)
		{
			if ($k == 'id')
				$hide_id = 1;
		}
		$pages_info = $this->CI->common_f->pages($dt['rows'], $page, $show_per_page);
		$this->CI->mysmarty->assign('total_cols', $total_cols);
		$this->CI->mysmarty->assign('page', $page);
		$this->CI->mysmarty->assign('hide_id', $hide_id);
		$this->CI->mysmarty->assign('pages_info', $pages_info);
		$this->CI->mysmarty->assign('select_names', $select_data);
		$this->CI->mysmarty->assign('output_rows', $dt['result']);
		$this->CI->mysmarty->assign('default_order', $set_order_id);
		$this->CI->mysmarty->assign('total_rows', $dt['rows']);
		$this->CI->mysmarty->assign('head_link', $this->header);
		$this->CI->mysmarty->assign('show_per_page_array', $this->show_per_page_array);
		$this->CI->mysmarty->assign('show_per_page', $show_per_page);

		$this->CI->mysmarty->assign('filter_array', $filter_array);

		// present output in xml
		if ($ajax_output == 1)
		{
			$this->final_touch(true);
			header("Content-type: text/xml; charset=utf-8;");
			header("Cache-Control: no-cache");
			echo $this->CI->mysmarty->fetch($this->templates_dir . '/xml_ajax_table_output');
			exit;
		}
	}
	////////////////////////////////////
	// main part initialisation
	public function init($data = array())
	{
		$this->active_string_search();
		$this->data_array = $data;
		if ($this->data_array != array())
		{

			$this->main_template = $this->templates_dir . '/generate_structure';
			//in case of edit get default object data
			if ($this->id > 0)
			{
				$show_data = $this->CI->main_model->get(array("id" => $this->id), $this->get_data_names(), 1);
				$this->CI->mysmarty->assign('post', $show_data);
			}
			elseif ($this->id == 0)
			{
				if ($this->user_status != '' && $this->user_status != 'm' && $this->user_status != 'p')
				{
					$this->restric_access();

					return;
				}
			}

			/////////////////////////
			//ON POST
			//ON POST
			//ON POST
			$post         = $this->CI->input->post('post');
			$save_changes = $this->CI->input->post('save_changes');

			//ansotov если теги есть то записываем их
			if ($save_changes == 1)
			{
				//ansotov получаем список тегов
				$tags = $this->CI->input->post('tags');

				//ansotov теги для текущего продукта
				$this->CI->load->model('mproduct_tags');

				$this->CI->mproduct_tags->delete(array('product_id' => $this->id));

				if ($tags)
				{
					$this->CI->load->model('mfolders');

					//ansotov получаем section_id для тегов
					$url     = '/catalog/' . $this->CI->input->post('type') . '/';
					$section = $this->CI->mfolders->get(array('url' => $url), 'id', 1);

					foreach ($tags as $key => $value)
					{
						$this->CI->mproduct_tags->insert(array('product_id' => $this->id, 'tag_id' => $value, 'section_id' => $section['id'], 'c_type' => $this->CI->input->post('c_type')));
					}
				}

				//ansotov категории промокодов
				$promocode_categories = $this->CI->input->post('promocode_categories');

				//ansotov теги для текущего продукта
				$this->CI->load->model('mpromocode_categories');
				$this->CI->mpromocode_categories->delete(array('promocode_id' => $this->id));

				if ($promocode_categories)
				{
					foreach ($promocode_categories as $k => $v)
					{
						$this->CI->mpromocode_categories->insert(array('promocode_id' => $this->id, 'cat_id' => $v));
					}
				}
			}


			if ($post > 0)
			{
				$post_data = array();
				/////////////////////////////////
				//taking prevalidation post data operations
				/////////////////////////////////
				foreach ($this->data_array as $k => $v)
				{
					isset($v['not_required']) ? $required = "" : $required = "required";

					switch ($v['type'])
					{
						case "string":
							$this->CI->form_validation->set_rules($k, $v['name'],/* "trim|max_length[200]|alpha_dotdash|$required" */
								isset($v['convert_text']) ? "prep_for_form" : "trim" . "|max_length[100000]|$required");
							$post_data[] = mysql_real_escape_string($k);
							break;
						case "integer":
							$this->CI->form_validation->set_rules($k, $v['name'], "trim|max_length[2000000000]|numeric|$required");
							$post_data[] = $k;
							break;
						case "date":
							$this->CI->form_validation->set_rules($k, $v['name'], "trim|callback_date_validation|$required");
							$post_data[] = $k;
							break;
						case "datetime":
							$this->CI->form_validation->set_rules($k, $v['name'], "trim|callback_datetime_validation|$required");
							$post_data[] = $k;
							break;
						case "time":
							$this->CI->form_validation->set_rules($k, $v['name'], "trim|callback_time_validation|$required");
							$post_data[] = $k;
							break;
						case "text":
							$this->CI->form_validation->set_rules($k, $v['name'], "trim|max_length[100000]|prep_for_form|$required");
							$post_data[] = $k;
							break;
						case "list":
							$this->CI->form_validation->set_rules($k, $v['name'], "trim|callback_list_includes|$required");
							$post_data[] = $k;
							break;
						case "model_list":
							$this->CI->form_validation->set_rules($k, $v['name'], "trim|callback_list_includes|$required");
							$post_data[] = $k;
							break;
						case "checkbox":
							$this->CI->form_validation->set_rules($k, $v['name'], "max_length[1]|numeric");
							$post_data[] = $k;
							break;
						case "jpg_file":
							if ($required != '' && ($this->id == 0 || ($this->id > 0 && !isset($show_data[$k]))))
								$this->CI->form_validation->set_rules($k, $v['name'], 'file_required');
							//if request for delete is send
							$delete_photo = $this->CI->input->post("delete_" . $k);
							if ($delete_photo > 0)
							{
								$inf = $this->CI->main_model->get(array("id" => $delete_photo), array($k), 1);
								unlink($this->CI->config->item('core_web') . $v['directory'] . $inf[$k] . ".jpg");
								unlink($this->CI->config->item('core_web') . $v['directory'] . $inf[$k] . "_small.jpg");
								$this->CI->main_model->update(array($k => ""), array("id" => $delete_photo));
								redirect($_SERVER['REQUEST_URI']);
							}
							break;
						case "flv_file":
							if ($required != '' && ($this->id == 0 || ($this->id > 0 && !isset($post_data[$k])))) $this->CI->form_validation->set_rules($k, $v['name'], 'file_required');
							//if request for delete is send
							$delete_flv = $this->CI->input->post("delete_" . $k);
							if ($delete_flv > 0)
							{
								$inf = $this->CI->main_model->get(array("id" => $delete_flv), array($k), 1);
								unlink($this->CI->config->item('core_web') . $v['directory'] . $inf[$k] . ".flv");
								$this->CI->main_model->update(array($k => ""), array("id" => $delete_flv));
								redirect($this->header);
							}
							break;

						case "any_file":
							if ($required != '' && ($this->id == 0 || ($this->id > 0 && !isset($post_data[$k])))) $this->CI->form_validation->set_rules($k, $v['name'], 'file_required');
							//if request for delete is send
							$delete_any = $this->CI->input->post("delete_" . $k);
							if ($delete_any > 0)
							{
								$inf = $this->CI->main_model->get(array("id" => $delete_any), array($k), 1);
								unlink($this->CI->config->item('core_web') . $v['directory'] . $inf[$k]);
								$this->CI->main_model->update(array($k => ""), array("id" => $delete_any));
								redirect($this->header);
							}
							break;
						case "ajax_data":
							$ajax_field = $this->CI->input->post("ajax_field");
							if ($ajax_field == $k) $this->xml_data($k);

							//загрузка временной фотографии
							$ajax_photo_upload = $this->CI->input->post('ajax_photo_upload');
							$folder_name       = $this->CI->input->post('folder_name');
							if ($ajax_photo_upload == $k)
								$this->ajax_photo_upload($k, $folder_name);
							//вывод временной фотографии
							$ajax_photo_show = $this->CI->input->post('ajax_photo_show');
							if ($ajax_photo_show == $k && isset($v['folders'][$folder_name]))
								$this->ajax_photo_show($v['folders'][$folder_name]['directory'], $k, $folder_name);

							//  ???????????
							foreach ($v['folders'] as $fk => $fv)
							{
								if ($fv['type'] == 'list_string')
								{
									$model_name = $fv['get_from'];
									$this->CI->load->model($model_name);
									$data                                           = $this->CI->$model_name->get(array(), array("id", $fv['get_what']), 1);
									$this->data_array[$k]['folders'][$fk]['values'] = $data;
								}
							}

							break;
						////////////////////////////////////
						//admin 2.0 functions
						case "checklist":
							$insert_checklist = $this->CI->input->post($k . '_insert');
							$values_set       = $this->CI->input->post($k);

							if ($insert_checklist == 1)
							{
								$model = $v['model'];
								$this->CI->load->model($model);
								if ($this->id > 0)
									$hook_id = $this->id;
								else
								{
									$hook_id = ($this->user_id - (2 * $this->user_id));
								}

								$values_possible = $v['values'];


								if ($values_set) foreach ($values_set as $k2 => $v2)
								{
									if (!isset($values_possible[$v2]))
										unset($values_set[$k2]);
								}


								if (isset($v['fixed_data']))
									$where_array = $v['fixed_data'];
								$where_array[$v['hook']] = $hook_id;


								$this->CI->$model->delete($where_array);
								if ($values_set) foreach ($values_set as $k2 => $v2)
								{
									$insert              = $where_array;
									$insert[$v['field']] = $v2;
									$this->CI->$model->insert($insert);
								}
							}
							break;
						case "ajax_photogallery":
							$this->ajax_gallery_photo_upload($k);
							break;
						// добавлеение фотографий c возможность вставить в текст
						case "ajax_photogallery_insert":
							$this->ajax_gallery_photo_upload($k, true);
							break;
						case "jcrop":
							$delete_photo = $this->CI->input->post("delete_" . $k);
							if ($delete_photo > 0)
							{
								$inf = $this->CI->main_model->get(array("id" => $delete_photo), array($k, $v['image_type_name']), 1);
								unlink($this->CI->config->item('core_web') . $v['directory'] . $inf[$k] . "." . $inf[$v['image_type_name']]);
								unlink($this->CI->config->item('core_web') . $v['directory'] . $inf[$k] . "_small." . $inf[$v['image_type_name']]);
								$this->CI->main_model->update(array($k => ""), array("id" => $delete_photo));
								redirect($_SERVER['REQUEST_URI']);
							}
							$cropped = $this->CI->input->post($k . '_save_croped');
							if (($this->id == 0 || ($this->id > 0 && $show_data[$k] == '')) && $cropped != 1)
							{
								$this->CI->form_validation->set_rules($k, $v['name'], "$required");
							}
							$this->ajax_jcrop($k);
							break;
						//admin 2.0 functions
						////////////////////////////////////

					}
				}

				$fdata = $this->CI->form_validation->get_field_data();


				if ($this->CI->form_validation->run() || $fdata == array())
				{
					$this->put_posts_to_insert($post_data);
					//taking postvalidation post data
					foreach ($this->data_array as $k => $v)
					{
						switch ($v['type'])
						{
							case $v['type'] == "date" && isset($this->insert_data_array[$k]):
								preg_match("/^([0-9]{2}).([0-9]{2}).([0-9]{4})$/", $this->insert_data_array[$k], $parts);
								$this->insert_data_array[$k] = $parts[3] . "-" . $parts[2] . "-" . $parts[1];
								break;

							case $v['type'] == "datetime" && isset($this->insert_data_array[$k]):
								preg_match("/^([0-9]{2}).([0-9]{2}).([0-9]{4}) ([0-9]{2}):([0-9]{2})$/", $this->insert_data_array[$k], $parts);
								$this->insert_data_array[$k] = $parts[3] . "-" . $parts[2] . "-" . $parts[1] . " " . $parts[4] . ':' . $parts[5];
								break;

							case $v['type'] == "time" && isset($this->insert_data_array[$k]):
								preg_match("/^([0-9]{2}):([0-9]{2})$/", $this->insert_data_array[$k], $parts);
								$this->insert_data_array[$k] = $parts[1] . ':' . $parts[2];
								break;
							/*
							case "jcrop":
								if(isset($_POST[$k.'_jcrop_picture_adress'])){
									$this->insert_data_array[$k]=$_POST[$k.'_jcrop_picture_adress'];
								}
								break;*/
							case 'string':
								$this->insert_data_array[$k] = htmlspecialchars($this->insert_data_array[$k], null, 'utf-8');
								break;
							case "jpg_file":
								if (isset($_FILES[$k]['name']) && $_FILES[$k]['name'] != '')
								{
									$string = $this->CI->common_f->genRandomString(20);
									$string = $string;
									if (isset($v['max_small_width']) && isset($v['max_small_height']))
									{
										$this->photo_upload($k, $v['directory'], $string, $v['max_width'], $v['max_height'], $v['max_small_width'], $v['max_small_height']);
									}
									else
										$this->photo_upload($k, $v['directory'], $string, $v['max_width'], $v['max_height']);
									$this->insert_data_array[$k] = $string;
								}
								break;
							case "flv_file":
								if (isset($_FILES[$k]['name']) && $_FILES[$k]['name'] != '')
								{
									$string = $this->CI->common_f->genRandomString(20);
									$string = $string;
									$this->file_upload($k, $v['directory'], $string, "flv");
									$this->insert_data_array[$k] = $string;
								}
								break;

							case "any_file":
								if (isset($_FILES[$k]['name']) && $_FILES[$k]['name'] != '')
								{
									$string    = $this->CI->common_f->genRandomString(20);
									$string    = $string;
									$extension = substr($_FILES[$k]['name'], strrpos($_FILES[$k]['name'], '.') + 1);
									$this->file_upload($k, $v['directory'], $string, $extension);
									$this->insert_data_array[$k] = $string . '.' . $extension;
								}
								break;
						}
					}
					//if there is no set id
					if ($this->id == 0)
					{
						$action   = 'insert';
						$this->id = $this->CI->main_model->insert($this->insert_data_array);
					}
					else
					{
						$action = 'update';
						if ($this->insert_data_array != array())
							$this->CI->main_model->update($this->insert_data_array, array('id' => $this->id));
					}


					$this->set_core_model();
					//adding ajax update
					foreach ($this->data_array as $k => $v)
					{
						switch ($v['type'])
						{
							case "ajax_photogallery":
								$model = $v['model'];
								//$this->set_model($model);
								$this->CI->load->model($model);
								$hook = $v['hook'];
								if ($action == 'update')
									$hook_id = $this->id;
								else
								{
									$hook_id = ($this->user_id - (2 * $this->user_id));
								}
								if (isset($v['fixed_data']))
									$where_array = $v['fixed_data'];
								$where_array[$hook] = $hook_id;

								$this->CI->$model->update(array($hook => $this->id), $where_array);
								// return main model
								//$this->set_model($this->core_model);
								break;
							//список из нескольких значений
							/*
							case "checklist":
								$model = $v['model'];
								$this->CI->load->model($model);
								if($action == 'update')
									$hook_id = $this->id;
								else{
									$hook_id = ($this->user_id - (2*$this->user_id));
								}
								if(isset($v['fixed_data']))
									$where_array = $v['fixed_data'];
								$where_array[$hook]=$hook_id;

								$this->CI->$model->update(array($hook=>$this->id),$where_array);
							break;
							*/
							case "ajax_photogallery_insert":
								$model = $v['model'];
								//$this->set_model($model);
								$this->CI->load->model($model);
								$hook = $v['hook'];
								if ($action == 'update')
									$hook_id = $this->id;
								else
								{
									$hook_id = ($this->user_id - (2 * $this->user_id));
								}
								$where_array        = $v['fixed_data'];
								$where_array[$hook] = $hook_id;

								$this->CI->$model->update(array($hook => $this->id), $where_array);
								// return main model
								//$this->set_model($this->core_model);
								break;

							case "checklist":
								$insert_checklist = $this->CI->input->post($k . '_insert');

								if ($insert_checklist == 1 && $action == 'insert')
								{
									$model = $v['model'];
									$this->CI->load->model($model);
									$hook_id = ($this->user_id - (2 * $this->user_id));

									if (isset($v['fixed_data']))
										$where_array = $v['fixed_data'];

									$where_array[$v['hook']] = $hook_id;
									$this->CI->$model->update(array($v['hook'] => $this->id), $where_array);
								}
								break;
							case "jcrop":
								$croped = $this->CI->input->post($k . '_save_croped');
								if ($croped == 1)
								{
									$string     = $this->CI->common_f->genRandomString(20);
									$string     = $string;
									$image_type = $v['image_type'];
									$file_name  = $k . '_' . $this->user_id;
									rename($this->CI->config->item('core_web') . $v['directory'] . $file_name . '_cropped.' . $image_type, $this->CI->config->item('core_web') . $v['directory'] . $string . '.' . $image_type);
									rename($this->CI->config->item('core_web') . $v['directory'] . $file_name . '_cropped_small.' . $image_type, $this->CI->config->item('core_web') . $v['directory'] . $string . '_small.' . $image_type);
									if (isset($v['any_image']) && $v['any_image'] == true && isset($v['image_type_name']))
									{
										$this->CI->main_model->update(array($v['image_type_name'] => $image_type, $k => $string), array('id' => $this->id));
									}
									else
										$this->CI->main_model->update(array($k => $string), array('id' => $this->id));
								}
								break;
						}

					}
					$save_on_change = $this->CI->input->post('save_on_change');

					///*
					if ($save_on_change == 0)
						redirect($this->CI->config->item('base_url') . $this->header, '');
					else
						redirect($this->CI->config->item('base_url') . $this->on_change_header . $this->id);
					//*/
				}
			}


			///////////////////////////////////////
			//finishing data grabbing
			///////////////////////////////////////

			foreach ($this->data_array as $k => $v)
			{
				switch ($v['type'])
				{
					case "model_list":
						$model_name = $v['model'];
						$this->CI->load->model($model_name);
						if (isset($v['order_by'])) $order_by = $v['order_by'];
						else $order_by = '';
						if (isset($v['select_by'])) $select_by = $v['select_by'];
						else $select_by = array();
						if (!is_array($v['field'])) $v['field'] = array($v['field']);


						isset($v['key']) ? $key = $v['key'] : $key = 'id';

						$fields = array_merge(array($key), $v['field']);

						$data                           = $this->CI->$model_name->get($select_by, $fields, 0, $order_by);
						$this->data_array[$k]['values'] = $data;
						$this->data_array[$k]['key']    = $key;
						break;
					case "ajax_data":
						foreach ($v['folders'] as $fk => $fv)
						{
							if ($fv['type'] == 'list_string')
							{
								$model_name = $fv['get_from'];
								$this->CI->load->model($model_name);
								$data                                           = $this->CI->$model_name->get(array(), array("id", $fv['get_what']), 0, "order_id ASC");
								$this->data_array[$k]['folders'][$fk]['values'] = $data;
							}
						}
						break;
					case "checklist":
						$model = $v['model'];
						$this->CI->load->model($model);
						$hook_id = $this->id;

						$where_array = array();

						if (isset($v['fixed_data']))
							$where_array = $v['fixed_data'];

						$where_array[$v['hook']] = $hook_id;


						$list = $this->CI->$model->get($where_array, array());
						if ($list) foreach ($list as $k2 => $v2)
						{
							$this->data_array[$k]['values'][$v2[$v['field']]]['checked'] = true;
						}
						break;
				}
			}
			$this->CI->mysmarty->assign('structure', $this->data_array);
		}
		$this->final_touch();
	}

    public function initUnloading()
    {
        $this->main_template = $this->templates_dir . '/unloading';
        $this->final_touch();
    }

	////////////////////////////////////////
	///final_script
	private function final_touch($xml_mode = false)
	{
		$errors = $this->CI->form_validation->get_errors();
		$this->CI->mysmarty->assign('errors', $errors);

		if (($_POST != array() && $this->force_validation_load != -1) || $this->force_validation_load == 1)
			$this->CI->mysmarty->assign('post', $this->CI->form_validation->get_postdata());

		$this->CI->mysmarty->assign('dir', $this->dir_name);


		$this->CI->mysmarty->assign('load_page', $this->main_template);
		$this->CI->mysmarty->assign('title_name', $this->title_name);
		if ($this->create_links == array() && $this->show_create_links == true)
		{
			$this->create_links = array(
				"create" => array("name" => "Create", "link" => "open_" . $this->core_name),
				"list"   => array("name" => "Back to list", "link" => $this->core_name),
				"link"   => array("name" => "Edit", "link" => "open_" . $this->core_name)
			);
		}
		$this->CI->mysmarty->assign('date_now', @date('d.m.Y'));
		$this->CI->mysmarty->assign('datetime_now', @date('d.m.Y H:i'));
		$this->CI->mysmarty->assign('links', $this->create_links);
		$this->CI->mysmarty->assign('end_link', $this->end_link);
		$this->CI->mysmarty->assign('core_name', $this->core_name);
		$this->CI->mysmarty->assign('id', $this->id);
		$this->CI->mysmarty->assign('links_after', $this->links_after);
		$this->CI->mysmarty->assign('links_before', $this->links_before);
		$this->CI->mysmarty->assign('header', $this->header);
		$this->CI->mysmarty->assign('order', $this->order);
		$this->CI->mysmarty->assign('rand', $this->CI->common_f->genRandomString(20));
		if ($this->user_status != '')
		{
			$this->CI->mysmarty->assign('user_id', $this->user_id);
			$this->CI->mysmarty->assign('auth_code', $this->auth_code);
			$this->CI->mysmarty->assign('user_status', $this->user_status);
			$this->CI->mysmarty->assign('user_auth', $this->user_auth);
		}
		if ($xml_mode == false)
			$this->CI->output->set_output($this->CI->mysmarty->fetch($this->core_template));
	}
}

?>
