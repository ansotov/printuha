function showForm(height,width,fclass){
	txtdata = '<div id="boxes">'+  
					'<div id="dialog" class="'+fclass+'">'+
					'</div>'+
					'<div id="mask" class="mask"></div>'+
				'</div>'
		$('body').prepend(txtdata)
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
		
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		$('#mask').fadeIn(1000);    
		$('#mask').fadeTo("slow",0.8);  
		
		var winH = $(window).height();
		var winW = $(window).width();
		
		$('#dialog').css('top',  10)
		$('#dialog').css('top',  ((winH - height)/ 2)>0?((winH - height)/2)+$(window).scrollTop():20+$(window).scrollTop())
		$('#dialog').css('left', (winW - width)/2)
		$("#dialog").width(width)
		//$("#dialog").height(height)
		$('#dialog').fadeIn(2000);
		$("#dialog").html('<img src="img/cg/close.png" width="10" height="10" alt="�������" id="close">');
		$("#dialog #close").click(function(){
			$(this).fadeTo("slow",0.0);
			$('.'+fclass).fadeTo("slow",0.0,function(){
				$("#boxes").remove();
			});
		})
		$('#mask').click(function () {
			$(this).fadeTo("slow",0.0);
			$('.'+fclass).fadeTo("slow",0.0,function(){
				$("#boxes").remove();
			});
		});
}
function showProduct(id){
	showForm(640,640,'product')
	$.ajax({
		  url: 				'/ajax/product/'+id+'/',
		  dataType: 		'json',
		  type:				'POST',
		  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
		  data:    			{},
		  beforeSend:		function(){
								
							},
		  success: 			function(data) {
			  					var str = ""
								$.each(data.sizes, function(k,v){
									str += "<option value='"+v+"'>"+v+"</option>"
								}); 
								$("#dialog").append('<picture>'+
								'<img src="img/pictures/'+data.picture+'.jpg">'+
								'</picture><picinfo>'+
									'<h1>'+data.name+'</h1>'+
									'<article>���. '+data.article+'<br>'+data.structure+'</article>'+
									'<price>'+data.price+' ���.</price>'+
									'<select id="size"><option value="">������� ������</option>'+str+'</select>'+
									'<input onclick="addToCart('+data.id+')" class="submit" type="submit" value="�������� � �������">'+
								'</picinfo>')
								 
							}
	});

	
}
function addToCart(product_id){
	$size = $("#dialog #size").val()
	if ($size ==''){
		alert('�� ������ ������ ������');
		return false
	}
	$.ajax({
		  url: 				'/ajax/add/'+product_id+'/',
		  dataType: 		'json',
		  type:				'POST',
		  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
		  data:    			{'size':$size},
		  beforeSend:		function(){
								
							},
		  success: 			function(data) {
			  					if(data != 'error'){
									$("cartcount").html(data)
			  						$("#dialog #close").click()
								}else
									alert('��������� ������') 
							}
	});
}
function deleteProduct($row_id){
	$.ajax({
		  url: 				'/ajax/delete/',
		  dataType: 		'json',
		  type:				'POST',
		  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
		  data:    			{'rowid':$row_id},
		  beforeSend:		function(){
								
							},
		  success: 			function(data) {
			  					if(data.result == 'success'){
									$("#total_price").html(data.total+' ���.')
									$("cartcount").html(data.total_items)
									$('#'+$row_id).hide(400,function(){
										$(this).remove();
									})
								}else
									alert('��������� ������') 
							}
	});
}
function sendOrder(){
	name = $("#dialog #iname").val()
	phone = $("#dialog #iphone").val()
	email = $("#dialog #iemail").val()
	if( name==''){
		alert('�� ��������� ���� `���`');
		return
	}
	
	if( phone==''){
		alert('�� ��������� ���� `�������`');
		return
	}
	
	if( email==''){
		alert('�� ��������� ���� `E-mail`');
		return
	}
	if( email !='' && !validateEmail(email)){
		alert('� ���� `E-mail` ������������ �����');
		return
	}
	
	$.ajax({
		  url: 				'/ajax/send/',
		  dataType: 		'json',
		  type:				'POST',
		  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
		  data:    			{'name':name,'phone':phone,'email':email},
		  beforeSend:		function(){
								
							},
		  success: 			function(data) {
			  
								$("cartcount").html('0')
			  					$('#dialog #inner_stuff').empty();
								$('#dialog #inner_stuff').append('<instructions>�������, ��� ����� ���������.</instructions>'+
								'<underline>�������� �������� � ���� � ������� 30 ����� ����� ���������� ������, <br>'+
								'��� ������� ��� ����� ��������� � ������� �����.<br>'+
								'���� ����� �������� �� � ������� �����, �������� �������� <br>'+
								'� ���� ����� ���������� �������� ���.</underline>'); 
							}
	});
	
}
function showHelp(){
	
}
function formOrder(){
	if( $("cartcount").html()=='0'){
		alert('� ����� ������� ��� �������.')
		return
	}
	var instructions = '������� ��� ���������� e-mail � ����� ��������,<br>'+
	'����� ��� �������� ��� ��������� � ���� � ��������<br>'+
	'����������� ������.'
	$('#dialog #inner_stuff').empty();
	$('#dialog #inner_stuff').append('<instructions>'+instructions+'</instructions>'+
	'<block><name>���:</name><input id="iname" name="name" type="text" required class="text"></block>'+
	'<block><name>�������:</name><input id="iphone" name="phone" type="phone" required class="text"></block>'+
	'<block><name>E-mail:</name><input id="iemail" type="email" name="email" required class="text"></block>'+
	'<input type="button" onClick="sendOrder()" class="submit" value="�������� �����">'+
	'<underline>�������� �������� � ���� � ������� 30 ����� ����� ���������� ������, <br>'+
	'��� ������� ��� ����� ��������� � ������� �����.<br>'+
	'���� ����� �������� �� � ������� �����, �������� �������� <br>'+
	'� ���� ����� ���������� �������� ���.</underline>');
	$('#dialog').css('top',  (20+$(window).scrollTop()))
}
function showHow(){
showForm(640,640,'how')
$("#dialog").append("<h1>��� �������� �����</h1>"+
'<p>����� �������� �����, ��� ���������� ������� � ������ ���� ������� � ������ ������� ���� �����, ��� �� ������ ��� ��� ����������� ���� ����� � ����������� ��� �������� ������ �������� �����.<br>'+
'� ����������� ���� ��� ���������� ����� ������� ���� ���, email � ������� ��� �����.<br>'+
'� ������� 30 ����� � ���� �������� �������� ��������-�������� � ������� ������ ��������.<br>'+
'��������� ��������!</p>');

}
function showInfo(){
	showForm(640,640,'big_info')
	$("#dialog").append("<h1>����������</h1>"+
	"<p>������ ������ ��������-�������� ������ Cyrille Gassiline �������� ��������.<br>"+
"� ��������� ����� ����� �������� ����������� ������ �����, ��� ����� ������������ �� ������ ��������� �������� � ������������ �������, �� ����� ������ ����� ������ � ��������� �������, ����������� �������������� � ��������-��������.</p>"+
"���������� � ������� ������� � ��������-�������� Cyrille Gassiline ��������� �����������. ��� ��������, ��� ��� ������, �������������� �� �����, ���� �� ������ � ������ � �������� � ����� �����. �������� �� ������ ���������.<br>"+
"������� �������� ������ ������ �� ����������� ����� <a href=\"mailto:shop@cyrillegassiline.com\">shop@cyrillegassiline.com</a> ��� �� �������� (495) 647-65-45</p>")

}
function showHelp(){
	showForm(640,640,'help')
	$("#dialog").append("<h1>������</h1>"+
	'<p>�� ������ ������ ����� ������ ���������� ��������-�������� �� email <a href="mailto:shop@cyrillegassiline.com">shop@cyrillegassiline.com</a> ��� �� �������� (495) 647-65-45<p>'+
	'<p><strong>��� ���������� ������� �� �����?</strong><br>'+
	'��� ���� ����� ���������� ������������� ��� ������, �� ������ ��������� �� �������� (495) 647-65-45 ��� �������� �� <a href="mailto:shop@cyrillegassiline.com">shop@cyrillegassiline.com</a> , � ��� ���������� �������� � ���� � ������� ����� � ������� �������� �����.</p>'+
	'<p><strong>��� � ���� �������� �����, ��������� � ��������-��������?</strong><br>'+
	'�� ������ ������ ���������� ���� ����� ��������� ������ ����� ��������: �������� ������. � ��������� ����� ����� �������� ������� ������ ���������� ������ � ������������ ��������.</p>'+
'<p><strong>����� �������� �����, ������� � �������/�?</strong><br>'+
'�������� �������������� � ������� 1-2 ����. �������� �� ������ ���������.<br>'+
'��� ��������� �������� ������������� �������� � ���������� ���������� ������ �� �������� (495) 647-65-45.</p>'+
'<p><strong>���� �� ���-��� ������ Cyrille Gassiline, ��� � ���� �������� ������������� ������?</strong><br>'+
'���-��� ������ ��������� �� ������: �.������, ��. �����������, �.15, ���. 41 (���������� ���������� �����������).<br>'+
'����� ������: � ������������ �� ������� � 10-00 �� 18-00.</p>'+
'������������ ����������� ��� ������� �������� ������� ������ � ����� ����������.</p>'

	)
}

function showCart(){
	showForm(640,640,'cart')
	$.ajax({
		  url: 				'/ajax/cart/',
		  dataType: 		'json',
		  type:				'POST',
		  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
		  data:    			{},
		  beforeSend:		function(){
								
							},
		  success: 			function(data) {
			  					var $items = '';
								$.each(data.contents, function(k,v){
									$items += '<item id="'+k+'"><preview><img class="preview" src="img/pictures/'+v.options.picture+'_small.jpg"></preview><infoblock>'+v.name+'<br>���. '+v.options.article+'<br>������: '+v.options.size+'<br><span onClick="deleteProduct(\''+k+'\')" class="delete">�������</span></infoblock><price>'+v.price+' ���.</price></item>'
								}); 
			  					$("#dialog").append('<h1>�������</h1>'+
						'<div id="inner_stuff"><items>'+ $items +'</items>'+'<total><word>�����:</word><price id="total_price">'+data.total+' ���.</price>'+
						'<info>�������� ���������!</info></total>'+
'<payment><info>'+
'�� ������ ������ ������ ������������ ������ ��������� �������.<br>'+
'� ��������� �����  ����� �������� ������� ������ ���������� ������ � ������������ ��������.</info>'+
'<input type="button" onClick="formOrder()" class="submit" value="�������� �����"></payment></div>')
							}
	});
	
}

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
	return re.test(email);
}