<?php
class Mmodel extends CI_Model
{
	protected $table="";
	protected $id="";
	protected $order_array=array();
	protected $default_order='order_id';


	public function __construct(){
		parent::__construct();
		//$this->db->query("SET lc_time_names = 'ru_RU'");
	}
	public function set_time_names($lang){
		$this->db->query("SET lc_time_names = '".$lang."'");
	}

	public function search($select_data,$fields_array,$words,$where_data=array()){
		$select_data!=array()?$this->db->select($select_data):$this->db->select('*');
		$this->db->get_where($this->table,$where_data);
		$query = $this->db->last_query();



		$query .= ($where_data!=array()?" AND ":" WHERE ")."MATCH(";
		$i=1;
		foreach($fields_array as $k=>$v){
			$i++;
			$query .= "`".$v."`".(($i <= count($fields_array))?",":"");
		}
		$query .= ") AGAINST ( '\"$words\"' IN BOOLEAN MODE)";

		$r = $this->db->query($query);
		if($r->num_rows()>0)
			return $r->result_array();
		else
			return false;
	}


	public function get_table(){
		return $this->table;
	}
	public function sum($field,$where_array){
		$this->db->select("SUM(`".$field."`) `sum`");
		$r=$this->db->get_where($this->table,$where_array);
		$rr=$r->result_array();
		if($rr[0]['sum']!=0)
			return $rr[0]['sum'];
		else
			return 0;
	}
	public function total($where_array){
		$this->db->select('*');
		$r=$this->db->get_where($this->table,$where_array);
		if($r->num_rows()>0){
			return $r->num_rows();
		}else
			return 0;
	}
	public function get_id(){
		return $this->id;
	}
	public function set_table($table){
		$this->table=$table;
	}

	public function set_id($id){
		$this->table=$id;
	}
	function get($where_data=array(),$select_data=array(),$number=0,$order_by=''){
		if($order_by!='')
			$this->db->order_by($order_by);
		$select_data!=array()?$this->db->select($select_data):$this->db->select('*');
		$r=$this->db->get_where($this->table,$where_data);
		if($r->num_rows()>0){
			if($number==1){
				$rr=$r->result_array();
				return $rr[0];
			}else{
				return $r->result_array();
			}
		}else
			return false;
	}

    /**
     * Формирование массива из многомерного
     * @author ansotov
     * @param array $where_data
     * @param array $select_data
     * @param int $number
     * @param string $order_by
     * @return bool
     */
	public function getIn($where_data = array(), $select_data = array(), $number = 0, $order_by = '', $column = 'tag_id') {
        $data = $this->get($where_data, $select_data, $number, $order_by);
        $arr = $this->multi_implode($column, $data);

        foreach ($arr as $key => $value)
            $result[$key] = $value;

        return $result;

    }

    /**
     * Работа с многомерным массивом
     * @author ansotov
     * @param $sep
     * @param $array
     * @return array
     */
    public function multi_implode($sep, $array)
    {
        $_array = array();

        foreach($array as $val)
        {
            $_array[] = $val[$sep];
        }

        return $_array;
    }

	function get_for_output($where_data,$type,$is_single,$total=0){

		$select_array= array("*",
				  "DATE_FORMAT(`date`, '%d %M %Y') AS `date_show`",
				  "DATE_FORMAT(`date`, '%H:%i') AS `time_show`",
				  "(SELECT COUNT(views.id) FROM views WHERE views.object_id = ".$this->table.".id AND views.type='".$type."') AS views",
				  "(SELECT SUM(rating.value) FROM rating WHERE rating.object_id = ".$this->table.".id AND rating.type='".$type."') AS likes",
				  "(SELECT SUM(rating.value) FROM rating WHERE rating.object_id = ".$this->table.".id AND rating.type='".$type."' AND rating.value=1) AS likes_plus",
				  "(SELECT ABS(SUM(rating.value)) FROM rating WHERE rating.object_id = ".$this->table.".id AND rating.type='".$type."' AND rating.value=-1) AS likes_minus",
				  "(SELECT COUNT(comments.id) FROM comments WHERE comments.object_id = ".$this->table.".id AND comments.type='".$type."') AS comments");

		if($total==0)
			$data=$this->get($where_data,$select_array, $is_single,"`date` DESC");
		else
			$data=$this-bget($where_data,$select_array,$is_single,"`date` DESC",$total);
		return $data;
	}
	function bget($where_data=array(),$select_data=array(),$number=0,$order_by='',$limit=0,$offset=0){

		if($order_by!='')
			$this->db->order_by($order_by);
		$select_data!=array()?$this->db->select($select_data):$this->db->select('*');
		if($limit!=0){
			$r=$this->db->get_where($this->table,$where_data,$limit,$offset);
			$r2=$this->db->get_where($this->table,$where_data);
			$data['rows']=$r2->num_rows();
		}else{
			$r=$this->db->get_where($this->table,$where_data);
			$data['rows']=$r->num_rows();

		}
		if($r->num_rows()>0){
			if($number==1){
				$rr=$r->result_array();
				$data['result']=$rr[0];

			}else{

				$data['result']=$r->result_array();
			}
			return $data;
		}else
			return false;
	}

	function update($data,$where_data){
		$this->db->where($where_data);
		$this->db->update($this->table,$data);
	}
	function set_default_order($order_name){
		$this->default_order=$order_name;
	}
	function set_orders($array){
		$this->order_array=$array;
	}
	function order(){
		$objects=$this->get($this->order_array,array($this->id,$this->default_order),0,$this->default_order." ASC");
		if(count($objects)>1)foreach($objects as $k=>$v){
			$order_id=$k+1;
			$this->update(array($this->default_order=>$order_id),array($this->id=>$v['id']));
		}
	}
	function push($from_order_id,$to_order_id,$type,$order){


		if($type !='forward' && $type !='backward')
			return false;

		$order_array=$this->order_array;

		//get id from_order_id
		$f_order= $order_array;

		if($type == 'forward'){
			$begin = $from_order_id;
			$end = $to_order_id;
		}else{
			$begin = $to_order_id;
			$end = $from_order_id;
		}

		$f_order[$this->default_order]=$begin;

		////////////////////////////
		$get_id = $this->get($f_order,array($this->id,$this->default_order),1,$this->default_order." ".$order);
		$this->update(array($this->default_order=>0),array($this->id=>$get_id[$this->id]));
		////////////////////////////


		if($order == 'ASC' && $type=='forward'){
			$temp_order_id = $from_order_id;
			$from_order_id = $to_order_id;
			$to_order_id = $temp_order_id;
		}


		if($order=='DESC' && $type == 'forward'){
			$type = 'backward';
			$temp_order_id = $from_order_id;
			$from_order_id = $to_order_id;
			$to_order_id = $temp_order_id;
		}elseif($order=='DESC' && $type == 'backward'){
			$type = 'forward';/*
			$temp_order_id = $to_order_id;
			$to_order_id = $from_order_id;
			$from_order_id = $temp_order_id;*/
		}


		$from_to_order = $order_array;

		if($type == 'forward'){
			$from_to_order[$this->default_order." >"] = $to_order_id;
			$from_to_order[$this->default_order." <="] = $from_order_id;
			$g = $this->get($from_to_order,array($this->id,$this->default_order),0,$this->default_order." ASC");
			foreach($g as $k=>$v){
				$order_id = $v[$this->default_order];
				$order_id--;
				$this->update(array($this->default_order=>$order_id),array($this->id=>$v[$this->id]));
			}

		}else{

			$from_to_order[$this->default_order." >="] = $from_order_id;
			$from_to_order[$this->default_order." <"] = $to_order_id;
			$g = $this->get($from_to_order,array($this->id,$this->default_order),0,$this->default_order." ASC");
			foreach($g as $k=>$v){
				$order_id = $v[$this->default_order];
				$order_id++;
				$this->update(array($this->default_order=>$order_id),array($this->id=>$v[$this->id]));
			}
		}

		//set = to_order_id
		$this->update(array($this->default_order=>$end),array($this->id=>$get_id[$this->id]));

		return true;
	}
	function move($order_id,$dir){
		$order_array=$this->order_array;
		$order_array[$this->default_order]=$order_id;
		$g1=$this->get($order_array,array($this->id),1);
		$up_or=$order_id-1;
		$down_or=$order_id+1;

		$order_array2=$this->order_array;
		$order_array2[$this->default_order]=$up_or;
		$order_array3=$this->order_array;
		$order_array3[$this->default_order]=$down_or;
		if($dir=='up' && $up_or>0){
			$g2=$this->get($order_array2,array($this->id),1);
			$array1[$this->id] = $g1[$this->id];
			$array2[$this->id] = $g2[$this->id];
			$this->update(array($this->default_order=>0),$array1);
			$this->update(array($this->default_order=>$order_id),$array2);
			$this->update(array($this->default_order=>$up_or),$array1);
		}elseif($dir=='down' && $g2=$this->get($order_array3,array($this->id),1)){
			$this->update(array($this->default_order=>0),$array1);
			$this->update(array($this->default_order=>$order_id),$array2);
			$this->update(array($this->default_order=>$down_or),$array1);
		}
	}
	function delete($data){
		$this->db->delete($this->table,$data);
		$objects=$this->get($this->order_array,array($this->id,$this->default_order),0,$this->default_order." ASC");
		if(count($objects)>1)foreach($objects as $k=>$v){
			$order_id=$k+1;
			$array[$this->id] = $v[$this->id];
			$this->update(array($this->default_order=>$order_id),$array);
		}
	}
	function insert($data){
		$this->db->select(array("(MAX(".$this->default_order.")+1) AS `val`"));

		$r=$this->db->get_where($this->table,$this->order_array);
		$rr=$r->result_array();
		if($rr[0]['val']==0)$rr[0]['val']=1;
		$data[$this->default_order]=$rr[0]['val'];

		//atension!!!
		$data=array_merge($data,$this->order_array);
		//

		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}

	/**
	 * Сохраняем данные
	 * @author ansotov
	 * @param $data
	 * @return mixed
	 */
	public function save($data) {
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}

	public function issetItem($name){
		return $this->db->get_where($this->table,$where_array);
	}
}
?>
