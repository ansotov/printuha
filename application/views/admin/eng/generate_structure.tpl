<script src="/js/admin.js"></script>
<script src="/js/admin2.js"></script>
{include file="admin/common_forms2.tpl"}

{if $title_name}<h2>{$title_name}</h2>{/if}


<form action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="post" value="1">
<div class="action_panel">
	{if $links}<a href="/{$dir}/{$links.list.link}/{$end_link}1/">{$links.list.name}</a>{/if}
    {if $id>0}
    {foreach from=$links_after item=v key=k}
    <a href="{$k}{$id}/1">{$v}</a>
    {/foreach}
    {/if}
    <input class="submit_button" type="submit" value="{if $id>0}Save{else}Create{/if}">
</div>

<table width="960" class="structure_panel" cellpadding="0" cellspacing="0">


{foreach from=$structure item=v key=k}
<tr>
	<td width="90" {if $v.visual.fatline==1} class="fatline"{/if}>{$v.name} 
    {if $v.type=='jcrop'}({$v.max_width} X {$v.max_height}){/if}
    {if $v.type=='flv_file' &&  $post[$k]!=''}<a href="/{$v.directory}{$post[$k]}.flv">File</a>{/if}
    {if $v.type=='any_file' &&  $post[$k]!=''}<a href="/{$v.directory}{$post[$k]}">File</a>{/if}
    {if $v.type=='jpg_file' || $v.type == 'jcrop'}{if $post[$k]!=''}{/if}{/if}
    {if $v.type=="map"}
        <div id="coors_{$k}">X: {$post[$v.x_id]} Y: {$post[$v.y_id]}</div>
        <div id="show_hide_map_{$k}">
            <span onclick="show_map('{$k}')" id="show_map_{$k}" class="show_map">Show</span> 
            / 
            <span onclick="hide_map('{$k}')" id="hide_map_{$k}" class="hide_map">Hide</span>
        </div>
    {/if}
    </td>
	<td {if $v.visual.fatline==1} class="fatline"{/if}> {if $v.type=='jpg_file' || $v.type == 'jcrop'}{if $post[$k]!=''}<div class="image_sm"><img src="/{$v.directory}{$post[$k]}{if !$v.max_size_thumb}_small{/if}.jpg"></div>{/if}{/if}{if $v.type=='jpg_file'}<input type="file" name="{$k}">{if $post[$k]!=''}<input onclick="del_something('{$k}')" value="�������" type="button">{/if} {/if}
    
        {if $v.type=='any_file'}<input type="file" name="{$k}">{if $post[$k]!=''}<input onclick="del_something('{$k}')" value="�������" type="button">{/if} {/if}
        {if $v.type=='flv_file'}<input type="file" name="{$k}">{if $post[$k]!=''}<input onclick="del_something('{$k}')" value="�������" type="button">{/if} {/if}
    	{if $v.type=='integer'}<input id="{$k}" class="admin_txt" type="text" value="{$post[$k]}"  name="{$k}">{/if}
        {if $v.type=='string'}<input id="{$k}" class="admin_txt" {if $v.is_password}type="password"{else}type="text"{/if} value="{$post[$k]}"  name="{$k}">{/if}
        {if $v.type=='date'}<input id="{$k}" class="admin_txt" type="text" value="{if !$id && !$post[$k]}{$date_now}{else}{$post[$k]}{/if}"  name="{$k}">{/if}
    	{if $v.type=='text'}<textarea  id="{$k}"  name="{$k}" class="tinymce t" cols="90" rows="10">{$post[$k]}</textarea>{/if}
    	{if $v.type=='list'}<select  id="{$k}" name="{$k}"><option value=""></option>{html_options options=$v.values selected=$post[$k]} </select>{/if}
    	{if $v.type=='model_list'}
        	<select  id="{$k}" name="{$k}"><option value=""></option>{foreach from=$v.values item=v2}<option {if $post[$k]==$v2.id} selected="selected"{/if} value="{$v2.id}">{foreach from=$v2 key=k3 item=v3}{if $k3!='id'} {$v3}{/if}{/foreach}</option>{/foreach} </select>
        {/if}
    	{if $v.type=='ajax_photogallery'}
        	<div id="ajax_photogallery_{$k}" class="swfupload-control">
                <div class="image_wraper"><div id="{$k}_image_holder" class="image_holder"></div></div>
                <div class="space"></div>
                <input type="button" id="gbutton_{$k}" class="gbutton" />
                
                <div id="{$k}_progress_bar" class="progress_bar">
                	<div id="{$k}_progress" class="progress"></div>
                </div>
                
                <div style="float:left; width:100%;">
                {if $v.watermark!=''}<input type="checkbox"  id="watermark_{$k}" name="watermark"   /> Place watermark{/if}
                </div>
            </div>
        {/if}
        
    	{if $v.type=='ajax_photogallery_insert'}
        	<div id="ajax_photogallery_{$k}" class="swfupload-control">
                <div class="image_wraper"><div id="{$k}_image_holder" class="image_holder"></div></div>
                <div class="space"></div>
                <input type="button" id="gbutton_{$k}" class="gbutton" />
                <div id="{$k}_progress_bar" class="progress_bar">
                	<div id="{$k}_progress" class="progress"></div>
                </div>
                <div style="float:left; width:100%;">
                <input type="checkbox"  onclick="select_all_gallery_photos('{$k}',this)" /> Select all
                <input type="button" onclick="add_text('{$k}','{$v.target}')" value="Paste gallery into text" />
                </div>
            </div>
        {/if}
        {if $v.type =='jcrop'}
        	
        	<div id="uploaded_image_{$k}" class="uploaded_image" {if $v.from.directory}style="display:block"{/if}><img id="uploaded_image_img_{$k}" src="{if $v.from.directory}/{$v.from.directory}{$post[$v.from.field_name]}.jpg{/if}" /></div>
            <div id="croped_image_{$k}" class="croped_image" ><img id="croped_image_img_{$k}" src="" /></div>
            <div id="upload_button_{$k}" class="upload_button" {if $v.from.directory}style="display:none"{/if}>Upload</div>
            <div id="crop_button_{$k}" class="crop_button"{if $v.from.directory}style="display:block"{/if}>Cut</div>
            <input type="hidden" name="{$k}_uploaded_cropable" value="" />
            <input type="hidden" id="{$k}_x1" name="{$k}_x1" value="" />
            <input type="hidden" id="{$k}_x2" name="{$k}_x2" value="" />
            <input type="hidden" id="{$k}_y1" name="{$k}_y1" value="" />
            <input type="hidden" id="{$k}_y2" name="{$k}_y2" value="" />
            <input type="hidden" id="{$k}_w" name="{$k}_w" value="" />
            <input type="hidden" id="{$k}_h" name="{$k}_h" value="" />
            <input type="hidden" id="{$k}_random" value="{$rand}" />
            <input type="hidden" id="{$k}_save_croped" name="{$k}_save_croped" value="" />
            {if $post[$k]!=''}<input style="float:left" onclick="del_something('{$k}')" value="Delete uploaded image" type="button" class="submit_button">{/if}
        {/if}
        {if $v.type=='checkbox'}<input  id="{$k}" type="checkbox" name="{$k}" {if $post[$k]==1 || ($id==0 && $v.checked)} checked="checked"{/if} value="1">{/if}
        {if $v.type=='map'}<div id="map_{$k}"><div id="dot_{$k}" class="dot"></div></div></td>{/if}
        
        {if $v.type==ajax_data}<div class="block">{foreach from=$v.get item=v2 key=k2}<div class="ajax_block"><strong>{$v.folders[$v2].name}</strong></div>{/foreach}</div><div class="area" id="area_{$k}"></div>
            {foreach from=$v.folders key=folder_key item=folder}<div class="shortie"><b>{$folder.name}</b> {if $folder.type=='image'}<div class="red">{$folder.max_width} x {$folder.max_height}</div> {/if}</div> 
                {if $folder.type==image}<div class="shortie" id="{$k}_{$folder_key}_button">��������� ����</div> <div id="{$k}_{$folder_key}_frame"></div>{/if}
                {if $folder.type==active_string}<input class="shortie" sso="$('#ajax_{$k}_{$folder_key}_sh').empty()"  onkeyup="auto_complete(this,'{$folder.get_from}','{$folder.get_what}');" type="text" id="ajax_{$k}_{$folder_key}" value="" /><div class="over_all" id="ajax_{$k}_{$folder_key}_sh"></div>{/if}
                {if $folder.type==string}<input  class="shortie" type="text" id="ajax_{$k}_{$folder_key}" value="" />{/if}
                {if $folder.type==list_string}
                    <select id="ajax_{$k}_{$folder_key}">{foreach  from=$folder.values item=fv}<option value="{$fv.id}">{$fv[$folder.get_what]}</option>{/foreach}</select>
                {/if}
                <div class="space"></div>
            {/foreach}
            <input class="add_str_butt" type="button" onclick="ajax_{$k}(1);" value="Add" />
        {/if}
        <div class="error">{$errors[$k]}</div>
    </td>
</tr>
{/foreach}

</table>
</form>