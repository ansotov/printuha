<div class="total">
  <div class="sum">
    Cумма заказа:<strong>{($total_sum+$discount_sum)|number_format:0:".":" "}.-</strong>
    {if $discount_sum > 0}
      Скидка: <strong>-{($discount_sum)|number_format:0:".":" "}.-</strong>
    {/if}
  </div><div class="delivery">
    Доставка:<strong id="delivery_sum">{$delivery}.-</strong>
  </div><div class="plus">
    Итого:<strong id="full_price">{$total_sum+$delivery|number_format:0:".":" "}.-</strong>
  </div>
</div>
<input id="hidden_sum" type="hidden" name="sum" value="{$total_sum}">