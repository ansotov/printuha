<?php

class main_common extends CI_Controller
{

	protected $temp_url = '';
	protected $type = "default";
	protected $type_page = "";
	protected $object_id = 0;
	protected $folders = array();
	protected $manufacturers = array();
	protected $buffer_this_page = 1;
	protected $user_id = 0;
	protected $link_here = "";
	protected $exclude_event = 0;

	protected $show_folder = 0;

	protected $description = '';
	protected $title = '';
	protected $h1 = '';
	protected $keywords = '';
	protected $og_image = '';
	protected $language = 'ru';
	protected $default_languages = array('ru' => 'ru', 'en' => 'en', 'ch' => 'ch');
	protected $index_page = 'index';
	protected $home_dir = 'bloom';
	protected $breadcrumbs = array();
	protected $selected_folders = array();
	protected $selected_folder_id = 0;
	protected $h2 = array();

	private $leaves_array = array();

	protected $user_info = array();
	protected $discount7 = false;

	protected $discount_sum = 0;

	protected $designers = array();
	protected $current_order = array();
	protected $products = array();
	protected $periods = array();
	protected $photos = array();

	protected $folders_indexes = array();
	protected $folders_struc = array();

	protected $threads = array();

	protected $main_page = array();
	protected $settings = array();

	function __construct()
	{
		parent::__construct();

		$errors = array();

		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('cart');
		$this->load->library('common_f');
		$this->load->library('form_validation');
		$this->load->library('sass');
		$this->load->model('mmain_page');
		$this->load->model('mfolders');
		$this->load->model('musers');
		$this->load->model('morders');
		$this->load->model('mcities');
		$this->load->model('mproducts_sizes');
		$this->load->model('mdiscounts');
		if ($this->language == 'ru')
		{
			$this->load->model('mmodel');
			$this->mmodel->set_time_names('ru_RU');
		}

		$this->index_page = "index";
		$this->home_dir   = "new_bloom";


		$this->settings = $this->mmain_page->get(array(), array(), 1);
		/*
		$scss        = new $this->sass();
		$scss->setImportPaths( $this->config->item('core_web') . "application/views/sass/");
		$import_path = $this->config->item('core_web') . "application/views/sass/girlsinbloom.scss";
		$export_path = $this->config->item('core_web') . "css/";
		file_put_contents($export_path . "girlsinbloom_test.css", $scss->compile(file_get_contents($import_path)));
		*/
		$main_page       = $this->mmain_page->get(array(), array(), 1);
		$this->main_page = $main_page;
		$this->mysmarty->assign('main_page', $main_page);

		$admitad_uid = $this->input->get('admitad_uid');
		if ($admitad_uid == '')
		{
			$admitad_uid = $this->input->get('cpamit_uid');
		}
		if ($admitad_uid != '')
		{
			$this->load->model('madmitad');
			$this->madmitad->insert(array('uid' => $admitad_uid, 'url' => $_SERVER['REQUEST_URI']));
			$this->session->set_userdata('admitad_uid', $admitad_uid);
		}


		if ($this->session->userdata('site_user') && $this->session->userdata('user_id'))
		{
			if ($user_info = $this->musers->get(array("id" => $this->session->userdata('user_id')), array('* , IF(created_at < "2014-01-01" && NOW() >= "2014-01-01" ,1,0) discount7 '), 1))
			{
				$this->user_id   = $this->session->userdata('user_id');
				$this->user_info = $user_info;
				if ($this->user_info['first_name'] == '')
				{
					$this->session->set_flashdata('error', '');
				}
				if ($this->user_info['discount7'] == 1)
				{
					$this->discount7 = true;
					$this->mysmarty->assign('discount7', true);
				}

				if ($current_order = $this->morders->get(array('user_id' => $this->user_id, 'status' => 'confirm'), array(), 1, 'date DESC'))
				{
					$this->load->model('morders_products');
					/*if($this->user_id == 1036){
						print_r($current_order);
						$temp = $this->morders_products->get(array('user_order_id'=>$current_order['id']),array(),0);
						print_r($temp);
					}*/
					$query  = '';
					$query2 = '';


					//check sizes in stock and their avaliability
					$temp = $this->morders_products->get(array('user_order_id' => $current_order['id']), array(), 0);
					if ($temp)
					{
						foreach ($temp as $k => $v)
						{
							$query  .= ($query == '') ? "id = " . $v['product_id'] : " OR id = " . $v['product_id'];
							$query2 .= ($query2 == '') ? "(product_id = " . $v['product_id'] . " AND code ='" . $v['size'] . "' )" : " OR ( product_id = " . $v['product_id'] . " AND code = '" . $v['size'] . "' )";
						}
						$this->load->model('mproducts');
						$this->load->model('mproducts_sizes');

						$data = $this->mproducts->get($query, array(), 0);
						foreach ($data as $k => $v)
						{
							$products[$v['id']] = $v;
						}

						$sizes = array();
						$data  = $this->mproducts_sizes->get($query2, array(), 0);
						foreach ($data as $k => $v)
						{
							$sizes[$v['product_id']][$v['code']] = array('on_hand' => $v['on_hand'], 'on_order' => $v['on_order']);
						}

						$fproducts = array();

						$warning = false;
						$sum     = 0;
						foreach ($temp as $k => $v)
						{
							$fproducts[$k]         = $v;
							$fproducts[$k]['info'] = $products[$v['product_id']];

							$qty      = $v['qty'];
							$in_stock = $sizes[$v['product_id']][$v['size']]['on_hand'];
							$on_order = $sizes[$v['product_id']][$v['size']]['on_order'];
							if (($in_stock == 1 && $on_order == 1) || $in_stock == 0)
							{
								$in_stock = 0;
								$warning  = true;
								unset($fproducts[$k]);
							}
							if (($in_stock >= 1 && $on_order == 0) || ($in_stock >= 2))
							{
								if ($qty > $in_stock)
								{
									$warning = true;
									$qty     = $in_stock;
								}
							}
							if (isset($fproducts[$k]))
							{

								$sum += $v['price'] * $qty;
							}
						}
						if ($warning == true)
							$this->session->set_flashdata('error', '');
						$current_order['sum']      = $sum;
						$current_order['products'] = $fproducts;

						$this->current_order = $current_order;
					}
				}
				$this->mysmarty->assign('user_info', $this->user_info);
			}
			else
			{
				$this->session->unset_userdata('site_user');
				$this->session->unset_userdata('user_id');
				$this->session->set_flashdata('error', '');
				redirect('/');
			}
		}

		// show menu @
		$temp = $this->mfolders->get(array('visible' => 1), array('*'), 0, 'order_id ASC');
		if ($temp) foreach ($temp as $k => $v)
		{
			$this->folders_indexes[$v['id']] = $v;
		}
		$this->folders_struc = $this->build_menu();


		/*

		//build days
		$days = array();
		for($i = 1; $i <= 31; $i++){
			$days[str_pad($i, 2,'0', STR_PAD_LEFT)]= str_pad($i, 2,'0', STR_PAD_LEFT);
		}
		$this->mysmarty->assign('days',$days);
		//builds months

		$months = array();
		$months[str_pad(1, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅпїЅпїЅ';
		$months[str_pad(2, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅпїЅпїЅпїЅ';
		$months[str_pad(3, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅ';
		$months[str_pad(4, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅпїЅпїЅ';
		$months[str_pad(5, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅ';
		$months[str_pad(6, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅ';
		$months[str_pad(7, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅ';
		$months[str_pad(8, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅпїЅпїЅ';
		$months[str_pad(9, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ';
		$months[str_pad(10, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅпїЅпїЅпїЅ';
		$months[str_pad(11, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅпїЅпїЅ';
		$months[str_pad(12, 2,'0', STR_PAD_LEFT)]='пїЅпїЅпїЅпїЅпїЅпїЅпїЅ';
		$this->mysmarty->assign('months',$months);
		//build years
		$this_year = intval(date("Y"));
		for($i = $this_year; $i >= 1930; $i--){
			$years[$i] = $i;
		}

		$this->mysmarty->assign('years',$years);
		*/
		/*
		$banners =  $this->mbanners->get(array(),array(),0,"order_id DESC");
		$this->mysmarty->assign('banners',$banners);
		*/
		/*
		$this->mysmarty->assign('baseurl',$this->config->item('base_url'));
		$this->mysmarty->assign('total',$this->cart->total_items());

		*/

		/*

		пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ

		// Loads the class
		require $this->config->item('core_web').'application/libraries/browscap/Browscap.php';

		// The Browscap class is in the phpbrowscap namespace, so import it
		//use phpbrowscap\Browscap;

		// Create a new Browscap object (loads or creates the cache)
		$bc = new Browscap($this->config->item('core_web').'application/libraries/browscap/cache/');

		// Get information about the current browser's user agent
		$current_browser = $bc->getBrowser();


		if($current_browser->Browser=='IE' && intval($current_browser->MajorVer)<=8){
				$this->index_page='old_browser';
				$this->finish('old_browser');
				return;
		}*/
	}

	protected function set_session($user)
	{
		$this->session->set_userdata(array('user_mail' => $user['email'], 'user_id' => $user['id'], 'site_user' => $user['email']));
		$this->user_id   = $user['id'];
		$this->user_info = $this->musers->get(array("id" => $this->user_id), array('*'), 1);
		$this->mysmarty->assign('user_info', $this->user_info);

		return true;
	}


	protected function encodepass($password, $salt)
	{
		$digest = openssl_digest($password . $salt, 'sha512');

		for ($i = 1; $i <= 19; $i++)
		{
			$digest = openssl_digest($digest, 'sha512');
		}

		return $digest;
	}

	protected function paginator($total_products, $per_page_products, $current_page)
	{
		$count       = 0;
		$pages_array = array();
		$page_n      = 1;
		while ($count <= $total_products)
		{
			$pages_array[$page_n] = array("offset"   => $count,
			                              'selected' => ($current_page == $page_n),
			                              'from'     => $count + 1,
			                              'till'     => ($total_products < ($count + $per_page_products)) ? $total_products : ($count + $per_page_products));
			$page_n++;
			$count += $per_page_products;
		}

		return $pages_array;
	}

	protected function build_breadcrumbs($selected_folder_id)
	{
		if (isset($this->folders_indexes[$selected_folder_id]))
			$cur_f = $this->folders_indexes[$selected_folder_id];
		else
			return false;
		$this->breadcrumbs[] = $this->folders_indexes[$selected_folder_id];
		foreach ($this->breadcrumbs as $k => $v)
		{
			$this->breadcrumbs[$k]['url'] = ($v['id'] != $cur_f['id']) ? $cur_f['url'] . '/' . $v['url'] : $v['url'];
		}

		if ($cur_f['parent_id'] != 0)
			$this->build_breadcrumbs($cur_f['parent_id']);
		else
			$this->breadcrumbs = array_reverse($this->breadcrumbs);
		$this->selected_folders[$v['id']] = true;

		return $this->breadcrumbs;
	}

	protected function get_folder_url($folder_id, $url = '')
	{
		if ($this->folders_indexes[$folder_id]['parent_id'] == 0)
		{
			return $this->folders_indexes[$folder_id]['url'] . '/' . $url;
		}
		else
		{
			$parent_id = $this->folders_indexes[$folder_id]['parent_id'];
			$new_url   = $this->folders_indexes[$folder_id]['url'];

			return $this->get_folder_url($parent_id, $new_url . '/' . $url);
		}
	}

	protected function handle_video_link($video, $new_width = 0)
	{
		$video = htmlspecialchars_decode($video);

		//preg_match('/<iframe\ width="([0-9]+)" height="([0-9]+)" src\="">',$video,$matches);
		return $video;
	}

	protected function build_menu($parent_id = 0, $level = 0)
	{
		$temp = array();
		$level++;
		foreach ($this->folders_indexes as $k => $v)
		{
			if ($v['parent_id'] == $parent_id)
			{
				$temp[$v['id']]          = $v;
				$temp[$v['id']]['level'] = $level;

				if ($data = $this->build_menu($v['id'], $level))
				{
					$temp[$v['id']]['first_key']  = key($data);
					$temp[$v['id']]['subfolders'] = $data;
				}
			}
		}
		if (empty($temp))
		{
			return false;
		}
		else
		{
			return $temp;
		}
	}

	//////////////////////////////////////////////////
	//CART OPERATIONS

	protected function build_cart($cart)
	{
		$query     = "";
		$query2    = "";
		$cart_keys = array();
		$total_qty = 0;

		if ($cart)
		{
			foreach ($cart as $k => $v)
			{
				$vals             = explode('_', $v['id']);
				$query            .= ($query == '') ? "id = " . $v['name'] : " OR id = " . $v['name'];
				$query2           .= ($query2 == '') ? "(product_id = " . $v['name'] . " AND code='" . $vals[1] . "')" : " OR (product_id = " . $v['name'] . " AND code='" . $vals[1] . "')";
				$cart[$k]['size'] = $vals[1];
				$cart[$k]['color'] = $v['color'];
				$total_qty        += $v['qty'];
			}
			//echo $query.'<br>';
			//echo $query2;
			$this->load->model('mmaterials');
			$this->load->model('mproducts');
			$data     = $this->mproducts->get($query, array(), 0);
			$products = array();
			foreach ($data as $k => $v)
			{
				$products[$v['id']] = $v;
			}
			$data           = $this->mproducts_sizes->get($query2, array(), 0);
			$products_sizes = array();

			if ($data) foreach ($data as $k => $v)
			{
				$products_sizes[$v['product_id'] . '_' . $v['code']] = $v;
			}
			$materials = array();
			$data      = $this->mmaterials->get(array(), array(), 0);
			if ($data) foreach ($data as $k => $v)
			{
				$materials[$v['id']] = $v;
			}
			$pp_discount_products = array();

			foreach ($cart as $k => $v)
			{
				$vals             = explode('_', $v['id']);
				$cart[$k]['info'] = $products[$v['name']];
				@$cart[$k]['size_info'] = $products_sizes[$v['name'] . '_' . $vals[1]];
				if (isset($vals[2]))
				{
					$cart[$k]['material'] = $materials[$vals[2]];
				}
				$product_info = $products[$v['name']];

				$product_size_info = $products_sizes[$vals[0] . '_' . $vals[1]];
				if ($product_size_info['price'] > 0)
				{
					$product_info['price']     = $product_size_info['price'];
					$v['price']                = $product_size_info['price'];
					$cart[$k]['price']         = $v['price'];
					$cart[$k]['info']['price'] = $v['price'];
				}
				$discounts = $this->mdiscounts->get(array('active' => 1), array(), 0, 'order_id DESC');

				if ($discounts) foreach ($discounts as $k2 => $v2)
				{

					$per_product_discount = false;
					if ($v2['per_product'])
					{
						$this->per_product    = $v2['per_product'];
						$per_product_discount = true;
					}
					$discount_valid = 1;

					if (strtotime($v2['from']) > time() || strtotime($v2['till']) < time())
					{
						$discount_valid = 0;
					}

					if ($v2['skus'] != '' && (!strpos($product_info['sku'], $v2['skus']) !== false && $product_info['sku'] != $v2['skus']) && !(strpos($v2['skus'], ",")))
					{
						$discount_valid = 0;
					}


					if ($v2['skus'] != '' && strpos($v2['skus'], ","))
					{
						$found = 0;
						if (strpos($v2['skus'], ',') == true)
						{
							$skus = explode(',', $v2['skus']);
							foreach ($skus as $sku)
							{
								if (strpos($this->r_ch($product_info['sku']), $this->r_ch($sku)) !== false || $this->r_ch($product_info['sku']) == $this->r_ch($sku))
								{
									$found = 1;
								}
							}
							if ($found == 0)
								$discount_valid = 0;

						}
					}
					if ($v2['min_cart'] > 0 && $v2['min_cart'] > $total_qty)
					{
						$discount_valid = 0;
					}

					if ($v2['exclude_skus'] != '' && (strpos($product_info['sku'], $v2['exclude_skus']) !== false || $product_info['sku'] == $v2['exclude_skus']) && !(strpos($v2['exclude_skus'], ",")))
					{
						$discount_valid = 0;
					}


					if ($v2['exclude_skus'] != '' && strpos($v2['exclude_skus'], ","))
					{
						$found = 0;
						if (strpos($v2['exclude_skus'], ',') == true)
						{
							$exclude_skus = explode(',', $v2['exclude_skus']);
							foreach ($exclude_skus as $sku)
							{
								if (strpos($this->r_ch($product_info['sku']), $this->r_ch($sku)) !== false || $this->r_ch($product_info['sku']) == $this->r_ch($sku))
								{
									$found = 1;
								}
							}
							if ($found == 1)
								$discount_valid = 0;
						}
					}


					if ($v2['type'] != '' && $product_info['type'] != $v2['type'])
					{
						$discount_valid = 0;
					}


					if ($discount_valid == 1)
					{

						if ($v2['per_product'] > 0)
						{
							$pp_discount_products[$k] = $cart[$k];
						}
						// IF CURRENT PRODUCT PRICE IS LESS THAN OLD PRICE WITH DISCOUNT MODIFIER
						if ($product_info['price'] <
							($product_info['old_price'] - ($product_info['old_price'] * ($v2['discount'] / 100))))
						{
							// DO NOTHING
						}
						else
							// IF CURRENT PRODUCT PRICE IS LESS THAN OLD PRICE WITH DISCOUNT MODIFIER AND OLD PRICE IS LARGER THAN 0
							if ($product_info['old_price'] > 0)
							{
								$cart[$k]['price'] = $product_info['old_price'] - ($product_info['old_price'] * ($v2['discount'] / 100));
								// IF OLD PRICE IS EQUAL ZERO
							}
							else
							{
								$product_info['old_price'] = $product_info['price'];
								$cart[$k]['price']         = $product_info['price'] - ($product_info['price'] * ($v2['discount'] / 100));
							}

						if (($cart[$k]['price'] < $cart[$k]['info']['price']))
						{
							$this->discount_sum += ($cart[$k]['info']['price'] - $cart[$k]['price']) * $cart[$k]['qty'];

						}
					}

				}//END FOREACH DISCOUNTS

				$this->mysmarty->assign('discount_sum', $this->discount_sum);
			}

			if ($pp_discount_products != array())
			{
				$pp_total = 0;
				foreach ($pp_discount_products as $k => $v)
				{
					$pp_total += $v['qty'];
				}
				$this->free_products = floor($pp_total / $this->per_product);

				$new_cart    = array();
				$cur_product = array();
				$cur_key     = "";
				$total_cart  = count($cart);
				$cycles      = 0;
				while (count($new_cart) < $total_cart)
				{
					$i = 0;
					$cycles++;

					$lowest_price = 0;
					foreach ($cart as $k => $v)
					{
						$i++;
						if ($lowest_price > 0 && $lowest_price < $v['price'])
						{
							$lowest_price = $v['price'];
							$cur_product  = $v;
							$cur_key      = $k;
						}
						if ($lowest_price == 0)
						{
							$lowest_price = $v['price'];
							$cur_product  = $v;
							$cur_key      = $k;
						}
						if ($i == count($cart))
						{
							$new_cart[$cur_key] = $cur_product;
							unset($cart[$cur_key]);
						}
					}

					if ($cycles > 500)
						exit;
				}
				$cart               = $new_cart;
				$cart               = array_reverse($cart);
				$free_products_left = $this->free_products;

				foreach ($cart as $k => $v)
				{
					if ($free_products_left > 0)
					{
						if ($free_products_left >= $v['qty'])
						{
							$free_products_left   -= $v['qty'];
							$cart[$k]['free_qty'] = $v['qty'];
						}
						else
						{
							$diff                 = abs($free_products_left - $v['qty']);
							$cart[$k]['free_qty'] = $v['qty'] - $diff;
							$free_products_left   = 0;
						}
					}
				}
				$cart = array_reverse($cart);
			}
			else
			{
				$cart = array_reverse($cart);
				$cart = $this->getStock($cart);

			}

			foreach ($cart as $k => $v)
			{
				$result[$k]                 = $v;
				$result[$k]['info']['name'] = $this->getLang($v['info'], 'name');

				if ($v['material'])
					$result[$k]['material']['name'] = $this->getLang($v['material'], 'name');
			}

			return $result;
		}
	}

	/**
	 * Акции
	 * @author ansotov
	 *
	 * @param $cart
	 *
	 * @return array
	 */
	protected function getStock($cart)
	{

		$types = array();
		if (!$cart)
			return $cart;


		foreach ($cart as $k => $v)
		{
			$types[$k]['size_id'] = $v['size_info']['id'];
			$types[$k]['type']    = $v['info']['type'];
			$types[$k]['qty']     = $v['qty'];
			$types[$k]['price']   = $v['price'];
		}

		//ansotov обсчёт данных для формирования акции
		$sort = array();

		foreach ($types as $k => $v)
			$sort[] = $v['type'];

		array_multisort($sort, SORT_DESC, $types);

		$result = array();

		foreach ($types as $k => $v)
		{
			$result[$v['type']][$k]['size_id'] = $v['size_id'];
			$result[$v['type']][$k]['qty']     = $v['qty'];
			$result[$v['type']][$k]['price']   = $v['price'];
		}

		$res = array();
		foreach ($result as $key => $value)
		{

			foreach ($value as $k => $v)
			{
				$res[$key]['qty']         += $v['qty'];
				$res[$key]['min_price'][] = $v;
			}
		}

		$data = array();

		foreach ($res as $k => $v)
		{
			$t = array();

			foreach ($v['min_price'] as $k2 => $v2)
			{
				$t[$v2['size_id']] = $v2['price'];
			}
			array_multisort($t, SORT_ASC, $v['min_price']);
			$data[$k]['qty']       = $v['qty'];
			$data[$k]['min_price'] = $v['min_price'];
		}

		$this->load->model('mstock');

		$arr = array();

		$inStock = 0;

		foreach ($data as $k => $v)
		{

			//ansotov акция на определённый тип товаров
			$stock = $this->mstock->get('`type` = "' . $k . '" AND `active` = 1 AND NOW() BETWEEN `start_at` AND (`end_at` + INTERVAL 1 DAY)', array(), 1);

			if ($stock)
				$inStock++;

			$iterations = floor($v['qty'] / $stock['products_count']);

			if ($stock && $v['qty'] >= $stock['products_count'])
			{
				//ansotov хранилище количества товаров
				$qty = 0;

				for ($i = 0; $i < $iterations; $i++)
				{
					if ($qty >= $iterations)
						continue;

					$iter_qty = $iterations - $qty;

					$arr[$k][$i]['id']    = $v['min_price'][$i]['size_id'];
					$arr[$k][$i]['qty']   = ($v['min_price'][$i]['qty'] > $iter_qty) ? $iter_qty : $v['min_price'][$i]['qty'];
					$arr[$k][$i]['price'] = $v['min_price'][$i]['price'];
					$arr[$k][$i]['in_stock_discount'] = $stock['discount'];

					$qty += $arr[$k][$i]['qty'];
				}
			}
		}

		//ansotov акция на все товары
		$stockAll = ($inStock) ? false : $this->mstock->get('`type` = "all" AND `active` = 1 AND NOW() BETWEEN `start_at` AND (`end_at` + INTERVAL 1 DAY)', array(), 1);

		//ansotov если нет акций по категориям и есть на все товары
		if ($stockAll)
		{

			//ansotov формирование массива для
			$cardItems = array();

			foreach ($data as $k => $v)
			{
				$cardItems = array_merge($cardItems, $v['min_price']);
			}

			usort($cardItems, function ($a, $b) {
				if ($a['price'] === $b['price'])
					return 0;

				return $a['price'] > $b['price'] ? 1 : -1;
			});

			$countGoods = 0;

			foreach ($cardItems as $k => $v)
			{
				$countGoods += $v['qty'];
			}

			//ansotov кличество товаров в подарок
			$qtyItems = floor($countGoods / $stockAll['products_count']);

			foreach ($cardItems as $k => $v)
			{

				if ($qtyItems > 0)
				{
					$qty              = ($v['qty'] > $qtyItems) ? $qtyItems : $v['qty'];
					$arr[$k]['id']    = $v['size_id'];
					$arr[$k]['qty']   = $qty;
					$arr[$k]['price'] = $v['price'];
                    $arr[$k]['in_stock_discount'] = $stockAll['discount'];

					$qtyItems -= $qty;
				}
			}

			$arr = array($arr);
		}

		$res_arr = array();

		foreach ($cart as $k => $v)
		{

			foreach ($arr as $key => $value)
			{
				foreach ($value as $k2 => $v2)
				{
					if ($v['size_info']['id'] == $v2['id'])
					{
						$v['in_stock']     = 1;
						$v['in_stock_discount']     = $v2['in_stock_discount'];
						$v['in_stock_qty'] = $v2['qty'];
					}
				}
			}

			$res_arr[$k] = $v;
		}

		return $res_arr;
	}

	protected function show_cart()
	{
		$show_cart = intval($this->input->post('show_cart'));
		if ($show_cart == 1)
		{
			$this->ajax_cart_render();
		}
	}

	protected function r_ch($string)
	{
		return str_replace(' ', '', $string);
	}

	protected function ajax_cart_render($delivery_type = false, $foreign_lands = false)
	{
		$cart         = $this->build_cart($this->cart->contents());
		$total_sum    = 0;
		$total_number = 0;
		if ($cart)
		{
			foreach ($cart as $k => $v)
			{
				$cart[$k]['price_sum'] = $v['price'] * $v['qty'];

				//ansotov бесплатный экземпляр по акции
				if ($v['in_stock']) {
                    			$cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
				}
				
				$total_sum    += $cart[$k]['price_sum'];
				$total_number = $total_number + $v['qty'];
			}
		}

		//ansotov костыль обязательно переписать

		$delivery = array();

		if ($foreign_lands == 1)
		{
			$delivery['price'] = 500;
			$delivery['days']  = 10;
		}

		$amount = main::get_instance()->getDiscountInfo($total_sum);

		$total_amount = round(($total_sum - ($total_sum * ($amount->current_discount / 100))));

		$this->mysmarty->assign("total_sum", $total_amount);
		$this->mysmarty->assign("amount", $amount);

		$this->mysmarty->assign('total_number', $total_number);
		$this->mysmarty->assign("delivery", $delivery);
		$this->mysmarty->assign("delivery_type", $delivery_type);
		$this->mysmarty->assign("foreign_lands", $foreign_lands);
		$this->mysmarty->assign('cart', $cart);
		$promocode = $this->getPromocode($total_amount);

		$this->mysmarty->assign('promocode', $promocode);

		$template_cart_informer = $this->mysmarty->fetch('new_bloom/ru/ajax/cart-informer.tpl');
		$template_cart_in_total = $this->mysmarty->fetch('new_bloom/ru/ajax/cart-in-total.tpl');
		$template_promocode     = $this->mysmarty->fetch('new_bloom/ru/ajax/promocode.tpl');
		$template               = $this->mysmarty->fetch('new_bloom/ru/_cart.tpl');
		echo json_encode(array('template' => $template, 'template_cart_informer' => $template_cart_informer, 'template_cart_in_total' => $template_cart_in_total, 'template_promocode' => $template_promocode));
		exit;
	}

	/**
	 * Генератор паролей
	 * @author ansotov
	 *
	 * @param $length
	 *
	 * @return string
	 */
	protected function passwordGenerator($length = 10)
	{
		$password = "";
		/* Массив со всеми возможными символами в пароле */
		$arr = array(
			'a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r',
			's', 't', 'u', 'v', 'w', 'x',
			'y', 'z', 'A', 'B', 'C', 'D',
			'E', 'F', 'G', 'H', 'I', 'J',
			'K', 'L', 'M', 'N', 'O', 'P',
			'Q', 'R', 'S', 'T', 'U', 'V',
			'W', 'X', 'Y', 'Z', '1', '2',
			'3', '4', '5', '6', '7', '8',
			'9', '0', '#', '!', "?", "&"
		);
		for ($i = 0; $i < $length; $i++)
			$password .= $arr[mt_rand(0, count($arr) - 1)]; // Берём случайный элемент из массива

		return $password;
	}

	protected function remove_p()
	{
		$rowid    = $this->input->post('rowid');
		$delivery = $this->input->post('delivery');
		//remove product
		$remove_p = intval($this->input->post('remove_p'));

		if ($remove_p == 1 && $rowid != '')
		{

			//ansotov если есть пользователь то удаляем данные о товаре в напоминаниях о не купленных товарах
			if ($this->user_info)
			{
				$product = $this->getProduct($rowid);

				$this->load->model('mremind_products');
				$this->mremind_products->delete(array('user_id' => $this->user_info['id'], 'product_id' => $product['info']['id']));
			}

			//ansotov удаление промокода
			$this->session->unset_userdata('promocode_discount');

			$this->cart->update(array('rowid' => $rowid, 'qty' => 0));
			$this->ajax_cart_render($this->input->post('delivery_type'));
		}
	}

	/**
	 * Продукт из корзины
	 * @author ansotov
	 *
	 * @param $rowid
	 *
	 * @return mixed
	 */
	private function getProduct($rowid)
	{
		$cart = $this->build_cart($this->cart->contents());

		return $cart[$rowid];
	}

	protected function decrease_p()
	{
		$rowid      = $this->input->post('rowid');
		$decrease_p = intval($this->input->post('decrease_p'));
		if ($decrease_p == 1 && $rowid != '')
		{

			$cart = $this->cart->contents();
			foreach ($cart as $k => $v)
			{
				if ($k == $rowid)
				{
					$qty = $v['qty'];
				}
			}

			//ansotov удаление промокода
			$this->session->unset_userdata('promocode_discount');

			$this->cart->update(array('rowid' => $rowid, 'qty' => ($qty - 1)));
			$this->ajax_cart_render($this->input->post('delivery_type'), $this->input->post('foreign_lands'));
		}

	}

	protected function increase_p()
	{
		$rowid      = $this->input->post('rowid');
		$increase_p = intval($this->input->post('increase_p'));

		if ($increase_p == 1 && $rowid != '')
		{

			$cart    = $this->cart->contents();
			$can_add = true;
			foreach ($cart as $k => $v)
			{
				if ($k == $rowid)
				{
					$qty = $v['qty'] + 1;
					$var = explode('_', $v['id']);

					$data = $this->mproducts_sizes->get(array('product_id' => $var[0], 'code' => $var[1]), array(), 1);

					$in_stock = $data['on_hand'];
					$on_order = $data['on_order'];

					if ($qty > $in_stock)
					{
						$can_add = false;
					}
				}
			}
			if ($can_add == true)
				$this->cart->update(array('rowid' => $rowid, 'qty' => $qty));
			else
				$this->mysmarty->assign('cart_notice', '');
			$this->ajax_cart_render($this->input->post('delivery_type'), $this->input->post('foreign_lands'));
		}
	}

	/**
	 * Добавление в корзину
	 * @author ansotov
	 *
	 * @param      $data
	 * @param bool $button_add
	 */
	protected function add_to_cart($data, $button_add = false)
	{

		$update_rowid = false;
		$qty_now      = $data['qty'];
		$cart_conts   = $this->cart->contents();
		foreach ($cart_conts as $k => $v)
		{
			if ($data['id'] . '_' . $data['size'] == $v['id'])
			{
				$update_rowid = $v['rowid'];
				if ($button_add == true)
					$qty_now = $v['qty'] + $data['qty'];
				else
					$qty_now = $data['qty'];
			}
		}

		if ($update_rowid != false)
		{
			$this->cart->update(array("qty" => $qty_now, "rowid" => $update_rowid));
		}
		else
		{
			if (isset($data['material']))
			{
				$insert_data = array("id" => $data['id'] . '_' . $data['size'] . "_" . $data['material'], "qty" => $qty_now, "price" => $data['price'], "name" => $data['id']);
			} elseif (isset($data['color']))
			{
				$insert_data = array("id" => $data['id'] . '_' . $data['size'], "color" => $data['color'], "qty" => $qty_now, "price" => $data['price'], "name" => $data['id']);
			}
			else
			{
				$insert_data = array("id" => $data['id'] . '_' . $data['size'], "qty" => $qty_now, "price" => $data['price'], "name" => $data['id']);
			}
			$this->cart->insert($insert_data);

			//ansotov если есть пользователь записываем напоминанием ему о не купленных товарах
			if ($this->user_info)
			{
				$this->load->model('mremind_products');
				$this->mremind_products->insert(
					array(
						'product_id' => $data['id'],
						'user_id'    => $this->user_info['id'],
						'qty'        => $qty_now,
						'size'       => $data['size'],
						'price'      => $data['price']
					)
				);
			}
		}


	}

	protected function catch_add_to_cart()
	{
		//all cart operations
		$this->show_cart();
		$this->increase_p();
		$this->decrease_p();
		$this->remove_p();

		$add_to_cart = $this->input->post('add_to_cart_m');
		$key         = $this->input->post('key');
		$ajax        = $this->input->post('ajax');
		$size        = $this->input->post('size');
		$material    = $this->input->post('material');
		$color    = $this->input->post('color');
		$qty         = $this->input->post('qty');

		$can_add = 'yes';


		if ($add_to_cart == 1 && isset($this->products[intval($key)]) && $size != '')
		{

			// detect if available

			$product_id = $this->products[$key]['id'];

			$data = $this->mproducts_sizes->get(array('product_id' => $this->products[$key]['id'], 'code' => $size), array('on_hand', 'on_order'), 1);

			$cart = $this->cart->contents();

			//ansotov если есть материал то добавляем данные для сохранения
			if ($material)
			{
				foreach ($cart as $k => $v)
				{
					$var = explode('_', $v['id']);
					if ($product_id == $v['name'] && $var[1] == $size && $material == $var[2])
					{
						$qty += $v['qty'];
					}
				}
			}

			$in_stock = $data['on_hand'];
			$on_order = $data['on_order'];
			//rainmaker fix
			$in_stock = 5;
			$on_order = 0;
			if (($in_stock == 1 && $on_order == 1) || $in_stock == 0)
			{
				$can_add = 'no';
			}

			if (($in_stock >= 1 && $on_order == 0) || ($in_stock >= 2))
			{

				if ($qty > $in_stock)
				{
					$can_add = 'no';
				}
			}

			if ($can_add == 'yes')
			{
				if ($material > 0)
				{
					$add_array = array("id" => $this->products[$key]['id'], 'size' => $size, "material" => $material, "price" => $this->products[$key]['price'], "qty" => $qty);
				}
				elseif ($color)
				{
					$add_array = array("id" => $this->products[$key]['id'], 'size' => $size, "color" => $color, "price" => $this->products[$key]['price'], "qty" => $qty);
				}
				else
				{
					$add_array = array("id" => $this->products[$key]['id'], 'size' => $size, "price" => $this->products[$key]['price'], "qty" => $qty);
				}

				$this->add_to_cart($add_array, true);
				$cart         = $this->build_cart($this->cart->contents());
				$total_sum    = 0;
				$total_number = 0;
				if ($cart)
				{
					foreach ($cart as $k => $v)
					{
						$cart[$k]['price_sum'] = $v['price'] * $v['qty'];

						//ansotov бесплатный экземпляр по акции
						if ($v['in_stock']) {
                            				$cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
						}
						
						$total_sum    = $total_sum + $cart[$k]['price_sum'];
						$total_number = $total_number + $v['qty'];
						if ($product_id == $v['info']['id'])
						{
							$this->mysmarty->assign("added_product", $v);
						}
					}
				}


				$amount = main::get_instance()->getDiscountInfo($total_sum);
				$this->mysmarty->assign("amount", $amount);


				$this->mysmarty->assign('cart', $cart);
				$this->mysmarty->assign("total_sum", $total_sum);
				$template      = $this->mysmarty->fetch('new_bloom/ru/_new_product.tpl');
				$cart_template = $this->mysmarty->fetch('new_bloom/ru/window-cart.tpl');

				$this->load->model('mproducts');

				$packages = $this->mproducts->get('type = "packages"');
				$this->mysmarty->assign('packages', $packages);

				$template_cart_informer = $this->mysmarty->fetch('new_bloom/ru/ajax/cart-informer.tpl');
				$template_cart_in_total = $this->mysmarty->fetch('new_bloom/ru/ajax/cart-in-total.tpl');
				$template_promocode     = $this->mysmarty->fetch('new_bloom/ru/ajax/promocode.tpl');
				$c_template             = $this->mysmarty->fetch('new_bloom/ru/_cart.tpl');

				echo json_encode(array(
						'total_sum'              => $total_sum,
						'total_number'           => $total_number,
						'can_add'                => $can_add,
						"template"               => $template,
						"cart_template"          => $cart_template,
						'template_cart_informer' => $template_cart_informer,
						'template_cart_in_total' => $template_cart_in_total,
						'template_promocode'     => $template_promocode,
						'c_template'             => $c_template
					)
				);
				exit;
			}
			else
			{
				echo json_encode(array('can_add' => 'no'));
				exit;
			}
			//redirect($this->config->item('base_url').'cart/');
		}
	}
	// CART OPERATIONS
	///////////////////////////////////////////


	protected function show_404()
	{
		$this->output->set_status_header('404');
		$this->title = "Страница не найдена";
		$this->h1    = "404";
		$this->finish("404");
	}


	protected function revertCur($val)
	{
		$mstring = sprintf("%.2f", $val);
		$dArray  = explode('.', $mstring);
		$rev     = strrev($dArray[0]);
		$i       = strlen($rev);
		$s       = '';
		$c       = 1;
		for ($x = 0; $x < $i; $x++)
		{
			$substr = substr($rev, $x, 1);
			$s      .= $substr;
			if ($c < 3)
				$c++;
			else if ($x + 1 != $i)
			{
				$s .= ' ';
				$c = 1;
			}
		}
		$s = strrev($s);
		//if(isset($dArray[1]))
		//	$s = $s . ',' . $dArray[1];
		return $s;
	}


	protected function discount_calc($v, $test = 0)
	{
		$discounts = $this->mdiscounts->get(array('active' => 1), array(), 0, 'order_id DESC');
		if ($discounts) foreach ($discounts as $k2 => $v2)
		{

			$discount_valid = 1;

			if (strtotime($v2['from']) > time() || strtotime($v2['till']) < time())
			{
				$discount_valid = 0;
			}

			if ($v2['skus'] != '' && (!strpos($v['sku'], $v2['skus']) !== false && $v['sku'] != $v2['skus']) && !(strpos($v2['skus'], ",")))
			{
				$discount_valid = 0;
			}

			if ($v2['skus'] != '' && strpos($v2['skus'], ","))
			{

				$found = 0;
				if (strpos($v2['skus'], ',') == true)
				{
					$skus = explode(',', $v2['skus']);
					foreach ($skus as $sku)
					{
						if (strpos($this->r_ch($v['sku']), $this->r_ch($sku)) !== false || $this->r_ch($v['sku']) == $this->r_ch($sku))
						{
							$found = 1;
						}
					}
					if ($found == 0)
						$discount_valid = 0;

				}
			}


			if ($v2['exclude_skus'] != '' && (strpos($v['sku'], $v2['exclude_skus']) !== false || $v['sku'] == $v2['exclude_skus']) && !(strpos($v2['exclude_skus'], ",")))
			{
				$discount_valid = 0;

			}
			if ($v2['exclude_skus'] != '' && strpos($v2['exclude_skus'], ","))
			{
				$found = 0;
				if (strpos($v2['exclude_skus'], ',') == true)
				{
					$exclude_skus = explode(',', $v2['exclude_skus']);
					foreach ($exclude_skus as $sku)
					{
						if (strpos($this->r_ch($v['sku']), $this->r_ch($sku)) !== false || $this->r_ch($v['sku']) == $this->r_ch($sku))
						{
							$found = 1;
						}
					}
					if ($found == 1)
						$discount_valid = 0;

				}
			}

			if ($v2['type'] != '' && $v['type'] != $v2['type'])
			{
				$discount_valid = 0;
			}
			if ($v2['min_cart'] > 0)
			{
				$discount_valid = 0;
			}

			if ($v2['show_info'] == 1 && $discount_valid == 1)
			{
				$v['discount_info'] = $v2;
			}

			if ($discount_valid == 1)
			{
				if ($v2['display'] == 1)
				{
					$v['display_discount'] = 1;
				}
				// if current price cheaper than original price with discount
				if ($v['price'] < ($v['old_price'] - ($v['old_price'] * ($v2['discount'] / 100))))
				{

				}
				else
					// if there is discount already
					if ($v['old_price'] > 0)
						$v['price'] = $v['old_price'] - ($v['old_price'] * ($v2['discount'] / 100));
					else
					{
						$v['old_price'] = $v['price'];
						$v['price']     = $v['price'] - ($v['price'] * ($v2['discount'] / 100));
					}
			}

		}


		return $v;
	}

	protected function page_type_info($data)
	{
		switch ($data['type'])
		{


		}

		if ($data['type'] != '')
			return $data['type'];
		else return 'info';
	}

	protected function fetch_template($name)
	{
		return $this->mysmarty->fetch($this->home_dir . '/' . ($this->language != '' ? $this->language . '/' : '') . $name . '.tpl');
	}

	/**
	 * Перевод констант
	 * @author ansotov
	 * @return array
	 */
	public function getLangConst($const)
	{

		//ansotov получаем текущий язык
		$lang = $_COOKIE['language'];

		//ansotov проверяем какой файл подгружать
		$lang = ($lang) ? $lang : 'ru';

		$arr = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . DS . 'langs' . DS . "lang_{$lang}.ini", true, INI_SCANNER_RAW);

		if (!$arr[$const])
			$arr = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . DS . 'langs' . DS . "lang_ru.ini", true, INI_SCANNER_RAW);

		return $arr[$const];
	}

	/**
	 * Перевод констант
	 * @author ansotov
	 * @return array
	 */
	private function getLangData($const)
	{

		//ansotov получаем текущий язык
		$lang = $_COOKIE['language'];

		//ansotov проверяем какой файл подгружать
		$lang = ($lang == 'ru') ? false : 'en_';

		return $lang . $const;
	}

	/**
	 * Перевод из базы, через подстановку нужного поля
	 * @author ansotov
	 *
	 * @param $data
	 * @param $const
	 *
	 * @return mixed
	 */
	protected function getLang($data, $const)
	{
		return ($data['en_' . $const] && $_COOKIE['language']) ? $data[$this->getLangData($const)] : $data[$const];
	}

	protected function finish($include_file = '', $show_events = true)
	{
		if ($this->title != '')
			$this->mysmarty->assign('title', $this->title);
		if ($this->h1 != '')
			$this->mysmarty->assign('h1', $this->h1);
		if ($this->description != '')
			$this->mysmarty->assign('description', $this->description);
		if ($this->keywords != '	')
			$this->mysmarty->assign('keywords', $this->keywords);
		if (count($this->breadcrumbs) > 0)
		{
			$this->mysmarty->assign('breadcrumbs', $this->breadcrumbs);
		}
		// hot fix login email
		if ($var = $this->session->flashdata('email'))
		{
			$this->mysmarty->assign('email', $var);
		}


		if ($var = $this->session->flashdata('error'))
		{
			$this->mysmarty->assign('flash_error', $var);
		}
		if ($var = $this->session->flashdata('notice'))
		{
			$this->mysmarty->assign('flash_notice', $var);
		}
		if ($this->session->userdata('admitad_uid'))
		{
			$this->mysmarty->assign('admitad_uid', $this->session->userdata('admitad_uid'));
		}

		if ($this->og_image != '')
			$this->mysmarty->assign('og_image', $this->config->item('base_url') . $this->og_image);
		if ($this->language != '')
			$this->mysmarty->assign('language', $this->language);
		$true_path = '';


		/*if(isset($_SERVER['PATH_INFO']) && (substr($_SERVER['REQUEST_URI'],0,10)=='/index.php' || substr($_SERVER['PATH_INFO'],-1) != '/'))
			redirect($this->config->item('base_url').$true_path, 'location', 301);
      */
		//
		/////////////////////////////////////////
		if ($this->session->userdata('auth_user'))
			$this->mysmarty->assign('auth_user');


		$this->catch_add_to_cart();
		////////////////////////////////////
		$cart         = $this->build_cart($this->cart->contents());
		$total_sum    = 0;
		$total_number = 0;
		if ($cart)
		{
			foreach ($cart as $k => $v)
			{
				$cart[$k]['price_sum'] = $v['price'] * $v['qty'];

				//ansotov бесплатный экземпляр по акции
				if ($v['in_stock']) {
                    			$cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
				}
				
				$total_sum    = $total_sum + $cart[$k]['price_sum'];
				$total_number = $total_number + $v['qty'];
			}

			$amount = main::get_instance()->getDiscountInfo($total_sum);

			$total_amount = round(($total_sum - ($total_sum * ($amount->current_discount / 100))));

			if ($this->session->userdata('promocode_discount') > 0)
			{
				if ($this->session->userdata('promocode_type') == 'percent')
					$total_amount = round(($total_amount - ($total_amount * ($this->session->userdata('promocode_discount') / 100))));
				else
					$total_amount = round(($total_amount - $this->session->userdata('promocode_discount')));
			}

			$this->mysmarty->assign('total_number', $total_number);
			$this->mysmarty->assign("total_sum", $total_amount);
			$this->mysmarty->assign("amount", $amount);
			$this->mysmarty->assign("cart", $cart);
		}
		/////////////////////////////////////


		$this->load->model('mcollections');
		$collections = $this->mcollections->get(array(), array(), 0, 'order_id ASC');
		$this->mysmarty->assign('collections', $collections);

		$this->load->model('mmenu');

		$top = $this->mmenu->get(array('type' => 'top', 'active' => 1), array(), 0, 'order_id ASC');
		if ($top) foreach ($top as $k => $v)
		{
			$data            = $this->folders_indexes[$v['folder_id']];
			$top[$k]['name'] = $this->getLang($data, 'name');
			$top[$k]['type'] = $data['type'];
			if ($data['type'] == 'link')
				$top[$k]['url'] = $data['url'];
			else
				$top[$k]['url'] = "/" . $this->get_folder_url($v['folder_id']);
			if (isset($this->folders_struc[$v['folder_id']]['subfolders']))
			{
				$top[$k]['subfolders'] = $this->folders_struc[$v['folder_id']]['subfolders'];
			}
		}

		$bottom = $this->mmenu->get(array('type' => 'bottom'), array(), 0, 'order_id ASC');
		if ($bottom) foreach ($bottom as $k => $v)
		{
			if (isset($this->folders_indexes[$v['folder_id']]))
			{
				$data               = $this->folders_indexes[$v['folder_id']];
				$bottom[$k]['name'] = $this->getLang($data, 'name');
				$bottom[$k]['url']  = "/" . trim($this->get_folder_url($v['folder_id']), '/');
				$bottom[$k]['type'] = $data['type'];
			}
		}

		$bottom2 = $this->mmenu->get(array('type' => 'bottom2'), array(), 0, 'order_id ASC');
		if ($bottom2) foreach ($bottom2 as $k => $v)
		{
			if (isset($this->folders_indexes[$v['folder_id']]))
			{
				$data                = $this->folders_indexes[$v['folder_id']];
				$bottom2[$k]['name'] = $this->getLang($data, 'name');
				$bottom2[$k]['url']  = "/" . trim($this->get_folder_url($v['folder_id']), '/');
				$bottom2[$k]['type'] = $data['type'];
			}
		}

		$this->mysmarty->assign('top', $top);
		$this->mysmarty->assign('bottom', $bottom);
		$this->mysmarty->assign('bottom2', $bottom2);
		$this->mysmarty->assign('menu', $this->folders_struc);
		$this->mysmarty->assign('page', $this->home_dir . '/' . ($this->language != '' ? $this->language . '/' : '') . $include_file . '.tpl');
		$this->mysmarty->assign('page_type', $include_file);
		$this->output->set_output($this->mysmarty->fetch($this->home_dir . '/' . ($this->language != '' ? $this->language . '/' : '') . $this->index_page . '.tpl'));
	}
}

?>
