<style>
    #catalog ul {
        display: none;
    }

    #catalog .selected {
        display: block;
    }

    #light {
        color: #09F;
    }

    .sp_space {
        display: inline-block;
        font-weight: bold;
        width: 8px;
    }

    .toggle {
        cursor: pointer;
    }

    a.selected {
        color: #00CCFF;
    }
</style>
<script>
    var $catalog = {};
    $().ready(function () {
        $(".collection").hide();
        $(".switch").click(function () {
            if ($(this).html() == '+') {
                $(this).parent().find('ul:first').addClass('selected');
                $(this).html('-');
            } else if ($(this).html() == '-') {
                $(this).parent().find('ul:first').removeClass('selected');
                $(this).html('+');
            }
        });
        $(".toggle").click(function () {
            var $val = $(this).html();
            var $v = $(this).attr('id').replace('toggle', '')

            if ($val == '-') {
                $("#folder" + $v).hide();
                $(this).html('+');
            } else {
                $("#folder" + $v).show();
                $(this).html('-');
            }

        });
        $(".toggle").click();
        {if $selected_folder>0}highlight({$selected_folder}){/if}
    });

    function highlight(id) {
        id_selector = "#toggle" + id;
        if ($(id_selector).length > 0) {
            //alert('closed '+id)
            $(id_selector).click()
            //alert('open'+id)
            if ($(id_selector).parent().parent().length > 0) {
                parent_id = $(id_selector).parent().parent().attr('id').replace('folder', '');
                highlight(parent_id)
            }


        }
    }
</script>
<!--<a style="width: 100%; display:block;" href="/{$dir}/folders/0/"><strong>Разделы</strong></a>-->
{* define the function *}
{function menu level=1}          {* short-hand *}
	<ul id="folder{$v.id}" class="level{$level}">
        {foreach $data as $k=>$v}
			<li>{if $v.subfolders}
					<span class="toggle sp_space" id="toggle{$v.id}">-</span>{else}
					<span class="sp_space"></span>
                {/if}
				<a
                        {if $v.id == $selected_subfolder}class="selected"{/if}
						href="/{$dir}/open_folders/{$v.parent_id}/{$v.id}/">{$v.name}</a>
			</li>
            {if $v.subfolders}
                {menu data=$v.subfolders level=$level+1}
            {/if}
        {/foreach}
	</ul>
{/function}

<!--{menu data=$menu}-->

<div class="menu">
	<a{if $smarty.get.main == 'about'} class="menu-active"{/if} href="/{$dir}/open_folders/46/0/?main=about&page=about">
		<strong>О
			компании
		</strong>
	</a>
	<ul class="submenu{if $smarty.get.main == 'about'} active-menu{/if}">
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/46/0/?main=about&page=about"{if $smarty.get.main == 'about' and $smarty.get.page == 'about'} class="menu-active"{/if}>
				О
				компании
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/94/0/?main=about&page=partners"{if $smarty.get.main == 'about' and $smarty.get.page == 'partners'} class="menu-active"{/if}>
				Партнёры
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/88/0/?main=about&page=requisites"{if $smarty.get.main == 'about' and $smarty.get.page == 'requisites'} class="menu-active"{/if}>
				Реквизиты
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/48/0/?main=about&page=portfolio"{if $smarty.get.main == 'about' and $smarty.get.page == 'portfolio'} class="menu-active"{/if}>
				Портфолио
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/50/0/?main=about&page=contacts"{if $smarty.get.main == 'about' and $smarty.get.page == 'contacts'} class="menu-active"{/if}>
				Контакты
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/62/0/?main=about&page=blog"{if $smarty.get.main == 'about' and $smarty.get.page == 'blog'} class="menu-active"{/if}>
				Blog
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/87/0/?main=about&page=work"{if $smarty.get.main == 'about' and $smarty.get.page == 'work'} class="menu-active"{/if}>
				Работа
				в Girls in Bloom
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/72/0/?main=about&page=gift-sertificate"{if $smarty.get.main == 'about' and $smarty.get.page == 'gift-sertificate'} class="menu-active"{/if}>
				Подарочные
				сертификаты
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/73/0/?main=about&page=corp-clients"{if $smarty.get.main == 'about' and $smarty.get.page == 'corp-clients'} class="menu-active"{/if}>
				Корпоративным
				клиентам
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/93/0/?main=about&page=gifts"{if $smarty.get.main == 'about' and $smarty.get.page == 'gifts'} class="menu-active"{/if}>
				Наклейки
				в подарок
			</a>
		</li>
	</ul>

	<a{if $smarty.get.main == 'girlsinbloom'} class="menu-active"{/if}
			href="/{$dir}/girlsinbloom/portfolio/?main=girlsinbloom&page=portfolio">
		<strong>Girls in bloom</strong>
	</a>
	<ul class="submenu{if $smarty.get.main == 'girlsinbloom'} active-menu{/if}">
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/girlsinbloom/portfolio/?main=girlsinbloom&page=portfolio"{if $smarty.get.main == 'girlsinbloom' and $smarty.get.page == 'portfolio'} class="menu-active"{/if}>
				Портфолио
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/girlsinbloom/smi/?main=girlsinbloom&page=smi"{if $smarty.get.main == 'girlsinbloom' and $smarty.get.page == 'smi'} class="menu-active"{/if}>
				Пресса
				о нас
			</a>
		</li>
	</ul>

	<a{if $smarty.get.main == 'sec-prod'} class="menu-active"{/if}
			href="/{$dir}/open_folders/76/0/?main=sec-prod&page=main"
	">
	<strong>Разделы товаров</strong>
	</a>
	<ul class="submenu{if $smarty.get.main == 'sec-prod'} active-menu{/if}">
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/76/0/?main=sec-prod&page=main"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 'main'} class="menu-active"{/if}>
				Главная
				страница
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/77/0/?main=sec-prod&page=catalog"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 'catalog'} class="menu-active"{/if}>
				Каталог
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/79/0/?main=sec-prod&page=pullovers"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 'pullovers'} class="menu-active"{/if}>
				Толстовки
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/80/0/?main=sec-prod&page=t-shorts"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 't-shorts'} class="menu-active"{/if}>
				Футболки
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/82/0/?main=sec-prod&page=covers"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 'covers'} class="menu-active"{/if}>
				Обложки
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/83/0/?main=sec-prod&page=sale"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 'sale'} class="menu-active"{/if}>
				Sale
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/84/0/?main=sec-prod&page=cases"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 'cases'} class="menu-active"{/if}>
				Чехлы
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/85/0/?main=sec-prod&page=girls-in-bloom"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 'girls-in-bloom'} class="menu-active"{/if}>
				Girls
				in Bloom
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/86/0/?main=sec-prod&page=new"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 'new'} class="menu-active"{/if}>
				New
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/95/0/?main=sec-prod&page=boxes"{if $smarty.get.main == 'sec-prod' and $smarty.get.page == 'boxes'} class="menu-active"{/if}>
				Коробки
			</a>
		</li>
	</ul>

	<a{if $smarty.get.main == 'clients'} class="menu-active"{/if}
			href="/{$dir}/open_folders/64/0/?main=clients&page=clients">
		<strong>Информация для клиентов</strong>
	</a>
	<ul class="submenu{if $smarty.get.main == 'clients'} active-menu{/if}">
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/64/0/?main=clients&page=clients"{if $smarty.get.main == 'clients' and $smarty.get.page == 'clients'} class="menu-active"{/if}>
				Клиенты
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/89/0/?main=clients&page=orders-payment"{if $smarty.get.main == 'clients' and $smarty.get.page == 'orders-payment'} class="menu-active"{/if}>
				Оплата
				заказа
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/90/0/?main=clients&page=delivery"{if $smarty.get.main == 'clients' and $smarty.get.page == 'delivery'} class="menu-active"{/if}>
				Доставка
				заказa
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/91/0/?main=clients&page=return"{if $smarty.get.main == 'clients' and $smarty.get.page == 'return'} class="menu-active"{/if}>
				Возврат
				товара
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/74/0/?main=clients&page=savety-payment"{if $smarty.get.main == 'clients' and $smarty.get.page == 'savety-payment'} class="menu-active"{/if}>
				Безопасность
				платежей
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/open_folders/92/0/?main=clients&page=license"{if $smarty.get.main == 'clients' and $smarty.get.page == 'license'} class="menu-active"{/if}>
				Публичная
				Oферта
			</a>
		</li>
	</ul>

	<a{if $smarty.get.main == 'orders'} class="menu-active"{/if}
			href="/{$dir}/orders/?main=orders">
		<strong>Заказы</strong>
	</a>

	<a{if $smarty.get.main == 'preorders'} class="menu-active"{/if}
			href="/{$dir}/preorders/?main=preorders">
		<strong>Предзаказы</strong>
	</a>

	<a{if $smarty.get.main == 'products'} class="menu-active"{/if}
			href="/{$dir}/products/?main=products">
		<strong>Товары</strong>
	</a>
	<ul class="submenu{if $smarty.get.main == 'products'} active-menu{/if}">

		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=masks&filter=masks"{if $smarty.get.main == 'products' and $smarty.get.page == 'masks'} class="menu-active"{/if}>
				Маски
			</a>
		</li>
        {*<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=male-costumes&filter=male-costumes"{if $smarty.get.main == 'products' and $smarty.get.page == 'male-costumes'} class="menu-active"{/if}>Костюмы мужские</a>
		</li>*}
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=female-costumes&filter=female-costumes"{if $smarty.get.main == 'products' and $smarty.get.page == 'female-costumes'} class="menu-active"{/if}>
				Костюмы
			</a>
		</li>
        {*<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=children-costumes&filter=children-costumes"{if $smarty.get.main == 'products' and $smarty.get.page == 'children-costumes'} class="menu-active"{/if}>Костюмы детские</a>
		</li>*}

		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=male-sweatshirts&filter=male-sweatshirts"{if $smarty.get.main == 'products' and $smarty.get.page == 'male-sweatshirts'} class="menu-active"{/if}>
				Толстовки мужские
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=female-sweatshirts&filter=female-sweatshirts"{if $smarty.get.main == 'products' and $smarty.get.page == 'female-sweatshirts'} class="menu-active"{/if}>
				Толстовки женские
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=children-sweatshirts&filter=children-sweatshirts"{if $smarty.get.main == 'products' and $smarty.get.page == 'children-sweatshirts'} class="menu-active"{/if}>
				Толстовки детские
			</a>
		</li>

		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=male-t-shirts&filter=male-t-shirts"{if $smarty.get.main == 'products' and $smarty.get.page == 'male-t-shirts'} class="menu-active"{/if}>
				Футболки мужские
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=female-t-shirts&filter=female-t-shirts"{if $smarty.get.main == 'products' and $smarty.get.page == 'female-t-shirts'} class="menu-active"{/if}>
				Футболки женские
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=children-t-shirts&filter=children-t-shirts"{if $smarty.get.main == 'products' and $smarty.get.page == 'children-t-shirts'} class="menu-active"{/if}>
				Футболки детские
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=t-shirts-with-sequins&filter=t-shirts-with-sequins"{if $smarty.get.main == 'products' and $smarty.get.page == 't-shirts-with-sequins'} class="menu-active"{/if}>
				Футболки с блестками
			</a>
		</li>

		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=gotovye-kejsy&filter=gotovye-kejsy&c_type=1"{if $smarty.get.main == 'products' and $smarty.get.page == 'gotovye-kejsy' and $smarty.get.c_type == 1} class="menu-active"{/if}>
				Кейсы
				на iPhone
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=gotovye-kejsy&filter=gotovye-kejsy&c_type=2"{if $smarty.get.main == 'products' and $smarty.get.page == 'gotovye-kejsy' and $smarty.get.c_type == 2} class="menu-active"{/if}>
				Кейсы
				на Samsung
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=swimsuits&filter=swimsuits"{if $smarty.get.main == 'products' and $smarty.get.page == 'swimsuits'} class="menu-active"{/if}>
				Купальники
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=bags&filter=bags"{if $smarty.get.main == 'products' and $smarty.get.page == 'bags'} class="menu-active"{/if}>
				Сумки
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=hoodie-hooded&filter=hoodie-hooded"{if $smarty.get.main == 'products' and $smarty.get.page == 'hoodie-hooded'} class="menu-active"{/if}>
				Худи с капюшоном
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=trousers&filter=trousers"{if $smarty.get.main == 'products' and $smarty.get.page == 'trousers'} class="menu-active"{/if}>
				Штаны
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=shorts&filter=shorts"{if $smarty.get.main == 'products' and $smarty.get.page == 'shorts'} class="menu-active"{/if}>
				Майки
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=notes&filter=notes"{if $smarty.get.main == 'products' and $smarty.get.page == 'notes'} class="menu-active"{/if}>
				Ежедневники
			</a>
		</li>

		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=raincoats&filter=raincoats"{if $smarty.get.main == 'products' and $smarty.get.page == 'raincoats'} class="menu-active"{/if}>
				Дождевики
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=body-without-sleeves&filter=body-without-sleeves"{if $smarty.get.main == 'products' and $smarty.get.page == 'body-without-sleeves'} class="menu-active"{/if}>
				Боди без рукавов
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=body-with-long-sleeves&filter=body-with-long-sleeves"{if $smarty.get.main == 'products' and $smarty.get.page == 'body-with-long-sleeves'} class="menu-active"{/if}>
				Боди с длинным рукавом
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=dresses&filter=dresses"{if $smarty.get.main == 'products' and $smarty.get.page == 'dresses'} class="menu-active"{/if}>
				Платья
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/products/?main=products&page=down-jackets&filter=down-jackets"{if $smarty.get.main == 'products' and $smarty.get.page == 'down-jackets'} class="menu-active"{/if}>
				Пуховики
			</a>
		</li>

		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/collections/?main=products&page=collections"{if $smarty.get.main == 'products' and $smarty.get.page == 'collections'} class="menu-active"{/if}>
				Коллекции
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/tags/?main=products&page=themes"{if $smarty.get.main == 'products' and $smarty.get.page == 'themes'} class="menu-active"{/if}>
				Темы
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/materials/?main=products&page=materials"{if $smarty.get.main == 'products' and $smarty.get.page == 'materials'} class="menu-active"{/if}>
				Материалы
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/discounts/?main=products&page=discounts"{if $smarty.get.main == 'products' and $smarty.get.page == 'discounts'} class="menu-active"{/if}>
				Скидки
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/types/?main=products&page=product-type"{if $smarty.get.main == 'products' and $smarty.get.page == 'product-type'} class="menu-active"{/if}>
				Типы
				товаров
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/cases_types/?main=products&page=cases-type"{if $smarty.get.main == 'products' and $smarty.get.page == 'cases-type'} class="menu-active"{/if}>
				Типы
				кейсов
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a
					target="_blank"
					href="/{$dir}/generate_sizes/?main=products&page=sizes-update"{if $smarty.get.main == 'products' and $smarty.get.page == 'update'} class="menu-active"{/if}>
				Обновить
				размеры товаров
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a target="_blank" href="/moysklad-update-products/">Синронизировать с "Мой склад"</a>
		</li>
	</ul>

	<a{if $smarty.get.main == 'banners'} class="menu-active"{/if} href="/{$dir}/banners/large/?main=banners">
		<strong>Баннеры</strong>
	</a>
	<ul class="submenu{if $smarty.get.main == 'banners'} active-menu{/if}">
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/banners/large/?main=banners&page=big-banners"{if $smarty.get.main == 'banners' and $smarty.get.page == 'big-banners'} class="menu-active"{/if}>
				Большие
				баннеры
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/banners/mobile-large/?main=banners&page=big-mobile-banners"{if $smarty.get.main == 'banners' and $smarty.get.page == 'big-mobile-banners'} class="menu-active"{/if}>
				Баннеры
				для мобильного
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/banners/middle/?main=banners&page=middle-banners"{if $smarty.get.main == 'banners' and $smarty.get.page == 'middle-banners'} class="menu-active"{/if}>
				Средние
				баннеры
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/banners/wide/?main=banners&page=wide-banners"{if $smarty.get.main == 'banners' and $smarty.get.page == 'wide-banners'} class="menu-active"{/if}>
				Широкие
				баннеры
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/banners/small/?main=banners&page=small-banners"{if $smarty.get.main == 'banners' and $smarty.get.page == 'small-banners'} class="menu-active"{/if}>
				Малые
				баннеры
			</a>
		</li>
	</ul>
	<a{if $smarty.get.main == 'menu'} class="menu-active"{/if} href="/{$dir}/menu/top/?main=menu">
		<strong>Меню</strong>
	</a>
	<ul class="submenu{if $smarty.get.main == 'menu'} active-menu{/if}">
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/menu/top/?main=menu&page=top-menu"{if $smarty.get.main == 'menu' and $smarty.get.page == 'top-menu'} class="menu-active"{/if}>
				Верхнее
				меню
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/menu/bottom/?main=menu&page=low-menu"{if $smarty.get.main == 'menu' and $smarty.get.page == 'low-menu'} class="menu-active"{/if}>
				Нижнее
				меню
			</a>
		</li>
		<li>
			<span class="sp_space"></span>
			<a href="/{$dir}/menu/bottom2/?main=menu&page=low-menu-2"{if $smarty.get.main == 'menu' and $smarty.get.page == 'low-menu-2'} class="menu-active"{/if}>
				Нижнее
				меню2
			</a>
		</li>
	</ul>


	<a{if $smarty.get.main == 'blog'} class="menu-active"{/if} href="/{$dir}/blog/?main=blog">
		<strong>Блог</strong>
	</a>
	<a{if $smarty.get.main == 'albums'} class="menu-active"{/if}
			href="/{$dir}/albums/?main=albums">
		<strong>Альбомы</strong>
	</a>
	<a{if $smarty.get.main == 'stock'} class="menu-active"{/if} href="/{$dir}/stock/?main=stock">
		<strong>Акции</strong>
	</a>
	<a{if $smarty.get.main == 'front_discounts'} class="menu-active"{/if}
			href="/{$dir}/front_discounts/?main=front_discounts">
		<strong>Скидки</strong>
	</a>
	<a{if $smarty.get.main == 'promocodes'} class="menu-active"{/if} href="/{$dir}/promocodes/?main=promocodes">
		<strong>Промокоды</strong>
	</a>
	<a{if $smarty.get.main == 'pp_types'} class="menu-active"{/if} href="/{$dir}/pp_types/?main=pp_types">
		<strong>Персональный
			промокоды
		</strong>
	</a>
	<a{if $smarty.get.main == 'free_delivery'} class="menu-active"{/if}
			href="/{$dir}/free_delivery/?main=free_delivery">
		<strong>Бесплатная
			доставка
		</strong>
	</a>
	<a{if $smarty.get.main == 'unloading'} class="menu-active"{/if} href="/{$dir}/unloading/users/?main=unloading">
		<strong>Выгрузка пользователей</strong>
	</a>
	<a target="_blank" href="/{$dir}/update_sitemap">
		<strong>Обновить sitemap.xml</strong>
	</a>
	<a{if $smarty.get.main == 'settings'} class="menu-active"{/if}
			href="/{$dir}/open_main_page/1/?main=settings">
		<strong>Основные настройки</strong>
	</a>
</div>
