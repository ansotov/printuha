<div class="holder">
  <table class="list">
    {if $orders}
    <thead>
      <tr>
        <td>{translateConst const = ORDER_NUMBER}</td>
        <td>{translateConst const = ORDER_DATE}</td>
        <td>{translateConst const = AMOUNT}</td>
        <td class="last">{translateConst const = DETAILS}</td>
      </tr>
    </thead>
    <tbody>
      {foreach $orders as $k=>$v}
      <tr>
        <td  class="left">{$v.number}</td>
        <td  class="left">{$v.show_date}</td>
        <td  class="left">{$v.sum} {translateConst const = RUB}</td>
        <td class="last"><a href="/cabinet/orders/{$v.number}/">{translateConst const = VIEW_ORDER}</a><td>
      </tr>
      {/foreach}
    </tbody>
    {else}
    <thead>
      <tr>
        <td>{translateConst const = ORDER_NUMBER}</td>
        <td>{translateConst const = ORDER_DATE}</td>
        <td>{translateConst const = AMOUNT}</td>
        <td class="last">{translateConst const = DETAILS}</td>
      </tr>
    </thead>
    <tbody>
      <tr><td colspan="4">{translateConst const = YOU_HAVENT_ANY_ORDERS}</td></tr>
    </tbody>
    {/if}
  </table>
</div>
