<?php
if ($url1 == 'cart') {
    $type = 'cart';
}
if ($url1 == 'checkout') {

    $send = intval($this->input->post('send'));
    if ($send == 1 && $_REQUEST['step'] != 2) {
        header('Location: /checkout?step=2');
    }

    $cart = $this->build_cart($this->cart->contents());

    //ansotov проверка на тип товара
    if ($cart) {
        foreach ($cart as $k => $v) {
            $this->load->model('mproduct_categories');

            $item = $this->mproduct_categories->get('const = "' . $v['info']['type'] . '"');

            $types[] = $item[0]['id'];

            //ansotov если есть товар со старой ценой
            if ($v['info']['old_price'] > 0) {
                $old_price = true;
            }
        }
    }

    $this->load->model('mproducts');

    $packages = $this->mproducts->get('type = "packages"');
    $this->mysmarty->assign('packages', $packages);

    //ansotov если есть коробки
    if (in_array('boxes', $types) || $old_price == true) {
        $this->mysmarty->assign('only_online', 1);
    }

    $type = 'checkout';

    $promocode = $this->getPromocode(false, $types);

    $this->mysmarty->assign('promocode', $promocode);

    //ansotov если промокод применён
    if ($this->session->userdata('promocode_discount')) {
        $this->mysmarty->assign('promocode_isset', true);
    }

    $ajax = intval($this->input->get('ajax'));
    if ($ajax == 1) {
        $name     = html_entity_decode($this->input->get('query'));
        $name     = iconv('utf-8', 'cp1251', $name);
        $state_id = intval($this->input->get('state_id'));
        $this->load->model('mcities');
        $data = $this->mcities->get("state_id = $state_id AND name LIKE '%$name%'", array('name'), 0, 'name ASC');
        if ($data) {
            foreach ($data as $k => $v) {
                $data[$k] = iconv('cp1251', 'utf-8', $v['name']);
            }
        }
        echo json_encode(array('query' => $name, 'suggestions' => $data));
        exit;
    }
    if ($url2 == 'cities') {
        $state_id = $this->input->get('state_id');
        $query    = $this->input->get('query');
        $this->load->model('mdelivery_addresses');

        $state = "state_id = $state_id";
        if ($state_id == 47 || $state_id == 78) {
            $state = "(state_id = 47 OR state_id = 78)";
        }

        if ($state_id == 77 || $state_id == 50) {
            $state = "(state_id = 77 OR state_id = 50)";
        }

        $data        = $this->mdelivery_addresses->get("$state AND city LIKE '%$query%'", array(), 0, 'city DESC');
        $suggestions = array();

        if ($data) {
            foreach ($data as $k => $v) {
                if ($v['company'] == 'russian-post') {
                    $v['company'] = 'russianpost';
                }
                $suggestions[$v['city']]['value']               = $v['city'];
                $suggestions[$v['city']]['data'][$v['company']] = array(
                    'price' => $v['price'],
                    'days'  => ($v['days_min'] == $v['days_max']) ? $v['days_min'] : $v['days_min'] . '-' . $v['days_max']
                );
            }
        }
        echo json_encode(array('query' => 'Unit', 'suggestions' => $suggestions));
        exit;
    }


    $this->load->model('musers');
    $this->load->model('morders');
    $this->load->model('mcountries');
    $this->load->model('mstates');
    $this->load->model('mpayment_types');
    $countries     = $this->mcountries->get(array(), array(), 0);
    $states        = $this->mstates->get(array(), array(), 0, 'order_id ASC');
    $payment_types = $this->mpayment_types->get(array('active' => 1), array(), 0, 'order_id ASC');
    $this->mysmarty->assign('countries', $countries);
    $this->mysmarty->assign('states', $states);
    $this->mysmarty->assign('payment_types', $payment_types);

    $this->lang->load('form_validation', 'russian');
    $this->load->library('form_validation');

    $postdata = array();

    if ($this->user_info != array()) {
        $postdata = $this->user_info;
        $this->load->model('maddresses');
        if ($address = $this->maddresses->get(array('active' => 1, 'user_id' => $this->user_id), array(), 1)) {
            $postdata = array_merge($postdata, $address);
        }
    }
    $this->mysmarty->assign('postdata');
    $send = intval($this->input->post('send'));

    //ansotov если данные отправлены и сумма заказа больше 0
    if ($send == 1 && $this->total_amount > 0) {

        if ($this->input->post('comments')) {
            $this->form_validation->set_rules('comments', 'Комментарий', 'trim|required|max_length[500]|alpha_dot_dash|required');
        }

        //ansotov разбираем массив адреса
        $address_arr = explode(' - ', $this->input->post('search_city_text'));

        if (count($address_arr) == 1) {
            $address_arr = explode(' - ', $this->input->post('search_city_text_hidden'));
        }

        $_POST['city'] = $address_arr[0];
        //ansotov валидация полей 2 этап, первый на frontend
        if ($this->input->post('foreign_lands') != 1) {
            $this->form_validation->set_rules('city', 'Город', 'trim|min_length[2]|max_length[90]|required');
        }

        $this->form_validation->set_rules('delivery_type', 'Тип доставки', 'trim|max_length[1]|numeric');

        if ($this->input->post('first_name')) {
            $this->form_validation->set_rules('first_name', 'Имя', 'trim|required|max_length[200]|alpha_dot_dash|required');
        }

        $this->form_validation->set_rules('phone', 'Телефон', 'trim|min_length[2]|max_length[90]|required');

        $this->form_validation->set_rules('email', 'E-mail', 'trim|max_length[100]|required|email');

        //ansotov установка суммы доставки по умолчанию
        $delivery_sum = 0;

        //ansotov установка количества дней доставки по умолчанию
        $delivery_days = 0;

        //$this->form_validation->set_rules('last_name', 'Фамилия','trim|required|max_length[200]|alpha_dot_dash|required');

        //ansotov если это не самовывоз
        if ($this->input->post('delivery_type') != 1) {

            //ansotov если это не pickpoint
            if ($this->input->post('delivery_type') == 5) {

                //ansotov если это SDEK получаем данные по доставке
                $delivery      = $this->getSDEKInfo($address_arr[0], $address_arr[1]);
                $delivery_sum  = $delivery['price'];
                $delivery_days = $delivery['days'];

                /*$delivery      = array('price' => 880, 'min_days' => 3, 'max_days' => 5);
                $delivery_sum  = $delivery['price'];
                $delivery_days = $delivery['max_days'];*/
            } elseif ($this->input->post('delivery_type') != 3) {

                if ($this->input->post('delivery_type') == 2) {
                    //ansotov если это курьер
                    $delivery      = $this->getCouriers($address_arr[0], $address_arr[1]);
                    $delivery_days = $delivery[0]['max_days'];
                } elseif ($this->input->post('delivery_type') == 4) {
                    //ansotov если это почта России
                    $delivery = $this->getCities($address_arr[0], 1, 4, true, $address_arr[1]);

                    //ansotov если другая страна, то индекс не нужен
                    /*if ($this->input->post('foreign_lands') != 1)
                      $this->form_validation->set_rules('zipcode', 'Индекс', 'trim|required|max_length[200]|alpha_dot_dash|required');*/

                    $delivery_days = $delivery[0]['days'];
                }

                $delivery_sum = $delivery[0]['price'];

                //if ($this->input->post('flat'))
                //$this->form_validation->set_rules('flat', 'Квартира', 'trim|required|max_length[200]|alpha_dot_dash|required');

                //ansotov для другой страны есть поле со всеми данными доставки
                /*if ($this->input->post('foreign_lands') != 1) {
                  $this->form_validation->set_rules('house', 'Дом', 'trim|required|max_length[200]|alpha_dot_dash|required');
                  $this->form_validation->set_rules('street', 'Улица', 'trim|required|max_length[200]|alpha_dot_dash|required');
                }*/
            } elseif ($this->input->post('delivery_type') == 3) {

                //ansotov если это Pickpoint получаем данные по доставке
                $delivery      = $this->getPickpointInfo($address_arr[0], $address_arr[1]);
                $delivery_sum  = $delivery['price'];
                $delivery_days = $delivery['days'];

                $this->form_validation->set_rules('pickpoint_address', 'Pickpoint адрес', 'trim|required|max_length[200]|alpha_dot_dash|required');
                $this->form_validation->set_rules('pickpoint_city', 'Pickpoint город', 'trim|required|max_length[200]|alpha_dot_dash|required');
            }
        }

        //ansotov костыль для доставки в другие страны
        if ($this->input->post('foreign_lands') == 1) {

            $free_delivery = $this->mfree_delivery->get(array("active" => 1, 'const' => 'ALL_DAYS'), array(), 1);

            $delivery_sum  = 500;//(($free_delivery['start_time'] <= date('H:i:s')) && ($free_delivery['end_time'] <= date('H:i:s')) && $this->total_amount >= $this->free_delivery) ? (($this->total_amount >= $this->free_delivery) ? 0 : 500) : 500;//ansotov 500 = стоимость доставки в другие страны, костыль
            $delivery_days = 10;
        }

        $this->form_validation->set_rules('payment_type', 'Тип оплаты', 'trim|required|max_length[200]|alpha_dot_dash|required');

        $errors = (filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL) !== false) ? false : true;

        //ansotov проверяем валидность поступившых данных
        if ($this->form_validation->run() == false || $errors == true) {
            //ansotov вероятность попадения в это условие очень мала, валидация идёт на frontend, но лучше подстраховаться
            $postdata = $this->form_validation->get_postdata();
            $errors   = $this->form_validation->get_errors();
            $this->mysmarty->assign('errors', $errors);
            $this->mysmarty->assign('postdata', $postdata);
        } else {

            //ansotov отвалидированные данные
            $data = $this->form_validation->get_postdata();
            //ansotov устанавливаем статус чо валидация пройдена успешно
            $data['status']        = 'complete';
            $data['delivery_days'] = $delivery_days;

            //ansotov удаляем технические данные
            if (isset($data['pickpoint_address'])) {
                //ansotov запись адреса для pickpoint
                $data['address'] = $data['pickpoint_address'];

                //ansotov удаляем технические данные
                unset($data['pickpoint_address']);
                unset($data['pickpoint_city']);
            }

            $this->load->model('morders_products');

            $sum = $allAmountDiscount = 0;

            if ($cart) {

                foreach ($cart as $k => $v) {
                    $cart[$k]['price_sum'] = $v['price'] * $v['qty'];

                    //ansotov бесплатный экземпляр по акции
                    if ($v['in_stock']) {
                        $cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
                        $allAmountDiscount     += ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
                    }

                    $sum = $sum + $cart[$k]['price_sum'];
                }
            }

            //ansotov сумма заказа, если онлайн то скидка
            $data['sum'] = $this->total_amount;

            $paymentDiscount = (in_array($data['payment_type'], array('online', 'paypal'))) ? 3 : 0;

            $data['sum'] = $data['sum'] - (($data['sum'] / 100) * $paymentDiscount);

            //ansotov сумма доставки
            $data['delivery_sum'] = $delivery_sum;

            $data['date'] = date("Y-m-d H:i:s");

            if ($this->input->post('foreign_lands') == 1) {
                $data['address'] = htmlentities(htmlspecialchars(trim($this->input->post('address'))));
            }

            $data['address'] = (isset($data['address'])) ? $data['address'] : htmlspecialchars(trim($this->input->post('address')));

            $order_id             = $this->morders->insert($data);
            $order_info           = $this->morders->get(array('id' => $order_id));
            $data['order_number'] = $order_info[0]['number'];

            $this->morders_products->set_orders(array('user_order_id' => $order_id));

            $this->morders_products->delete(array('user_order_id' => $order_id));

            //ansotov все продукты
            $products_arr = array();

            foreach ($cart as $k => $v) {
                $order_product = array(
                    'price'         => $v['price'],
                    'qty'           => $v['qty'],
                    'user_order_id' => $order_id,
                    'name'          => $v['info']['name'],
                    'sku'           => $v['info']['sku'],
                    'color'         => $v['color'],
                    'product_id'    => $v['info']['id'],
                    'size'          => $v['size'],
                    'moysklad_id'   => $v['size_info']['moysklad_id']
                );
                if (isset($v['material'])) {
                    $order_product['material'] = $v['material']['name'];
                }
                $product_data               = $order_product;
                $product_data['variant_id'] = $v['size_info']['id'];
                $product_data['sizeName']   = ($v['size_info']['name']) ? $v['size_info']['name'] : $v['size_info']['code'];
                $this->morders_products->insert($order_product);
                $products_arr[$k]      = $order_product;
                $products_arr_info[$k] = $product_data;
            }

            $data['products']      = $products_arr;
            $data['products_info'] = $products_arr_info;
            $data['order_id']      = $order_id;
            $data['p_type']        = ($data['payment_type'] == 'courier') ? 'при получении' : 'онлайн';
            $data['d_type']        = $this->getDeliveryTitle($this->input->post('delivery_type'));
            $data['comment']       = htmlentities(htmlspecialchars(trim($order_info[0]['comments'])));

            //ansotov сохранение в "мой склад"
            //$this->setMoySkladOrder($data);

            $orderData = array();

            // Order ID from shop
            $orderData['externalId'] = $order_id;

            // Order number
            $orderData['number'] = $data['order_number'];

            // Customer information
            $orderData['firstName']       = $this->input->post('first_name');
            $orderData['phone']           = $this->input->post('phone');
            $orderData['email']           = $this->input->post('email');
            $orderData['customerComment'] = $this->input->post('comments');
            //$orderData['customerComment'] = htmlspecialchars($data['comment']);

            foreach ($products_arr_info as $k => $v) {
                // Products
                $orderData['items'][$k]['offer']['externalId'] = $v['variant_id'];
                /*$orderData['items'][$k]['properties'][0]['name']  = 'size';
                $orderData['items'][$k]['properties'][0]['value'] = $v['sizeName'] . (($v['color']) ? ' - Цвет:' . $v['color'] : false) . (($v['material']) ? ' - Материал:' . $v['material'] : false);
                */
                $iData = 0;
                /*if($v['commentDesign']) {
                  $orderData['items'][$k]['properties'][$iData]['name']  = 'Свой дизайн';
                  $orderData['items'][$k]['properties'][$iData]['value'] = $v['commentDesign'];
                  $iData++;
                }*/

                if ($v['color']) {
                    $orderData['items'][$k]['properties'][$iData]['name']  = 'Цвет';
                    $orderData['items'][$k]['properties'][$iData]['value'] = $v['color'];
                    $iData++;
                }

                if ($v['material']) {
                    $orderData['items'][$k]['properties'][$iData]['name']  = 'Материал';
                    $orderData['items'][$k]['properties'][$iData]['value'] = $v['material'];
                }

                $orderData['items'][$k]['quantity'] = $v['qty'];
            }

            // Delivery
            $orderData['delivery']['code']            = $this->getDeliveryConst($this->input->post('delivery_type'));
            $orderData['delivery']['cost']            = $data['delivery_sum'];
            $orderData['delivery']['address']['text'] = $data['address'];

            // Payment
            $orderData['payments'][]['type'] = $data['payment_type'];
            // Method
            //$orderData['orderMethod'] = 'one-click';

            $allAmountDiscount += (($sum / 100) * $paymentDiscount);

            // Discount
            $orderData['discountManualAmount'] = round($allAmountDiscount);

            $data['number'] = $this->morders->number;
            //$orderData['discountManualPercent'] = $paymentDiscount;

            // Set order to Retail Crm
            $result = $this->RCCreateOrder($orderData);

            // Send info about incorrect order
            if (isset($result) && $result->getStatusCode() != 201) {
                $this->mysmarty->assign('orderNumber', $data['number']);
                $template = $this->mysmarty->fetch('new_bloom/ru/failed_order_mail.tpl');

                $this->send_mail('my-gebbels@yandex.ru', 'my-gebbels@yandex.ru', 'Не прошел заказ', $template);
                $this->send_mail('info@printuha.ru', 'info@printuha.ru', 'Не прошел заказ', $template);
                $this->send_mail('darya@girlsinbloom.ru', 'darya@girlsinbloom.ru', 'Не прошел заказ', $template);
                mail('my-gebbels@yandex.ru', 'Не прошел заказ', 'Не прошел заказ №' . $data['number']);
            }

            //ansotov sms по завершении заказа
            $sms_message = "Спасибо за заказ на printuha.ru! Наш менеджер свяжется с вами.";
            $sms_phone   = preg_replace('/[^0-9]/', '', $this->input->post('phone'));
            $this->sendSms("$sms_phone", $sms_message);

            if ($this->input->post('foreign_lands') == 1) {
                $this->mysmarty->assign('foreign_lands', $this->input->post('foreign_lands'));
            }
            $this->mysmarty->assign('cart', $cart);
            $this->mysmarty->assign('order', $data);
            $this->mysmarty->assign('order_number', $data['number']);
            $headline = "Подтверждение заказа " . $data['number'];
            $template = $this->mysmarty->fetch('new_bloom/ru/order_mail.tpl');
            if ( ! isset($data['email']) && $this->user_id > 0 && (isset($this->user_info['email']))) {
                $data['email'] = $this->user_info['email'];
            }
            //if($data['payment_type']!='online'){
            $this->cart->destroy();

            if (isset($data['email'])) {
                $this->send_mail($data['first_name'], $data['email'], $headline, $template);
            }

			//$this->send_mail('my-gebbels@yandex.ru', 'my-gebbels@yandex.ru', $headline, $template);
			$this->send_mail('store@printuha.ru','store@printuha.ru',$headline,$template);

            //ansotov если сумма достаточная то генерим промокод и отправляем на указанную почту
            $promocode = $this->getPersonalPromocodeData($this->total_amount);
            if ($promocode) {
                $headline = "Ваш секретный промокод внутри!";
                $this->mysmarty->assign('promocode', $promocode);
                $template = $this->mysmarty->fetch('new_bloom/ru/promocode_mail.tpl');
                $this->send_mail($data['first_name'], $data['email'], $headline, $template);
            }

            //ansotov удаляем данные о промокоде
            $this->session->unset_userdata('promocode_discount');

            //ansotov удаляем персональный промокод при завершении заказа
            if ($this->session->userdata('promocode_value')) {
                $this->delPersonalPromocode($this->session->userdata('promocode_value'));
            }


            //ansotov если пользователь авторизован, то удаляем напоминания для него
            if ($this->user_info) {
                $this->load->model('mremind_products');
                $this->mremind_products->delete(array('user_id' => $this->user_info['id']));
            }

            if ($data['payment_type'] == 'online') {
                header('Location: /online-payment/' . $data['number'] . '/');

                return;
            } elseif ($data['payment_type'] == 'paypal') {
                header('Location: /paypal-payment/' . $data['number'] . '/');
            } else {
                header('Location: /complete/');

                return;
            }
        }
    }

    if ($postdata == array() && $this->current_order != array()) {
        $postdata = $this->current_order;
        $this->mysmarty->assign('postdata', $this->current_order);
    }
    if ($postdata == array() && $this->user_info != array()) {
        $postdata = $this->user_info;
    }
    if ($postdata != array()) {
        if (isset($postdata['city']) && isset($postdata['state_id']) && ($r = $this->mcities->get(array("`name` like" => $postdata['city'], "state_id" => $postdata['state_id']), array(), 1))) {
            $this->mysmarty->assign('show_region_payment', 1);
        }

        $this->mysmarty->assign('postdata', $postdata);
    }

}
if ($url1 == 'online-payment' && $url2 != '') {
    $this->load->model('morders');
    $type = "online-payment";
    if ($order = $this->morders->get(array('number' => $url2, 'payment_type' => 'online'), array(), 1)) {
        $url       = 'https://pay.best2pay.net/webapi/Register';
        $signature = base64_encode(md5($this->sector . (($order['sum'] + $order['delivery_sum']) * 100) . $this->currency . $this->password));
        $data      = array(
            'sector'      => $this->sector,
            'amount'      => ($order['sum'] + $order['delivery_sum']) * 100,
            'currency'    => $this->currency,
            'mail'        => $order['email'],
            'reference'   => $order['number'],
            'description' => 'Printuha order.',
            'url'         => 'http://www.printuha.ru/',
            'signature'   => $signature
        );

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $sector  = $this->sector;
        $context = stream_context_create($options);
        $result  = file_get_contents($url, false, $context);

        $xml = simplexml_load_string($result);

        if (isset($xml->code)) {
            echo $xml->description;
            exit;
        }

		$this->cart->destroy();

        $id = $xml->id;


        $signature = base64_encode(md5($this->sector . $id . $this->password));


        $this->morders->update(array('online_payment_number' => $id), array('number' => $order['number']));

        header("Location: https://pay.best2pay.net/webapi/Purchase?sector=$sector&id=$id&signature=$signature");

        return;
    } else {
        $this->show_404();

        return;
    }
}

//ansotov оплата PayPal
if ($url1 == 'paypal-payment' && $url2 != '') {
    //ansotov подключение модели заказов
    $this->load->model('morders');

    //ansotov тип страницы
    $type = "paypal-payment";

    //ansotov если заказ есть то переходим к оплате
    if ($order = $this->morders->get(array('number' => $url2, 'payment_type' => 'paypal'), array(), 1)) {
        //ansotov куда отправляем данные
        $url = 'https://www.paypal.com/cgi-bin/websc';

        //ansotov кому отправляем деньги
        $receiverEmail = 'info@girlsinbloom.ru';

        //ansotov параметры для отправки
        $data = array(
            'cmd'           => '_xclick',
            'business'      => $receiverEmail,
            'amount'        => ($order['sum'] + $order['delivery_sum']),
            'address1'      => $order['address'],
            'city'          => $order['city'],
            'first_name'    => $order['first_name'],
            'last_name'     => $order['last_name'],
            'currency_code' => 'RUB',
            'invoice'       => $order['number'],
            'return'        => 'http://www.girlsinbloom.ru/online-payment-results/',
            'cancel_return' => 'https://girlsinbloom.ru'
        );

        //ansotov отправка данных
        header("Location: $url" . "?" . http_build_query($data));
    } else {
        $this->show_404();

        return;
    }
}
if ($url1 == 'online-payment-results') {
    $error = $this->input->get('error');
    if ($error != '') {
        $type = 'error';

    } else {
        $this->load->model('morders');
        $this->load->model('morders_products');
        $id    = $this->input->get('id');
        $order = $this->morders->get(array('online_payment_number' => $id), array(), 1);

        $this->load->model('morders_products');

        $query  = '';
        $query2 = '';
        $cart   = array();
        $temp   = $this->morders_products->get(array('user_order_id' => $order['id']), array(), 0);
        if ($temp) {
            foreach ($temp as $k => $v) {
                $query  .= ($query == '') ? "id = " . $v['product_id'] : " OR id = " . $v['product_id'];
                $query2 .= ($query2 == '') ? "(product_id = " . $v['product_id'] . " AND code ='" . $v['size'] . "' )" : " OR ( product_id = " . $v['product_id'] . " AND code = '" . $v['size'] . "' )";
            }
            $this->load->model('mproducts');
            $this->load->model('mproducts_sizes');

            $data = $this->mproducts->get($query, array(), 0);
            foreach ($data as $k => $v) {
                $products[$v['id']] = $v;
            }

            $sizes = array();
            $data  = $this->mproducts_sizes->get($query2, array(), 0);
            foreach ($data as $k => $v) {
                $sizes[$v['product_id']][$v['code']] = array('on_hand' => $v['on_hand'], 'on_order' => $v['on_order']);
            }


            $warning = false;
            $sum     = 0;
            foreach ($temp as $k => $v) {
                $fproducts[$k]         = $v;
                $fproducts[$k]['info'] = $products[$v['product_id']];

            }
            $cart = $fproducts;

        }
        /*$this->mysmarty->assign('cart',$cart);
        $this->mysmarty->assign('order',$order);
        $this->mysmarty->assign('order_number',$order['number']);
        $headline= "Подтверждение заказа ".$order['number'];
        $template = $this->mysmarty->fetch('bloom/ru/order_mail.tpl');
        $this->cart->destroy()

        if(isset($order['email']))
          $this->send_mail($order['first_name'],$order['email'],$headline,$template);
        $this->send_mail('girlsinbloom@mail.ru','girlsinbloom@mail.ru',$headline,$template);
        $this->send_mail('_raimaker_@mail.ru','_raimaker_@mail.ru',$headline,$template);
        ;*/

        $type = 'complete';
    }
}

if ($url1 == 'complete') {
    $type = 'complete';
}
?>
