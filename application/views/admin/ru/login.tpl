<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{$site_name} - Админ панель</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/css/rwd.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700|Open+Sans&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
<link href="/css/font-awesome.css" rel='stylesheet' type='text/css'>
<style>

html,body{ text-align:center; width:100%; height:100%; margin:0; background:#000000}
.login, td,tr{
	vertical-align:middle;
}
.string {
    border: medium none;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 2px 3px #DCE4EA inset;
    color: #313131;
    font-family: Helvetica Neue;
    font-size: 14px;
    margin: 3px;
    padding: 12px;
    width: 244px;
}
</style>
<script src="/js/jquery-1.7.1.min.js"></script>
<script>

$(function() {
	$(".pretty_button").mouseup(function(){
      $(this).removeClass('pb_clicked');
    }).mousedown(function(){
      $(this).addClass('pb_clicked');
    });
})
</script>
</head>
<body>
<table class="login" border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr>

<td align="center">
<form action="" method="post">
<table class="tab_style">
<tr>
	<td align="center"><div>

					<div class="logo"></div>
					<div class="underline">Админ панель</div>
        </div></td>
</tr>
<tr height="20"></tr>
<tr>
	<td align="center"><input type="text" name="login" placeholder="Логин" class="string required"></td>
</tr>
<tr>
	<td align="center"><input type="password" name="pass" placeholder="Пароль" class="string required"></td>
</tr>
{if $errors}
<tr>
    <td>Error:</td>
    <td><div class="error">{foreach from=$errors  item=v}{$v}{/foreach}</div></td>
</tr>
{/if}
<tr>
<td width="10"></td>
</tr>
<tr>
<td colspan="2" align="center">
<input type="submit" value="Войти" class="button">
</td>
</tr>
</table>
</form></td></tr>
</table>
</body>
</html>
