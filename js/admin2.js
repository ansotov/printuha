var $default_rows;
var $data_array = {};
var $structure_array = {};
var $default_rows = {};

function aj_checkbox($obj,$aj_par,$aj_id, $product_id){
	if($($obj).is(':checked'))
		$aj_val = '1';
	else
		$aj_val = '0';

    $product_id = ($product_id != undefined) ? $product_id : 0;

	$.ajax({
	  url: document.location.href,
	  dataType: 'json',
	  type:'post',
	  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
	  data:{
			"set_aj_val":$aj_val,
			"set_aj_par":$aj_par,
			"set_aj_id":$aj_id
			},
	  success: function(data) {
	  	//ansotov сообщаем о поступлении товара
		if ($aj_par == 'not_available' && $aj_val == 0) {
            sendRemindProducts($aj_id);
		} else if($aj_par == 'active' && $aj_val == 1 && $product_id > 0) {
            sendRemindProductSize($product_id, $aj_id);
		}
	  }
	});

}

//ansotov сообщаем о поступлении товара
function sendRemindProducts(id) {
    $.ajax({
        type: "POST",
        url: "/reminder/",
        data: {'id': id},
        success: function (data) {

        }
    });
}

//ansotov сообщаем о поступлении товара
function sendRemindProductSize(id, size_id) {
    $.ajax({
        type: "POST",
        url: "/reminder/",
        data: {'id': id, 'size_id': size_id},
        success: function (data) {

        }
    });
}

function Admin () {
	this.init = adminInit;
    this.rows = ajax_build_rows;
	this.page = change_per_page;
}
$.ajaxSetup({
    cache: false // for ie
});

function saveonchange(){
	$("#save_on_change").val(1);
	$("#main_form").submit();
}
function get_filters(){
	$(".filter").each(function(index) {
		if($(this).val() !=''){
			$data_array[$(this).attr('name')] = $(this).val();
		}else{
			delete $data_array[$(this).attr('name')];
		}
	});
	ajax_build_rows();
}

function adminInit(){
	
	$data_array['ajax_output']=1;
	$data_array['page'] = $('page').val();
	
	$structure_array = {'order_id':0};
	
	$(".structure_array").each(function(index) { 
		$structure_array[$(this).attr('name')] = $(this).val();
	});
	
}


function ajax_build_rows(){
	var $rows = {};
	$.ajax({
			  url: document.location.href,
			  dataType: 'xml',
			  type:"POST",
			  data:$data_array,
			  beforeSend:function(){
				  //$(".showAllCol").remove();
			  },
			  success: function(xml) {
				if($(xml).size()>0){
					$('#table_body').empty();
					$("row_data",xml).each(function(row){
						
							row_data = $(this);
							
							var $text = '<tr class="draggable" id="row_'+row_data.find('order_id').text()+'">';
							$.each($structure_array, function($key, $value) { 
							  if (row_data.find($key).attr("width")){
								  $td= ' width="'+row_data.find($key).attr("width")+'" ';
							  }else
							  	$td = 'width="40" class="numberList" ';
							  $text = $text +'<td '+$td+'>'+row_data.find($key).text()+'</td>';
							  
							});
							$text = $text +'<td width="90">'+row_data.find('options').text()+'</td>';
							$text = $text + '</tr>';
							$('#table_body').append($text);
							
							//adding row info
							$rows['row_'+row_data.find('order_id').text()]=row_data.find('order_id').text();
							
					});
					$(".pages").html($("pages",xml).text());
					$(".total").html($("total",xml).text());
					
				}
			  }
	});
	$default_rows = $rows;
}


$(document).ready(function() {
	
	var admin = new Admin();
	admin.init();
	admin.rows();
	
	
	
	$( "#table_body" ).sortable({
		revert: false,
		helper: 'clone',
		axis: 'y',
		update: function(event, ui) {
					
				 var $k = 0;
				 var $from_value = 0;
				 var $find_value = 0;
				 var $to_value = 0;
				 var $from = 0;
				 var $from_k = 0;
				 var $to = 0;
				 var $to_k = 0;
				 var $type ='';
				 var $send = 0;
				 
				 var $new_rows = $(this).sortable('toArray');
				 var $default_vals={};
				 
				 if($("#order").val()=='ASC')
					var $order = 1;
				 else
				 	var $order = -1;
				 $.each($default_rows, function($key, $value) { 
					$default_vals[$k]=$value;
					
					if($new_rows[$k]!=$key && $from_k == 0){
						$from_k = $key;
					}
					if($type == '' && $from_k != 0 && $from_k != $new_rows[$k]){
						var $k1 = parseInt($from_k.replace('row_',''));
						var $k2 = parseInt($new_rows[$k].replace('row_',''));
						
							
						if($k2 == ($k1 + $order)){
							$type ='forward';
						}else{
							$type ='backward';
						}
						$from = $k1;
					}
					
					if($type == 'forward' && ('row_'+$from ==$new_rows[$k])){
						$to = $key.replace('row_','');
					}
					if($type == 'backward' && ('row_'+$from == $key) ){
						$to = $new_rows[$k].replace('row_','');
					}
					if($to != 0 && $send ==0){
						var $par = {'ajax_output':1};
						$par['push_from'] = $from;
						$par['push_to'] = $to;
						$par['push_type'] = $type;
						$.ajax({
							  url: ' ',
							  dataType: 'json',
							  type:"POST",
							  data:$par,
							  success: function(data) {
							  }
						});
						$send = 1;
					}
					
					
					
					$k++;
				 });
				 $('#table_body tr').each(
	
						// For each hottie, run this code. The "indIndex" is the
						// loop iteration index on the current element.
						function( intIndex ){
							$(this).attr('id','row_'+$default_vals[intIndex]);
							$( this ).find('td:first').html($default_vals[intIndex]);
						}
				  );
					 
			}
	});
	
	
	/*
	$( "tbody, tr" ).disableSelection();*/
});


function to_page(page){
	$data_array['page']= page;
	ajax_build_rows();
	
}
function delete_o(del){
	
	if(confirm("Delete?")){
		$data_array['delete'] = del;
		ajax_build_rows();
	}
	
}
function change_per_page(per_page){
	$data_array['show_per_page']= per_page;
	ajax_build_rows();
}

function delete_photo_gallery_photo($gallery_key,photo_id,obj){
	
	if(confirm("Delete?")){
		data_up = {'post':1};
		data_up['gallery_photo_delete_'+$gallery_key]=photo_id;
		$.ajax({
				  url: 				document.location.href,
				  dataType: 		'json',
				  type:				'POST',
				  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
				  data:    			data_up,
				  success: 			function(data) {
										$(obj).parent().remove();
										$('#'+$gallery_key+'_image_holder').width($('#'+$gallery_key+'_image_holder').width()-110);
									}
			});
	}
}


function copyToClipboard (text) {
  window.prompt ("��� �� �����������  �������: Ctrl+C, Enter", text);
}
function photo_gallery_get_insert($gallery_key,photo_folder,link_to_folder){
	data_up = {'post':1};
	data_up['get_gallery_photos_'+$gallery_key]=1;
	$.ajax({
			  url: 				document.location.href,
			  dataType: 		'json',
			  type:				'POST',
			  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
			  data:    			data_up,
			  beforeSend:		function(){
									$("#area_{$k}").html("<div>��������...</div>");
								},
			  success: 			function(data) {
				  					 $.each(data, function(i,photo){
										image_type = (typeof(photo.image_type) != "undefined" ? photo.image_type : 'jpg') 
										dada ='<div class="gallery_photo" id="gallery_photo_'+$gallery_key+'_'+photo.id+'">'+
													'<div class="photo_dell" onclick="delete_photo_gallery_photo(\''+$gallery_key+'\','+photo.id+',this)"></div>'+
													'<a target="_new" href="'+link_to_folder+''+photo.id+'/">'+
													'<img src="/'+photo_folder+''+photo.photo_field+'_small.'+image_type+'">'+
													'</a>'+
													'<div class="photo_sel"><input type="checkbox" name="/'+photo_folder+photo.photo_field+'.'+image_type+'" class="select_'+$gallery_key+'" id="select_'+$gallery_key+'_'+photo.id+'"></div>'+
													'<a target="blank" href=\'/'+photo_folder+photo.photo_field+'.'+image_type+'\'>Link</a>'+
											  '</div>';
										$('#'+$gallery_key+'_image_holder').append(dada);
										
										$('#'+$gallery_key+'_image_holder').width(110+$('#'+$gallery_key+'_image_holder').width());
									});
	

								},
			  error:			function(){
									$("#area_{$k}").html("");
								}
		});
}
function select_all_gallery_photos($gallery_key,obj){

	var checked_status = obj.checked;
	$(".select_"+$gallery_key).each(function()
	{
		this.checked = checked_status;
	});
}

function add_text($gallery_key,object_id){
	text = '<div class="common_slider"><ul>';
	$(".select_"+$gallery_key).each(function()
	{
		if(this.checked == true){
			text  = text + '<li><a class="customLink" rel="next"><img src="'+$(this).attr('name')+'"></a><p></p></li>';
		}
	});
	text = text + '</ul></div>';
	var pp1=' <p>';
	var pp2='</p> ';
	
	
	txt = $("#"+object_id).val();
	my_pp = '&nbsp;'+pp2;
	
	len=txt.length;
	if(len==0){
		$("#"+object_id).val(pp1+" "+text+" "+my_pp);
	}else{
		len=txt.length-4;
		txt=txt.substr(0,len);
		$("#"+object_id).val(txt+" "+text+" "+my_pp);
	}
	
}

function photo_gallery_get($gallery_key,photo_folder,link_to_folder){
	data_up = {'post':1};
	data_up['get_gallery_photos_'+$gallery_key]=1;
	$.ajax({
			  url: 				document.location.href,
			  dataType: 		'json',
			  type:				'POST',
			  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
			  data:    			data_up,
			  beforeSend:		function(){
									$("#area_{$k}").html("<div>��������...</div>");
								},
			  success: 			function(data) {
				  					 $.each(data, function(i,photo){
										image_type = (typeof(photo.image_type) != "undefined" ? photo.image_type : 'jpg')
										dada ='<div class="gallery_photo" id="gallery_photo_'+$gallery_key+'_'+photo.id+'">'+
													'<div class="photo_dell" onclick="delete_photo_gallery_photo(\''+$gallery_key+'\','+photo.id+',this)"></div>'+
													'<a  target="_new"  href="'+link_to_folder+''+photo.id+'/">'+
													'<img src="/'+photo_folder+''+photo.photo_field+'_small.'+image_type+'">'+
													'</a>'+'</div>';
										$('#'+$gallery_key+'_image_holder').append(dada);
										
										$('#'+$gallery_key+'_image_holder').width(110+$('#'+$gallery_key+'_image_holder').width());
									});
	

								},
			  error:			function(){
									$("#area_{$k}").html("");
								}
		});
}
function jcrop_image_init($key,$crop_width,$crop_height,$min_width,$min_height,$directory,$user_id,$rand,$loaded_image ,$aspect_ratio,$max_fit){
	
	if($loaded_image==0){
		new AjaxUpload('#upload_button_'+$key, {
			action: document.location.href,			
			data: {
				'upload_cropable' : $key,
				'post' : 1,
			},
			
			name: $key+'_ajax_file',
			
			onSubmit : function(file, ext){		
				$('#upload_button_'+$key).html('<img src="/img/loading.gif">');

				this.disable();
			},
			onComplete : function(file,image_type){
				image_type = image_type.replace(/"/g,'')
				$('#upload_button_'+$key).text('���������');
				$('#upload_button_'+$key).hide();
				$('#uploaded_image_'+$key).show();
				$('#crop_button_'+$key).show();

				way = "/"+$directory+$key+"_"+$user_id+"."+(image_type?image_type:'jpg')+"?rand="+$rand;
				
				$('#uploaded_image_'+$key).html("<img id='uploaded_image_img_"+$key+"' src='"+way+"'>");
				
				$('#uploaded_image_'+$key+" img").load(function(){
					
					data = {
							onSelect: function(c){
												$('#'+$key+'_x1').val(c.x);
												$('#'+$key+'_y1').val(c.y);
												$('#'+$key+'_x2').val(c.x2);
												$('#'+$key+'_y2').val(c.y2);
												$('#'+$key+'_w').val(c.w);
												$('#'+$key+'_h').val(c.h);
												$('#'+$key+'_image_type').val(image_type);
											},
							minSize: [$min_width,$min_height],
							setSelect:   [ 0, 0,  $min_width, $min_height ]
					}
					if($max_fit==1){
						var img = document.getElementById("uploaded_image_img_"+$key); 
						var width = img.clientWidth;
						var height = img.clientHeight;
						data['setSelect'] = [0,0,width,height]
					}
					if ($aspect_ratio == 1 && $crop_width !=0 && $crop_height != 0){
						data['aspectRatio'] = $crop_width  / $crop_height
					}else if($aspect_ratio==0){
						data['aspectRatio'] = 0;
					}
					$('#uploaded_image_img_'+$key).Jcrop(data);
					
					
				});
				
			}	
		});		

		$("#crop_button_"+$key).click(function() {
				data_up ={ 'x1':$('#'+$key+'_x1').attr('value'),
						   'x2':$('#'+$key+'_x2').attr('value'),
						   'y1':$('#'+$key+'_y1').attr('value'),
						   'y2':$('#'+$key+'_y2').attr('value'),
						   'w':$('#'+$key+'_w').attr('value'),
						   'h':$('#'+$key+'_h').attr('value'),
						   'image_type':$('#'+$key+'_image_type').attr('value'),
						   'random':$('#'+$key+'_random').attr('value'),
						   'post':1
						   };
				data_up[$key+'_crop_image']=1;
				$.ajax({
					  url: 				document.location.href,
					  dataType: 		'json',
					  type:				'POST',
					  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
					  data:    			data_up,
					  success: 			function(data) {
											$('#crop_button_'+$key).hide();
											$('#uploaded_image_'+$key).hide();
											$('#croped_image_'+$key).show();
											$('#croped_image_img_'+$key).attr('src',"/"+$directory+$key+"_"+$user_id+"_cropped."+($('#'+$key+'_image_type').attr('value'))+"?"+$rand);
											$('#'+$key+'_save_croped').attr('value',1);
										}
				});
		});
	}
	else{
		if ($crop_width ==0 || $crop_height== 0){
			$('#uploaded_image_img_'+$key).Jcrop({
				onSelect: function(c){
									$('#'+$key+'_x1').val(c.x);
									$('#'+$key+'_y1').val(c.y);
									$('#'+$key+'_x2').val(c.x2);
									$('#'+$key+'_y2').val(c.y2);
									$('#'+$key+'_w').val(c.w);
									$('#'+$key+'_h').val(c.h);
									$('#'+$key+'_image_type').val(image_type);
								},
				minSize: [$crop_width,$crop_height],
				setSelect:   [ 0, 0,  $crop_width, $crop_height ],
			});
		}else{
			$('#uploaded_image_img_'+$key).Jcrop({
				aspectRatio:   $crop_width  / $crop_height,
				onSelect: function(c){
									$('#'+$key+'_x1').val(c.x);
									$('#'+$key+'_y1').val(c.y);
									$('#'+$key+'_x2').val(c.x2);
									$('#'+$key+'_y2').val(c.y2);
									$('#'+$key+'_w').val(c.w);
									$('#'+$key+'_h').val(c.h);
									$('#'+$key+'_image_type').val(image_type);
								},
				minSize: [$crop_width,$crop_height],
				setSelect:   [ 0, 0,  $crop_width, $crop_height ],
			});
		}
		$("#crop_button_"+$key).click(function() {
			data_up ={ 'x1':$('#'+$key+'_x1').attr('value'),
					   'x2':$('#'+$key+'_x2').attr('value'),
					   'y1':$('#'+$key+'_y1').attr('value'),
					   'y2':$('#'+$key+'_y2').attr('value'),
					   'w':$('#'+$key+'_w').attr('value'),
					   'h':$('#'+$key+'_h').attr('value'),
					   'image_type':$('#'+$key+'_image_type').attr('value'),
					   'random':$('#'+$key+'_random').attr('value'),
					   'post':1
					   };
			data_up[$key+'_crop_image']=1;
			$.ajax({
				  url: 				document.location.href,
				  dataType: 		'json',
				  type:				'POST',
				  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
				  data:    			data_up,
				  success: 			function(data) {
					  					$('#crop_button_'+$key).hide();
										$('#uploaded_image_'+$key).hide();
										$('#croped_image_'+$key).show();
										
										$('#croped_image_img_'+$key).attr('src',"/"+$directory+$key+"_"+$user_id+"_cropped."+image_type?image_type:'jpg'+"?"+$rand);
										$('#'+$key+'_save_croped').attr('value',1);
									}
			});
	});
	}

}