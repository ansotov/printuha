<section class="blog">
  <div class="banner"><img src="/img/girlsinbloom/13.jpg"></div>
  <ul>
  {foreach $blog as $k=>$v}<li {if $v@iteration is div by 3}class="third"{/if}><a href="./{$v.url}/"><img src="/img/blog/{$v.preview}.{$v.preview_type}" title="{$v.name}"><h2>{$v.name}</h2><p>{$v.description}</p><div class="read_more">{translateConst const = READ_MORE}</div></a>
  </li>{/foreach}
  </ul>
</section>
