<?php
class My_Image_lib extends CI_Image_lib {
	
	var $image_library		= 'imagemagick';	// Can be:  imagemagick, netpbm, gd, gd2
	var $library_path		= '/usr/bin/';
	var $quality			= '100';
	
	function __construct(){
		parent::__construct();
	}
	
}
?>