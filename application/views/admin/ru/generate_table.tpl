<script src="/js/admin.js"></script>
<script src="/js/admin2.js"></script>
{include file="admin/ru/common_forms.tpl"}
<input type="hidden" name="page" id="page" value="{$page}" />

<input type="hidden" name="order" id="order" value="{$order}" />


{if $title_name}<h2>{$title_name}</h2>{/if}

<div class="action_panel">
    <div style="width: 960px">
        <div>
            <ul>
                <li>
                    <div class="total_name" style="font-weight: bold">Всего: <span class="total"> </span></div>
                </li>
                <li>
                    <a href="/{$dir}/{$links.create.link}/0/{$end_link}">Создать</a>
                </li>
                {foreach from=$links_before item=v key=k}
                    <li>
                        <a href="{$k}">{$v}</a>
                    </li>
                {/foreach}
            </ul>
        </div>
    </div>

</div>

<!--
<table width="960" class="search_panel"  cellpadding="2" cellspacing="6">
{foreach from=$links_before item=v key=k}
<tr><td colspan="2"><a href="{$k}">{$v}</a></td></tr>
{/foreach}
</table>
-->

{if $filter_array}
    <div class="action_panel filter-block">
        <ul>
        {foreach from=$filter_array item=v key=k}
            {if $v.type=='date'}
            <script>
            $().ready(function(){
              $('#filter_{$k}').datepicker({
                        lang:"ru",
                        firstDay:1,
                        dateFormat: "dd.mm.yy"
                    });
            });
            </script>
            {/if}
            {if $v.type=='datetime'}
            <script>
            $().ready(function(){
              $('#filter_{$k}').datetimepicker({
                        lang:"ru",
                        firstDay:1,
                        dateFormat: "dd.mm.yy hh:mm"
                    });
            });
            </script>
            {/if}

            <li>
                {$v.name}:
                {if $v.type=='model_list' || $v.type == 'list'}
                <select name="ajax_filter_{$k}" class="filter"  id="filter_{$j}">
                <option value=""></option>
                {foreach $v.values as $k2=>$v2}
                <option value="{$k2}">{$v2}</option>
                {/foreach}
                </select>
                {else}
                <input type="text" name="ajax_filter_{$k}" id="filter_{$k}" class="filter" value="" />
                {/if}
            </li>
            {/foreach}
            <li>
                <input class="button" type="button" onclick="get_filters();" value="искать"/>
            </li>
        </ul>
    </div>
{/if}

<table class="admin_table" width="960" cellpadding="0" cellspacing="0">
<thead>
    <tr>
      <td width="40" class="numberList">ID</td>
      {foreach from=$select_names key=k item=v}
      <td {if $v.width} width="{$v.width}"{/if}>{$v.name}<input type="hidden" class="structure_array" name="{$k}" value="{$v.name}" /></td>
      {/foreach}
      <td width="30">Действия</td>
    </tr>
</thead>
<tbody id="table_body">
</tbody>
</table>

<table width="960" class="search_panel"  cellpadding="0" cellspacing="0">
<tr>
	<td width="200" >Показать по <select name="show_per_page" onchange="change_per_page($(this).val())">
    		{foreach $show_per_page_array item=v key=k}
            	<option  {if $k==$show_per_page || ($k=='all' && $show_per_page == 0)}selected{/if} value="{$k}" >{$v}</option>
            {/foreach}
            </select>
    </td>
    <td class="pages">
    <div class="leftTxt">Pages: </div>
    {foreach from=$pages_info.pages_array item=i}
                            {if $i == '...'}
                                <div class="normal">...</div>
                            {else}
                                {if $i <> $page}
                                 <div class="normal"><a onclick="to_page({$i});">{$i}</a></div>
                                {else}
                                   <div class="selected">{$page}</div>
                                {/if}
                            {/if}
    {/foreach}

    {if $pages_info.back_page}
        <div class="normal"><a  onclick="to_page({$pages_info.back_page});" ><</a></div>
    {/if}
    {if $pages_info.foward_page}
        <div class="normal"><a   onclick="to_page({$pages_info.back_page});"  >></a></div>
    {/if}
    </td>
</tr>
</table>
