<script src="/js/admin.js"></script>
<script src="/js/admin2.js"></script>
{include file="admin/ru/common_forms.tpl"}

{if $title_name}<h2>{$title_name}</h2>{/if}

<form id="main_form" action="/emails-data" method="post">
	<input type="hidden" name="save_on_change" id="save_on_change" value="0"/>
	<input type="hidden" name="save_changes" value="1"/>
	<input type="hidden" name="post" value="1">

	<div class="filters">
		<div class="item">
			<input type="text" class="datepicker" name="filter[date_from]">
		</div>
		<div class="item">
			<input type="text" class="datepicker" name="filter[date_to]">
		</div>
		<div class="actions">
			<input class="button" type="submit" value="Скачать">
		</div>
	</div>
</form>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            lang: "ru",
            firstDay: 1,
            dateFormat: "yy-mm-dd"
        });
    });
</script>