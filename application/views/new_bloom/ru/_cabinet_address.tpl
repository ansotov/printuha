<form action="" method="post">

  <p id="bcity" class="field">
    <strong><label for="city">{translateConst const = CITY}</label></strong>
    <input type="text" value="{$postdata.city}" size="30" name="city" id="order_bill_address_attributes_city" class="required">
    {if $errors.city}<label class="error">{$errors.city}</label>{/if}
  </p>

  <p id="bstreet" class="field">
    <strong><label for="street">{translateConst const = STREET}</label></strong>
    <input type="text" value="{$postdata.street}" size="30" name="street" id="order_bill_address_attributes_street" class="required">
    {if $errors.street}<label class="error">{$errors.street}</label>{/if}
  </p>

  <p id="bhouse" class="field">
    <strong><label for="house">{translateConst const = HOUSE}</label></strong>
    <input type="text" value="{$postdata.house}" size="30" name="house" id="order_bill_address_attributes_house" class="required">
    {if $errors.house}<label class="error">{$errors.house}</label>{/if}
  </p>

  <p id="bflat" class="field">
    <strong><label for="flat">{translateConst const = FLAT}</label></strong>
    <input type="text" value="{$postdata.flat}" size="30" name="flat" id="order_bill_address_attributes_flat" class="required">
    {if $errors.flat}<label class="error">{$errors.flat}</label>{/if}
  </p>

  <p id="bzipcode" class="field">
    <strong><label for="zipcode">{translateConst const = INDEX}</label></strong>
    <input type="text" value="{$postdata.zipcode}" size="30" name="zipcode" id="order_bill_address_attributes_zipcode" class="required">
    {if $errors.zipcode}<label class="error">{$errors.zipcode}</label>{/if}
  </p>
  <p class="field"><span class="area" style="overflow:visible;"><button class="button">{translateConst const = SAVE}</button></span></p>
</form>
