<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$folder_name}{if $title_name}| {$title_name}{/if}</title>
<link href="/css/rainadmin.css" rel="stylesheet" type="text/css" />
<script src="/js/jquery-1.7.1.min.js"></script>
<script src="/js/jquery-ui-1.8.9.custom.min.js"></script>
<script src="/js/jquery.synctranslit.min.js"></script>
<script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<script src="/js/ajaxupload.js"></script>
<link href="/css/custom-theme/jquery-ui-1.8.9.custom.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
<script type="text/javascript" src="/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tinymce/jscripts/tiny_mce/tiny_mce.js',
			// General options
			theme : "advanced",
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			relative_urls : false,

			content_css : "/css/neon_admin.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			},

        	plugins : "paste,media",
			paste_use_dialog : true,
			paste_convert_middot_lists : false,
			paste_unindented_list_class : "unindentedList",
			paste_convert_headers_to_strong : true,
			paste_strip_class_attributes:"all",
			paste_remove_spans:true,
			paste_remove_styles:true,
			invalid_elements: "span",
			extended_valid_elements : "div[*],table,td,tr,thead,tbody,iframe[src|width|height|name|align],object[classid|codebase|width|height|align|type|data],param[id|name|type|value|valuetype<DATA?OBJECT?REF]"
		});

		$("a.fancybox").fancybox({
			 'width' : '90%',
			 'height' : '90%',
			 'autoScale' : false,
			 'transitionIn' : 'none',
			 'transitionOut' : 'none',
			 'type' : 'iframe'
     	});
	});
function toggle_prop($sw){
	if( $sw ==1 ){
		$('.large_slider').show();
		$('.slider').hide();
	}else{
		$('.large_slider').hide();
		$('.slider').show();
	}
}
</script>
<script type="text/javascript" src="/js/swfupload.js"></script>
<script type="text/javascript" src="/js/jquery.swfupload.js"></script>
<link rel="stylesheet" href="/css/jquery.Jcrop.css" type="text/css" media="screen" />

<script type="text/javascript"src="/js/jquery.Jcrop.min.js"></script>


</head>
<body>
<div class="main_part">{if $load_page!=''}{include file="$load_page.tpl"}{/if}</div>
</body>
</html>
