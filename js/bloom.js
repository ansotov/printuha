function signup_form() {
    $('#signup_form').submit(function (event) {
        val = $("#signup_form").serializeArray()
        val.push({"name": "ajax", "value": 1})
        val.push({"name": "post_form", "value": "signup"})
        $.ajax({
            url: '/signup/',
            dataType: 'json',
            type: 'POST',
            contentType: "application/x-www-form-urlencoded; charset=windows-1251",
            data: val,
            success: function (data) {
                $("#veil_stuff").html(data.template)
                signup_form()
            }
        })
        return false;
    })
    $(".close").click(function (e) {
        $("#veil_stuff, .veil").removeClass("selected")
        e.stopPropagation()
    })
    $("#veil_stuff .signup_form").click(function (e) {
        e.stopPropagation()
    })
    $("#header-envelope > a.show_window").click(function (e) {
        $("#veil_stuff").addClass("selected");
        $('html').removeClass('overflow-none').addClass('overflow-hidden');
        e.stopPropagation()
        return false;
    })

    $(".cart_button_reminder").click(function (e) {
            $("#reminder").addClass("selected");

            var data = $(this).html();

            $('body').append('<div id="temp">' + data + '</div>');

            var size_id = $('#temp input').val();
            $('#temp').remove();

            $('input[name="size_id"]').val(size_id);

            $('html').removeClass('overflow-none').addClass('overflow-hidden');
            e.stopPropagation();
            return false;
        }
    );

    /**
     * Заказ в 1 клик
     */
    $('.one-click-button').on('click', function (e) {
        $('#one_click').addClass('selected');

        var comment = $('textarea.designComment').val();

        if (comment) {
            $('form input[name="comment"]').val(comment);
        }
    });

    //ansotov обработка заказа в 1 клик
    $('article ul.materials').on('click', function (e) {

        if ($('li').hasClass('selected')) {

            var material = $.trim($('.materials li.selected').text());

            if (material) {
                $('form input[name="material"]').val(material);
            }

        }
    });

    //ansotov обработка заказа в 1 клик
    $('article ul.sizes').on('click', function (e) {

        if ($('li').hasClass('selected')) {

            var size = $.trim($('.sizes li.selected').text());

            if (size) {
                $('form input[name="size_id"]').val(size);
            }

        }
    });

    $("#veil_stuff, #reminder, #one_click, .delete-button").click(function (e) {
        $(this).removeClass("selected");
        $('html').removeClass('overflow-hidden').addClass('overflow-none');
        e.stopPropagation()
    });

    $(".new_product .continue").click(function (e) {
        e.stopPropagation()
        alert('test')
        $(".new_product").removeClass("selected");
        return false;
    })
}

$(function () {

    if ($('.materials li').hasClass('selected')) {

        var material = $('.materials li.selected input').attr('data-material');

        if (material)
            $('form input[name="material"]').val(material);

    }

    $(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.menu-block').addClass("sticky");
        }
        else {
            $('.menu-block').removeClass("sticky");
        }
    });

    $('#basket-info, #cart-wrapper').hover(function () {
            $('.new_product').removeClass('selected');
            $('#cart-wrapper').stop().fadeIn(400);
        },
        function () {
            $('#cart-wrapper').stop().fadeOut(400);
        });

    signup_form()
    $(".fa-bars").click(function (e) {
        $(".mobile-menu").addClass('selected')
        return false;
    })
    $(".filter-button").click(function (e) {
        if ($("#filter_form").hasClass('selected')) {
            $("#filter_form").removeClass('selected');
        } else {
            $("#filter_form").addClass('selected');
            $('.filter-block.isset-mobile').addClass('active');
        }
        return false;
    })
    $('.top-menu-wrap a i').click(function (e) {
        $('aside.filter, .footer, .mobile, article').hide();
    });
    $(".close").click(function (e) {
        $(".mobile-menu").removeClass('selected');
        $('.footer, article').show();
        return false;
    })
    $(".selected").elevateZoom();
    $(".fancybox").fancybox();
    var sudoSlider = $(".banner .slider").sudoSlider({
        prevNext: false,
        numeric: false,
        auto: true,
        continuous: true,
        effect: "slide"
    });
    $(document).bind("contextmenu", function (e) {

        return false;

    });

    $(document).keydown(function (e) {
        if (e.which == 39) { // Arrow right
            sudoSlider.goToSlide("next");
        }
        else if (e.which == 37) { // Arrow left.
            sudoSlider.goToSlide("prev");
        }
    });
    $(".top-menu li").hover(
        /*function(){
          $(this).find(".wrap").stop().fadeIn()
        }
        ,
        function(){
          $(this).find(".wrap").stop().fadeOut()
        }*/
    );
    $("aside h2").click(function (e) {
        if ($(this).hasClass('selected')) {
            $(this).parent().find('ul').removeClass('selected')
            $(this).removeClass('selected')
        } else {
            $(this).parent().find('ul').addClass('selected')
            $(this).addClass('selected')
        }
    });
    $(".show_larger").click(function (e) {
        $.fancybox.update()
        $.fancybox([
            {href: $(".thumnails .selected").find('img').attr('src').replace('small2', 'largest')}, {autoHeight: true}
        ]);
    });

    $(".thumnails li").click(function (e) {
        $(".view li").removeClass("selected")
        id = $(this).attr("id").replace('t_', '')
        $(".view #v_" + id).addClass('selected')
        $(".selected").elevateZoom();
        $(".show_larger").click(function (e) {
            $.fancybox.update()
            $.fancybox([
                {href: $(".thumnails #t_" + id).find('img').attr('src').replace('small2', 'largest')}, {autoHeight: true}
            ]);
        });
    });
    $('img').bind('contextmenu', function (e) {
        return false;
    });

});

function increase_p(rowid, delivery_type, foreign_lands) {
    $.ajax({
        url: document.location.href,
        dataType: 'json',
        type: 'POST',
        contentType: "application/x-www-form-urlencoded; charset=windows-1251",
        data: {
            'increase_p': 1,
            'delivery_type': delivery_type,
            'rowid': rowid,
            'ajax': 1,
            'foreign_lands': foreign_lands
        },
        success: function (data) {
            show_cart(data.template, data.template_cart_informer, data.template_cart_in_total, data.template_promocode)
        }
    })
}

//ansotov обновляем данные полей доставки
function change_delivery(delivery_type) {
    var name_arr = $('#search_city_text_hidden').val();

    if (!name_arr) {
        name_arr = 'Москва - Московская обл.';
    }
    var result = name_arr.split(' - ');

    $.ajax({
        url: '/delivery-types',
        dataType: 'json',
        type: 'POST',
        data: {'delivery_type': delivery_type, 'name': result[0], 'region': result[1]},
        success: function (data) {
            showOrderForm(data.template, data.template_in_total);
        }
    })
}

//ansotov обновляем данные полей доставки для pickpoint
function change_pickpoint_form(info) {

    $.ajax({
        url: '/pickpoint-form',
        dataType: 'json',
        type: 'POST',
        data: {'pickpoint_info': info},
        success: function (data) {
            showOrderForm(data.template, data.template_in_total);
        }
    })
}

//ansotov показывает типы доставки для России
function delivery_types(city, courier_city, pickpoint) {

    $.ajax({
        url: '/all-delivery-types',
        dataType: 'json',
        type: 'POST',
        data: {'russian_post': city, 'courier_city': courier_city, 'pickpoint': pickpoint},
        success: function (data) {
            showDeliveryTypes(data.template, data.template_in_total);
            $('input#courier-radio').click();
        }
    })
}

function change_delivery_types(type) {

    $('input[name="foreign_lands"]').val(type);

    $.ajax({
        url: '/foreign-all-delivery-types',
        dataType: 'json',
        type: 'POST',
        data: {'type': type},
        success: function (data) {
            showForeignDeliveryTypes(data.template, data.template_order_form, data.template_in_total, type);

            if (type == 0) {
                setDefaultDelivery();
                $('#search_city_text').attr('required', 'required');
            } else if (type == 1) {
                $('#search_city_text').removeAttr('required');
            }
        }
    })
}


function setDefaultDelivery() {

    var city = {
        days: "3",
        delivery_price: 230,
        label: "Москва - Московская обл.",
        name: "Москва",
        price: 230,
        region: "Московская обл.",
        value: "Москва - Московская обл.",
        zone: "42"
    };

    $('#search_city_text_hidden').val('Москва - Московская обл.');

    $.ajax({
        url: '/all-types',
        type: 'POST',
        data: {
            name: 'Москва', // поисковая фраза
            region: 'Московская обл.' // поисковая фраза
        },
        success: function (data) {

            var data_arr = $.parseJSON(data);

            delivery_types(city, data_arr.couriers, data_arr.pickpoint);
        }
    });
}

function decrease_p(rowid, delivery_type, foreign_lands) {
    $.ajax({
        url: document.location.href,
        dataType: 'json',
        type: 'POST',
        contentType: "application/x-www-form-urlencoded; charset=windows-1251",
        data: {
            'decrease_p': 1,
            'delivery_type': delivery_type,
            'rowid': rowid,
            'ajax': 1,
            'foreign_lands': foreign_lands
        },
        success: function (data) {
            show_cart(data.template, data.template_cart_informer, data.template_cart_in_total, data.template_promocode)
        }
    })
}

function remove_p(rowid, delivery_type) {
    $.ajax({
        url: document.location.href,
        dataType: 'json',
        type: 'POST',
        contentType: "application/x-www-form-urlencoded; charset=windows-1251",
        data: {'remove_p': 1, 'delivery': delivery_type, 'rowid': rowid, 'ajax': 1},
        success: function (data) {
            location.reload();
        }
    })
}

function notice(message, red) {
    $("body").prepend('<div ' + (red == true ? 'class="red"' : '') + ' id="notice_ms">' + message + '</div>')
    var winH = $(window).height()
    var winW = $(window).width();
    $('#notice_ms').css('top', (winH - $("#notice_ms").height()) / 2 + $(window).scrollTop());
    $('#notice_ms').css('left', (winW - $("#notice_ms").width()) / 2);

    $('#notice_ms').delay(1000).animate({
        opacity: 0
    }, 2000, function () {
        $(this).remove();
    });
}

function add_to_cart(key) {

    var size = '';
    var material = ($('#sel-mat').hasClass('select_material')) ? '' : 1;

    $(".size").each(function (index, element) {
        if ($(this).is(":checked")) {
            size = $(this).val();
        }
    });

    $(".material").each(function (index, element) {
        if ($(this).is(":checked")) {
            material = $(this).val();
        }
    });


    qty = $(".qty").val()
    $(".qty option").each(function (index, element) {
        if ($(this).is(":selected")) {
            qty = $(this).val()
        }
    })

    if (size == '') {
        $(".sizes .select_size").addClass('selected').delay(4000).queue(function (next) {
            $(this).removeClass('selected')
        })
    }

    if (material == '') {
        $(".materials .select_material").addClass('selected').delay(4000).queue(function (next) {
            $(this).removeClass('selected')
        })
    }

    if (size != '' && material != '') {
        val = {'add_to_cart_m': 1, 'key': key, 'ajax': 1, 'size': size, 'qty': qty}
        material = $("#material").val()
        if (material != '') {
            val['material'] = material
        }

        var color = $("#color").val();
        if (color != '') {
            val['color'] = color;
        }

        $.ajax({
            url: document.location.href,
            dataType: 'json',
            type: 'POST',
            contentType: "application/x-www-form-urlencoded; charset=windows-1251",
            data: val,
            success: function (data) {
                if (data.can_add == 'yes') {
                    fbq('track', 'AddToCart');
                    $('#header-basket').removeClass('disabled');
                    
                    window.scrollTo(0, 0);
                    $(".new_product").html(data.template)
                    $("#cart-wrapper").html(data.cart_template)
                    $(".new_product").addClass('selected')
                    setTimeout(function () {
                        $(".new_product").removeClass("selected");
                    }, 4000);
                    //notice('Товар добавлен в корзину!',false)
                    $("#total_sum").html(data.total_sum + ' руб')
                    $("#basket-items-count").html(data.total_number)
                } else {
                    notice('Нельзя добавить большее количество товаров данного типа', true)
                }
            }
        })
    }
}

function addToCard(url) {

    if (url) {

        var val = {'add_to_cart_m': 1, 'key': 1, 'ajax': 1, 'size': 'onesize', 'qty': 1}

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            contentType: "application/x-www-form-urlencoded; charset=windows-1251",
            data: val,
            success: function (data) {
                if (data.can_add == 'yes') {
                    yaCounter37587550.reachGoal('polozhitKorzina');

                    show_cart(data.c_template, data.template_cart_informer, data.template_cart_in_total, data.template_promocode);

                    $("#basket-items-count").html(data.total_number);
                } else {
                    notice('Нельзя добавить большее количество товаров данного типа', true)
                }
            }
        })
    }
}

function show_cart(template, template_cart_informer, template_cart_in_total, template_promocode) {
    $("#update-cart").html(template);
    $("#cart-informer").html(template_cart_informer);
    $("#cart-in-total").html(template_cart_in_total);
    $("#promocode").html(template_promocode);
}

//ansotov получение и обновление способов доставки
function showDeliveryTypes(template, template_in_total) {
    $("#delivery-types").html(template);
    $("#cart-in-total").html(template_in_total);
    //ansotov показываем типы доставки
    $('#delivery-types').fadeIn(500);
    //ansotov скрываем поля отправки
    $('#order-info').hide();
}

//ansotov получение и обновление способов доставки не для России
function showForeignDeliveryTypes(template, template_order_form, template_in_total, type) {

    $('.lands-preloder').fadeIn(0);
    $("#delivery-types").html(template);
    $("#order-form").html(template_order_form);
    $("#cart-in-total").html(template_in_total);
    //ansotov если это другая страна
    if (type == 1) {

        //$('#delivery-types').css('margin', '-12px 0 0 0');
        $('#delivery-types, #order-info, #order-form, #search_city').fadeOut(0);

        $('#search_city').fadeOut(0, (function () {
            $('#free-delivery-info').css("visibility", "hidden");
            $('#delivery-types').addClass('mobile');
            $('#search_city_text').val('');
            $('.lands-preloder').fadeOut(500, (function () {
                $('#delivery-types, #order-info, #order-form').fadeIn(500);
            }));
        }));

    } else if (type == 0) {
        $('#delivery-types, #order-info, #order-form, #search_city').fadeOut(0, (function () {
            $('#delivery-types').removeClass('mobile');
            $('.lands-preloder').fadeOut(500, (function () {
                $('#search_city').fadeIn(500);
                $('#free-delivery-info').css("visibility", "visible");
            }));
        }));
    }
}

function showOrderForm(template, template_in_total) {
    $("#order-form").html(template);
    $("#cart-in-total").html(template_in_total);
}

/***
 number - исходное число
 decimals - количество знаков после разделителя
 dec_point - символ разделителя
 thousands_sep - разделитель тысячных
 ***/
function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k)
                .toFixed(prec);
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '')
            .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
            .join('0');
    }
    return s.join(dec);
}

/**
 * Смена языка
 *
 * @author ansotov
 * @param lang
 */
function changeLang(lang) {
    $.cookie('language', lang, {path: '/'});
    location.reload();
}

//ansotov онлайн оплата
function onlinePayment(type) {

    var delivery_type = $('input[name="delivery_type"]:checked').val();
    var name_arr = $('#search_city_text_hidden').val();
    if (!name_arr) {
        name_arr = 'Москва - Московская обл.';
    }
    var result = name_arr.split(' - ');

    var pickpointCity = $('#pickpoint_city').val();
    var pickpointRegion = $('#pickpoint_region').val();

    console.log({'delivery_type': delivery_type, 'name': result[0], 'region': result[1], 'onlinePayment': type});

    $.ajax({
        url: '/delivery-types',
        dataType: 'json',
        type: 'POST',
        data: {'delivery_type': delivery_type, 'name': result[0], 'pickpointCity': pickpointCity, 'region': result[1], 'pickpointRegion': pickpointRegion, 'onlinePayment': type},
        success: function (data) {
            $("#cart-in-total").html(data.template_in_total);
        }
    })
}