<section class="cabinet clearfix">
  <aside>
    <nav>
      <ul>
        <li><a href="/cabinet/personal/">{translateConst const = PROFILE_DATA}</a></li>
        <li><a href="/cabinet/addresses/">{translateConst const = MY_DATA}</a></li>
        <li><a href="/cabinet/orders/">{translateConst const = MY_BYES}</a></li>
        <li><a href="/cabinet/feedback/">{translateConst const = FEEDBACK}</a></li>
        <li><a href="/logout/">{translateConst const = LOGOUT}</a></li>
      </ul>
    </nav>
  </aside>
  <article>
    <h1 class="cursive"><div>{translateConst const = PROFILE}</div></h1>
    {include file=$include_cabinet}
  </article>
</section>
