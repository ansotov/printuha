{if $product.name}
	<article class="product_page clearfix">
		{include file="new_bloom/ru/info_block.tpl"}
		<!--{if $folder_info.top_banner_text!=''}<div class="top_banner" style="margin: -5px 0 20px 0">{$folder_info.top_banner_text}</div>{/if}-->
		<div class="top_banner" style="margin-top: -5px">{include file="new_bloom/ru/top_banner.tpl"}</div>
		<div class="left">
			<div class="image">
				<div class="mobile">
					<div id="slides" class="slides">
						{foreach $thumbnails as $k=>$v}<img
							src="/img/products_pictures/{$v.picture}_large.{$v.picture_type}">{/foreach}
					</div>
				</div>
				<!--<ul class="mobile">{foreach $thumbnails as $k=>$v}<li data-id="{$k}"  id="picture_mobile_{$k}"
          class="zoom{if $k==0} selected{/if}"><img src="/img/products_pictures/{$v.picture}_large.{$v.picture_type}"></li>{/foreach}</ul>-->
				<ul class="desktop">
					{foreach $thumbnails as $k=>$v}
						<li data-id="{$k}" id="picture_desktop_{$k}" class="{if $k==0} selected{/if}">
							<img alt="{$product.name|@escape}" class="product zoom-image" style="cursor: pointer"
								 src="/img/products_pictures/{$v.picture}_largest.{$v.picture_type}">
						</li>
					{/foreach}
				</ul>
			</div>
			<div class="previews_part">
				<div class="previews">
					<ul>
						{foreach $thumbnails as $k=>$v}
						<li data-id="{$k}" {if $k==0}class="selected"{/if}>
							<a target="_blank" href="/img/products_pictures/{$v.picture}_large.{$v.picture_type}"><img
										alt="{$product.name|@escape}"
										src="/img/products_pictures/{$v.picture}_large.{$v.picture_type}"></a>
							</li>{/foreach}
					</ul>
				</div>
			</div>
		</div>
		<div class="right">
			<h1>{$product.name}</h1>
			<div class="properties">
				{$product.properties}
			</div>
			<div class="price">
				{if $product.old_price}
					<span class="old-price"><del>{$product.old_price|number_format:0:".":" "} <i
									class="fa fa-ruble"></i></del></span>
				{/if}
				<span id="price_val">{$product.price|number_format:0:".":" "}</span> <i class="fa fa-ruble"></i>
			</div>

			{if $product.not_available == 0}
				<ul class="product-info">
					<li>
						<h2>{translateConst const = SIZE}</h2>
						<ul class="sizes select">
							{foreach $sizes as $k=>$v}
								<li{if $v.active == false} class="cart_button_reminder not-active-size"{else}{if $sizes|count == 1} class="selected"{/if}{/if}
										data-price="{if $v.price >0}{$v.price|number_format:0:".":" "}{else}{$product.price|number_format:0:".":" "}{/if}"
										data-size="{$v.code}">
									{if $v.active == 1}
										<div class="radio">
											<input data-size="{$v.code}" type="radio" name="size" class="size"
												   value="{$v.code}">
										</div>
									{else}
										<input type="hidden" value="{$v.id}">
									{/if}
									{if $v.name!=''}{$v.name}{else}{$v.code}{/if}
								</li>
							{/foreach}
							{if $sizes|count > 1}
								<li class="select_size">{translateConst const = CHOICE_SIZE}</li>
							{/if}
						</ul>
						{if $sizes|@count > 4}
							<div style="height: 50px"></div>
						{/if}
					</li>


					{if $last_size}
						<li class="last_size">
							<div class="grid-row low-in-stock" style="margin: 20px 0 0 0">
								<img src="/img/last_size_clock.png" height="auto" width="18">
								<span data-bind="text:lowInStockText">{translateConst const = GOODS_QTY_LIMITED}</span>
							</div>
						</li>
					{/if}
					{if $product.colors}
						<li style="margin: 20px 0 20px 0">
							<h2>{translateConst const = COLOR}</h2>

							<ul class="select materials" style="max-width: 390px">
								{foreach $product.colors as $k=>$v}
									<li title="{$v}"
										data-order="{$v@index}"
										data-item="{$v@index}" value="{$v}"{if $product.colors|@count == 1} class="selected"{/if}>
										<div class="radio">
											<input data-material="{$v}" data-order="{$k}" type="radio" name="color" class="material" value="{$v}">
											<span>{$v|@escape}</span>
										</div>
									</li>
								{/foreach}
								<li class="select_size select_material">{translateConst const = CHOICE_COLOR}</li>
							</ul>

						</li>
					{elseif $group_products}
						<li style="margin: 20px 0 0 0">
							<h2>{translateConst const = COLOR}</h2>
							<div class="colors">
								{if $group_products}
									{foreach $group_products as $k=>$v}
										<div class="color_picture"><a href="/new-products/{$v.url}"><img
														alt="{$product.name|@escape}" title="{$v.color}"
														src="/img/color_pictures/{$v.color_picture}.{$v.color_picture_type}"></a>
										</div>
									{/foreach}
								{else}
									{if $product.color_picture!=''}
										<div class="color_picture"><img alt="{$product.name|@escape}"
																		title="{$product.color}"
																		src="/img/color_pictures/{$product.color_picture}.{$product.color_picture_type}">
										</div>
									{else}
										{$product.color}
									{/if}
								{/if}
							</div>
						</li>
						{if $product.colors|count > 1}
									<li class="select_size select_material">{translateConst const = CHOICE_COLOR}</li>
								{/if}
					{/if}
					<li style="margin: 15px 0 0 0">
						<div>
							<h2>{translateConst const = QTY}</h2>
							<select class="qty" name="qty">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
							<div class="select_qty">{translateConst const = CHOICE}<br>{translateConst const = QTY}
							</div>
						</div>
					</li>
					{if $product.not_available==0}
						<li style="text-align: right" class="in-cart in-cart-products">
							<button class="cart_button"
									style="min-width: 150px">{translateConst const = IN_CARD}</button>
						</li>
						<li style="text-align: right">
							<button class="one-click-button"
									style="min-width: 150px">{translateConst const = ONE_CLICK}</button>
						</li>
					{/if}
				</ul>
			{else}
				<div class="info_block" style="display: inline-block;z-index: 1;">
					<button class="cart_button_reminder cases-in-cart">{translateConst const = SAY_ABOUT_NEW}</button>
				</div>
			{/if}

			{if $product.type == 'tolstovki' or $product.type == 'futbolki'}
				<h2 data-id="sizes" class="header selected" style="margin-bottom: 5px;text-align: left">Обмеры</h2>
				<div data-id="sizes" class="desc sizes-img selected">
					{if $product.sizes_img}
						<img alt="{$product.name|@escape}"
							 src="/img/front_pictures/{$product.sizes_img}.{$product.sizes_img_type}">
					{else}
						{if $product.type == 'tolstovki'}
							<img alt="{$product.name|@escape}" src="/img/girlsinbloom/sweatshirts-sizes.jpg">
						{elseif $product.type == 'futbolki'}
							<img alt="{$product.name|@escape}" src="/img/girlsinbloom/t-short-sizes.jpg">
						{/if}
					{/if}

				</div>
			{elseif $product.sizes_img}
				<h2 data-id="sizes" class="header selected" style="margin-bottom: 5px;text-align: left">Обмеры</h2>
				<div data-id="sizes" class="desc sizes-img selected">
					<img alt="{$product.name|@escape}"
						 src="/img/front_pictures/{$product.sizes_img}.{$product.sizes_img_type}">
				</div>
			{/if}

			<div class="info_block">
				{if $product.sku}
					<div class="name">{translateConst const = VENDOR}:</div>
					<div class="value">{$product.sku}</div>
				{/if}
			</div>

			<h2 data-id="desc" class="header"
				style="margin-bottom: 5px;text-align: left">{translateConst const = DESCRIPTION}</h2>
			<div data-id="desc" class="desc">
				{$product.content}
			</div>

			{if $product.model_params}
				<h2 data-id="model_params" class="header" style="margin-bottom: 5px;text-align: left">Параметры
					модели</h2>
				<div data-id="model_params" class="desc">
					{$product.model_params}
				</div>
			{/if}

			{if $product.care}
				<h2 data-id="care" class="header"
					style="margin-bottom: 5px;text-align: left">{translateConst const = ITEM_CARE}</h2>
				<div data-id="care" class="desc">
					{$product.care}
				</div>
			{/if}

			{if $product.type != 'boxes'}
				<h2 data-id="delivery" class="header"
					style="margin-top: 20px">{translateConst const = CONDITIONS_FOR_DELIVERY_AND_FREE_REFUND_TITLE}</h2>
				<div data-id="delivery" class="desc">
					<strong>{translateConst const = TERMS_OF_DELIVERY_TITLE}</strong>
					<p>{translateConst const = TERMS_OF_DELIVERY_TEXT}</p>
					<p><a href="http://printuha.ru/delivery/">{translateConst const = MORE}</a></p>

					<strong>{translateConst const = RETURN_CONDITIONS_TITLE}</strong>
					<p>{translateConst const = RETURN_CONDITIONS_TEXT}</p>
					<p><a href="http://printuha.ru/return-polisy/">{translateConst const = MORE}</a></p>
				</div>
			{/if}

			{if $product.type=='gotovye-kejsy' || $product.type=='kejs-na-zakaz'}
				{if $group_products}
					<h2>{translateConst const = IMAGE}</h2>
					<ul class="select pictures">
						{foreach $group_products as $k=>$v}
							<li {if $v.id==$product.id}class="selected"{/if}><a
										href="/new-products/{$v.url}">{$v.group_variant}</a></li>
						{/foreach}
					</ul>
				{/if}
				<h2>{translateConst const = MODEL}</h2>
				{if $product.not_available==1}
					{translateConst const = EXPECTED_RECEIPT}
				{else}
					<ul class="sizes select">
						{foreach $sizes as $k=>$v}
							<li data-price="{if $v.price >0}{$v.price|number_format:0:".":" "}{else}{$product.price|number_format:0:".":" "}{/if}">
								<div class="radio"><input data-size="{$v.code}" type="radio" class="size" name="size"
														  value="{$v.code}">
								</div>{if $v.name!=''}{$v.name}{else}{$v.code}{/if}</li>
						{/foreach}
						<li class="select_size">{translateConst const = CHOICE}<br>{translateConst const = MODEL}</li>
					</ul>
				{/if}
				<h2>{translateConst const = MATERIAL}</h2>
				<ul class="materials select">
					{foreach $materials as $k=>$v}
						<li data-id="{$v.id}" data-order="{$v@index}">
							<div class="radio"><input data-material="{$v.id}" type="radio" name="material"
													  class="material"
													  value="{$v.id}"></div> {$v.name}</li>
					{/foreach}
					<li id="sel-mat" class="select_size select_material"
						style="width: 70px;">{translateConst const = CHOICE}<br>{translateConst const = MATERIAL}</li>
				</ul>
				<input type="hidden" name="material" id="material" value="">
				<h2>{translateConst const = QTY}</h2>
				<select class="qty" name="qty">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
				</select>
				{if $product.not_available==0}
					<button class="cart_button">{translateConst const = IN_CARD}</button>
				{else}
					<br>
					<br>
				{/if}
				<div class="info_block">
					<div class="name">{translateConst const = VENDOR}</div>
					<div class="value">{$product.sku}</div>
				</div>
			{/if}
		</div>
		<!--<div class="description-details clearfix">
    <h2>Описание</h2>
    {$product.content}
  </div>-->

		<input type="hidden" name="color" id="color" value="">

		{if $other_products}
			<section class="you-also-viewed products">
			<div class="name">{translateConst const = RECENTLY_VIEWED}</div>
			<ul>
				{foreach $other_products as $k=>$v}
					<li class="">
					<a href="/new-products/{$v.url}/">
						<img alt="{$v.name|@escape}" src="/img/front_pictures/{$v.front_picture}.jpg" class="front">
					</a>
					<div class="info">
						<h3>{$v.name}</h3>
						<div class="lower">
							<div class="price">{$v.price|number_format:0:".":" "}
								<div class="fa fa-rub"></div>
							</div>
						</div>
					</div>
					</li>{/foreach}
			</ul>
			</section>{/if}
		<div class="veil_white">
			{foreach $thumbnails as $k=>$v}<img alt="{$v.name|@escape}" class="fullsize_image"
												src="/img/products_pictures/{$v.picture}_largest.{$v.picture_type}"
												id="fullsize_{$k}">{/foreach}
		</div>
	</article>
	<script>
        $("#slides").slidesjs({
            width: 50,
            height: 70
        });
        $("h2.header").click(function (e) {
            id = $(this).data('id')
            $("h2.header").removeClass('selected')
            $(this).addClass('selected')
            $(".right .desc").removeClass('selected')
            $(".desc[data-id='" + id + "']").addClass('selected')
        })
        $(".image > ul > li").click(function (e) {
            id = $(this).data('id')
            //console.log('picture_id = '+id)
            //$(".veil_white").addClass('selected')
            //$("#fullsize_"+id).addClass('selected')
        })
        $(".veil_white").click(function (e) {
            $('.fullsize_image').removeClass('selected')
            $(this).removeClass('selected')
        })
        $("select").selectBoxIt();
        $(".cart_button").click(function () {
            add_to_cart(1)
        })
        $(".sizes li").click(function (e) {
            $(".sizes li").removeClass('selected');
            $(this).addClass('selected');
            $(this).find('.size').attr('checked', true);
            val = $(this).data('price');

            $("#price_val").html(val);
        })

        $(".materials li").click(function (e) {
            $(".materials li").removeClass('selected');
            $(this).addClass('selected');
            $(this).find('.material').attr('checked', true);
            val = $(this).data('price');

            $("#price_val").html(val);
        })
		{if $sizes|@count==1}
        $(".sizes li").click()
		{/if}
		
		$(".materials li:first").click();
		var id = $('.materials li:first').data('id');
		$("#material").val(id);

		var color = $('.materials li:first').attr('value');
		$("#color").val(color);

        //$(".product_page .image .desktop .selected").elevateZoom();
        $(".materials li").click(function () {
            $(".materials li").removeClass('selected');
            $(this).addClass('selected');
            var id = $(this).data('id');
            $("#material").val(id);

            var color = $(this).attr('value');
            $("#color").val(color);

            var order = $(this).data('order');

            $(".previews li[data-id='" + order + "']").find('a').click();
        });

        $(".previews li a").click(function () {
            $(".previews li").removeClass('selected')
            $(this).parent().addClass('selected')
            $(".image li").removeClass("selected")
            li = $(this).parent()
            id = $(li).data('id')
            $("#picture_desktop_" + id).addClass('selected')
            $("#picture_mobile_" + id).addClass('selected')
            //$(".product_page .image  .desktop .selected").elevateZoom();
            return false;
        })
	</script>
{/if}