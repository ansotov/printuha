<?php

include('moysklad_routine_library.php');

$apiSettings = getSettings();
$setup_curl = setupCurl($apiSettings);

$attributes = setCurl(
    $setup_curl,
    $apiSettings[MOYSKLAD_API_URL] . $apiSettings[MOYSKLAD_GET_METADATA],
    $apiSettings[MOYSKLAD_GET_METADATA_METHOD]);

$attr_data = getAttributes($attributes);

$name = 'Андрей';
$phone = '+7 (926) 464-92-28';
$address = 'Мой адрес';

$attr_arr = array();
foreach ($attr_data as $key => $value) {
    $attr_arr[$key] = $value;

    switch ($value->name) {
        case 'Имя':
            $attr_arr[$key]->value = $name;
            break;
        case 'Телефон':
            $attr_arr[$key]->value = $phone;
            break;
        case 'Адрес':
            $attr_arr[$key]->value = $address;
            break;
    }
}

$products = array(
    array(
        'quantity' => 2,
        'price' => 105100,
        'discount' => 0,
        'assortment' => array(
            'meta' => array(
                'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/product/a325f4cb-0d40-11e7-7a69-93a7007f128d',
                'type' => 'product',
                'mediaType' => 'application/json'
            )
        )
    ), array(
        'quantity' => 4,
        'price' => 39000,
        'discount' => 0,
        'assortment' => array(
            'meta' => array(
                'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/product/a317d353-0d40-11e7-7a69-93a7007f1254',
                'type' => 'product',
                'mediaType' => 'application/json'
            )
        )
    ), array(
        "quantity" => 1,
        "price" => 48000,
        "discount" => 0,
        "assortment" => array(
            "meta" => array(
                "href" => "https://online.moysklad.ru/api/remap/1.1/entity/product/a308e017-0d40-11e7-7a69-93a7007f1211",
                "type" => "product",
                "mediaType" => "application/json"
            )
        )
    )
);
$prducts_arr = array();
foreach ($products as $k => $v) {
    $prducts_arr[$k] = $v;
}

echo '<pre>';
    print_r(json_encode($prducts_arr));
    die();

$textAddCustomerOrder = '
{
  "name": "00315565686814",
  "organization": {
    "meta": {
      "href": "https://online.moysklad.ru/api/remap/1.1/entity/organization/9b11e588-0984-11e7-7a34-5acf0027a2a4",
      "type": "organization",
      "mediaType": "application/json"
    }
  },
  "code": "Номер заказа",
  "moment": "' . date('Y-m-d H:i:s') . '",
  "applicable": false,
  "vatEnabled": false,
  "agent": {
    "meta": {
      "href": "https://online.moysklad.ru/api/remap/1.1/entity/counterparty/9b13aa74-0984-11e7-7a34-5acf0027a2a7",
      "type": "counterparty",
      "mediaType": "application/json"
    }
  },
  "positions": ' . json_encode($prducts_arr) . ',
  "attributes": 
        ' . json_encode($attr_arr). '
}
';

$order = setCurl(
    $setup_curl,
    $apiSettings[MOYSKLAD_API_URL] . $apiSettings[MOYSKLAD_ADD_CUSTOMER_ORDER],
    $apiSettings[MOYSKLAD_ADD_CUSTOMER_ORDER_METHOD]);

curl_setopt($order, CURLOPT_POSTFIELDS, $textAddCustomerOrder);
curl_setopt($order, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($textAddCustomerOrder))
);

$customerOrderId = setCustomerOrder($order);
echo $customerOrderId;