<form method="post">

    {if $success}<label class="success" style="color:#fff;background: #000;font-family: tahoma; margin: 0 0 20px 0">{translateConst const = SUCCESS_SAVE}</label>{/if}
  <input type="hidden" name="send" value="1">
  <p id="bemail" class="field">
    <strong><label for="email">Email</label></strong>
    <input{if $postdata.email} readonly="readonly"{/if} type="text" value="{$postdata.email}" size="30" name="email" id="order_bill_address_attributes_email" class="required">
    {if $errors.email}<label class="error">{$errors.email}</label>{/if}
  </p>

  <p id="bfirst_name" class="field">
    <strong><label for="first_name">{translateConst const = NAME}</label></strong>
    <input type="text" value="{$postdata.first_name}" size="30" name="first_name" id="order_bill_address_attributes_first_name" class="required">
    {if $errors.first_name}<label class="error">{$errors.first_name}</label>{/if}
  </p>

  <p id="blast_name" class="field">
    <strong><label for="last_name">{translateConst const = SURNAME}</label></strong>
    <input type="text" value="{$postdata.last_name}" size="30" name="last_name" id="order_bill_address_attributes_last_name" class="required">
    {if $errors.last_name}<label class="error">{$errors.last_name}</label>{/if}
  </p>
  <p id="blast_name" class="field">
    <strong><label for="last_name">{translateConst const = GENDER}</label></strong>
    <select class="gender" name="gender">
      <option value=""></option>
      <option {if $postdata.gender=='female'}selected="selected"{/if} value="female">{translateConst const = FEMALE}</option>
      <option {if $postdata.gender=='male'}selected="selected"{/if} value="male">{translateConst const = MALE}</option>
    </select>
    {if $errors.gender}<label class="error">{$errors.gender}</label>{/if}
  </p>

  <p id="bbirthdate" class="field sp_field">
    <strong><label for="birthdate">{translateConst const = BIRTHDATE}</label></strong>
    <input type="date" value="{$postdata.birthdate}" size="30" name="birthdate" id="order_bill_address_attributes_birthdate" class="required">
    {if $errors.birthdate}<label class="error">{$errors.birthdate}</label>{/if}
  </p>
  <p id="bphone" class="field sp_field">
  <strong><label for="order_bill_address_attributes_phone">{translateConst const = PHONE}</label></strong>
  <input type="text" value="{$postdata.phone}" size="30" name="phone" id="phone" class="required">
  {if $errors.phone}<label class="error">{$errors.phone}</label>{/if}
  </p>
  <p class="field">
    <span class="area"><input type="checkbox" name="subscribe" value="1" {if $postdata.subscribe==1}checked="checked"{/if}> {translateConst const = NEWSLETTERS_ACCEPT}</span>
  </p>
  <p class="field"><span class="area" style="overflow:visible;"><button class="button">{translateConst const = SAVE}</button></span></p>
</form>
{literal}
    <script type="text/javascript">
        //ansotov маска для номера телефона
        $.mask.definitions['X'] = '[01234569]';
        $('#phone').mask('+7 (X99) 999-99-99');
    </script>
{/literal}