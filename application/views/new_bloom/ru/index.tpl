<!doctype html>
<html>

<head>
	<title>{if $title}{$title|@strip_tags|regex_replace:"/\s+/":" "|escape:'quotes'|trim} - {/if}{$main_page.title}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1, shrink-to-fit=yes">
	<meta property="og:image" content="/img/girlsinbloom/ogg-image.png"/>
	<meta name="yandex-verification" content="2a755b5d0500f6df" />
	<meta name="description" content="{if $description}{$description|@strip_tags|regex_replace:"/\s+/":" "|escape:'quotes'|trim}{else}{$main_page.description}{/if}">
	<meta name="facebook-domain-verification" content="49ze0amw48s75lny0itg9wpawoiixm" />
	<link href="/css/girlsinbloom.css" media="screen" rel="stylesheet" type="text/css"/>
	<link href="/css/slider.css" media="screen" rel="stylesheet" type="text/css"/>
	<link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="/css/jquery.selectBoxIt.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="/js/fancybox/jquery.fancybox.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="/css/jquery-ui.css" type="text/css" media="screen"/>
	<script src="/js/jquery-1.9.1.min.js"></script>
	<script src="/js/jquery.maskedinput.js"></script>
	<script src="/js/jquery.form.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.js"></script>

	<script src="/js/jquery.sudoSlider.min.js"></script>
	<script src="/js/jquery.slides.min.js"></script>
	<script src="/js/jquery.elevateZoom-3.0.8.min.js"></script>
	<script src="/js/jquery.selectBoxIt.js"></script>
	<script src="/js/bloom.js?233452343231"></script>
	<script src="/js/jquery.scrollUp.js"></script>
	<script src="/js/product-page.min.js"></script>
	<script src="/js/fancybox/jquery.fancybox.js"></script>
	<script type="text/javascript" src="/js/newWaterfall.js"></script>
	<script type="text/javascript" src="/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="/js/jquery.ui.datepicker-ru.js"></script>

	<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-223936-5NKA5';</script>

	<script>
        $(function () {

            $(".datepicker").datepicker({
                    changeYear: true,
                    changeMonth: true,
                    yearRange: '-70:+0',
                    maxDate: 0
                }
            );

            $.datepicker.regional["ru"];

            $('img.product.zoom-image').imageCursorZoom();
            $('#info-wall').NewWaterfall();
            //ansotov портфолио
            $('.portfolio').on('click', function (event) {

                // ansotov останавливаем переход по ссылке
                event.preventDefault();

                $.ajax({
                    cache: false,
                    url: this.href,
                    type: 'post',
                    success: function (data) {
                        var newBody = $.parseJSON(data);
                        var url = '/img/portfolio/' + newBody[0].image + '.' + newBody[0].image_type;
                        $.fancybox(
                            {
                                href: url,
                                title: newBody[0].description,
                            }
                        )
                    }

                });
            });

            //ansotov портфолио
            $('.info-page').on('click', function (event) {

                // ansotov останавливаем переход по ссылке
                event.preventDefault();

                $.ajax({
                    cache: false,
                    url: this.href,
                    type: 'post',
                    success: function (data) {
                        var newBody = $.parseJSON(data);

                        $.fancybox(newBody)
                    }

                });
            });

            //ansotov СМИ
            $('.smi').on('click', function (event) {

                // ansotov останавливаем переход по ссылке
                event.preventDefault();

                $.ajax({
                    cache: false,
                    url: this.href,
                    type: 'post',
                    success: function (data) {
                        var newBody = $.parseJSON(data);
                        var url = '/img/smi/' + newBody[0].image + '.' + newBody[0].image_type;
                        $.fancybox(
                            {
                                href: url,
                                title: newBody[0].description,
                            }
                        )
                    }

                });
            });

        });
	</script>

	<script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-49030894-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();


        $(function () {
            $.scrollUp({
                scrollName: 'scrollUp', //  ID элемента
                topDistance: '300', // расстояние после которого появится кнопка (px)
                topSpeed: 300, // скорость переноса (миллисекунды)
                animation: 'slide', // вид анимации: fade, slide, none
                animationInSpeed: 200, // скорость разгона анимации (миллисекунды)
                animationOutSpeed: 200, // скорость торможения анимации (миллисекунды)
                scrollText: '^' // текст
            });
        });

	</script>
	{if $smarty.server.HTTP_HOST == 'printuha.ru' || $smarty.server.HTTP_HOST == 'www.printuha.ru' || $smarty.server.HTTP_HOST == 'printuha.com' || $smarty.server.HTTP_HOST == 'www.printuha.com'}
	{literal}
		<!-- Tik Tok -->
		<script>
            !function (w, d, t) {
                w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};

                ttq.load('C8HNBRGB3BVP1792IT9G');
                ttq.page();
            }(window, document, 'ttq');
		</script>

		<!-- VK -->
		<!-- Rating Mail.ru counter -->
		<script type="text/javascript">
            var _tmr = window._tmr || (window._tmr = []);
            _tmr.push({id: "3239626", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID"});
            (function (d, w, id) {
                if (d.getElementById(id)) return;
                var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
                ts.src = "https://top-fwz1.mail.ru/js/code.js";
                var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
                if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
            })(document, window, "topmailru-code");
		</script><noscript><div>
			<img src="https://top-fwz1.mail.ru/counter?id=3239626;js=na" style="border:0;position:absolute;left:-9999px;" alt="Top.Mail.Ru" />
		</div></noscript>
		<!-- //Rating Mail.ru counter -->

		<!-- Rating@Mail.ru counter dynamic remarketing appendix -->
		<script type="text/javascript">
            var _tmr = _tmr || [];
            _tmr.push({
                type: 'itemView',
                productid: 'VALUE',
                pagetype: 'VALUE',
                list: 'VALUE',
                totalvalue: 'VALUE'
            });
		</script>
		<!-- // Rating@Mail.ru counter dynamic remarketing appendix -->

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-T2XKKJ5');</script>
		<!-- End Google Tag Manager -->

		<!-- Facebook Pixel Code -->
		<!--<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1030992797057548');
			fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
					   src="https://www.facebook.com/tr?id=1030992797057548&ev=PageView&noscript=1"
			/></noscript>
		 End Facebook Pixel Code -->

		<!-- Facebook Pixel Code -->
		<script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '1001229060298766');
            fbq('track', 'PageView');
		</script>
		<noscript>
			<img height="1" width="1"
				 src="https://www.facebook.com/tr?id=1001229060298766&ev=PageView&noscript=1"/>
		</noscript>
		<!-- End Facebook Pixel Code -->

		<!-- Facebook Pixel Code -->
		<script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '208433076475733');
            fbq('track', 'PageView');
		</script>
		<noscript>
			<img
					height="1" width="1" style="display:none"
					src="https://www.facebook.com/tr?id=208433076475733&ev=PageView&noscript=1"/>
		</noscript>
		<!-- End Facebook Pixel Code -->
	{/literal}
	{/if}

</head>
<body>

{if $smarty.server.HTTP_HOST == 'printuha.ru' || $smarty.server.HTTP_HOST == 'www.printuha.ru' || $smarty.server.HTTP_HOST == 'printuha.com' || $smarty.server.HTTP_HOST == 'www.printuha.com'}

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T2XKKJ5"
					  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter49456186 = new Ya.Metrika2({
                        id: 49456186,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/tag.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks2");
	</script>
	<noscript>
		<div>
			<img src="https://mc.yandex.ru/watch/49456186" style="position:absolute; left:-9999px;" alt=""/>
		</div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

{/if}

{include file="new_bloom/ru/head.tpl"}
{include file=$page}
{include file="new_bloom/ru/foot.tpl"}
<div id="veil_stuff" class="veil">
	{include file="new_bloom/ru/_signup_form.tpl"}
</div>


<div id="reminder" class="veil">
	{include file="new_bloom/ru/reminder.tpl"}
</div>

<div id="one_click" class="veil">
	{include file="new_bloom/ru/one_click_order.tpl"}
</div>

</body>

</html>
