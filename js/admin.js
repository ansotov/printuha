function a_photos($k,$folder_name){
	new AjaxUpload('#'+$k+'_'+$folder_name+'_button', {
		action:  ' ',			
		data: {
			'ajax_photo_upload' : $k,
			'post' : 1,
			'folder_name' : $folder_name
		},
		name: $k+'_'+$folder_name,
		onSubmit : function(file, ext){		
			$('#'+$k+'_'+$folder_name+'_button').html('<img src="/img/loader.gif">');
			
			this.disable();
		},

		onComplete : function(file){
			$('#'+$k+'_'+$folder_name+'_button').text('��������� ����');
			show_temp_photo($k,$folder_name);
			a_photos($k,$folder_name);
		}	
	});		
}

function show_temp_photo($k,$folder_name){
	$.ajax({
		  url: 				" ",
		  dataType: 		'xml',
		  data:				{
								'post' : 1,
			  					'ajax_photo_show':$k,
								'folder_name':$folder_name
							},
		  type:				'POST',
		  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
		  beforeSend:		function(){
							},
		  success: 			function(xml) {
								$('#'+$k+'_'+$folder_name+'_frame').html("<img src=\"/"+$("value",xml).text()+"\" border=0 >");
							}
	});
}

function map_control(dir,x_c,y_c){
	x_coor="#"+x_c;
	y_coor="#"+y_c;
	hide_map();
	dot=$("#dot_"+dir);
	dot.css('margin-left',($(x_coor).val()-2)+'px');
   	dot.css('margin-top',($(y_coor).val()-2)+'px');
	$("#map_"+dir).click(function(ev) {
		var x = ev.pageX - this.offsetLeft;
		var y = ev.pageY - this.offsetTop;
   		dot.css('margin-left',(x-2)+'px');
   		dot.css('margin-top',(y-2)+'px');
		$(x_coor).val(x);
		$(y_coor).val(y);
		
	});
	$('#map_'+dir).mousemove(function(e){
        var x = e.pageX - this.offsetLeft;
        var y = e.pageY - this.offsetTop;
        $('#coors_'+dir).html("X: " + x + " Y: " + y); 
    });

}
function show_map(dir){
	$("#map_"+dir).show();
	$("#show_map_"+dir).css("color","green");
	$("#hide_map_"+dir).css("color","#FF0000");
}
function hide_map(dir){
	$("#map_"+dir).hide();
	$("#show_map_"+dir).css("color","#FF0000");
	$("#hide_map_"+dir).css("color","green");
}
function auto_complete(obj,model,field){
	word=$(obj).val();
	var m_id = $(obj).attr('id');
	if(word.length>=1){
		$.ajax({
			  url: " ",
			  dataType: 'xml',
			  type:'post',
			  contentType: 		"application/x-www-form-urlencoded; charset=windows-1251",
			  data:{
				  	"ajax_data_post":model,
					"get_field":field,
					"like":word
			  		},
			  beforeSend:function(){
				
			  },
			  success: function(xml) {
				$("#"+m_id+"_sh").empty();
				$("data",xml).each(function(id){
						data = $("data",xml).get(id);
						$("#"+m_id+"_sh").append("<div onclick=\"set_obj_v('"+m_id+"','"+$("field",data).text()+"');\" class=\"drop\">"+$("field",data).text()+"</div>");
				});
			  }
			});
	}else{
		$("#"+m_id+"_sh").empty();
	}
}
function set_obj_v(id,vl){
	$("#"+id).val(vl);
	$("#"+id+"_sh").empty();
}
function fav_action(val){
	/*
	if($("#fav"+val).attr("checked")){
		alert('gaga');
	}else{
		alert('dada');
	}*/
	$("#fav").val(val);
	$("#fav_form").submit();
}
function del(val){
	if(confirm("Delete?")){
		$("#delete").val(val);
		$("#delete_form").submit();
	}
}
function del_something(something){
	if(confirm("Delete "+something+"?")){
		$("#d"+something).submit();
	}
}
function del_photo(val){
	if(confirm("Delete photo?")){
		$("#p").val(val);
		$("#fp").submit();
	}
}
function del_file(val){
	if(confirm("Delete file?")){
		$("#f").val(val);
		$("#ff").submit();
	}
}
function move_up(val){
	$("#move_up").val(val);
	$("#mp").submit();
}
function move_down(val){
	$("#move_down").val(val);
	$("#mp").submit();
}