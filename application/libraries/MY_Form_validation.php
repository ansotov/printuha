<?
class MY_Form_validation extends CI_Form_validation {
	function __construct(){
		parent::__construct();
	}
	function get_field_data(){
		$data=array();
		foreach($this->_field_data as $k=>$v){
			$data[$k]=array('field'=>$v['field'],'label'=>$v['label'],'error'=>$v['error']);
		}
		return $data;
	}
	function get_errors(){
		$errors_array=array();
		foreach($this->_field_data as $k=>$v)if ($v['error']!=''){
			$errors_array[$k]=$v['error'];
		}
		return $errors_array;
	}
	function get_postdata(){
		$postdata_array=array();
		foreach($this->_field_data as $k=>$v)if ($v['postdata']!=''){
			$postdata_array[$k]=$v['postdata'];
		}
		return $postdata_array;
	}
	function alpha($str){
		return ( ! preg_match("/^([a-z�-��-�])+$/i", $str)) ? FALSE : TRUE;
	}
	function latin($str){
		return ( ! preg_match("/^([a-z])+$/i", $str)) ? FALSE : TRUE;
	}
	function alpha_space($str){
		return ( ! preg_match("/^([a-z�-��-� ])+$/i", $str)) ? FALSE : TRUE;
	}
	function numeric_space($str){
		return ( ! preg_match("/^([0-9 ])+$/i", $str)) ? FALSE : TRUE;
	}
	function alpha_numeric($str)
	{
		return ( ! preg_match("/^([a-z�-��-�0-9])+$/i", $str)) ? FALSE : TRUE;
	}
	function latin_numeric($str){
		return ( ! preg_match("/^([a-z0-9])+$/i", $str)) ? FALSE : TRUE;
	}
	function alpha_dash($str)
	{
		return ( ! preg_match("/^([-a-z�-��-�0-9_-])+$/i", $str)) ? FALSE : TRUE;
	}	
	function alpha_dotdash($str)
	{
		return ( ! preg_match("/^([-a-z�-��-� !.,0-9_-])+$/i", $str)) ? FALSE : TRUE;
	}
	function numeric_dashspace($str)
	{
		return ( ! preg_match("/^([0-9() .,-])+$/i", $str)) ? FALSE : TRUE;
	}	
	function checkbox($str){
		return ($str==1) ? TRUE:FALSE;
	}
	function alpha_dashspace($str)
	{
		return ( ! preg_match("/^([a-z�-��-�0-9 -])+$/i", $str)) ? FALSE : TRUE;
	}
	function file_required(){
		if(isset($_FILES[$this->selected_field])){
			$file_error=$_FILES[$this->selected_field]['error'];
		}else
			$file_error=4;
		if ($file_error > 0){
			$this->error_variant=$file_error;
			return FALSE;
		}else
			return TRUE;
		//return ($_FILES[$this->selected_field]['name']=='') ? FALSE : TRUE;
			
	}
	function get_all_data(){
		return $this->_field_data;
	}
	function reset_all_data(){
		$this->_field_data=array();
	}
}
?>