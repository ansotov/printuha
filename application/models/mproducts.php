<?php
include_once APPPATH . 'models/mmodel.php';

class mproducts extends mmodel
{
	public function __construct()
	{
		parent::__construct();
		$this->table = "products";
		$this->id    = "id";
	}

	public function get_designer_types($designer)
	{
		$query = " SELECT p.type `url`, t.name FROM products p 
				  INNER JOIN types t ON t.url = p.type 
				  INNER JOIN folders f ON f.url = t.url AND f.type = 'products'
				  INNER JOIN menu m ON m.folder_id = f.id WHERE p.designer = '$designer' AND p.active = 1 GROUP BY p.type ORDER BY m.order_id ASC";
		$r     = $this->db->query($query);
		if ($r->num_rows() >= 1)
		{
			return $r->result_array();
		}
		else
			return false;

	}

	public function get_designer_subtypes($designer, $type_url)
	{
		$query = "SELECT p.subtype `url`, st.name FROM products p
				  INNER JOIN subtypes st ON p.subtype = st.url
				  WHERE p.type ='$type_url' AND p.designer = '$designer' AND p.active =1 ORDER BY st.order_id ASC";
		$r     = $this->db->query($query);
		if ($r->num_rows() >= 1)
		{
			return $r->result_array();
		}
		else
			return false;

	}

	public function get_with_orders($where_data = array(), $select_data = array(), $number = 0, $order_by = '')
	{
		if ($order_by != '')
			$this->db->order_by($order_by);
		$select_data != array() ? $this->db->select($select_data) : $this->db->select('*');
		$this->db->join('types_orders', 'types_orders.type = products.type AND types_orders.season = products.season AND types_orders.collection = products.collection', 'left');
		$r = $this->db->get_where($this->table, $where_data);
		if ($r->num_rows() > 0)
		{
			if ($number == 1)
			{
				$rr = $r->result_array();

				return $rr[0];
			}
			else
			{
				return $r->result_array();
			}
		}
		else
			return false;
	}

	public function decrease_size_qty($product_id, $code, $qty)
	{
		$this->table       = "products_sizes";
		$product_size_info = $this->get(array("product_id" => $product_id, 'code' => $code), array('on_hand'), 1);
		$on_order          = 0;
		$on_hand           = $product_size_info['on_hand'];
		if ($qty >= $on_hand)
		{
			$on_hand  = 1;
			$on_order = 1;
		}
		else
		{
			$on_hand = $on_hand - $qty;
		}
		$this->update(array('on_hand' => $on_hand, 'on_order' => $on_order), array('product_id' => $product_id, 'code' => $code));
		$this->table = 'products';
	}

	public function update($data, $where_data)
	{
		parent:: update($data, $where_data);
		$d = $this->get($where_data, '*');
		if (isset($d[0]['front_picture']) && isset($d[0]['back_picture']) && $d[0]['front_picture'] == '' && $d[0]['back_picture'] == '')
		{
			$this->fill_from_gallery($d[0]['id']);
		}
		if (!empty($d) && count($d) == 1)
		{
			return $d[0]['id'];
		}
		else
			return false;

	}

	private function gtranslate($str, $lang_from, $lang_to)
	{
		$query_data = array(
			'client' => 'x',
			'q'      => $str,
			'sl'     => $lang_from,
			'tl'     => $lang_to
		);
		$filename   = 'http://translate.google.ru/translate_a/t';
		$options    = array(
			'http' => array(
				'user_agent' => 'Mozilla/5.0 (Windows NT 6.0; rv:26.0) Gecko/20100101 Firefox/26.0',
				'method'     => 'POST',
				'header'     => 'Content-type: application/x-www-form-urlencoded',
				'content'    => http_build_query($query_data)
			)
		);
		$context    = stream_context_create($options);
		$response   = file_get_contents($filename, false, $context);

		return json_decode($response);
	}

	private function translitURL($str)
	{
		$tr = array(
			"А"  => "a", "Б" => "b", "В" => "v", "Г" => "g",
			"Д"  => "d", "Е" => "e", "Ё" => "e", "Ж" => "j", "З" => "z", "И" => "i",
			"Й"  => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
			"О"  => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
			"У"  => "u", "Ф" => "f", "Х" => "h", "Ц" => "ts", "Ч" => "ch",
			"Ш"  => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "yi", "Ь" => "",
			"Э"  => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
			"в"  => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "e", "ж" => "j",
			"з"  => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
			"м"  => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
			"с"  => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
			"ц"  => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
			"ы"  => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
			" -" => "", "," => "", " " => "-", "." => "", "/" => "_",
			"-"  => ""
		);

		return strtr($str, $tr);
	}

	private function fill_from_gallery($product_id)
	{
		$query = "SELECT * FROM photos WHERE parent_id = " . $product_id . " ORDER BY order_id ASC LIMIT 0,2 ";
		$r     = $this->db->query($query);
		if ($r->num_rows() >= 1)
		{
			$photos = $r->result_array();
			$query  = "UPDATE `products` SET ";
			foreach ($photos as $k => $v)
			{
				if ($k == 0)
				{
					$query .= " `front_picture` = '" . $v['picture'] . "' , `front_picture_type` = '" . $v['picture_type'] . "' ";
					copy($this->config->item('core_web') . "/img/products_pictures/" . $v['picture'] . '_product.' . $v['picture_type'], $this->config->item('core_web') . "/img/front_pictures/" . $v['picture'] . '.' . $v['picture_type']);
					copy($this->config->item('core_web') . "/img/products_pictures/" . $v['picture'] . '_small.' . $v['picture_type'], $this->config->item('core_web') . "/img/front_pictures/" . $v['picture'] . '_small.' . $v['picture_type']);
				}
				if ($k == 1)
				{
					$query .= " ,`back_picture` = '" . $v['picture'] . "' , `back_picture_type` = '" . $v['picture_type'] . "' ";
					copy($this->config->item('core_web') . "/img/products_pictures/" . $v['picture'] . '_product.' . $v['picture_type'], $this->config->item('core_web') . "/img/back_pictures/" . $v['picture'] . '.' . $v['picture_type']);
					copy($this->config->item('core_web') . "/img/products_pictures/" . $v['picture'] . '_small.' . $v['picture_type'], $this->config->item('core_web') . "/img/back_pictures/" . $v['picture'] . '_small.' . $v['picture_type']);
				}
			}
			$query .= " WHERE  id =" . $product_id;
			$this->db->query($query);
		}
	}

	public function set_sizes($product_id, $sku, $code, $on_hand, $on_order)
	{
		$this->db->query("
			INSERT INTO `products_sizes` (`product_id`,`product_sku`, `code`,`on_hand`,`on_order`) values('$product_id','$sku', '$code', $on_hand,$on_order) on duplicate key update on_hand=values(on_hand), on_order = values(on_order),product_id = values(product_id)
		");

		return true;
	}

	public function update_data($sku)
	{
		$this->table = "products_sizes";
		if ($sizes = $this->get(array('product_sku' => $sku), array(), 0, 'CAST(code as DECIMAL) ASC'))
		{

			$sizes_string = '';
			$total        = 0;
			$active       = 1;
			$set_active   = 0;
			foreach ($sizes as $k => $v)
			{
				if ($v['on_hand'] <= 0)
					$sizes_string .= ('<span class="s_s">' . $v['code'] . '</span>');
				else
				{
					$sizes_string .= ('<span class="s_e">' . $v['code'] . '</span>');
					$set_active   = 1;
				}
				if ($k + 1 != count($sizes))
					$sizes_string .= ' - ';

				$total += (int) $v['on_hand'];
			}
			if ($set_active == 1)
			{
				$array = array('sizes' => $sizes_string, 'on_hand' => $total, 'active' => $active);
			}
			else
			{
				$array = array('sizes' => $sizes_string, 'on_hand' => $total);
			}
			$this->table = "products";
			$this->update($array, array('sku' => $sku));
		}
	}

	public function insert($data)
	{
		if (isset($data['position']) && intval($data['position']) == 0)
		{
			$data['position'] = 500;
		}
		if (isset($data['front_picture']) && isset($data['back_picture']))
		{

		}
		if (isset($data['name']) && isset($data['sku']))
		{
			$data['url'] = strtolower(url_title($this->gtranslate($data['name'], 'ru', 'en')));

			// If don`t get data from google translate, url in translit
			if (!$data['url'])
			{
				if (preg_match('/[^A-Za-z0-9_\-]/', $data['name']))
				{
					$urlstr      = $this->translitURL($data['name']);
					$data['url'] = strtolower(preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr));
				}
			}
		}

		$product_id = parent::insert($data);
		$this->fill_from_gallery($product_id);

		return $product_id;
	}

	public function delete($data)
	{
		parent::delete($data); // TODO: Change the autogenerated stub

		//ansotov ������� ��������� ����
		$query = "DELETE FROM `product_tags` WHERE `product_id` = " . $data['id'];
		$this->db->query($query);
	}
}
