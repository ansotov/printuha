<?php
ini_set('MAX_EXECUTION_TIME', -1);
include_once './application/controllers/main_common.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/integrations/retailcrm/app.php';

function fetchData($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	$result = curl_exec($ch);
	curl_close($ch);

	return $result;
}

class main extends main_common
{

	//ansotov общая сумма заказа
	public $promocode = array();
	private $total_amount = 0;
	private $selected_state_id = 0;

	//ansotov количество на страницу
	private $payment_type_id = 0;
	//ansotov текущая страница
	private $limit = 14;
	//ansotov с какого места показываем
	private $page = 0;

	//ansotov стоимость доставки курьрера по Москве
	private $offset = 0;
	private $moscow_delivery_price = 0;

	//ansotov стоимость доставки почтой России по Москве
	private $moscow_pickpoint_price = 260;
	private $moscow_post_price = 0;

	//ansotov сумма бесплатной доставки
	private $moscow_post_days = 3;
	private $free_delivery = 0;
	private $sector = 7129;
	//private $password = 'test';
	private $currency = 643;
	private $password = 'D9040vr5';

	// Retail Crm consts
	private $RCShop = 'printuha';
	private $RCUrl = 'https://girlsinbloom.retailcrm.ru';
	private $RCApiKey = 'Lp93bsE6HJUdeL8COXQoJR4hveirokPL';
	private $RCClient = false;

	//ansotov ключ для API
	private $api_key = 'YjdsqRkbexLkzFgb';

	//ansotov сумма вычета доставки
	private $minusSum = 10;


	public function __construct()
	{
		parent::__construct();

		$this->moscow_delivery_price = $this->settings['delivery_price'];
		$this->moscow_post_price     = $this->settings['delivery_price2'];

		$this->free_delivery = $this->settings['free_delivery'];

		$amount = $this->getDiscountInfo($this->getCartAmount());

		$this->total_amount = round(($this->getCartAmount() - ($this->getCartAmount() * ($amount->current_discount / 100))));

		$this->minusSum = ($this->total_amount < $this->free_delivery) ? 10 : 0;

		//$this->setFreeDelivery();
	}

	/**
	 * Скидки
	 *
	 * @param $amount
	 *
	 * @return mixed
	 * @author ansotov
	 *
	 */
	public function getDiscountInfo($amount)
	{

		$res                 = $this->db->query('SELECT MAX(`amount`) AS amount FROM front_discounts WHERE NOW() BETWEEN `start_at` AND `end_at` AND `active` = 1');
		$max_data            = $res->result();
		$max_discount_amount = $max_data[0];

		if ($amount >= $max_discount_amount->amount) {
			$result = $this->db->query("SELECT `discount` AS current_discount, 0 AS next_discount, 0 AS yet_need FROM `front_discounts` WHERE `amount` = (SELECT MAX(`amount`) FROM `front_discounts` WHERE  NOW() BETWEEN `start_at` AND `end_at`
                                      AND
                                      `active` = 1)");
		} else {
			$result = $this->db->query("SELECT
                                      discount as next_discount,
                                      (amount - " . $amount . ") as yet_need,
                                      (SELECT discount FROM front_discounts WHERE amount < " . $amount . " AND NOW() BETWEEN `start_at` AND `end_at` AND `active` = 1 ORDER BY amount DESC LIMIT 1) as current_discount
                                    FROM front_discounts
                                    WHERE
                                      amount > " . $amount . "
                                      AND
                                        NOW() BETWEEN `start_at` AND `end_at`
                                      AND
                                      `active` = 1
                                    ORDER BY amount ASC
                                    LIMIT 1");
		}

		$data = $result->result();

		return $data[0];
	}

	public function getCartAmount()
	{
		$cart         = $this->build_cart($this->cart->contents());
		$total_sum    = 0;
		$total_number = 0;
		if ($cart) {
			foreach ($cart as $k => $v) {
				$cart[$k]['price_sum'] = $v['price'] * $v['qty'];

				//ansotov бесплатный экземпляр по акции
				if ($v['in_stock']) {
					$cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
				}

				$total_sum    = $total_sum + $cart[$k]['price_sum'];
				$total_number = $total_number + $v['qty'];
			}
		}
		if ($this->session->userdata('promocode_discount') > 0) {
			if ($this->session->userdata('promocode_type') == 'percent') {
				$total_sum = round(($total_sum - ($total_sum * ($this->session->userdata('promocode_discount') / 100))));
			} else {
				$total_sum = round(($total_sum - $this->session->userdata('promocode_discount')));
			}
		}

		return $total_sum;
	}

	public function captcha_check()
	{
		// Then see if a captcha exists:
		$exp   = time() - 600;
		$sql   = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($this->input->post('captcha'), $this->input->ip_address(), $exp);
		$query = $this->db->query($sql, $binds);
		$row   = $query->row();

		if ($row->count == 0) {
			$this->form_validation->set_message('captcha_check', 'Не правильно введен код');

			return false;
		} else {
			return true;
		}

	}

	public function city_check($str)
	{
		if ($this->payment_type_id == 732546000 && ( ! $r = $this->mcities->get(array("`name` like" => $str, "state_id" => $this->selected_state_id), array(), 1))) {
			$this->form_validation->set_message('city_check', "Данный населенный пункт не поддерживает выбранный вид оплаты.");

			return false;
		} else {
			return true;
		}
	}

	public function email_check($str)
	{

		if ($r = $this->musers->get(array("`email` like" => $str, "active" => 1), array(), 1)) {
			$this->form_validation->set_message('email_check', "Уже существует %s со значением $str .");

			return false;
		} else {
			return true;
		}
	}

	public function email_check2($str)
	{

		if ($r = $this->musers->get(array("`email` like" => $str, "active" => 1, 'id !=' => $this->user_info['id']), array(), 1)) {
			$this->form_validation->set_message('email_check2', "Уже существует %s со значением $str .");

			return false;
		} else {
			return true;
		}
	}

	/**
	 * Заголовок доставки
	 *
	 * @param $type
	 *
	 * @return string
	 * @author ansotov
	 *
	 */
	public function getDeliveryTitle($type)
	{
		switch ($type) {
			case 1:
				$title = 'самовывоз';
				break;
			case 2:
				$title = 'курьер boxberry';
				break;
			case 3:
				$title = 'pickpoint';
				break;
			case 4:
				$title = 'почта России';
				break;
		}

		return $title;
	}

	/**
	 * Отправка sms
	 *
	 * @param        $sms_to
	 * @param        $message
	 * @param string $sms_from
	 *
	 * @author ansotov
	 *
	 */
	public function sendSms($sms_to, $message, $sms_from = 'PRINTUHA')
	{
		// Ваш ключ доступа к API (из Личного Кабинета)
		$api_key = "5qarfrceytnk7cjgt8ihb3doh84g6y55udnhcc6e";

		// Параметры сообщения
		// Если скрипт в кодировке UTF-8, не используйте iconv
		$sms_text = $message;

		// Создаём POST-запрос
		$POST = array(
			'api_key' => $api_key,
			'phone'   => $sms_to,
			'sender'  => $sms_from,
			'text'    => $sms_text
		);


		// Устанавливаем соединение
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $POST);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_URL,
			'https://api.unisender.com/ru/api/sendSms?format=json');
		$result = curl_exec($ch);


		if ($result) {

			// Раскодируем ответ API-сервера
			$jsonObj = json_decode($result);

			if (null === $jsonObj) {
				// Ошибка в полученном ответе
				echo "Invalid JSON";

			} elseif ( ! empty($jsonObj->error)) {
				// Ошибка отправки сообщения
				echo "An error occured: " . $jsonObj->error . "(code: " . $jsonObj->code . ")";

			} else {
				// Сообщение успешно отправлено
				echo "SMS message is sent. Message id " . $jsonObj->result->sms_id;
				echo "SMS cost is " . $jsonObj->result->price . " " . $jsonObj->result->currency;

			}
		} else {
			// Ошибка соединения с API-сервером
			echo "API access error";
		}
	}

	/**
	 * Delivery const
	 *
	 * @param $type
	 *
	 * @return string
	 */
	public function getDeliveryConst($type)
	{
		switch ($type) {
			case 1:
				$title = 'self-delivery';
				break;
			case 2:
				$title = 'courier';
				break;
			case 3:
				$title = 'pickpoint';
				break;
			case 4:
				$title = 'russian-post';
				break;
			case 5:
				$title = 'sdek';
				break;
		}

		return $title;
	}

	public function index($url1 = '', $url2 = "", $url3 = "", $url4 = '')
	{

		$type        = '';
		$folder_info = array();

		//ansotov авторизация через социальные сети
		if ($url1 == 'new-orders') {

			if ($this->input->get('api_key') == $this->api_key) {
				echo $this->getOrders();
			}

			exit;
		}

		//ansotov авторизация через социальные сети
		if ($url1 == 'social-auth') {
			$this->auth($url2);
			exit;
		}

		// RetailCRM catalog
		if ($url1 == 'retailcrm-catalog') {
			$this->YMLData();
			exit;
		}

		//ansotov авторизация через социальные сети
		if ($url1 == 'delete-large-files') {

			$files    = $this->myscandir($_SERVER['DOCUMENT_ROOT'] . '/img/products_pictures');
			$str_find = "_large";

			$result = false;

			foreach ($files as $k => $v) {
				if (strripos($v, $str_find) === false) {
					continue;
				} else {
					unlink($_SERVER['DOCUMENT_ROOT'] . '/img/products_pictures/' . $v);
				}
			}

			exit;
		}

		if ($url1 == 'moysklad-update-products') {
			$this->updateMoySkladIds();
			exit;
		}

		if ($url1 == 'emails-data') {
			$this->getEmailsFile();
			exit;
		}

		if ($url1 == 'remind-orders') {
			$this->remindOrders();
			exit;
		}

		if ($url1 == 'promocode') {
			$this->setPromocode($this->input->request('promocode'));
			exit;
		}

		if ($url1 == 'all-users') {
			$this->getAuthUsers();
			exit;
		}

		//ansotov заказ в 1 клик
		if ($url1 == 'one-click-order') {

			$this->load->model('morders');
			$this->load->model('morders_products');

			$size = ($this->input->request('size_id')) ? $this->input->request('size_id') : false;

			$and_where = ($size) ? ' AND (ps.code = "' . $size . '" OR ps.name = "' . $size . '")' : false;

			$product_query = $this->db->query('SELECT *, p.name AS name, ps.id AS size_id, IF(p.price, p.price, ps.price) AS price, ps.name AS ps_name
                                          FROM products p, products_sizes ps WHERE p.id = ps.product_id AND p.id = ' . (int)$this->input->request('product_id') . $and_where);

			$product = $product_query->row();

			$order_id = $this->morders->insert(array(
					'first_name' => $this->input->request('name'),
					'phone'      => $this->input->request('phone'),
					'sum'        => $product->price,
					'comments'   => $this->input->request('comment'),
					'email'      => 'Заказ в 1 клик',
					'order_type' => 1
				)
			);

			$order = $this->morders->get(array('id' => $order_id), array(), 1);

			$material = ($this->input->request('material')) ? $this->input->request('material') : '';
			$color    = ($this->input->request('color')) ? $this->input->request('color') : '';

			$this->morders_products->insert(array(
				'user_order_id' => $order_id,
				'name'          => $product->name,
				'size'          => $size,
				'product_id'    => (int)$this->input->request('product_id'),
				'price'         => $product->price,
				'qty'           => 1
			));

			$this->mysmarty->assign('data', array(
					'product_name' => $product->name,
					'image'        => $product->front_picture . '.' . $product->front_picture_type,
					'url'          => $product->url,
					'size'         => $size,
					'material'     => $material,
					'comment'      => $this->input->request('comment'),
					'user_name'    => $this->input->request('name'),
					'user_phone'   => $this->input->request('phone')
				)
			);

			$headline = "Заказ в 1 клик";
			$template = $this->mysmarty->fetch('new_bloom/ru/one_click_email.tpl');

			$orderData = array();

			// Order ID from shop
			$orderData['externalId'] = $order_id;

			// Order number
			$orderData['number'] = $order['number'];

			// Customer information
			$orderData['firstName']       = $this->input->post('name');
			$orderData['phone']           = $this->input->post('phone');
			$orderData['customerComment'] = $this->input->post('comment');

			// Products
			$orderData['items'][0]['offer']['externalId'] = $product->size_id;
			/*if ($size) {
		  $orderData['items'][0]['properties'][0]['name']  = 'size';
		  $orderData['items'][0]['properties'][0]['value'] = $size . (($material) ? ' - Материал:' . $material : false);
	  }*/
			$iData = 0;

			if ($color) {
				$orderData['items'][0]['properties'][$iData]['name']  = 'Цвет';
				$orderData['items'][0]['properties'][$iData]['value'] = $color;
				$iData++;
			}

			if ($material) {
				$orderData['items'][0]['properties'][$iData]['name']  = 'Материал';
				$orderData['items'][0]['properties'][$iData]['value'] = $material;
			}
			$orderData['items'][0]['quantity'] = 1;

			// Method
			$orderData['orderMethod'] = 'one-click';

			// Set order to Retail Crm
			// Set order to Retail Crm
			$result = $this->RCCreateOrder($orderData);

			// Send info about incorrect order
			if (isset($result) && $result->getStatusCode() != 201) {
				$this->mysmarty->assign('orderNumber', $orderData['number']);
				$template = $this->mysmarty->fetch('new_bloom/ru/failed_order_mail.tpl');

				$this->send_mail('my-gebbels@yandex.ru', 'my-gebbels@yandex.ru', 'Не прошел заказ', $template);
				$this->send_mail('info@printuha.ru', 'info@printuha.ru', 'Не прошел заказ', $template);
				$this->send_mail('darya@girlsinbloom.ru', 'darya@girlsinbloom.ru', 'Не прошел заказ', $template);
				mail('my-gebbels@yandex.ru', 'Не прошел заказ', 'Не прошел заказ №' . $orderData['number']);
			}

			exit();
		}

		if ($url1 == 'reminder') {

			$this->load->model('mreminder');

			if ($this->input->request('email')) {

				try {
					$this->mreminder->insert(
						array(
							'name'       => $this->input->request('name'),
							'email'      => $this->input->request('email'),
							'phone'      => $this->input->request('phone'),
							'product_id' => (int)$this->input->request('product_id'),
							'size_id'    => (int)$this->input->request('size_id')
						)
					);

					echo json_encode(array('success' => true));
				} catch (Exception $e) {
					echo json_encode(array('success' => $e));
				}


			} elseif ($this->input->request('id')) {

				$size_id = ($this->input->request('size_id')) ? (int)$this->input->request('size_id') : 0;

				if ($size_id > 0) {
					$result = $this->db->query("SELECT `name`, `email` FROM `reminder` WHERE `product_id` = {$this->input->request('id')} AND `size_id` = {$this->input->request('size_id')} AND `active` = 1");
				} else {
					$result = $this->db->query("SELECT `name`, `email` FROM `reminder` WHERE `product_id` = {$this->input->request('id')} AND `active` = 1");
				}

				$arr = $result->result_array();

				foreach ($arr as $k => $v) {

					if ($size_id > 0) {
						$result = $this->db->query("SELECT p.*, r.`name` AS r_name, r.`email` AS r_email FROM `reminder` r, `products` p WHERE p.`id` = r.`product_id` AND p.`id` = " . $this->input->request('id') . " AND r.`size_id` = " . $size_id . " AND r.email = '" . $v['email'] . "' AND r.`name` = '" . $v['name'] . "'");
					} else {
						$result = $this->db->query("SELECT p.*, r.`name` AS r_name, r.`email` AS r_email FROM `reminder` r, `products` p WHERE p.`id` = r.`product_id` AND p.`id` = " . $this->input->request('id') . " AND r.email = '" . $v['email'] . "' AND r.`name` = '" . $v['name'] . "'");
					}

					$reminder = $result->result_array();

					$this->mysmarty->assign('reminder', $reminder[0]);
					$template = $this->mysmarty->fetch('new_bloom/ru/reminder_mail.tpl');

					$headline = "Поступление товара " . $reminder[0]['name'];

					//ansotov отправляем письмо
					$this->send_mail($v['name'], $v['email'], $headline, $template);

					//ansotov обновляем данные о напоминании
					$this->mreminder->update(
						array(
							'active' => 0
						),
						array(
							'name'       => $v['name'],
							'email'      => $v['email'],
							'product_id' => $this->input->request('id'),
							'size_id'    => $size_id
						)
					);
				}
			}
			exit;
		}

		$this->mysmarty->assign('auth_facebook_link', $this->authFacebook());
		$this->mysmarty->assign('auth_vk_link', $this->authVk());

		if ($url1 == 'show-all') {
			$result = $this->db->query("SELECT email FROM orders GROUP BY email ORDER BY  `date` DESC");
			$data   = $result->result_array();
			foreach ($data as $k => $v) {
				echo $v['email'] . '<br>';
			}
			exit;
		}
		if ($url1 == 'show-all2') {
			$result = $this->db->query("SELECT phone FROM orders GROUP BY phone ORDER BY  `date` DESC");

			return $result->result_array();

			exit;
		}

		if ($url1 == 'yandex-market') {
			$this->yandexMarket();
			exit();
		}

		if ($url1 == 'all-delivery-types') {

			$cart         = $this->build_cart($this->cart->contents());
			$total_sum    = 0;
			$total_number = 0;
			if ($cart) {
				foreach ($cart as $k => $v) {
					$cart[$k]['price_sum'] = $v['price'] * $v['qty'];

					//ansotov бесплатный экземпляр по акции
					if ($v['in_stock']) {
						$cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
					}

					$total_sum    = $total_sum + $cart[$k]['price_sum'];
					$total_number = $total_number + $v['qty'];
				}
			}

			$russianPost = $this->getCities($_REQUEST['russian_post']['name'], 15, 4, $_REQUEST['russian_post']['region']);
			$SDEK        = $this->getSDEKInfo($_REQUEST['russian_post']['name'], $_REQUEST['russian_post']['region']);

			$this->mysmarty->assign('total_number', $total_number);
			$this->mysmarty->assign("total_sum", $this->total_amount);
			$this->mysmarty->assign('cart', $cart);

			$this->mysmarty->assign("russian_post", $russianPost[0]);
			$this->mysmarty->assign("courier_city", $_POST['courier_city']);
			$this->mysmarty->assign("pickpoint", $_POST['pickpoint']);
			$this->mysmarty->assign("sdek", $SDEK);

			$deliveries                 = array();//ansotov если это SDEK получаем данные по доставке
			$deliveries['courier']      = $_POST['courier_city'][0]['price'];
			$deliveries['pickpoint']    = $_POST['pickpoint']['price'];
			$deliveries['russian_post'] = $russianPost[0]['price'];

			if ( ! in_array($_REQUEST['russian_post']['region'], array('Московская обл.', 'Ленинградская обл.'))) {
				$deliveries['sdek'] = 880;
			}

			asort($deliveries);

			$this->mysmarty->assign("deliverySort", $deliveries);

			$template          = $this->mysmarty->fetch('new_bloom/ru/ajax/delivery_types.tpl');
			$template_in_total = $this->mysmarty->fetch('new_bloom/ru/ajax/cart-in-total.tpl');
			echo json_encode(array('template' => $template, 'template_in_total' => $template_in_total));
			exit;
		}

		if ($url1 == 'foreign-all-delivery-types') {

			$cart         = $this->build_cart($this->cart->contents());
			$total_sum    = 0;
			$total_number = 0;
			if ($cart) {
				foreach ($cart as $k => $v) {
					$cart[$k]['price_sum'] = $v['price'] * $v['qty'];

					//ansotov бесплатный экземпляр по акции
					if ($v['in_stock']) {
						$cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
					}

					$total_sum    = $total_sum + $cart[$k]['price_sum'];
					$total_number = $total_number + $v['qty'];
				}
			}

			$this->mysmarty->assign('total_number', $total_number);
			$this->mysmarty->assign("total_sum", $this->total_amount);
			$this->mysmarty->assign('cart', $cart);

			$this->mysmarty->assign('user', $this->getUser($this->user_info['id']));

			$free_delivery = $this->mfree_delivery->get(array("active" => 1, 'const' => 'ALL_DAYS'), array(), 1);

			//ansotov данные доставки для других стран, нужно переписать опционально
			$fl_arr['price'] = 500;//(($free_delivery['start_time'] <= date('H:i:s')) && ($free_delivery['end_time'] <= date('H:i:s')) && $this->total_amount >= $this->free_delivery) ? (($this->total_amount >= $this->free_delivery) ? 0 : 500) : 500;//ansotov 500 = стоимость доставки в другие страны, костыль;
			$fl_arr['days']  = 10;

			if ($_POST['type'] == 0) {
				$fl_arr = array();
			}

			$this->mysmarty->assign("foreign_lands", $_POST['type']);
			$this->mysmarty->assign("russian_post", $fl_arr);
			$this->mysmarty->assign('delivery', $fl_arr);

			$this->mysmarty->assign("delivery_type", 4);

			$template            = $this->mysmarty->fetch('new_bloom/ru/ajax/delivery_types.tpl');
			$template_order_form = $this->mysmarty->fetch('new_bloom/ru/ajax/order_form.tpl');
			$template_in_total   = $this->mysmarty->fetch('new_bloom/ru/ajax/cart-in-total.tpl');
			echo json_encode(array('template' => $template, 'template_in_total' => $template_in_total, 'template_order_form' => $template_order_form));
			exit;
		}

		if ($url1 == 'delivery-types') {

			$cart         = $this->build_cart($this->cart->contents());
			$total_sum    = 0;
			$total_number = 0;
			if ($cart) {
				foreach ($cart as $k => $v) {
					$cart[$k]['price_sum'] = $v['price'] * $v['qty'];

					//ansotov бесплатный экземпляр по акции
					if ($v['in_stock']) {
						$cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
					}

					$total_sum    = $total_sum + $cart[$k]['price_sum'];
					$total_number = $total_number + $v['qty'];
				}
			}

			//ansotov признак оплаты только онлайн
			$onlyOnlinePayment = false;

			//ansotov получаем стоимость доставки
			if ($_REQUEST['delivery_type'] == 3) {
				$delivery = array(0 => $this->getPickpointInfo($_REQUEST['pickpointCity'], $_REQUEST['pickpointRegion']));
				if ($delivery['only_online'] == 1) {
					$onlyOnlinePayment = true;
				}
			} elseif ($_REQUEST['delivery_type'] == 5) {

				$SDEK     = $this->getSDEKInfo($_REQUEST['name'], $_REQUEST['region']);
				$delivery = array(0 => $SDEK);
			} else {
				if ($_REQUEST['name']) {
					$delivery = $this->getCities($_REQUEST['name'], 1, $_REQUEST['delivery_type'], true, $_REQUEST['region']);
				} else {
					$delivery = array(0 => array('price' => 500));
				}
			}

			//ansotov если оплата онлайн то скидка 5%
			$this->total_amount = ($_POST['onlinePayment'] == true) ? (($this->total_amount * (100 - 3)) / 100) : $this->total_amount;

			$this->mysmarty->assign('total_number', $total_number);
			$this->mysmarty->assign('delivery', $delivery[0]);
			$this->mysmarty->assign("total_sum", $this->total_amount);
			$this->mysmarty->assign('cart', $cart);

			$this->mysmarty->assign('user', $this->getUser($this->user_info['id']));

			//ansotov проверка на тип товара
			if ($cart) {
				foreach ($cart as $k => $v) {
					$types[] = $v['info']['type'];

					//ansotov если есть товар со старой ценой
					if ($v['info']['old_price'] > 0) {
						$old_price = true;
					}

					//ansotov если есть товар со старой ценой
					if ($v['info']['online_payment'] == 1) {
						$onlyOnlinePayment = true;
					}
				}
			}

			//ansotov если есть коробки
			if (in_array('boxes', $types) || $old_price == true) {
				$this->mysmarty->assign('only_online', 1);
			}

			$this->mysmarty->assign("delivery_type", $_POST['delivery_type']);
			$this->mysmarty->assign("onlyOnlinePayment", $onlyOnlinePayment);
			$this->mysmarty->assign("pickpoint_info", $_POST['pickpoint_info']);
			$template            = $this->mysmarty->fetch('new_bloom/ru/ajax/order_form.tpl');
			$template_in_total   = $this->mysmarty->fetch('new_bloom/ru/ajax/cart-in-total.tpl');
			$template_order_form = $this->mysmarty->fetch('new_bloom/ru/ajax/order_form.tpl');
			echo json_encode(array('template' => $template, 'template_in_total' => $template_in_total, 'template_order_form' => $template_order_form));

			exit;
		}

		if ($url1 == 'pickpoint-form') {

			$cart         = $this->build_cart($this->cart->contents());
			$total_sum    = 0;
			$total_number = 0;
			if ($cart) {
				foreach ($cart as $k => $v) {
					$cart[$k]['price_sum'] = $v['price'] * $v['qty'];

					//ansotov бесплатный экземпляр по акции
					if ($v['in_stock']) {
						$cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
					}

					$total_sum    = $total_sum + $cart[$k]['price_sum'];
					$total_number = $total_number + $v['qty'];

					$types[] = $v['info']['type'];

					//ansotov если есть товар со старой ценой
					if ($v['info']['old_price'] > 0) {
						$old_price = true;
					}
				}
			}

			$delivery = $this->getPickpointInfo($_POST['pickpoint_info']['cityname'], $_POST['pickpoint_info']['region']);

			//mail('my-gebbels@yandex.ru', 333, print_r($delivery, true));

			$onlyOnlinePayment = false;

			//ansotov если есть коробки
			if (in_array('boxes', $types) || $old_price == true || $delivery['only_online'] == 1) {
				$this->mysmarty->assign('only_online', 1);
				$onlyOnlinePayment = true;
			}

			$this->mysmarty->assign("onlyOnlinePayment", $onlyOnlinePayment);

			$this->mysmarty->assign('total_number', $total_number);
			$this->mysmarty->assign('delivery', $delivery);
			$this->mysmarty->assign("total_sum", $this->total_amount);
			$this->mysmarty->assign('cart', $cart);

			$this->mysmarty->assign("pickpoint_info", $_POST['pickpoint_info']);
			$template          = $this->mysmarty->fetch('new_bloom/ru/ajax/order_form.tpl');
			$template_in_total = $this->mysmarty->fetch('new_bloom/ru/ajax/cart-in-total.tpl');
			echo json_encode(array('template' => $template, 'template_in_total' => $template_in_total));
			exit;
		}

		if ($url1 == 'city-search') {
			//Выводим JSON на страницу
			echo json_encode($this->getCities($_POST['query'], $_POST['maxRows'], 4));
			exit;
		}

		if ($url1 == 'city-couriers-search') {
			//Выводим JSON на страницу
			echo json_encode($this->getCouriers($_POST['query']));
			exit;
		}

		//ansotov получение данных из портфолио
		if ($url1 == 'portfolio' && $url2) {

			$result = $this->db->query("SELECT * FROM portfolio WHERE `id` = " . $url2);

			$data = $result->result_array();
			$res  = $result->result();

			$res[0]->title       = $this->getLang($data[0], 'title');
			$res[0]->description = $this->getLang($data[0], 'description');

			//Выводим JSON на страницу
			echo json_encode($res);
			exit;
		}

		//ansotov получение данных из портфолио
		if ($url1 == 'license' && $_REQUEST['json']) {

			$result = $this->db->query("SELECT * FROM folders WHERE `url` = '" . $url1 . "'");

			$data = $result->result_array();
			$res  = $result->result();

			$res[0]->content = $this->getLang($data[0], 'content');

			//Выводим JSON на страницу
			echo json_encode($res);
			exit;
		}

		//ansotov получение данных из СМИ
		if ($url1 == 'smi' && $url2) {

			$result = $this->db->query("SELECT * FROM smi WHERE `id` = " . $url2);

			$data = $result->result_array();
			$res  = $result->result();

			$res[0]->title       = $this->getLang($data[0], 'title');
			$res[0]->description = $this->getLang($data[0], 'description');

			//Выводим JSON на страницу
			echo json_encode($res);
			exit;
		}

		if ($url1 == 'pickpoint-info') {
			echo json_encode($this->getPickpointInfo($_POST['name'], $_POST['region']));
			exit;
		}

		if ($url1 == 'all-types') {
			$data['couriers']    = $this->getCouriers($_POST['name'], $_POST['region']);
			$data['pickpoint']   = $this->getPickpointInfo($_POST['name'], $_POST['region']);
			$data['russia_post'] = $this->getCities($_POST['name'], 1, 4, false, $_POST['region']);
			echo json_encode($data);
			exit;
		}

		if ($url1 == 'upgrade-deliveries') {
			exit();
			ini_set('max_execution_time', 10000);
			$this->load->model('mdelivery');
			$this->load->model('mdelivery_cities');

			require($this->config->item('core_web') . "application/libraries/excelreader/PHPExcel.php");
			$inputFile    = $this->config->item('core_web') . 'docs/pickpoint-rus-post.xlsx';
			$objPHPExcel  = PHPExcel_IOFactory::load($inputFile);
			$objWorksheet = $objPHPExcel->getActiveSheet();

			//  Get worksheet dimensions
			$sheet         = $objPHPExcel->getSheet(0);
			$highestRow    = $objWorksheet->getHighestRow();
			$highestColumn = $objWorksheet->getHighestColumn();

			//ansotov вычищаем таблицы, плохое решение, но нужно было быстро. Если понадобятся города, то переписать
			$this->db->query("TRUNCATE TABLE delivery");
			$this->db->query("TRUNCATE TABLE delivery_cities");

			for ($row = 3; $row <= $highestRow; $row++) {
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);

				//ansotov формируем данные для города
				$city_arr = array(
					'name'   => $rowData[0][0],
					'region' => $rowData[0][1]
				);

				//ansotov сохраняем город и получаем его id
				$city = $this->mdelivery_cities->save($city_arr);

				$pickpoint_arr = array(
					'city_id' => $city,
					'price'   => $rowData[0][4],
					'type'    => 1,
					'zone'    => $rowData[0][2],
					'days'    => $rowData[0][3]
				);

				$rus_post_arr = array(
					'city_id' => $city,
					'price'   => $rowData[0][6],
					'type'    => 2,
					'zone'    => $rowData[0][2],
					'days'    => $rowData[0][5]
				);

				$this->mdelivery->save($pickpoint_arr);
				$this->mdelivery->save($rus_post_arr);
			}

		}

		if ($url1 == 'upgrade-couriers') {
			exit();
			ini_set('max_execution_time', 100000);
			$this->load->model('mcouriers');
			$this->load->model('mcouriers_cities');

			require($this->config->item('core_web') . "application/libraries/excelreader/PHPExcel.php");
			$inputFile    = $this->config->item('core_web') . 'docs/couriers.xlsx';
			$objPHPExcel  = PHPExcel_IOFactory::load($inputFile);
			$objWorksheet = $objPHPExcel->getActiveSheet();

			//  Get worksheet dimensions
			$sheet         = $objPHPExcel->getSheet(0);
			$highestRow    = $objWorksheet->getHighestRow();
			$highestColumn = $objWorksheet->getHighestColumn();

			//ansotov вычищаем таблицы, плохое решение, но нужно было быстро. Если понадобятся города, то переписать
			$this->db->query("TRUNCATE TABLE couriers");
			$this->db->query("TRUNCATE TABLE couriers_cities");

			for ($row = 3; $row <= $highestRow; $row++) {
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);

				//ansotov формируем данные для города
				$city_arr = array(
					'name'   => $rowData[0][0],
					'region' => $rowData[0][1],
					'area'   => $rowData[0][3]
				);

				//ansotov сохраняем город и получаем его id
				$city = $this->mcouriers_cities->save($city_arr);

				$couriers_arr = array(
					'city_id'  => $city,
					'price'    => $rowData[0][7],
					'zone'     => $rowData[0][4],
					'min_days' => $rowData[0][5],
					'max_days' => $rowData[0][6]
				);

				$this->mcouriers->save($couriers_arr);
			}

		}

		include_once('includes/_user.php');
		include_once('includes/_order.php');
		include_once('includes/_products.php');
		include_once('includes/_cabinet.php');

		if ($type == '' && $folder_info = $this->mfolders->get(array('url' => $url1), array(), 1)) {
			include_once('includes/_pages.php');

			//ansotov с какого начинаем показ
			$this->offset = ($this->page - 1) * $this->limit;
			$this->offset = ($this->offset > 0) ? $this->offset : 0;

			$pagination['pages'] = ceil($countItems / $this->limit);

			$sendUrl = (($url1) ? '/' . $url1 : false) . (($url2) ? '/' . $url2 : false) . (($url3) ? '/' . $url3 : false) . (($url4) ? '/' . $url4 : false);

			$pagination['pagination'] = $this->getPagination($this->page, ceil($countItems / $this->limit), $sendUrl);

			if (isset($products) && $products) {

				$products_arr = array();

				foreach ($products as $key => $value) {

					$products_arr[$key] = $value;

					//ansotov проверка на последний размер
					$this->load->model('mproducts_sizes');
					$sizes = $this->mproducts_sizes->get(array('product_id' => $value['id']), array(), 0,/*"CAST(code as DECIMAL) ASC"*/
						"order_id ASC");

					foreach ($sizes as $k => $v) {
						if ($v['last_size'] == 1) {
							$products_arr[$key]['last_size'] = 1;
						}
					}

					//ansotov скидка на определённый размер
					foreach ($sizes as $k => $v) {
						if ($v['discount_active'] == 1 && $v['discount_type'] && $v['discount'] > 0) {
							$name                                        = ($v['name']) ? $v['name'] : $v['code'];
							$v['price']                                  = ($v['price']) ? $v['price'] : $value['price'];
							$products_arr[$key]['discount_sizes'][$name] = $this->getDiscountSum($v);
						}
					}

					if ($value['discount_active'] == true && $value['discount'] > 0) {
						$products_arr[$key]['price']     = ($value['price']) ? ($value['price'] - ($value['price'] / 100) * $value['discount']) : $value['price'];
						$products_arr[$key]['old_price'] = ($value['price']) ? $value['price'] : $value['old_price'];
					}
				}

				$this->mysmarty->assign('products', $products_arr);
				$this->mysmarty->assign('pagination', $pagination);
			}
		} elseif ($url1 != '' && $type == '') {
			$this->show_404();

			return;
			exit;
		}

		//ansotov текущий url страницы
		$currentUrl = '/' . $GLOBALS['URI']->uri_string . '/';

		//ansotov текущий url
		$this->mysmarty->assign('current_url', $currentUrl);

		if ($folder_info && isset($folder_info['type']) && $type == '') {
			$type = $folder_info['type'];
		}

		$metaData = $this->mfolders->get(array('url' => $currentUrl), array(), 1);

		//ansotov если это товар
		if ( ! $metaData) {
			$urls     = explode('/', trim($currentUrl, '/'));
			$metaData = $this->mproducts->get(array('url' => $urls[1]), array(), 1);
		}

		//ansotov другой тип данных
		if ( ! $metaData) {
			$metaData = $this->mfolders->get(array('url' => trim($currentUrl, '/')), array(), 1);
		}

		$metaData['meta_title'] = ($metaData['meta_title']) ? $metaData['meta_title'] : $metaData['name'];

		if ($folder_info != array()) {
			$this->mysmarty->assign('selected_folder', '/' . $folder_info['url'] . '/');
			$this->title                    = $metaData['meta_title'];
			$this->h1                       = $metaData['h1'];
			$folder_info['content']         = $this->getLang($folder_info, 'content');
			$folder_info['top_banner_text'] = $this->getLang($folder_info, 'top_banner_text');
			$this->description              = $metaData['meta_description'];
			$this->keywords                 = $folder_info['keywords'];
			$this->mysmarty->assign('folder_info', $folder_info);
		}

		$this->finish($type);

	}

	/**
	 * Список заказов для интеграции с 1C
	 *
	 * @param $days
	 *
	 * @return string
	 * @author ansotov
	 *
	 */
	private function getOrders($days = 2)
	{
		$arr = $this->db->query("SELECT *
                                        FROM orders o
                                        WHERE
                                          o.`date` BETWEEN DATE_ADD(CURDATE(), INTERVAL -" . $days . " DAY) AND CURDATE()");

		$result = array();

		foreach ($arr->result_array() as $k => $v) {
			$result[$k] = $v;

			$products = $this->db->query("SELECT * FROM `orders_products` WHERE `user_order_id` = " . $v['id']);

			foreach ($products->result_array() as $key => $value) {
				$result[$k][$key] = $value;
			}
		}

		return json_encode($result);
	}

	private function auth($type)
	{

		if ($type == 'facebook') {
			$this->authFacebook();
		} elseif ($type == 'vk') {
			$this->authVK();
		}
	}

	private function authFacebook()
	{
		$client_id     = '1850662381876707'; // Client ID
		$client_secret = '1d34f9e257235589918ff2cf8e8d8ce6'; // Client secret
		$redirect_uri  = 'https://girlsinbloom.ru/social-auth/facebook/'; // Redirect URIs

		$url = 'https://www.facebook.com/dialog/oauth';

		$params = array(
			'client_id'     => $client_id,
			'redirect_uri'  => $redirect_uri,
			'response_type' => 'code'
		);

		if (isset($_GET['code'])) {

			$result = false;

			$params = array(
				'client_id'     => $client_id,
				'redirect_uri'  => $redirect_uri,
				'client_secret' => $client_secret,
				'code'          => $_GET['code']
			);

			$url = 'https://graph.facebook.com/oauth/access_token';

			$tokenInfo = null;
			parse_str(file_get_contents($url . '?' . http_build_query($params)), $tokenInfo);

			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
				$params = array('access_token' => $tokenInfo['access_token']);

				$userInfo = json_decode(file_get_contents('https://graph.facebook.com/me' . '?' . urldecode(http_build_query($params))), true);

				if (isset($userInfo['id'])) {
					$userInfo = $userInfo;
					$result   = true;
				}
			}

			$user = ($userInfo['id']) ? $this->musers->get(array('social_id' => $userInfo['id']), array(), 1) : false;

			//ansotov если пользователь есть то авторизуемся
			if ($user) {
				redirect('/signin/?social_id=' . $userInfo['id']);
			} else {
				redirect('/signup/?type=facebook&social-auth-data=' . json_encode($userInfo));
			}

		} else {
			return $url . '?' . urldecode(http_build_query($params));
		}
	}

	private function authVk()
	{
		$client_id     = '5836456'; // Client ID
		$client_secret = 'ii3XYuM5eeEy07SpJFdU'; // Client secret
		$redirect_uri  = 'https://girlsinbloom.ru/social-auth/vk/'; // Redirect URIs

		$url = 'http://oauth.vk.com/authorize';

		$params = array(
			'client_id'     => $client_id,
			'redirect_uri'  => $redirect_uri,
			'response_type' => 'code'
		);

		if (isset($_GET['code'])) {

			$result = false;
			$params = array(
				'client_id'     => $client_id,
				'client_secret' => $client_secret,
				'code'          => $_GET['code'],
				'redirect_uri'  => $redirect_uri
			);

			$token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);

			if (isset($token['access_token'])) {
				$params = array(
					'uids'         => $token['user_id'],
					'fields'       => 'uid,first_name,last_name,screen_name,sex,bdate,photo_big',
					'access_token' => $token['access_token']
				);

				$userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
				if (isset($userInfo['response'][0]['uid'])) {
					$userInfo = $userInfo['response'][0];
					$result   = true;
				}
			}

			if ($result) {
				$user = ($userInfo['uid']) ? $this->musers->get(array('social_id' => $userInfo['uid']), array(), 1) : false;

				//ansotov если пользователь есть то авторизуемся
				if ($user) {
					redirect('/signin/?social_id=' . $userInfo['uid']);
				} else {
					redirect('/signup/?type=vk&social-auth-data=' . json_encode($userInfo));
				}
			}
		} else {
			return $url . '?' . urldecode(http_build_query($params));
		}
	}

	/**
	 * RetailCRM
	 *
	 * @param bool $recommendable
	 */
	private function YMLData($recommendable = true, $url = 'http://printuha.ru')
	{
		$search = array(
			"'<script[^>]*?>.*?</script>'si", // Вырезает javaScript
			"'<[\/\!]*?[^<>]*?>'si", // Вырезает HTML-теги
			"'([\r\n])[\s]+'", // Вырезает пробельные символы
			"'&(quot|#34);'i", // Заменяет HTML-сущности
			"'&(amp|#38);'i",
			"'&(lt|#60);'i",
			"'&(gt|#62);'i",
			"'&(nbsp|#160);'i",
			"'&(iexcl|#161);'i",
			"'&(cent|#162);'i",
			"'&(pound|#163);'i",
			"'&(copy|#169);'i",
			"'&(omega|#60);'i",
			"'&(deg|#60);'i",
			"'&(bull|#60);'i",
			"'&(laquo|#60);'i",
			"'&(raquo|#60);'i",
			"'&(ndash);'i",
			"'&(mdash);'i",
			"'&(sup2);'i",
			"'&(sup3);'i",
			"'&(middot);'i",
			"'&(reg);'i",
			"'(\[\[|\]\])'i",
			"'( \|)'i",
			"' &'i",
			"'&#(\d+);'e",
		); // интерпретировать как php-код

		$replace = array(
			"",
			"",
			"\\1",
			"\"",
			"&",
			"<",
			">",
			" ",
			chr(161),
			chr(162),
			chr(163),
			chr(169),
			" Ом",
			" гр.",
			"",
			"*",
			"*",
			"-",
			"-",
			"в кв.",
			"в кубе.",
			"",
			"(R)",
			"",
			": ",
			" и",
			"chr(\\1)",
		);

		$result   = $this->db->query(
			"SELECT
    `p`.`id`                                            AS `product_id`,
    `ps`.`id`                                           AS `variant_id`,
    `p`.`sku`                                           AS `sku`,
    `ps`.`code`                                         AS `code`,
    `p`.`url`                                           AS `url`,
    `p`.`name`                                          AS `name`,
    `p`.`content`                                       AS `description`,
    `p`.`front_picture`                                 AS `front_picture`,
    `p`.`front_picture_type`                            AS `front_picture_type`,
    `p`.`type`                                          AS `category_id`,
    if((`ps`.`name` = ''), `ps`.`code`, `ps`.`name`)    AS `size`,
    if(isnull(`ps`.`price`), `p`.`price`, `ps`.`price`) AS `price`,
    `p`.`active`                                        AS `active`
  FROM
    (`products` `p` LEFT JOIN `products_sizes` `ps` ON ((`p`.`id` = `ps`.`product_id`))) WHERE p.`active` = 1"
		);
		$products = $result->result_array();

		//ansotov проверка на последний размер
		$this->load->model('mtypes');
		$categories = $this->mtypes->get(array('active' => 1), array(), 0);

		header("Content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"; ?>
        <!DOCTYPE yml_catalog SYSTEM "shops.dtd">
        <yml_catalog date="<?= date("Y-m-d H:i") ?>">
            <shop>
                <name><?php echo $url; ?></name>
                <company>Принтуха</company>
                <url><?= $url ?>/</url>

                <currencies>
                    <currency id="RUR" rate="1"/>
                </currencies>

                <categories>
					<?php foreach ($categories as $k => $v) : ?>
                        <category id="<?php echo $v['id']; ?>" parentId="0"><?= preg_replace($search, $replace, $v['name']); ?></category>
					<?php endforeach; ?>
                </categories>

                <delivery-options>
                    <option cost="290" days="2"/>
                </delivery-options>

                <offers>
					<?php foreach ($products as $key => $value) : ?>
                        <offer
                            id="<?php echo $value['variant_id'] ?>" available='true'
                            productId="<?php echo $value['product_id']; ?>"
                            quantity="100">
                            <url><?php echo $url ?>/new-products/<?php echo $value['url']; ?>/</url>
                            <externalId><?php echo $value['variant_id']; ?>/</externalId>
                            <article><?php echo $value['sku'] ?></article>
							<?php if ($value['discount_active'] == 1 && $value['discount'] > 0) : ?>
                                <price><?php echo round(($value['price'] - ($value['price'] / 100) * $value['discount'])) ?></price>
                                <oldprice><?php echo $value['price']; ?></oldprice>
							<?php else : ?>
                                <price><?php echo $value['price'] ?></price>
							<?php endif; ?>
                            <currencyId>RUR</currencyId>
                            <categoryId><?php echo $this->getCategoryId($value['category_id']); ?></categoryId>
							<?php if ($value['front_picture']) : ?>
                                <picture><?php echo $url; ?>/img/front_pictures/<?php echo $value['front_picture'] . '.' . $value['front_picture_type']; ?></picture>
							<?php endif; ?>
							<?php if ($value['back_picture']) : ?>
                                <picture><?php echo $url; ?>/img/back_pictures/<?php echo $value['back_picture'] . '.' . $value['back_picture_type']; ?></picture>
							<?php endif; ?>
                            <delivery>true</delivery>
                            <name><?php echo $value['name']; ?></name>
							<?php if ($value['description']) { ?>
                                <description><?= preg_replace($search, $replace, htmlspecialchars(strip_tags($value['description']))); ?></description>
							<?php } ?>
							<?php if (($value['category_id'] == 'gotovye-kejsy') && $recommendable == true) : ?>
                                <recommendable>false</recommendable>
							<?php endif; ?>
							<?php if ($value['category_id'] == 'boxes') : ?>
                                <sales_notes>ПОКУПКА ВОЗМОЖНА ТОЛЬКО ПО 100% ПРЕДОПЛАТЕ ОНЛАЙНТОВАР ОБМЕНУ И ВОЗВРАТУ НЕ ПОДЛЕЖИТ.</sales_notes>
							<?php endif; ?>
                            <param name="Размер" code="size"><?php echo $value['size'] ?></param>
                        </offer>
					<?php endforeach; ?>
                </offers>
            </shop>
        </yml_catalog>
		<?php
	}

	private function getCategoryId($url)
	{
		$this->load->model('mtypes');
		$categories = $this->mtypes->get(array('url' => $url), array(), 0);

		return $categories[0]['id'];
	}

	private function myscandir($dir, $sort = 0)
	{
		$list = scandir($dir, $sort);

		// если директории не существует
		if ( ! $list) {
			return false;
		}

		// удаляем . и .. (я думаю редко кто использует)
		if ($sort == 0) {
			unset($list[0], $list[1]);
		} else {
			unset($list[count($list) - 1], $list[count($list) - 1]);
		}

		return $list;
	}

	public function updateMoySkladIds()
	{

		$setup_curl = $this->setupCurl($this->constMoySkladInit());

		$limits = array();

		for ($x = 0; $x < 100; $x++) {
			$limits[] = ($x == 0) ? 1 : $x * 100;
		}

		$nomenclature = array();

		$consts = $this->constMoySkladInit();

		foreach ($limits as $k => $v) {
			$products         = $this->setCurl(
				$setup_curl,
				$consts[MOYSKLAD_API_URL] . $consts[MOYSKLAD_GET_VARIANTS] . '?limit=100&offset=' . $v,
				MOYSKLAD_GET_NOMENCLATURE_METHOD);
			$prod_arr         = $this->getMoySkladData($products);
			$nomenclature[$k] = $prod_arr['rows'];
		}

		$arrData = array();

		foreach ($nomenclature as $k2 => $v2) {
			$arrData = array_merge($arrData, $v2);
		}

		//ansotov берём варианты товаров
		$this->load->model('mproducts_sizes');

		//ansotov обновляем id "мой склад"
		foreach ($arrData as $key => $value) {
			$this->mproducts_sizes->update(array('moysklad_id' => $value['id']), array('id' => (int)$value['code']));
		}

		return true;
	}

	private function setupCurl()
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($curl, CURLOPT_USERPWD, "admin@darya6:789dc743f990");

		return $curl;
	}

	/**
	 * Инициализация констант моего склада
	 *
	 * @return array
	 * @author ansotov
	 */
	private function constMoySkladInit()
	{
		define('MOYSKLAD_METHOD_GET', 'GET');
		define('MOYSKLAD_METHOD_POST', 'POST');
		define('MOYSKLAD_METHOD_PUT', 'PUT');
		define('MOYSKLAD_USERNAME', 'USERNAME');
		define('MOYSKLAD_PASSWORD', 'PASSWORD');
		define('MOYSKLAD_USER_AGENT', 'USER_AGENT');
		define('MOYSKLAD_API_URL', 'API_URL');
		define('MOYSKLAD_GET_NOMENCLATURE', 'GET_NOMENCLATURE');
		define('MOYSKLAD_GET_VARIANTS', 'GET_VARIANTS');
		define('MOYSKLAD_GET_NOMENCLATURE_METHOD', 'GET_NOMENCLATURE_METHOD');
		define('MOYSKLAD_GET_JURIDICAL_PERSON', 'GET_JURIDICAL_PERSON');
		define('MOYSKLAD_GET_JURIDICAL_PERSON_METHOD', 'GET_JURIDICAL_PERSON_METHOD');
		define('MOYSKLAD_GET_COUNTERPARTY', 'GET_COUNTERPARTY');
		define('MOYSKLAD_GET_COUNTERPARTY_METHOD', 'GET_COUNTERPARTY_METHOD');
		define('MOYSKLAD_GET_METADATA', 'GET_METADATA');
		define('MOYSKLAD_GET_METADATA_METHOD', 'GET_METADATA_METHOD');
		define('MOYSKLAD_ADD_CUSTOMER_ORDER', 'ADD_CUSTOMER_ORDER');
		define('MOYSKLAD_ADD_CUSTOMER_ORDER_METHOD', 'ADD_CUSTOMER_ORDER_METHOD');
		define('MOYSKLAD_ADD_ORDER_POSITION_METHOD', 'ADD_ORDER_POSITION_METHOD');
		define('MOYSKLAD_ADD_ORDER_POSITION_PREFIX', 'ADD_ORDER_POSITION_PREFIX');
		define('MOYSKLAD_ADD_ORDER_POSITION_SUFFIX', 'ADD_ORDER_POSITION_SUFFIX');
		$curl_details = array(
			MOYSKLAD_USERNAME                    => 'admin@darya6',
			MOYSKLAD_PASSWORD                    => '789dc743f990',
			MOYSKLAD_USER_AGENT                  => 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36',
			MOYSKLAD_API_URL                     => 'https://online.moysklad.ru/api/remap/1.1',
			MOYSKLAD_GET_NOMENCLATURE            => '/entity/product',
			MOYSKLAD_GET_VARIANTS                => '/entity/variant',
			MOYSKLAD_GET_NOMENCLATURE_METHOD     => MOYSKLAD_METHOD_GET,
			MOYSKLAD_GET_JURIDICAL_PERSON        => '/entity/organization',
			MOYSKLAD_GET_JURIDICAL_PERSON_METHOD => MOYSKLAD_METHOD_GET,
			MOYSKLAD_GET_COUNTERPARTY            => '/entity/counterparty',
			MOYSKLAD_GET_JURIDICAL_PERSON_METHOD => MOYSKLAD_METHOD_GET,
			MOYSKLAD_GET_METADATA                => '/entity/customerorder/metadata/',
			MOYSKLAD_GET_METADATA_METHOD         => MOYSKLAD_METHOD_GET,
			MOYSKLAD_ADD_CUSTOMER_ORDER          => '/entity/customerorder',
			MOYSKLAD_ADD_CUSTOMER_ORDER_METHOD   => MOYSKLAD_METHOD_POST,
			MOYSKLAD_ADD_ORDER_POSITION_PREFIX   => '/entity/customerorder/',
			MOYSKLAD_ADD_ORDER_POSITION_SUFFIX   => '/positions',
			MOYSKLAD_ADD_ORDER_POSITION_METHOD   => MOYSKLAD_METHOD_POST,
		);

		return $curl_details;
	}

	private function setCurl(&$curlObject, $uri, $method)
	{
		curl_setopt($curlObject, CURLOPT_URL, $uri);
		curl_setopt($curlObject, CURLOPT_HTTPGET, true);
		switch ($method) {
			case MOYSKLAD_METHOD_GET:
				curl_setopt($curlObject, CURLOPT_HTTPGET, true);
				break;
			case MOYSKLAD_METHOD_POST:
				curl_setopt($curlObject, CURLOPT_POST, true);
				break;
			case MOYSKLAD_METHOD_PUT:
				curl_setopt($curlObject, CURLOPT_PUT, true);
				break;
		}

		return $curlObject;
	}

	/**
	 * Получение данных из мой склад
	 *
	 * @param $curlObject
	 *
	 * @return mixed
	 */
	private function getMoySkladData($curlObject)
	{
		$response = $this->curlExec($curlObject);
		$data     = json_decode($response, true);
		$result   = $data;

		return $result;
	}

	private function curlExec($curlObject)
	{
		$response        = curl_exec($curlObject);
		$curlErrorNumber = curl_errno($curlObject);
		if ($curlErrorNumber) {
			throw new Exception(curl_error($curlObject));
		}

		return $response;
	}

	private function getEmailsFile()
	{
		$filter   = $this->input->request('filter');
		$where    = false;
		$fileName = false;

		if ($filter['date_from'] && $filter['date_to']) {
			$where    = "e.created_at BETWEEN DATE_FORMAT('" . $filter['date_from'] . "', '%Y-%m-%d') AND DATE_FORMAT('" . $filter['date_to'] . "', '%Y-%m-%d') AND ";
			$fileName = $filter['date_from'] . '--' . $filter['date_to'] . '--';
		} elseif ($filter['date_from']) {
			$where    = "e.created_at >= DATE_FORMAT('" . $filter['date_from'] . "', '%Y-%m-%d') AND ";
			$fileName = $filter['date_from'] . '--';
		} elseif ($filter['date_to']) {
			$where    = "e.created_at < DATE_FORMAT('" . $filter['date_to'] . "', '%Y-%m-%d') AND ";
			$fileName = $filter['date_to'] . '--';
		}

		$result = $this->db->query("SELECT
                                      email,
                                      created_at
                                    FROM (SELECT
                                            `email` AS email,
                                            date    AS created_at
                                          FROM orders
                                          UNION SELECT
                                                  `email`    AS email,
                                                  created_at AS created_at
                                                FROM `users`
                                          UNION SELECT
                                                  `email`    AS email,
                                                  created_at AS created_at
                                                FROM `reminder`
                                          ORDER BY email DESC) e
                                    WHERE " . $where . "e.email != '' AND email != 'Заказ в 1 клик'
                                    GROUP BY e.email");
		$arr    = $result->result_array();

		$filename = $fileName . 'emails.txt';

		$data = false;

		foreach ($arr as $k => $v) {
			$data .= $v['email'] . PHP_EOL;
		}


		$create_file = fopen($filename, "w+");
		fwrite($create_file, $data);
		fclose($create_file);

		header("Content-Disposition: attachment; filename=" . $filename);
		header("Content-Length: " . filesize($filename));
		header('Content-Type: application/x-force-download; name="' . $filename . '"');
		readfile($filename);
	}

	/**
	 * Напоминание о незаказанных товарах
	 *
	 * @author ansotov
	 */
	private function remindOrders()
	{
		$result    = $this->db->query("SELECT u.*, rm.`created_at`, (NOW() - INTERVAL 5 HOUR) AS send_time FROM `users` u, `remind_products` rm
                                        WHERE u.`id` = rm.`user_id`
                                        GROUP BY u.`id`
                                        HAVING rm.`created_at` < send_time");
		$reminders = $result->result_array();

		$this->load->model('mremind_products');

		foreach ($reminders as $k => $v) {

			$res = $this->db->query('SELECT p.*, if(ps.name = "", ps.code, ps.name) AS size_name 
                                        FROM remind_products rp, products_sizes ps, products p
                                        WHERE rp.user_id = ' . $v['id'] . ' 
                                        AND ps.product_id = rp.product_id 
                                        AND rp.size = ps.code
                                        AND p.id = ps.product_id');

			//ansotov если данных нет пропускаем итерацию
			if ( ! $res || ! $v['email']) {
				continue;
			}

			$v['products'] = $res->result_array();
			$v['name']     = $v['first_name'] . ' ' . $v['last_name'];

			$this->mysmarty->assign('reminder', $v);
			$template = $this->mysmarty->fetch('new_bloom/ru/remind_orders.tpl');

			$headline = "Вы добавили товары в корзину, но возможно забыли заказать.";

			//ansotov если письмо ушло, то удаляем запись
			if ($this->send_mail($v['name'], $v['email'], $headline, $template)) {
				$this->mremind_products->delete(array('user_id' => $v['id']));
			}
		}
	}

	private function setPromocode($value)
	{

		$this->load->model('mpromocodes');
		$promocode = false;//$this->mpromocodes->get('`value` = "' . $value . '" AND `min_sum` <= ' . $this->total_amount . ' AND NOW() BETWEEN `start_at` AND (`end_date` + INTERVAL 1 DAY) AND `active` = 1', array(), 1);

		$cart = $this->build_cart($this->cart->contents());

		//ansotov проверка на тип товара
		if ($cart) {
			foreach ($cart as $k => $v) {
				$this->load->model('mproduct_categories');

				$item = $this->mproduct_categories->get('const = "' . $v['info']['type'] . '"');

				$types[] = $item[0]['id'];
			}
		}

		if ($promocode == false) {
			$promocode = $this->getPromocode($this->total_amount, $types);
			$promocode = $promocode[0];
		}

		if ($promocode == false) {
			$this->load->model('mpersonal_promocodes');
			$promocode = $this->mpersonal_promocodes->get('`value` = "' . $value . '"', array(), 1);
		}


		if (strtolower($promocode['value']) == strtolower($this->input->post('promocode'))) {
			$this->total_amount = round(($this->getCartAmount() - ($this->getCartAmount() * ($promocode['discount'] / 100))));
			$this->session->set_userdata('promocode_discount', $promocode['discount']);
			$this->session->set_userdata('promocode_type', $promocode['type']);
			$this->session->set_userdata('promocode_value', $value);
			$this->session->set_userdata('promocode_categories', $promocode['categories']);
		}

		redirect('/checkout');
	}

	/**
	 * Получение общего промокода
	 *
	 * @return mixed
	 * @author ansotov
	 */
	protected function getPromocode($total = false, $types = array())
	{

		$total = ($total) ? $total : $this->total_amount;

		$result = $this->db->query("SELECT *
                                        FROM promocodes p, promocode_categories pc
                                        WHERE p.`min_sum` <= " . $total . "
                                              AND NOW() BETWEEN p.`start_at`
                                              AND (p.`end_date` + INTERVAL 1 DAY)
                                              AND p.id = pc.promocode_id
                                              AND p.`active` = 1
                                        HAVING pc.cat_id  IN(" . "\"" . implode('","', $types) . '"' . ")");

		return $result->result_array();
	}

	/**
	 * Все авторизованные пользователи
	 *
	 * @author ansotov
	 */
	private function getAuthUsers()
	{

		$this->load->model('musers');
		$users = $this->musers->get('`email` != "" ORDER BY id DESC');

		foreach ($users as $k => $v) {
			echo $k + 1 . '. ' . $v['email'] . '<hr>';
		}
	}

	/**
	 * @param array $data
	 *
	 * @return \ApiResponse
	 */
	private function RCCreateOrder($data = array())
	{
		return $this->RCClient()->ordersCreate($data, $this->RCShop);
	}

	/**
	 * Create connection to Retail Crm
	 *
	 * @return \ApiClient
	 */
	private function RCClient()
	{
		return $client = new ApiClient($this->RCUrl, $this->RCApiKey);
	}

	//ansotov отправка данных в мой склад

	private function yandexMarket()
	{
		$search = array(
			"'<script[^>]*?>.*?</script>'si", // Вырезает javaScript
			"'<[\/\!]*?[^<>]*?>'si", // Вырезает HTML-теги
			"'([\r\n])[\s]+'", // Вырезает пробельные символы
			"'&(quot|#34);'i", // Заменяет HTML-сущности
			"'&(amp|#38);'i",
			"'&(lt|#60);'i",
			"'&(gt|#62);'i",
			"'&(nbsp|#160);'i",
			"'&(iexcl|#161);'i",
			"'&(cent|#162);'i",
			"'&(pound|#163);'i",
			"'&(copy|#169);'i",
			"'&(omega|#60);'i",
			"'&(deg|#60);'i",
			"'&(bull|#60);'i",
			"'&(laquo|#60);'i",
			"'&(raquo|#60);'i",
			"'&(ndash);'i",
			"'&(mdash);'i",
			"'&(sup2);'i",
			"'&(sup3);'i",
			"'&(middot);'i",
			"'&(reg);'i",
			"'(\[\[|\]\])'i",
			"'( \|)'i",
			"' &'i",
			"'&#(\d+);'e"
		); // интерпретировать как php-код

		$replace = array(
			"",
			"",
			"\\1",
			"\"",
			"&",
			"<",
			">",
			" ",
			chr(161),
			chr(162),
			chr(163),
			chr(169),
			" Ом",
			" гр.",
			"",
			"*",
			"*",
			"-",
			"-",
			"в кв.",
			"в кубе.",
			"",
			"(R)",
			"",
			": ",
			" и",
			"chr(\\1)"
		);

		$result   = $this->db->query("SELECT
    `p`.`id`                                            AS `product_id`,
    `ps`.`id`                                           AS `variant_id`,
    `p`.`sku`                                           AS `sku`,
    `p`.`url`                                           AS `url`,
    `p`.`name`                                          AS `name`,
    `p`.`content`                                       AS `description`,
    `p`.`front_picture`                                 AS `front_picture`,
    `p`.`front_picture_type`                            AS `front_picture_type`,
    `p`.`type`                                          AS `category_id`,
    if((`ps`.`name` = ''), `ps`.`code`, `ps`.`name`)    AS `size`,
    if(isnull(`ps`.`price`), `p`.`price`, `ps`.`price`) AS `price`,
    `p`.`active`                                        AS `active`
  FROM
    (`products` `p` LEFT JOIN `products_sizes` `ps` ON ((`p`.`id` = `ps`.`product_id`)))");
		$products = $result->result_array();


		header("Content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		$web_url = "https://www.girlsinbloom.ru"; ?>
        <!DOCTYPE yml_catalog SYSTEM "shops.dtd">
        <yml_catalog date="<?= date("Y-m-d H:i") ?>">
            <shop>
                <name>girlsinbloom.ru</name>
                <company>Girls In Bloom</company>
                <url><?= $web_url ?>/</url>

                <currencies>
                    <currency id="RUR" rate="1"/>
                </currencies>

                <categories>
                    <category id="futbolki" parentId="0"><?= preg_replace($search, $replace, 'Футболки'); ?></category>
                    <category
                        id="tolstovki"
                        parentId="0"><?= preg_replace($search, $replace, 'Толстовки'); ?></category>
                    <category
                        id="gotovye-kejsy"
                        parentId="0"><?= preg_replace($search, $replace, 'Чехлы'); ?></category>
                    <category
                        id="oblozhki-na-pasport"
                        parentId="0"><?= preg_replace($search, $replace, 'Обложки на паспорт'); ?></category>
                    <category
                        id="cardholders"
                        parentId="0"><?= preg_replace($search, $replace, 'Визитницы'); ?></category>
                    <category id="boxes" parentId="0"><?= preg_replace($search, $replace, 'Boxes'); ?></category>
                    <category id="shirts" parentId="0"><?= preg_replace($search, $replace, 'Рубашки'); ?></category>
                    <category id="skirts" parentId="0"><?= preg_replace($search, $replace, 'Юбки'); ?></category>
                    <category id="dresses" parentId="0"><?= preg_replace($search, $replace, 'Платья'); ?></category>
                    <category id="hoodie" parentId="0"><?= preg_replace($search, $replace, 'худи'); ?></category>
                    <category id="backpacks" parentId="0"><?= preg_replace($search, $replace, 'Рюкзаки'); ?></category>
                    <category id="berets" parentId="0"><?= preg_replace($search, $replace, 'Береты'); ?></category>
                </categories>

                <offers>
					<?php foreach ($products as $key => $value) : ?>
                        <offer
                            id="<?php echo $value['variant_id'] ?>" available='true'
                            group_id="<?php echo $value['product_id']; ?>">
                            <url><?php echo $web_url ?>/new-products/<?php echo $value['url']; ?>/</url>
                            <code><?php echo $value['sku']; ?></code>
                            <barcode><?php echo $value['sku']; ?></barcode>
                            <price><?php echo $value['price'] ?></price>
                            <salePrice><?php echo $value['price']; ?></salePrice>
                            <currencyId>RUR</currencyId>
                            <categoryId><?php echo $value['category_id']; ?></categoryId>
							<?php if ($value['front_picture']) : ?>
                                <picture><?php echo $web_url; ?>
                                    /img/front_pictures/<?php echo $value['front_picture'] . '.' . $value['front_picture_type']; ?></picture>
							<?php endif; ?>
                            <delivery>true</delivery>
                            <name><?php echo $value['name']; ?></name>
							<?php if ($value['description']) { ?>
                                <description><?= preg_replace($search, $replace, htmlspecialchars($value['description'])); ?></description>
							<?php } ?>
                            <param name="Размер"><?php echo $value['size'] ?></param>
                            <param name="Артикул"><?php echo $value['sku'] ?></param>
                        </offer>
					<?php endforeach; ?>
                </offers>
            </shop>
        </yml_catalog>
		<?php
	}

	private function getCities($query, $limit = 15, $delivery_type = 4, $like = true, $region)
	{
		$region = $region ? $region : 'Московская обл.';

		//$this->freeDelivery($region, $query);

		//ansotov в зависимости от типа запроса выводим либо точное совпадение, либо вхождение из подстроки
		$like_query = ($like == true) ? " AND delivery_cities.name LIKE '%{$query}%' " : " AND delivery_cities.name = '{$query}' ";

		if ($delivery_type == 4) {
			$result = $this->db->query(
				"SELECT delivery_cities.name as name, delivery_cities.region as region, IF({$this->total_amount} < {$this->free_delivery}, delivery.price, 0) as price, delivery.price as delivery_price, delivery.zone as city_zone, delivery.days as days FROM delivery, delivery_cities
                                            WHERE delivery.city_id = delivery_cities.id
                                                  AND delivery.days > 0 
                                                  AND delivery.type = 2
                                                  " . $like_query . "
                                                  LIMIT {$limit}"
			);
			foreach ($result->result_array() as $k => $value) {

				$cities[$k] = array(
					"name"           => $value["name"],
					"region"         => $value["region"],
					"label"          => $value["name"] . ' - ' . $value["region"],
					"price"          => round((($value["price"] * 1.1) - $this->minusSum), -1),
					"delivery_price" => round((($value["delivery_price"] * 1.1) - $this->minusSum), -1),
					"zone"           => $value["city_zone"],
					"days"           => $value["days"]
				);
			}

		} elseif ($delivery_type == 2) {
			$cities = $this->getCouriers($query, $region);
		}

		return $cities;
	}

	private function getCouriers($query, $region)
	{
		//$this->freeDelivery($region, $query);

		$result = $this->db->query("SELECT couriers_cities.name as name, IF({$this->total_amount} < {$this->free_delivery}, couriers.price, 0) as price, couriers.price as delivery_price, couriers.min_days as min_days, couriers.max_days as max_days FROM couriers, couriers_cities
                                            WHERE couriers.city_id = couriers_cities.id
                                              AND couriers.max_days > 0
                                              AND couriers_cities.name = '{$query}'
                                              AND couriers_cities.region = '{$region}'");
		foreach ($result->result_array() as $k => $value) {
			$cities[$k] = array(
				"name"           => $value["name"],
				"price"          => ($value["price"] - $this->minusSum),
				"delivery_price" => ($value["delivery_price"] - $this->minusSum),
				"min_days"       => $value["min_days"],
				"max_days"       => $value["max_days"]
			);
		}

		return $cities;
	}

	/**
	 * Данные по SDEK
	 *
	 * @param $name
	 *
	 * @return array
	 * @author ansotov
	 *
	 */
	private function getSDEKInfo($name, $region)
	{

		//$this->freeDelivery($region, $name);

		$result = $this->db->query(
			"SELECT delivery_cities.name as name, IF({$this->total_amount} < {$this->free_delivery}, delivery.price, 0) as price, delivery.price as delivery_price, delivery.days as days, delivery_cities.id as city_id FROM delivery, delivery_cities
                                            WHERE delivery.city_id = delivery_cities.id
                                                  AND delivery.days > 0 
                                                  AND delivery.type = 1 
                                                  AND delivery_cities.name = '{$name}' 
                                                  AND delivery_cities.region LIKE '{$region}%' 
                                                  GROUP BY delivery_cities.name
                                                  LIMIT 1"
		);

		foreach ($result->result_array() as $k => $value) {
			$cities = array(
				"price"          => round((($value["price"] * 1.1) - $this->minusSum), -1) * 2,
				"city_id"        => $value["city_id"],
				"delivery_price" => round((($value["delivery_price"] * 1.1) - $this->minusSum), -1) * 2,
				"min_days"       => 1,
				"max_days"       => round(($value["days"] / 2)),
				"days"           => round(($value["days"] / 2)),
			);
		}

		//Выводим JSON на страницу
		return $cities;
	}

	/**
	 * Порльзователь с адресом
	 *
	 * @param $id
	 *
	 * @return mixed
	 * @author ansotov
	 *
	 */
	private function getUser($id)
	{
		$user = $this->musers->get(array("active" => 1, 'id' => $id), array(), 1);

		$this->load->model('maddresses');
		if ($user) {
			$user['address'] = $this->maddresses->get(array("active" => 1, 'user_id' => $id), array(), 1);
		}

		return $user;
	}

	/**
	 * Данные по pickpoint
	 *
	 * @param $name
	 *
	 * @return array
	 * @author ansotov
	 *
	 */
	private function getPickpointInfo($name, $region)
	{

		//$this->freeDelivery($region, $name);

		$result = $this->db->query(
			"SELECT delivery_cities.name as name, IF({$this->total_amount} < {$this->free_delivery}, delivery.price, 0) as price, delivery.price as delivery_price, delivery.days as days, delivery_cities.id as city_id, delivery.only_online FROM delivery, delivery_cities
                                            WHERE delivery.city_id = delivery_cities.id
                                                  AND delivery.days > 0 
                                                  AND delivery.type = 1 
                                                  AND delivery_cities.name = '{$name}' 
                                                  AND delivery_cities.region LIKE '{$region}%' 
                                                  GROUP BY delivery_cities.name
                                                  LIMIT 1"
		);

		foreach ($result->result_array() as $k => $value) {
			$cities = array(
				"price"          => round((($value["price"] * 1.1) - $this->minusSum), -1),
				"city_id"        => $value["city_id"],
				"delivery_price" => round((($value["delivery_price"] * 1.1) - $this->minusSum), -1),
				"days"           => $value["days"],
				"only_online"    => $value["only_online"],
			);
		}

		//Выводим JSON на страницу
		return $cities;
	}

	/**
	 * Пагинация
	 *
	 * @param $page
	 * @param $pages
	 * @param $url
	 *
	 * @return bool|string
	 * @author ansotov
	 *
	 */
	private function getPagination($page, $pages, $url)
	{

		$this->page  = $page;
		$this->pages = $pages;

		//ansotov если страница одна то дальше не идём
		if ($this->pages <= 1) {
			return false;
		}

		//ansotov основная ссылка для перехода
		$link    = $url;
		$add_end = false;

		$this->page = ($this->page == 0) ? 1 : $this->page;

		if ($this->pages > 10) {
			$f_start = 1;
			$f_end   = 9;
			$add_end = "<li>...</li><li><a href=\"" . $link . "?page={$this->pages}{$this->getUrlParams()}\" rel=\"nofollow\">{$this->pages}</a></li>";
		} else {
			$f_start = 1;
			$f_end   = $this->pages;
		}


		$str = false;

		if ($this->page >= 3 && $this->pages > 10) {
			if ($this->page < 6) {
				$f_start          = 1;
				$f_end            = 9;
				$add_start_status = 0;
			}
			if ($this->page == 6) {
				$f_start          = $this->page - 5;
				$f_end            = $this->page + 4;
				$add_start_status = 0;
			}
			if ($this->page > 6) {

				for ($i = 1; $i <= 1; $i++) {
					$str .= '<li>';
					if ($i == $this->page) {
						$str .= "<span>{$i}</span> ";
					} else {
						if ($i == 1) {
							$str .= "<a href=\"" . $link . $this->getUrlParams('?') . "/\">{$i}</a>";
						} else {
							$str .= "<a href=\"" . $link . "?page={$i}{$this->getUrlParams()}\">{$i}</a>";
						}
					}
					$str .= '</li>';
				}


				$str              .= '<li>...</li>';
				$f_start          = $this->page - 4;
				$f_end            = $this->page + 4;
				$add_start_status = 1;
			}
			if ($this->page > $this->pages - 8) {
				$f_start          = $this->pages - 8;
				$f_end            = $this->pages;
				$add_start_status = 1;
				$add_end          = "";
			}
			$add_start = "<a href=\"" . $link . $this->getUrlParams('?') . "\">1</a>...";
		}

		for ($i = $f_start; $i <= $f_end; $i++) {
			$str .= '<li>';
			if ($i == $this->page) {
				$str .= "<span>{$i}</span> ";
			} else {
				if ($i == 1) {
					$str .= "<a href=\"" . $link . $this->getUrlParams('?') . "\">{$i}</a>";
				} else {
					$str .= "<a href=\"" . $link . "?page={$i}{$this->getUrlParams()}\">{$i}</a>";
				}
			}
			$str .= '</li>';
		}

		$str .= $add_end;

		return $str;
	}

	// Get user emails

	/**
	 * Параметры для url
	 *
	 * @param $data
	 *
	 * @return bool|string
	 * @author ansotov
	 *
	 */
	private function getUrlParams($sign = '&')
	{
		$result = false;

		foreach ($_REQUEST as $key => $value) {
			foreach ($value as $k => $v) {
				$result .= ($k > 0) ? '&' : false;
				$result .= $key . '[]=' . $v;
			}
		}

		return ($result) ? $sign . $result : false;
	}

	/**
	 * Получение скидки
	 *
	 * @param $data
	 *
	 * @return float
	 * @author ansotov
	 *
	 */
	public function getDiscountSum($data)
	{
		if ($data['discount_type'] == 1) {
			return round(($data['price'] - ($data['price'] * ($data['discount'] / 100))));
		} else {
			return round($data['price'] - $data['discount']);
		}
	}

	protected function session($type)
	{
		if ($type == 'facebook' || $type == 'vkontakte') {
			$this->load->helper('url_helper');
			//$this->load->library('oauth2/0.3.1');
			$auth_array = array();


			switch ($type) {
				case "facebook":
					$auth_array = array(
						'id'     => '1850662381876707',
						'secret' => '1d34f9e257235589918ff2cf8e8d8ce6',
					);
					break;
				case "vkontakte":
					$auth_array = array(
						'id'     => '3255164',
						'secret' => 'wUHKamMGpZXqmbKBw3mK',
					);
					break;
			}
			$provider = $this->oauth2->provider($type, $auth_array);
			if ( ! $this->input->get('code')) {
				// By sending no options it'll come back here
				$provider->authorize();
			} else {

				// Howzit?
				try {
					$token = $provider->access($_GET['code']);

					$user = $provider->get_user_info($token);

					//check if it's in database
					if ($site_user = $this->musers->get(array("oauth_id " => $user['uid'], "oauth_type" => $type), array(), 1)) {
						$this->set_session($site_user);

						return true;
					} else {
						$this->register_user($type, $user);

						return true;
					}


					echo "<pre>Tokens: ";
					var_dump($token);

					echo "\n\nUser Info: ";
					var_dump($user);
				} catch (OAuth2_Exception $e) {
					show_error('That didnt work: ' . $e);
				}

			}
		} elseif ($type == 'manual') {
			$email    = $this->input->post("email");
			$password = $this->input->post("password");
			if ($user = $this->musers->get(array("active" => 1, "email" => $email, "password" => md5($password)), array(), 1)) {
				$this->set_session($user);

				return;
			} else {
				$this->mysmarty->assign('email', $email);
				$this->mysmarty->assign("error", "Не найдена запись с таким cочетанием email и пароля");
			}
		}

	}

	protected function register_user($type, $user)
	{
		$insert_user = array();
		switch ($type) {
			case 'vkontakte':

				$insert_user = array(
					'oauth_id'       => $user['uid'],
					'oauth_type'     => $type,
					'oauth_nickname' => iconv('utf-8', 'cp1251', $user['nickname']),
					'first_name'     => iconv('utf-8', 'cp1251', $user['first_name']),
					'last_name'      => iconv('utf-8', 'cp1251', $user['last_name']),
					'oauth_picture'  => $user['image'],
					'active'         => 1
				);
				break;
			case 'facebook':

				$insert_user = array(
					'oauth_id'       => $user['uid'],
					'oauth_type'     => $type,
					'oauth_nickname' => $user['nickname'],
					'first_name'     => $user['first_name'],
					'last_name'      => $user['last_name'],
					'email'          => $user['email'],
					'oauth_picture'  => $user['image'],
					'oauth_profile'  => $type == 'facebook' ? $user['urls']['Facebook'] : '',
					'active'         => 1
				);

				break;
		}
		$this->musers->insert($insert_user);
		$user = $this->musers->get(array("oauth_id" => $user['uid'], "oauth_type" => $type), array(), 1);
		$this->set_session($user);

		return true;

	}

	protected function check_val($product_val, $field_value, $product_id)
	{
		$found = false;
		foreach ($field_value as $k => $v) {
			if ($product_val == $v) {
				$found = true;
			}
		}

		return $found;
	}

	protected function check_field($fields_array, $field_value, $product_id)
	{
		$found = false;
		foreach ($field_value as $k => $v) {
			if (isset($fields_array[$v])) {
				$found = true;
			}
		}

		return $found;
	}

	/**
	 * Записываем минимальную сумму для бесплатной доставки
	 *
	 * @author ansotov
	 */
	private function setFreeDelivery()
	{
		$this->load->model('mfree_delivery');
		$free_delivery = $this->mfree_delivery->get(array("active" => 1, 'const' => 'ALL_DAYS'), array(), 1);

		if ($free_delivery) {
			if (($free_delivery['start_time'] <= date('H:i:s')) && ($free_delivery['end_time'] <= date('H:i:s'))) {
				$this->db->query("UPDATE `main_page` SET `free_delivery` = 500");
			} else {
				$this->db->query("UPDATE `main_page` SET `free_delivery` = 5000");
			}
		} //else
		//$this->db->query("UPDATE `main_page` SET `free_delivery` = 5000");
	}

	private function getInTotal()
	{
		$cart         = $this->build_cart($this->cart->contents());
		$total_sum    = 0;
		$total_number = 0;
		if ($cart) {
			foreach ($cart as $k => $v) {
				$cart[$k]['price_sum'] = $v['price'] * $v['qty'];

				//ansotov бесплатный экземпляр по акции
				if ($v['in_stock']) {
					$cart[$k]['price_sum'] = $cart[$k]['price_sum'] - ((($v['price'] / 100) * $v['in_stock_discount']) * $v['in_stock_qty']);
				}

				$total_sum    = $total_sum + $cart[$k]['price_sum'];
				$total_number = $total_number + $v['qty'];
			}
		}

		$this->mysmarty->assign('total_number', $total_number);
		$this->mysmarty->assign("total_sum", $this->total_amount);
		$this->mysmarty->assign('cart', $cart);

		$template = $this->mysmarty->fetch('new_bloom/ru/ajax/cart-in-total.tpl');
		echo json_encode(array('template' => $template));
		exit;
	}

	private function freeDelivery($region, $query)
	{
		$this->free_delivery = (in_array($region, array('Московская обл.', 'Московская', 'Ленинградская обл.')) && ! in_array($query, array('Санкт-Петербург', 'Санкт-Петербург - Ленинградская обл.', 'Москва', 'Москва - Московская обл.'))) ? 99999999999 : $this->free_delivery;
	}

	/**
	 * Получение товаров по типу
	 *
	 * @param $type
	 *
	 * @return mixed
	 * @author ansotov
	 *
	 */
	private function getItemsByType($type)
	{
		$result = $this->db->query("SELECT `id` FROM products WHERE `type` = '{$type}'");

		return $result->result_array();
	}

	/**
	 * Проверка на существование записи для типа размера
	 *
	 * @param $id
	 * @param $code
	 *
	 * @return mixed
	 * @author ansotov
	 *
	 */
	private function getIssetItemByType($id, $code)
	{
		$result = $this->db->query("SELECT `id` FROM products_sizes WHERE `product_id` = '{$id}' AND `code` = '{$code}'");

		return $result->result_array();
	}

	/**
	 * Товары
	 *
	 * @return array|bool
	 * @author ansotov
	 */
	private function getProducts()
	{

		$this->load->model('mproducts');

		$products = $this->mproducts->get(array('active' => 1), array(), 0,/*"CAST(code as DECIMAL) ASC"*/
			"order_id ASC");

		if (isset($products) && $products) {

			$products_arr = array();

			foreach ($products as $key => $value) {

				$products_arr[$key] = $value;

				//ansotov проверка на последний размер
				$this->load->model('mproducts_sizes');
				$sizes = $this->mproducts_sizes->get(array('product_id' => $value['id']), array(), 0,/*"CAST(code as DECIMAL) ASC"*/
					"order_id ASC");

				foreach ($sizes as $k => $v) {
					if ($v['last_size'] == 1) {
						$products_arr[$key]['last_size'] = 1;
					}
				}
			}

			return $products_arr;
		}

		return false;
	}

	/**
	 * Запись заказа в "мой склад"
	 *
	 * @param $data
	 *
	 * @return mixed
	 * @author ansotov
	 *
	 */
	private function setMoySkladOrder($data)
	{

		$setup_curl = $this->setupCurl($this->constMoySkladInit());


		$consts = $this->constMoySkladInit();

		$attributes = $this->setCurl(
			$setup_curl,
			$consts[MOYSKLAD_API_URL] . $consts[MOYSKLAD_GET_METADATA],
			$consts[MOYSKLAD_GET_METADATA_METHOD]);

		$attr_data = $this->getMoySkladData($attributes);

		$name          = $data['first_name'] . ' ' . $data['last_name'];
		$phone         = $data['phone'];
		$address       = 'г.' . $data['city'] . ' ' . $data['address'];
		$index         = ($data['zipcode']) ? $data['zipcode'] : '';
		$delivery_sum  = $data['delivery_sum'];
		$email         = $data['email'];
		$payment_type  = $data['p_type'];
		$delivery_type = $data['d_type'];
		$comment       = $data['comment'];

		$materials = '';

		foreach ($data['products_info'] as $key => $value) {
			if ($value['material']) {
				$materials .= $value['variant_id'] . ': ' . $value['material'] . ';' . "\n";
			}
		}

		$attr_arr = array();
		foreach ($attr_data['attributes'] as $key => $value) {
			$attr_arr[$key]['id'] = $value['id'];

			if ($value['name'] == 'Имя') {
				$attr_arr[$key]['value'] = (string)$name;
			} elseif ($value['name'] == 'Телефон') {
				$attr_arr[$key]['value'] = (string)$phone;
			} elseif ($value['name'] == 'Стоимость доставки') {
				$attr_arr[$key]['value'] = (int)$delivery_sum;
			} elseif ($value['name'] == 'Адрес') {
				$attr_arr[$key]['value'] = (string)$address;
			} elseif ($value['name'] == 'Индекс') {
				$attr_arr[$key]['value'] = (string)$index;
			} elseif ($value['name'] == 'Email') {
				$attr_arr[$key]['value'] = (string)$email;
			} elseif ($value['name'] == 'Тип оплаты') {
				$attr_arr[$key]['value'] = (string)$payment_type;
			} elseif ($value['name'] == 'Тип доставки') {
				$attr_arr[$key]['value'] = (string)$delivery_type;
			} elseif ($value['name'] == 'Материал') {
				$attr_arr[$key]['value'] = (string)$materials;
			} elseif ($value['name'] == 'Комментарий') {
				$attr_arr[$key]['value'] = (string)$comment;
			} else {
				$attr_arr[$key]['value'] = '';
			}
		}

		$products = array();

		$i = 0;

		foreach ($data['products'] as $k => $v) {

			$variant = $this->setCurl(
				$setup_curl,
				$consts[MOYSKLAD_API_URL] . $consts[MOYSKLAD_GET_VARIANTS] . '/' . $v['moysklad_id'],
				$consts[MOYSKLAD_GET_METADATA_METHOD]);

			$isset_variant = $this->getMoySkladData($variant);

			$prod_type = ($isset_variant['errors']) ? 'product' : 'variant';

			$products[$i] = array(
				'quantity'   => (int)$v['qty'],
				'price'      => (int)($v['price'] * 100),
				'discount'   => 0,
				'assortment' => array(
					'meta' => array(
						'href'      => 'https://online.moysklad.ru/api/remap/1.1/entity/' . $prod_type . '/' . $v['moysklad_id'] . '',
						'type'      => '' . $prod_type . '',
						'mediaType' => 'application/json'
					)
				)
			);
			$i++;
		}

		$textAddCustomerOrder = '
            {
              "name": "' . $data['order_number'] . '",
              "organization": {
                "meta": {
                  "href": "https://online.moysklad.ru/api/remap/1.1/entity/organization/b53f16f5-eecf-11e6-7a69-9711000f1aba",
                  "type": "organization",
                  "mediaType": "application/json"
                }
              },
              "moment": "' . date('Y-m-d H:i:s') . '",
              "applicable": false,
              "vatEnabled": false,
              "agent": {
                "meta": {
                  "href": "https://online.moysklad.ru/api/remap/1.1/entity/counterparty/1b81e0b7-0c1f-11e7-7a34-5acf000dd7c7",
                  "type": "counterparty",
                  "mediaType": "application/json"
                }
              },
              "positions": ' . json_encode($products) . ',
              "attributes": ' . json_encode($attr_arr) . '
            }
            ';

		$order = $this->setCurl(
			$setup_curl,
			$consts[MOYSKLAD_API_URL] . $consts[MOYSKLAD_ADD_CUSTOMER_ORDER],
			$consts[MOYSKLAD_ADD_CUSTOMER_ORDER_METHOD]);

		curl_setopt($order, CURLOPT_POSTFIELDS, $textAddCustomerOrder);
		curl_setopt($order, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($textAddCustomerOrder)
			)
		);

		return $this->setCustomerOrder($order);
	}

	private function setCustomerOrder($curlObject)
	{
		$response = $this->curlExec($curlObject);
		$data     = json_decode($response, true);

		return $data['id'];
	}

	/**
	 * Удвление персонального промокода
	 *
	 * @param $value
	 *
	 * @return mixed
	 * @author ansotov
	 *
	 */
	private function delPersonalPromocode($value)
	{
		return $this->db->query('DELETE FROM `personal_promocodes` WHERE `value` = "' . $value . '"');
	}

	/**
	 * Получение данных по персональному промокоду
	 *
	 * @param $sum
	 *
	 * @return bool
	 * @author ansotov
	 *
	 */
	private function getPersonalPromocodeData($sum)
	{
		//ansotov получаем данные по промокоду
		$query     = $this->db->query('SELECT * FROM `pp_types` WHERE `min_sum` <= ' . $sum . ' AND `active` = 1 ORDER BY `min_sum` DESC LIMIT 1');
		$promocode = $query->row();

		if ( ! $promocode) {
			return false;
		}

		a:
		//ansotov генерим промокод
		$promocode_val = $this->passwordGenerator();
		$result        = ($this->db->query("INSERT INTO `personal_promocodes` SET `value` = '" . $promocode_val . "', `discount` = " . $promocode->value . ', `type` = "' . $promocode->discount_type . '"')) ? true : false;

		//ansotov подстраховка, если запись не произошла
		if ($result == false) {
			goto a;
		}

		return array('value' => $promocode_val, 'discount' => $promocode->value, 'type' => $promocode->discount_type);
	}
}
