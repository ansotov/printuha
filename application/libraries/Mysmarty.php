<?php if (!defined('APPPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'libraries/smarty31/Smarty.class.php');

/**
* @file system/application/libraries/Mysmarty.php
*/
class Mysmarty extends Smarty
{
	function __construct()
	{
		parent::__construct();

		$this->error_reporting = 1;
		$config =& get_config();



		// absolute path prevents "template not found" errors
		$this->template_dir = APPPATH . 'views/';
		//echo $this->template_dir;
		$this->compile_dir  = APPPATH . 'cache/'; //use CI's cache folder

		//$this->debugging = true;
		if (function_exists('site_url')) {
    		// URL helper required
			$this->assign("site_url", site_url()); // so we can get the full path to CI easily
		}

	}

	/**
	* @param $resource_name string
	* @param $params array holds params that will be passed to the template
	* @desc loads the template
	*/

	function view($resource_name, $params = array())   {
		if (strpos($resource_name, '.') === false) {
			$resource_name .= '.tpl';
		}
		if (is_array($params) && count($params)) {
			foreach ($params as $key => $value) {
				$this->assign($key, $value);
			}
		}
		// check if the template file exists.
		if (!is_file($this->template_dir[0] . $resource_name)) {
			show_error("template: [$resource_name] cannot be found.");
		}
		return parent::display($resource_name);
	}

	function fetch($resource_name, $params = array())   {
		if (strpos($resource_name, '.') === false) {
			$resource_name .= '.tpl';
		}
		if (is_array($params) && count($params)) {
			foreach ($params as $key => $value) {
				$this->assign($key, $value);
			}
		}
		// check if the template file exists.
		if (!is_file($this->template_dir[0] . $resource_name)) {
			show_error("template: [$resource_name] cannot be found.");
		}
		return parent::fetch($resource_name);
	}
} // END class smarty_library
?>
