<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


include_once $_SERVER['DOCUMENT_ROOT'] . '/application/controllers/includes/Sitemap.php';

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	private static $instance;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		self::$instance =& $this;
		
		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');

		$this->load->initialize();
		
		log_message('debug', "Controller Class Initialized");
	}

	public static function &get_instance()
	{
		return self::$instance;
	}

	protected function sitemap()
	{
		Sitemap::getInstance()->set_ignore(array("javascript:", ".css", ".js", ".ico", ".jpg", ".png", ".jpeg", ".swf", ".gif"));

		Sitemap::getInstance()->get_links('http://' .$_SERVER['SERVER_NAME']);
		$map = Sitemap::getInstance()->generate_sitemap();

		$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/sitemap.xml", "w");
		fwrite($fp, $map);
	}

    public function send_mail($user_name,$user_mail,$subject,$body,$from_mail='',$from_name='',$options = array()){
        $this->load->library('mailer');

        $this->mailer->Subject =$this->mailer->mime_header_encode($subject);

        if($options!=array()){
            if(isset($options['smtp_username'])){
                $this->mailer->Username = $options['smtp_username'];
            }
        }
        if($from_mail!=''){
            $this->mailer->From = $from_mail;
        }
        @$body = iconv('utf-8', 'KOI8-R', $body);
        $this->mailer->Body = $body;
        $this->mailer->AddAddress($user_mail, $this->mailer->mime_header_encode($user_name));


        $this->mailer->IsHTML(true);
        if($from_mail!='')
            $this->mailer->From = $from_mail;
        if($from_name!='')
            $this->mailer->FromName = $this->mailer->mime_header_encode($from_name);


        @$this->mailer->Send();
        $this->mailer->ClearAddresses();
        $this->mailer->ClearAttachments();

    }
}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */
