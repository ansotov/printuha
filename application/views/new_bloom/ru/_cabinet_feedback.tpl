<form action="" method="post">
  {if $success}<label class="error">{translateConst const = YOUR_MESSAGE_SEND}</label>{/if}
  <input type="hidden" name="send" value="1">
  <p id="bemail" class="field">
    <strong><label for="topic">{translateConst const = SUBJECT}</label></strong>
    <input type="text" value="{$postdata.topic}" size="30" name="topic" id="order_bill_address_attributes_email" class="required">
    {if $errors.email}<label class="error">{$errors.topic}</label>{/if}
  </p>
  <p id="bcontent" class="field sp_field">
    <strong><label for="content">{translateConst const = MESSAGE}</label></strong>
    <textarea name="content">{$postdata.content}</textarea>
    {if $errors.content}<label class="error">{$errors.content}</label>{/if}
  </p>
  <p class="field"><span class="area" style="overflow:visible;"><button class="button">{translateConst const = SEND}</button></span></p>
</form>
