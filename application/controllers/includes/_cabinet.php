<?php
if($url1=='cabinet'){
  if($this->user_id==0){
    $this->show_404();
    return;
  }
  $type="cabinet";
  $this->lang->load('form_validation', 'russian');
  if($url2=='' || $url2=='personal'){
    $send = intval($this->input->post('send'));
    $data = $this->user_info;
    $this->mysmarty->assign('postdata',$data);
      $this->mysmarty->assign('include_cabinet',"new_bloom/ru/_cabinet_personal.tpl");
    if($send == 1){

        $data = $this->musers->get(array('id' => $this->user_id),array(),1);

        if ($data['email'] == '')
            $this->form_validation->set_rules('email', 'Email','trim|required|max_length[200]|alpha_dot_dash|is_unique[users.email]');

      $this->form_validation->set_rules('first_name', 'Имя','trim|required|max_length[200]|alpha_dot_dash');
      $this->form_validation->set_rules('last_name', 'Фамилия','trim|required|max_length[200]|alpha_dot_dash');
      $this->form_validation->set_rules('gender', 'Пол','trim|max_length[200]|alpha_dot_dash');
      $this->form_validation->set_rules('birthdate', 'Дата рождения','trim|max_length[200]|alpha_dot_dash');
      $this->form_validation->set_rules('phone', 'Телефон','trim|min_length[5]|max_length[90]');
      $this->form_validation->set_rules('subscribe', 'Телефон','trim|max_length[1]');

      if ($this->form_validation->run() == FALSE)
      {
        $postdata = $this->form_validation->get_postdata();
        $errors = $this->form_validation->get_errors();
        $this->mysmarty->assign('errors',$errors);
        $postdata['email'] = $this->user_info['email'];
        $this->mysmarty->assign('postdata',$postdata);
      }
      else
      {
        $data = $this->form_validation->get_postdata();
        $order_id = $this->musers->update($data,array('id'=>$this->user_id));
        $data['email'] = $this->user_info['email'];
        $this->mysmarty->assign('postdata',$data);
        $this->mysmarty->assign('success',1);
        redirect('/cabinet/personal/');
      }
    }
  }elseif($url2=='addresses'){
    $this->load->model('maddresses');
    if($url3==''){
        //ansotov редактирование главного адреса
        $select_address = intval($this->input->post('select_address'));
        if($select_address != 0 && ($address = $this->maddresses->get(array('id'=>$select_address,'user_id'=>$this->user_id),array(),1))){
            $this->maddresses->update(array('active'=>0),array('user_id'=>$this->user_id));
            $this->maddresses->update(array('active'=>1),array('user_id'=>$this->user_id,'id'=>$select_address));
            echo json_encode(array('success'=>1));
            exit;
        }
        //ansotov удаление адреса
        $delete_address = intval($this->input->post('delete_address'));
        if($delete_address != 0 && ($address = $this->maddresses->get(array('id' => $delete_address, 'user_id' => $this->user_id),array(),1))){
            $this->maddresses->delete(array('user_id' => $this->user_id,'id' => $delete_address));
            echo json_encode(array('success'=>1));
            exit;
        }
      $addresses  = $this->maddresses->get(array('user_id'=>$this->user_id),array(),0,'order_id DESC');
      $this->mysmarty->assign('addresses',$addresses);
      $this->mysmarty->assign('include_cabinet',"new_bloom/ru/_cabinet_addresses.tpl");
    }elseif($url3=='add' ||
            ($address = $this->maddresses(array('id'=>intval($url3),'user_id'=>$this->user_id,array(),1))) ){

        //ansotov проверка на существование ещё адресов у пользователя
        $isset_address = $this->maddresses->get(array('user_id'=>$this->user_id),array());



      $this->form_validation->set_rules('city', 'Город','trim|required|max_length[200]|alpha_dot_dash|required');
      $this->form_validation->set_rules('street', 'Улица','trim|required|max_length[200]|alpha_dot_dash|required');
      $this->form_validation->set_rules('house', 'Дом','trim|required|max_length[200]|alpha_dot_dash|required');
      $this->form_validation->set_rules('flat', 'Квартира','trim|required|max_length[200]|alpha_dot_dash|required');
      $this->form_validation->set_rules('zipcode', 'Индекс','trim|required|max_length[200]|alpha_dot_dash|required');
      if(isset($address)){
        $this->mysmarty->assign('postdata',$address);
      }
      $send = intval($this->input->post('send'));
      if ($this->form_validation->run() == FALSE)
      {
        $postdata = $this->form_validation->get_postdata();
        $errors = $this->form_validation->get_errors();
        $this->mysmarty->assign('errors',$errors);
        $this->mysmarty->assign('postdata',$postdata);
      }
      else
      {
        $data = $this->form_validation->get_postdata();

        if (!$isset_address)
            $data['active'] = 1;

        if($url3=='add'){
          $data['user_id'] = $this->user_id;
          $this->maddresses->insert($data);
          redirect('/cabinet/addresses/');
        }else {
          $this->maddresses->update($data,array('id'=>intval($url3)));
        }
        $this->mysmarty->assign('postdata',$data);
      }

      $this->mysmarty->assign('include_cabinet',"new_bloom/ru/_cabinet_address.tpl");
    }
  }elseif($url2=='feedback'){
    $send = intval($this->input->post('send'));
    if($send == 1){

      $this->form_validation->set_rules('topic', 'Тема','trim|required|max_length[200]|required|alpha_dot_dash');
      $this->form_validation->set_rules('content', 'Сообщение','trim|required|max_length[5000]|required|alpha_dot_dash');

      if ($this->form_validation->run() == FALSE)
      {
        $postdata = $this->form_validation->get_postdata();
        $errors = $this->form_validation->get_errors();
        $this->mysmarty->assign('errors',$errors);
        print_r($errors);
        exit;
        $postdata['email'] = $this->user_info['email'];
        $this->mysmarty->assign('postdata',$postdata);
      }
      else
      {
        $data = $this->form_validation->get_postdata();

        $this->mysmarty->assign('postdata',$data);


        $headline= "Обратная связь от пользователя ".$this->user_info['email'];
        $this->mysmarty->assign('user_info',$this->user_info);
        $template = $this->mysmarty->fetch('new_bloom/ru/feedback_mail.tpl');
        $this->send_mail('info@girlsinbloom.ru','info@girlsinbloom.ru',$headline,$template);

        $this->mysmarty->assign('success',1);
      }
    }
    $this->mysmarty->assign('include_cabinet',"new_bloom/ru/_cabinet_feedback.tpl");
  }elseif($url2=='orders'){
    if($url3!='' && ($order = $this->morders->get(array('number'=>$url3,
                                                        'email'=>$this->user_info['email'])
                                                        ,array(),1))){
      $this->mysmarty->assign('order',$order);
      $this->load->model('morders_products');
      $cart = $this->morders_products->get(array('user_order_id'=>$order['id']),array(),0,'order_id DESC');
      $query = "";
      $query2 = "";
      $total_qty = 0;
      foreach($cart as $k=>$v){

				$query .= ($query=='')?"id = ".$v['product_id']:" OR id = ".$v['product_id'];
				$query2 .= ($query2=='')?"(product_id = ".$v['product_id']." AND code='".$v['size']."')":" OR (product_id = ".$v['product_id']." AND code='".$v['size']."')";
        $total_qty += $v['qty'];
			}
      $this->load->model('mmaterials');
			$this->load->model('mproducts');
			$data = $this->mproducts->get($query,array(),0);
			$products = array();

			foreach($data as $k=>$v){
				$products[$v['id']] = $v;
			}


      $data = $this->mproducts_sizes->get($query2,array(),0);
      $products_sizes = array();

      if($data)foreach($data as $k=>$v){
        $products_sizes[$v['product_id'].'_'.$v['code']] = $v;
      }
      $materials = array();
      $data = $this->mmaterials->get(array(),array(),0);
      if($data)foreach($data as $k=>$v){
        $materials[$v['name']] = $v;
      }
      $pp_discount_products = array();


      foreach($cart as $k=>$v){

        $cart[$k]['info'] = $products[$v['product_id']];
        @$cart[$k]['size_info']= $products_sizes[$v['product_id'].'_'.$v['size']];
        if(isset($v['material']) && isset($materials[$v['material']])){
          $cart[$k]['material'] = $materials[$v['material']];
        }
      }


      $this->mysmarty->assign('products',$cart);
      $this->mysmarty->assign('include_cabinet',"new_bloom/ru/_cabinet_order.tpl");
    }elseif($url3==''){
      $orders = $this->morders->get(array('email'=>$this->user_info['email']),array("*","DATE_FORMAT(date,'%d.%m.%Y %H:%i') show_date"),0,'date DESC');
      $this->mysmarty->assign('orders',$orders);
      $this->mysmarty->assign('include_cabinet',"new_bloom/ru/_cabinet_orders.tpl");
    }else{
      $this->show_404();
      return;
    }

  }else{
    $this->show_404();
    return;
  }
}
?>
