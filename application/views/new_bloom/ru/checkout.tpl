<script type="text/javascript" src="https://pickpoint.ru/select/postamat.js"></script>

<script>

    function number_format(number, decimals, dec_point, thousands_sep) {	// Format a number with grouped thousands
        //
        // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +	 bugfix by: Michael White (http://crestidg.com)

        var i, j, kw, kd, km;

        // input sanitation & defaults
        if (isNaN(decimals = Math.abs(decimals))) {
            decimals = 2;
        }
        if (dec_point == undefined) {
            dec_point = ",";
        }
        if (thousands_sep == undefined) {
            thousands_sep = ".";
        }

        i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

        if ((j = i.length) > 3) {
            j = j % 3;
        } else {
            j = 0;
        }

        km = (j ? i.substr(0, j) + thousands_sep : "");
        kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
        //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
        kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


        return km + kw + kd;
    }

    function pickpoint_result(result) {

        //ansotov убираем информацию под пунктом доставки
        $('#pickpoint-infos').hide();
        var region_arr = result['region'].split(' ');
        console.log();

        $.ajax({
            url: '/pickpoint-info',
            type: 'POST',
            data: {
                name: result['cityname'],
                region: region_arr[0]
            },
            success: function (data) {

                var info = $.parseJSON(data);

                $('#pickpoint_days, #pickpoint_price').fadeIn(500);

                $('#pickpoint_days span').html(info.days);
                $('#pickpoint-price').html(info.price);

                if (info.price > 0) {
                    $('#pickpoint_price').show(300).html(' - ' + info.price + ' {translateConst const = RUB}');
                } else {
                    $('#pickpoint_price').html(' - {translateConst const = FREE}');
                }

                change_pickpoint_form(result);
            }
        });
    }

    function productQty(id, type) {
        var delivery = $('.delivery-type').val();
        var foreign_lands = $('input[name="foreign_lands"]').val();

        //ansotov если другая страна
        if (foreign_lands != 1)
            $('#delivery-types, #order-info, #order-form').hide();

        $('#search_city_text').val('');

        if (type == 1)
            increase_p(id, delivery, foreign_lands);
        if (type == 0)
            decrease_p(id, delivery, foreign_lands);
    }

    function check_delivery(val) {

        change_delivery(val);
        //ansotov показываем блок данных для оформления заказа
        $('#order-info, #order-form').show();

        //ansotov если pickpoint
        if (val == 3) {
            PickPoint.open(pickpoint_result);
        }
    }

</script>

<div class="wrapper">

	{include file="new_bloom/ru/info_block.tpl"}
	<div class="top_banner">{include file="new_bloom/ru/top_banner.tpl"}</div>
	{if $total_sum}
		<article class="checkout clearfix">

			<input type="hidden" name="data_first_name">
			<input type="hidden" name="data_last_name">
			<input type="hidden" name="data_phone">
			<input type="hidden" name="data_email">
			<input type="hidden" name="data_street">
			<input type="hidden" name="data_house">
			<input type="hidden" name="data_flat">
			<input type="hidden" name="data_zipcode">
			<input type="hidden" name="data_comments">

			{if $smarty.get.step != 2}
				{include file="new_bloom/ru/ajax/promocode.tpl"}
			{/if}

			<form action="{if $smarty.get.step != 2}/checkout/?step=2{/if}" method="post"{if $smarty.get.step != 2} novalidate{/if}>
				<input type="hidden" name="send" value="1">
				{if $only_online}
					<div class="cart_info_banner"></div>
				{/if}
				<!-- ansotov товары в корзине -->
				<div id="update-cart" class="right-column"{if $smarty.get.step == 2} style="display: none" {/if}>
					{include file="new_bloom/ru/_cart.tpl"}
				</div>

				<!--<div class="divider" style="height: 25px"></div>-->

				{if $smarty.get.step != 2}
					<div style="margin: -40px 0 0 0"></div>
				{/if}

				<div class="tabs" style="{if $smarty.get.step != 2}display: none;{/if}min-height: auto">
					<!--<ul>
						<li class="name first"
							onclick="change_delivery_types(0)">{translateConst const = YOUR_RUSSIA_CITY}</li>
						<li class="name second"
							onclick="change_delivery_types(1)">{translateConst const = ANOTHER_COUNTRY}</li>
					</ul>-->
					<div style="margin: 14px 0 -2px 0" class="lands-tabs-wrapper">
						<div style="text-align: center">
							<div class="preloader-wrapper first">
								<img src="/img/preloader.gif" class="lands-preloder">
							</div>
							<div id="search_city">
								{include file="new_bloom/ru/ajax/search_city.tpl"}
							</div>
						</div>
						<div style="text-align: center;display: block">
							<input type="hidden" name="foreign_lands" value="0">
							<div class="preloader-wrapper">
								<img src="/img/preloader.gif" class="lands-preloder">
							</div>
						</div>
					</div>
				</div>

				{if $smarty.get.step == 2}
					<!-- ansotov типы доставки -->
					<div id="delivery-types">
						{include file="new_bloom/ru/ajax/delivery_types.tpl"}
					</div>

					<div id="order-info">
						<!-- ansotov данные для оформления заказа -->
						<div id="order-form">
							{include file="new_bloom/ru/ajax/order_form.tpl"}
						</div>

						<!--<div class="right-banner">{if $main_page.checkout_banner}<img alt="{translateConst const = SHOP_GIRLS_IN_BLOOM}"
								src="{if $smarty.get.step != 2}/img/banners/{$main_page.checkout_banner}{else}/img/girlsinbloom/sdfgshhdgfhdfgjdjdghj.jpg{/if}">{/if}</div>-->
					</div>
				{/if}

				<!-- ansotov сумма заказа -->
				<div id="cart-in-total">
					{include file="new_bloom/ru/ajax/cart-in-total.tpl"}
				</div>

				<div class="to_right"><input{if $smarty.get.step == 2} onclick="checkCity();"{/if} class="button" type="submit" style="box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);background: #ff7a07;"
											 value="{if $smarty.get.step == 2}{translateConst const = SEE_ACCEPT_ORDER}{else}{translateConst const = ACCEPT_ORDER}{/if}"></div>
			</form>
			<div class="last_order clearfix">
				{if $smarty.get.step == 2}
					<!--{translateConst const = VALIDATE_PURCHASE}
					<a class="info-page" href="/license?json=1" style="text-decoration: underline">{translateConst const = PURCHASE_TEXT}</a>.-->
				{/if}
			</div>
		</article>
	{else}
		{translateConst const = CARD_EMPTY}
	{/if}
</div>
<style>
	article.checkout .payment_type strong {
		width: 120px;
	}

	article.checkout .payment_type input {
		width: 30px;
	}

	article.checkout p {
		clear: both;
		margin: 0 0 10px;
	}

	.selector.hidden {
		display: none;
	}

	article.checkout p.selector {
		float: right;
		width: 340px;
	}
</style>

<script type="text/javascript">
    function checkCity() {

        //ansotov если не выбран город
        if (!$('#search_city_text').val()) {
            $('#search_city_text').css('border', '2px solid red').focus();
        }

        //ansotov если не выбран тип доставки
        if (!$('input[name=delivery_type]:checked').val()) {
            //$('#delivery-types .long_line .name').css('color', 'red');

            $('html,body').stop().animate({ scrollTop: $('#delivery-types').offset().top - 90 }, 1000);
        }
        else
            $('#delivery-types .long_line .name').css('color', 'black');
    }
</script>

<style type="text/css">
	@media (min-width: 320px) and (max-width: 767px) {
		.footer {
			display: none;
		}
	}
</style>
